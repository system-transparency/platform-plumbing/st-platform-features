# Custom Format

A simple custom format could just concatinate the raw kernel, initramfs, kays
ans signatures. The cmdline and length and offset as well as any metadata is
stored at the beginning as  newline-separated ASCII text. The artifacts kernel
and initramfs follow that header. At the end there are signatures and key located.

Details TBD