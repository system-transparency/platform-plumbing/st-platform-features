# Status Quo
- OS package content is divided into two files (descriptor and zip archive)
- Descriptor contains
  - Pointer to fetch archive file
  - Signatures of archive file
  - Corresponding certificates
- Archive file contains: 
  - Manifest containing 
    - Cmdline
    - Meta data like name of the OS package, filenames
  - Kernel
  - Initramfs

  ## Retrospective of Status Quo

  The Descriptor was designed to achieve two things:
  - Separate signatures from artifacts
    - Security concern: Do not touch/open the archive before verification
  - More flexibility to fetch/organize the OS package
    - Only bake the path to Descriptor into stboot image
    - Allow relocation of archive files by only changing their pointers
      in the Descriptor

The first is not treated as a concern in the younger discussions. Signatures
and Certs could be bundled with the artifacts. 
The later turned out to be an unused feature and therefore only adds complexity.

The zip format only serves the purpose of building a container for the artifacts
(kernel, initramfs). Compression was never treated as a major focus. 

If the filenames/paths (or generally the location) of kernel and initramfs
inside the archive/container are well defined, the Manifest becomes obsolete.

# The Essence of an OS Package

Taking the retrospective into account an OS package is made up of the following:

- kernel
- initramfs
- cmdline
- N signatures over (kernel, initramfs, cmdline)
- N corresponding certificates

Currently X509 certificates are used. The public keys used are signed by a
root key which is packed into the stboot image. Each public keys and its
corresponding certificate is stored in the certificate among other data
according to X509. 

Depending on weather other features of validation shell be used or not - like
time validation etc - the essence of an OS package could also be seen as:

-kernel
-initramfs
-cmdline
- N public keys
- N signatures over the corresponding public key
- N signatures over (kernel,  initramfs, cmdline)

The above needs to be packed together to be handled as a file so it can be
handled as one entity. E.g. for downloading/fetching.

Further cryptographic details are out of scope of this discussion.

# Intro to OS Package Format Alternatives

The subject of the discussion is therefore a suitable container format for the
artifacts of an OS package.
The alternatives to the current format that are evaluated during the further
discussion are:

- Unified Kernel Image (UKI)
- Custom Format

## Custom Format

A simple custom format could just concatenate the raw kernel, initramfs, kays
ans signatures. The cmdline and length and offset as well as any metadata is
stored at the beginning as  newline-separated ASCII text. The artifacts kernel
and initramfs follow that header. At the end there are signatures and key located.
[Further details here](./custom-format.md)

With an appropriate arrangement of the artifacts the OS package can be parsed 
efficiently. E.g. the kernel and initramfs can be hashed immediately during 
reading. Then, with N iterations a key, its signature and the signature over
tha artifacts are read. After is process is done the validation of the OS
package is already done and and the kernel and intramfs are ready to be hand
over to the next stage. 

The benefits of this approach are simplicity and efficiency of the design.
There are no conventions we need to apply with and any changes to the structure
are under our domain. Further it help optimizing the processing of the OS package
by stboot. This process can be very fast and memory efficient.

## UKI Format

A UKI already forms a container for a kernel, initramfs and cmdline. It uses the
Portable Executable (PE) format, which is a well known and widely used standard.
However, in its existing form it needs to be modified to serve the ST signatures.
[Further details here](./custom-format.md)

The benefits of this approach are:
- Reusing existing standards
- Existing tooling

# Requirements

To further discuss the alternatives for OS packages the requirements of the
infrastructure that uses OS packages needs to be evaluated. 

OS packages are exclusively consumed by stboot. Consumed means that stboot needs
to fetch, parse, validate and execute OS packages. 

OS packages are produced by tooling like stmgr according to its specification.

Although stboot is a reference implementation of a bootloader that implements
the concepts of ST, it not that likely that there is a need of alternative
implementations neither from the ST project's perspective nor from the ST user
perspective.
The tooling on the other hand is more likely to come in different flavours.
E.g. as a subcommand of stmgr, as bash scripts or via existing tooling. This
makes sense since user of ST need to be able to create and sign OS packages,
it is maybe the most frequent task ST users perform, when working with ST setups.

So there is only one consumer of OS packages and potentially multiple producers.

For both the consumer and the producer side if it comes to implementation, the
code should be robust and easy to audit, in the best case without the need of
third party libraries. If existing tools are used to consume or produce 
OS packages, they should be well maintained and available via standard packages
of the main Linux distributions.

The following sections evaluate the introduced alternative with respect to the
requirements and other aspects of ST.

## Custom Format

The custom format has its major advantages on the consumer side. Parsing and 
validation can be implemented with basic string and I/O operations. Leading to 
small and efficient code without any dependencies which is easy to audit.

To enable solid tools to produce OS packages in that format the most important
deliverable is precise specification. By nature there will be no existing
tooling for that format. Since the custom format is not forced to use any existing
structure the spec will be rather short without reference to other specs.

## UKI Format

The UKI format is already used in ST to build up the stboot images for the hosts.
This is essential since ST builds on top of the UEFI firmware. UKIs for stboot
images are UKIs in its intended form and use case.

There is already existing tooling both for UKIs itself as well as for the 
underlying PE format which brings major advantages for the producer side of 
OS packages in that format.
On the consumer side - if not done via existing tooling - there are packages
in the Golang std lib at least for PE file parsing.


However the UKI for an OS package would be different from stboot UKI
- The EFI stub (systemd stub) is not needed.
- The UKI does not need to be signed in terms of the UKI spec (secure boot).
- The kernel inside the UKI does not need to be signed (secure boot).
- The ST signatures must be added to the UKI

In general no UEFI or Windows specific metadata (e.g wrt memory management) is
needed and the UKI is only serves as a container for the kernel and initramfs.
All differences that are not used when using UKIs as containers for OS packages
can just be ignored and thus are no major concern. 
This makes the last point the main challenge in order to make UKIs work as an
OS package format, since with the ST signatures something needs to be added which
is not already part of the UKI spec. 

To still get the advantage of all existing tooling, an OS package UKI format
should be designed in way, that this additions are not added by the existing
tools (with patches etc.) but find a way to add the ST signatures in some
kind of post processing that does not interfere with reading or writing UKIs
with existing tooling.

