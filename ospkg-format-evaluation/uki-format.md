# UKI Format

To use UKIs as a container for OS packages the main challenge is to embed
the ST signatures over (kernel, initramfs, cmdline) into the existing structure.

One approach was already figured out in this [proposal](https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/proposals/2023-12-01-ospkg-uki-transition.md)

Alternatively the ST signatures can become own sections. (TBD)
