OS package format evaluation
----------------------------

- Sketch a design and architecture of a new OS package format
 - Approach 1: UKI
   - Use UKI as defined in [1]
   - Do not use the stub
   - Use further PE section to add OS package signatures in PKCS 7 structure
   - Continue work from Philipp [2]
 - Approach 2: Raw binary fomrat with string based metadata (Idea from Kai)
   - Newline seperated strings define metadata (name, version, ...), offsets
     and hashes of kernel and initramfs
   - kernel and initrmafs are concatenated without further containerners
   - signatures to be evaluated 
- Concentrate on structure, APIs and Interfaces, not necessarily on Implementation
- Take into account that the cryptographic part of signature verification in detail
 is out of scope
- Aim to use existing standards where possible
- Focus on the bare needs of an OS package: A signed container for
 kernel + initramfs
- Sketch the API of librarie(s) for creating, signing, fetching and executing an
 OS package (pull complexity down into the libs)
- Outcome: Which questions do we want to answer
 - What should the OS package format be? There are 2 main options
   1 UKI
   2 Custom format designed by us
 - What are our requirements?
 - What are the major pros and cons of the approaches?
   - In particular, when you investigate pros and cons, take into consideration that we must use UKI (essentially) for stboot


[1] https://github.com/uapi-group/specifications/blob/main/specs/unified_kernel_image.md
[2] https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/50
