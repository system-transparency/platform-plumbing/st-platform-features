package stvmm

import (
	"context"
	"crypto/rand"
	"encoding/base32"
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"os"
	"path"

	zl "github.com/rs/zerolog"
)

type firecrackerBootSource struct {
	KernelImagePath string `json:"kernel_image_path"`
	BootArgs        string `json:"boot_args"`
	InitrdPath      string `json:"initrd_path"`
}

type firecrackerDrive struct {
	DriveID     string `json:"drive_id"`
	Partuuid    string `json:"partuuid"`
	IsRootDev   bool   `json:"is_root_device"`
	CacheType   string `json:"cache_type"`
	IsReadOnly  bool   `json:"is_read_only"`
	PathOnHost  string `json:"path_on_host"`
	IOEngine    string `json:"io_engine"`
	RateLimiter string `json:"rate_limiter"`
	Socket      string `json:"socket"`
}

type firecrackerMachine struct {
	VcpuCount       int    `json:"vcpu_count"`
	MemSizeMib      int    `json:"mem_size_mib"`
	Smt             bool   `json:"smt"`
	TrackDirtyPages bool   `json:"track_dirty_pages"`
	HugePages       string `json:"huge_pages"`
}

type firecrackerNic struct {
	GuestMac      string `json:"guest_mac,omitempty"`
	HostDevName   string `json:"host_dev_name"`
	IfaceId       string `json:"iface_id"`
	RxRateLimiter string `json:"rx_rate_limiter,omitempty"`
	TxRateLimiter string `json:"tx_rate_limiter,omitempty"`
}

type firecrackerBalloon struct {
}

type firecrackerVsock struct {
	GuestCid uint32 `json:"guest_cid"`
	UdsPath  string `json:"uds_path"`
}

type firecrackerLogger struct {
}

type firecrackerMetrics struct {
}

type firecrackerMmds struct {
}

type firecrackerEntropy struct {
}

type firecrackerStvmm struct {
	Name string `json:"name"`
}

type firecrackerConfig struct {
	BootSource        firecrackerBootSource `json:"boot-source"`
	Drives            []firecrackerDrive    `json:"drives"`
	Machine           firecrackerMachine    `json:"machine-config"`
	Balloon           *firecrackerBalloon   `json:"balloon"`
	NetworkInterfaces []firecrackerNic      `json:"network-interfaces"`
	Vsock             *firecrackerVsock     `json:"vsock"`
	Logger            *firecrackerLogger    `json:"logger"`
	Metrics           *firecrackerMetrics   `json:"metrics"`
	MmdsConfig        *firecrackerMmds      `json:"mmds-config"`
	Entropy           *firecrackerEntropy   `json:"entropy"`
	Stvmm             *firecrackerStvmm     `json:"stvmm"`
}

const defaultMachinesPath = "/var/lib/stvmm"

var defaultFirecrackerMacVendor = []byte{0x02, 0x00}
var defaultFirecrackerMask net.IPMask = net.CIDRMask(24, 32)
var defaultFirecrackerNet net.IP = net.IP{192, 168, 42, 0}

func StartMachine(ctx context.Context, vm *StartMachineParams, kernel, initrd, cmdline string) (string, error) {
	zl.Ctx(ctx).Info().
		Str("name", vm.Name).
		Int("cpus", vm.CPUs).
		Int("memory", vm.Memory).
		Str("ip", vm.IP).
		Msg("StartMachine")

	var entropy [2]byte
	_, err := rand.Read(entropy[:])
	if err != nil {
		return "", err
	}

	fcId := base32.StdEncoding.WithPadding(base32.NoPadding).EncodeToString(entropy[:])
	fcConfigPath := path.Join(defaultMachinesPath, fcId+".json")
	fcIp, fcMask, err := net.ParseCIDR(vm.IP)
	if err != nil {
		return "", err
	}
	fcIp = fcIp.To4()
	if !fcIp.Mask(fcMask.Mask).Equal(defaultFirecrackerNet) {
		return "", errors.New("invalid IP address, not in firecracker subnet")
	}
	if fcMask != nil && !fcMask.IP.Equal(defaultFirecrackerNet) {
		return "", errors.New("invalid IP address, mask must be /24")
	}
	fcMac := net.HardwareAddr{
		defaultFirecrackerMacVendor[0],
		defaultFirecrackerMacVendor[1],
		fcIp[3], fcIp[2], fcIp[1], fcIp[0],
	}

	hvIp := net.IP{defaultFirecrackerNet[0], defaultFirecrackerNet[1], defaultFirecrackerNet[2], fcIp[3] + 1}
	hvIp = hvIp.To4()
	hvMac := net.HardwareAddr{
		defaultFirecrackerMacVendor[0],
		defaultFirecrackerMacVendor[1],
		hvIp[3], hvIp[2], hvIp[1], hvIp[0],
	}
	fcCmdLine := fmt.Sprintf("%s root=/dev/ram0 ifname=bootnet:%s ip=%s::%s:255.255.255.0:%s:bootnet:off",
		cmdline, fcMac.String(), fcIp.String(), hvIp.String(), fcId)

	config := firecrackerConfig{
		Machine: firecrackerMachine{
			HugePages:  "None",
			VcpuCount:  vm.CPUs,
			MemSizeMib: vm.Memory,
		},
		BootSource: firecrackerBootSource{
			KernelImagePath: kernel,
			InitrdPath:      initrd,
			BootArgs:        fcCmdLine,
		},
		Drives: []firecrackerDrive{},
		NetworkInterfaces: []firecrackerNic{
			firecrackerNic{
				IfaceId:     "eth0",
				HostDevName: "stvmm-" + fcId,
				GuestMac:    fcMac.String(),
			},
		},
		Vsock: &firecrackerVsock{
			GuestCid: 3,
			UdsPath:  "/var/lib/stvmm/" + fcId + ".vsock",
		},
		Stvmm: &firecrackerStvmm{
			Name: vm.Name,
		},
	}

	fd, err := os.Create(fcConfigPath)
	if err != nil {
		return "", err
	}
	enc := json.NewEncoder(fd)
	enc.SetIndent("", "  ")
	err = enc.Encode(config)
	if fd.Close() != nil {
		return "", err
	}
	if err != nil {
		return "", err
	}
	err = setupFirecrackerNic(ctx, fcId, hvMac, hvIp, defaultFirecrackerMask)
	if err != nil {
		return "", err
	}
	err = startFirecrackerUnit(fcId)
	if err != nil {
		return "", err
	}

	zl.Ctx(ctx).Info().Str("id", fcId).Msg("machine started")

	return fcId, nil
}

func StopMachine(ctx context.Context, id string) error {
	zl.Ctx(ctx).Info().Str("id", id).Msg("StopMachine")

	err := stopFirecrackerUnit(id)
	if err != nil {
		return err
	}

	fcConfigPath := path.Join(defaultMachinesPath, id+".json")
	err = os.Remove(fcConfigPath)
	if errors.Is(err, os.ErrNotExist) {
		// ignore
		zl.Ctx(ctx).Warn().Err(err).Str("id", id).Str("path", fcConfigPath).Msg("config file not found")
	} else if err != nil {
		return err
	}

	err = removeFirecrackerNic(ctx, id)
	if err != nil {
		return err
	}

	return nil
}
