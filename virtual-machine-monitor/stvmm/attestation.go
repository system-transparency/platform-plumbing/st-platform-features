package stvmm

import (
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/binary"
	"errors"
	"io"
	"os"
	"path/filepath"
	"sort"

	"system-transparency.org/ppg/stvmm/internal/tpm"
)

var (
	// No TPM device node found.
	ErrNoTPM = errors.New("no TPM found")
	// Failed to read PCR values off the TPM.
	ErrPcrRead = errors.New("error reading PCR")
	// The PCR values in the quote do not match the expected values.
	ErrPcrSelection = errors.New("invalid PCR selection")
	// The quote structure is invalid.
	ErrAttestationStructure = errors.New("attestation structure is invalid")
	// The signature or hash algorithm used in the quote is not supported.
	ErrAlgorithm = errors.New("unsupported algorithm")
	// The signature verification of the quote or endorsement failed.
	ErrSignature = errors.New("signature verification failed")

	// we skip PCR 10 as it is used for IMA, which is essentially random
	pcClientPcr = []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15,
		16, 17, 18, 19, 20, 21, 22, 23}
	guestPcr = 23

	defaultAikTemplate = tpm.Template{
		Public: tpm.Public{
			Type:             tpm.TPM_ALG_ECC,
			NameAlg:          tpm.TPM_ALG_SHA256,
			ObjectAttributes: tpm.TPMA_OBJECT_RESTRICTED_SIGN,
			AuthPolicy:       make([]byte, 32),
			EccParameters: &tpm.EccParms{
				Symmetric: tpm.SymDefObject{
					Algorithm: tpm.TPM_ALG_NULL,
				},
				Scheme: tpm.Scheme{
					Scheme:  tpm.TPM_ALG_ECDSA,
					HashAlg: tpm.TPM_ALG_SHA256,
				},
				CurveID: tpm.TPM_ECC_NIST_P256,
				KDF: tpm.Scheme{
					Scheme: tpm.TPM_ALG_NULL,
				},
				Unique: tpm.EccPoint{},
			},
		},
	}
	defaultAikLabel = "ST-ECC-AIK-LABEL-V0"

	linuxSystemTpmGlobs = []string{
		"/dev/tpm*",
	}
)

func findSystemTPM() (tpm.Device, string, error) {
	for _, pat := range linuxSystemTpmGlobs {
		m, err := filepath.Glob(pat)
		if err != nil {
			return nil, "", err
		}
		if len(m) == 0 {
			continue
		}
		for _, path := range m {
			dev, err := tpm.OpenFile(path)
			if err != nil {
				continue
			}
			return dev, path, nil
		}
	}
	return nil, "", ErrNoTPM
}

func loadAttestationIdentityKey(ctx context.Context, dev tpm.Device, ownerAuth string) (tpm.U32, *tpm.Public, error) {
	h, pub, _, _, _, _, err := tpm.CreatePrimary(ctx, dev, tpm.Password(ownerAuth), tpm.TPM_RH_OWNER, "", defaultAikTemplate, []byte(defaultAikLabel), nil)
	if err != nil {
		return 0, nil, err
	}

	return h, &pub, nil
}

// UINT32: 4
// TPML_PCR_SELECTION: 4 + alg_count * (2 + 1 + 3) = 10
// TPML_DIGEST: 4 + x * (2 + hash_size) = 4 + x * 34
// 18 + 34x

func readAllPcr(ctx context.Context, dev tpm.Device) (map[int][]byte, error) {
	todo := append([]int{}, pcClientPcr...)
	pcrs := make(map[int][]byte)

	for len(todo) > 0 {
		maxSel := (tpm.MaxResponseSize - 18) / 34
		if maxSel > len(todo) {
			maxSel = len(todo)
		}

		var sel tpm.PcrSelect
		sel.Hash = tpm.TPM_ALG_SHA256
		sel.Pcr = todo[:maxSel]

		_, outsel, digests, err := tpm.PcrRead(ctx, dev, []tpm.PcrSelect{sel})
		if err != nil {
			return nil, err
		}
		if len(outsel) != 1 {
			return nil, ErrPcrRead
		}
		if outsel[0].Hash != tpm.TPM_ALG_SHA256 {
			return nil, ErrPcrRead
		}

		for i, pcr := range outsel[0].Pcr {
			pcrs[pcr] = append([]byte{}, digests[i]...)
			i := sort.SearchInts(todo, pcr)
			if i < len(todo) && todo[i] == pcr {
				todo = append(todo[:i], todo[i+1:]...)
			} else {
				return nil, ErrPcrRead
			}
		}
	}

	return pcrs, nil
}

func generateQuote(ctx context.Context, dev tpm.Device, ownerAuth string, nonce []byte) (*tpm.Attest, *tpm.Signature, map[int][]byte, error) {
	digests, err := readAllPcr(ctx, dev)
	if err != nil {
		return nil, nil, nil, err
	}

	aikHandle, _, err := loadAttestationIdentityKey(ctx, dev, ownerAuth)
	if err != nil {
		return nil, nil, nil, err
	}
	defer func() { _ = tpm.FlushContext(ctx, dev, aikHandle) }()

	sch := tpm.Scheme{Scheme: tpm.TPM_ALG_NULL}
	sel := tpm.PcrSelect{
		Hash: tpm.TPM_ALG_SHA256,
		Pcr:  append([]int{}, pcClientPcr...),
	}
	attest, sig, err := tpm.Quote(ctx, dev, aikHandle, tpm.Password(ownerAuth), sch, nonce, []tpm.PcrSelect{sel})
	if err != nil {
		return nil, nil, nil, err
	}

	return &attest, &sig, digests, nil
}

func measureFile(ctx context.Context, dev tpm.Device, path string, hashTwice bool) error {
	h := sha256.New()

	fd, err := os.Open(path)
	if err != nil {
		return err
	}
	defer fd.Close()
	if _, err := io.Copy(h, fd); err != nil {
		return err
	}

	sum := h.Sum(nil)
	if hashTwice {
		h.Reset()
		h.Write(sum)
		sum = h.Sum(nil)
	}

	digest := tpm.Ha{Alg: tpm.TPM_ALG_SHA256, Digest: sum}
	if err := tpm.PcrExtend(ctx, dev, tpm.U32(guestPcr), tpm.Password(""), tpm.DigestValues{digest}); err != nil {
		return err
	}
	return nil
}

func measureMachine(ctx context.Context, dev tpm.Device, ownerAuth, platformEndorsementPath, descPath, archivePath string, nonce []byte) ([]byte, error) {
	if err := measureFile(ctx, dev, archivePath, true); err != nil {
		return nil, err
	}
	if err := measureFile(ctx, dev, descPath, false); err != nil {
		return nil, err
	}

	attest, sig, _, err := generateQuote(ctx, dev, ownerAuth, nonce)
	if err != nil {
		return nil, err
	}

	if err := tpm.PcrReset(ctx, dev, tpm.U32(guestPcr), tpm.Password("")); err != nil {
		return nil, err
	}

	attestBytes, err := attest.Pack()
	if err != nil {
		return nil, err
	}
	sigBytes, err := sig.Pack()
	if err != nil {
		return nil, err
	}

	buf := new(bytes.Buffer)
	if err := writeSizePrefix(buf, nonce, attestBytes, sigBytes, nil, nil); err != nil {
		return nil, err
	}

	fd, err := os.Open(platformEndorsementPath)
	if err != nil {
		return nil, err
	}
	defer fd.Close()

	if _, err := io.Copy(buf, fd); err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

func writeSizePrefix(wr io.Writer, data ...[]byte) error {
	for _, d := range data {
		err := binary.Write(wr, binary.LittleEndian, uint32(len(d)))
		if err != nil {
			return err
		}
		_, err = wr.Write(d)
		if err != nil {
			return err
		}
	}

	return nil
}
