package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"

	"github.com/alecthomas/kong"
	"system-transparency.org/ppg/stvmm"
)

type startCmd struct {
	Name string `arg:"" help:"Name of the machine."`
}

func (c *startCmd) Run(opts *options) error {
	// hard-coded parameters for now
	reqBody := stvmm.StartMachineParams{
		CPUs:   1,
		Memory: 2048,
		IP:     "192.168.42.1/24",
		Name:   c.Name,
		Nonce:  "something",
	}
	var respBody stvmm.StartMachineResponse

	err := send(opts.Host, "machine/start", reqBody, &respBody)
	if err != nil {
		return err
	}

	fmt.Printf("VM ID: %s\n", respBody.ID)

	dec := base64.NewDecoder(base64.StdEncoding, bytes.NewReader([]byte(respBody.Quote)))
	fd, err := os.Create("quote.bin")
	if err != nil {
		return err
	}
	defer fd.Close()
	_, err = io.Copy(fd, dec)
	if err != nil {
		return err
	}
	err = fd.Close()
	if err != nil {
		return err
	}

	fmt.Printf("Quote written to quote.bin\n")
	return nil
}

type stopCmd struct {
	ID string `arg:"" help:"ID of the machine."`
}

func (c *stopCmd) Run(opts *options) error {
	reqBody := stvmm.StopMachineParams{
		ID: c.ID,
	}

	err := send(opts.Host, "machine/stop", reqBody, nil)
	if err != nil {
		return err
	}

	return nil
}

type options struct {
	Host string
}

var cli struct {
	Host string `env:"STVMM_HOST" default:"localhost:8080" help:"Host of the STVMM server."`

	Start startCmd `cmd:"" help:"Start a machine."`
	Stop  stopCmd  `cmd:"" help:"Stop a machine."`
}

func main() {
	ctx := kong.Parse(&cli,
		kong.Name("stv"),
		kong.Description("System Transparency Virtual Machine client"),
		kong.UsageOnError(),
		kong.ConfigureHelp(kong.HelpOptions{
			Compact: true,
		}),
		kong.Vars{
			"version": "0.1",
		})
	if err := ctx.Run(&options{Host: cli.Host}); err != nil {
		ctx.FatalIfErrorf(err)
	}
}

func send(host, action string, requestBody, responseBody interface{}) error {
	reqStr, err := json.Marshal(requestBody)
	if err != nil {
		return err
	}

	url := fmt.Sprintf("http://%s/v1/%s", host, action)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(reqStr))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.SetBasicAuth(stvmm.DEFAULT_USER, stvmm.DEFAULT_PASS)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	respStr := new(bytes.Buffer)
	_, err = respStr.ReadFrom(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Unexpected status code: %d", resp.StatusCode)
	}

	if responseBody == nil {
		return nil
	}

	err = json.NewDecoder(respStr).Decode(responseBody)
	if err != nil {
		return err
	}

	return nil
}
