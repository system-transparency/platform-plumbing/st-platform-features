package main

import (
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"strings"
	"syscall"

	"github.com/alecthomas/kong"

	"system-transparency.org/ppg/stvmm"
	"system-transparency.org/stboot/ospkg"
)

type options struct {
	Port int `help:"Port to listen on" default:"8080"`
}

const sdListenFdsStart = 3

var cli struct {
	options

	Version VersionCmd `cmd:"" help:"Show version information"`
	Daemon  DaemonCmd  `cmd:"" help:"Run the STVMM daemon"`
	Receive ReceiveCmd `cmd:"" help:"Serve the guest to hypervisor API via systemd socket activation"`
}

func main() {
	ctx := kong.Parse(&cli,
		kong.Name("stvmmd"),
		kong.Description("A System Transparency Virtual Machine Manager Daemon"),
		kong.UsageOnError(),
		kong.ConfigureHelp(kong.HelpOptions{
			Compact: true,
		}),
		kong.Vars{
			"version": "0.1",
		})
	if err := ctx.Run(&options{Port: cli.Port}); err != nil {
		ctx.FatalIfErrorf(err)
	}
}

type VersionCmd struct{}

type DaemonCmd struct {
	OspkgDescriptor string `flag:"descriptor" default:"/var/lib/stvmm/stimage.json" help:"path to ospkg descriptor file" type:"path"`
	OspkgZip        string `flag:"zip" help:"path to ospkg zip file. Default is to derive it from --descriptor." type:"path"`
	RootCertificate string `flag:"root-cert" default:"/var/lib/stvmm/root-certificate.pem" help:"path to root certificate file" type:"path"`
}

// Receiving end of the guest to hypervisor VSOCK interface. Firecracker will
// forward all connections to a UNIX domain socket that is given to this
// process via systemd socket activation.
type ReceiveCmd struct{}

func readWriteSectionPath(r io.ReaderAt, sectionOffset int64, bufferSize int64) (string, error) {
	file, err := os.CreateTemp("/tmp", "ospkg")
	if err != nil {
		return "", err
	}
	defer file.Close()
	if bufferSize == 0 {
		bufferSize = 1024
	}
	buffer := make([]byte, bufferSize)
	for {
		n, err := r.ReadAt(buffer, sectionOffset)
		if err != nil && err != io.EOF {
			return "", err
		}
		if n == 0 {
			break
		}
		_, err = file.Write(buffer[:n])
		if err != nil {
			return "", err
		}
		sectionOffset += int64(n)
	}
	info, err := file.Stat()
	if err != nil {
		return "", err
	}
	filepath := fmt.Sprintf("/tmp/%s", info.Name())
	return filepath, nil
}

func unpackOspkg(oszipPath, osdescPath, rootCaPath string) (string, string, string, error) {
	// unpack OS package
	oszipData, err := os.ReadFile(oszipPath)
	if err != nil {
		return "", "", "", err
	}
	osdescData, err := os.ReadFile(osdescPath)
	if err != nil {
		return "", "", "", err
	}
	ospkg, err := ospkg.NewOSPackage(oszipData, osdescData)
	if err != nil {
		return "", "", "", err
	}

	// parse root certificate and validate the OS package with it
	rootCaData, err := os.ReadFile(rootCaPath)
	if err != nil {
		return "", "", "", err
	}
	rootCaPem, _ := pem.Decode(rootCaData)
	if rootCaPem != nil {
		rootCaData = rootCaPem.Bytes
	}
	rootCa, err := x509.ParseCertificate(rootCaData)
	if err != nil {
		return "", "", "", err
	}
	_, _, err = ospkg.Verify(rootCa)
	if err != nil {
		return "", "", "", err
	}

	// extract kernel, initrd, and cmdline
	uki, err := ospkg.LinuxImage()
	if err != nil {
		return "", "", "", err
	}
	kernelPath, err := readWriteSectionPath(uki.Kernel, 0, 0)
	if err != nil {
		return "", "", "", err
	}
	initrdPath, err := readWriteSectionPath(uki.Initrd, 0, 0)
	if err != nil {
		return "", "", "", err
	}
	return kernelPath, initrdPath, uki.Cmdline, nil
}

func (cmd *VersionCmd) Run() error {
	return nil
}

func (cmd *DaemonCmd) Run(opts *options) error {
	// preflight checks
	err := stvmm.EnsureFirecrackerUnit()
	if errors.Is(err, stvmm.ErrUnitNotInstalled) {
		return fmt.Errorf("Firecracker unit not installed")
	} else if err != nil {
		return err
	}
	err = os.MkdirAll("/var/lib/stvmm", 0755)
	if err != nil && !os.IsExist(err) {
		return err
	}
	if _, err := os.Stat("/dev/kvm"); err != nil {
		return fmt.Errorf("KVM is not available")
	}
	if _, err := os.Stat("/dev/net/tun"); err != nil {
		return fmt.Errorf("TUN/TAP is not available")
	}

	var zipPath, descPath string

	if cmd.OspkgZip != "" && cmd.OspkgDescriptor != "" {
		zipPath = cmd.OspkgZip
		descPath = cmd.OspkgDescriptor
	} else if cmd.OspkgZip == "" && cmd.OspkgDescriptor != "" {
		zipPath = strings.TrimSuffix(cmd.OspkgDescriptor, ".json") + ".zip"
		descPath = cmd.OspkgDescriptor
	} else if cmd.OspkgZip != "" && cmd.OspkgDescriptor == "" {
		zipPath = cmd.OspkgZip
		descPath = strings.TrimSuffix(cmd.OspkgZip, ".zip") + ".json"
	} else {
		return fmt.Errorf("either --zip or --descriptor must be provided")
	}

	kernelPath, initrdPath, cmdline, err := unpackOspkg(zipPath, descPath, cmd.RootCertificate)
	if err != nil {
		return err
	}
	if err := stvmm.ServeExternalAPI(opts.Port, kernelPath, initrdPath, cmdline); err != nil {
		return err
	}
	return nil
}

// sdListener is a net.Listener that accepts connections from a systemd socket
type sdListener struct {
	fd int
}

func (l *sdListener) Accept() (net.Conn, error) {
	fd, _, err := syscall.Accept(l.fd)
	if err != nil {
		return nil, err
	}
	return net.FileConn(os.NewFile(uintptr(fd), ""))
}

func (l *sdListener) Close() error {
	return syscall.Close(l.fd)
}

func (l *sdListener) Addr() net.Addr {
	return nil
}

func (cmd *ReceiveCmd) Run() error {
	listenPid := os.Getenv("LISTEN_PID")
	mypid := os.Getpid()
	if fmt.Sprintf("%d", mypid) != listenPid {
		return fmt.Errorf("LISTEN_PID does not match my PID")
	}

	listenFds := os.Getenv("LISTEN_FDS")
	if listenFds != "1" {
		return fmt.Errorf("LISTEN_FDS is not 1")
	}

	syscall.CloseOnExec(sdListenFdsStart)
	listener := &sdListener{fd: sdListenFdsStart}
	server := http.Server{
		Handler: stvmm.BuildGuestAPI(),
	}
	server.Serve(listener)
	return nil
}
