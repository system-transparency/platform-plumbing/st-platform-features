package stvmm

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	zl "github.com/rs/zerolog"
)

const (
	DEFAULT_USER = "foo"
	DEFAULT_PASS = "blah"
	DEFAULT_LOG  = "gin.log"
	API_VERSION  = "v1"

	DefaultKernelPath    = "/var/lib/stvmm/vmlinux"
	DefaultInitrdPath    = "/var/lib/stvmm/initrd.cpio.gz"
	DefaultKernelCmdline = "console=ttyS0 reboot=k panic=1 pci=off rw rdinit=/lib/systemd/systemd root=/dev/ram0"
)

var (
	// hard-coded path to the platform endorsement file included in the quote
	platformEndorsementPath = "/var/lib/stvmm/platform.endorsement.bin"
	// hard-coded paths to the OS package started
	ospkgDescriptorPath = "/var/lib/stvmm/stimage.json"
	ospkgArchivePath    = "/var/lib/stvmm/stimage.zip"
)

type StartMachineParams struct {
	CPUs   int    `json:"cpus"`
	Memory int    `json:"memory"`
	IP     string `json:"ip"`
	Name   string `json:"name"`
	Nonce  string `json:"nonce"`
}

type StartMachineResponse struct {
	Result string `json:"result"`
	ID     string `json:"id"`
	Quote  string `json:"quote"`
}

type StopMachineParams struct {
	ID string `json:"id"`
}

// HTTP API exposed by the hypervisor
func ServeExternalAPI(port int, kernelPath, initrdPath, kernelCmdline string) error {
	consoleWriter := zl.ConsoleWriter{Out: os.Stdout}
	log := zl.New(consoleWriter).With().Timestamp().Logger()

	log.Info().
		Int("port", port).
		Str("kernel", kernelPath).
		Str("initrd", initrdPath).
		Str("cmdline", kernelCmdline).
		Str("platform-endorsement", platformEndorsementPath).
		Str("ospkg-descriptor", ospkgDescriptorPath).
		Str("ospkg-archive", ospkgArchivePath).
		Msg("Starting external API server")

	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	router.Use(func(c *gin.Context) {
		log.Info().Str("method", c.Request.Method).Str("path", c.Request.URL.Path).Str("ip", c.ClientIP()).Msg("Request")
		c.Request = c.Request.WithContext(log.WithContext(c.Request.Context()))
		c.Next()
	}, gin.Recovery())

	// API version 1 with basic auth
	v1 := router.Group(API_VERSION, gin.BasicAuth(gin.Accounts{
		DEFAULT_USER: DEFAULT_PASS,
	}))

	// StartMachine(cpus, memory, ip, name, nonce) -> id, quote
	v1.POST("/machine/start", func(c *gin.Context) {
		ctx := c.Request.Context()

		var params StartMachineParams
		if err := c.BindJSON(&params); err != nil {
			c.String(http.StatusBadRequest, "Invalid JSON: %s", err)
			return
		}
		id, err := StartMachine(ctx, &params, kernelPath, initrdPath, kernelCmdline)
		if err != nil {
			c.String(http.StatusInternalServerError, "Failed to start machine: %s", err)
			return
		}
		dev, _, err := findSystemTPM()
		if err != nil {
			c.String(http.StatusInternalServerError, "Failed to find TPM: %s", err)
			return
		}
		defer dev.Close()

		quote, err := measureMachine(c.Request.Context(), dev, "", platformEndorsementPath, ospkgDescriptorPath, ospkgArchivePath, []byte(params.Nonce))
		if err != nil {
			c.String(http.StatusInternalServerError, "Failed to measure machine: %s", err)
			return
		}
		dev.Close()

		resp := StartMachineResponse{
			ID:     id,
			Result: "Machine started",
			Quote:  base64.StdEncoding.EncodeToString(quote),
		}

		c.JSON(http.StatusOK, resp)
	})

	// stops the machine with the given ID
	v1.POST("/machine/stop", func(c *gin.Context) {
		ctx := c.Request.Context()

		var params StopMachineParams
		if err := c.BindJSON(&params); err != nil {
			c.String(http.StatusBadRequest, "Invalid JSON: %s", err)
			return
		}
		if err := StopMachine(ctx, params.ID); err != nil {
			c.String(http.StatusInternalServerError, "Failed to stop machine: %s", err)
			return
		}

		c.String(http.StatusOK, "Machine stopped")
	})

	// returns the API version
	v1.GET("/version", func(c *gin.Context) {
		c.String(http.StatusOK, "API version: %s", API_VERSION)
	})

	router.Run(fmt.Sprintf(":%d", port))
	return nil
}

// HTTP API callable from the guest via vsock
func BuildGuestAPI() http.Handler {
	consoleWriter := zl.ConsoleWriter{Out: os.Stdout}
	log := zl.New(consoleWriter).With().Timestamp().Logger()

	log.Info().
		Str("platform-endorsement", platformEndorsementPath).
		Str("ospkg-descriptor", ospkgDescriptorPath).
		Str("ospkg-archive", ospkgArchivePath).
		Msg("Starting guest API server")

	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	router.Use(func(c *gin.Context) {
		log.Info().Str("method", c.Request.Method).Str("path", c.Request.URL.Path).Str("ip", c.ClientIP()).Msg("Request")
		c.Request = c.Request.WithContext(log.WithContext(c.Request.Context()))
		c.Next()
	}, gin.Recovery())

	// API version 1 with basic auth
	v1 := router.Group(API_VERSION)

	// Quote(nonce) -> quote (M1)
	v1.POST("/quote", func(c *gin.Context) {
		ctx := c.Request.Context()

		dev, _, err := findSystemTPM()
		if err != nil {
			c.String(http.StatusInternalServerError, "Failed to find TPM: %s", err)
			return
		}
		defer dev.Close()

		nonce, err := c.GetRawData()
		if err != nil {
			c.String(http.StatusBadRequest, "Failed to read nonce: %s", err)
			return
		}

		quote, err := measureMachine(ctx, dev, "", platformEndorsementPath, ospkgDescriptorPath, ospkgArchivePath, nonce)
		if err != nil {
			c.String(http.StatusInternalServerError, "Failed to measure machine: %s", err)
			return
		}
		c.Data(http.StatusOK, "application/octet-stream", quote)
	})

	// Version() -> version & api (M1)
	v1.GET("/version", func(c *gin.Context) {
		c.String(http.StatusOK, "API version: %s", API_VERSION)
	})

	return router
}
