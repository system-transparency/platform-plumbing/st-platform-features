package stvmm

import (
	"context"
	"errors"
	"fmt"
	"html/template"
	"net"
	"os"
	"time"

	"github.com/godbus/dbus/v5"
	zl "github.com/rs/zerolog"
	"github.com/vishvananda/netlink"
)

var (
	netdevTemplate = template.Must(template.New("").Parse(`
[NetDev]
Name=stvmm-{{.Id}}
Kind=tap
`))
	networkTemplate = template.Must(template.New("").Parse(`
[Match]
Name=stvmm-{{.Id}}

[Link]
MACAddress={{.MacAddress}}

[Network]
Address={{.IpAddress}}
IPv4Forwarding=yes
IPv6Forwarding=yes
`))

	ErrUnitNotInstalled = errors.New("firecracker unit not installed")
)

func EnsureFirecrackerUnit() error {
	conn, err := dbus.SystemBus()
	if err != nil {
		return err
	}
	defer conn.Close()

	var unit interface{}
	obj := conn.Object("org.freedesktop.systemd1", "/org/freedesktop/systemd1")
	err = obj.Call("org.freedesktop.systemd1.Manager.GetUnitFileState", 0, "stvmm-firecracker@.service").Store(&unit)
	if os.IsNotExist(err) {
		return ErrUnitNotInstalled
	} else if err != nil {
		return err
	}

	return nil
}

func startFirecrackerUnit(id string) error {
	conn, err := dbus.SystemBus()
	if err != nil {
		return err
	}

	unit := fmt.Sprintf("stvmm-firecracker@%s.service", id)

	obj := conn.Object("org.freedesktop.systemd1", "/org/freedesktop/systemd1")
	call := obj.Call("org.freedesktop.systemd1.Manager.StartUnit", 0, unit, "replace")
	if call.Err != nil {
		return call.Err
	}

	return nil
}

func stopFirecrackerUnit(id string) error {
	conn, err := dbus.SystemBus()
	if err != nil {
		return err
	}

	unit := fmt.Sprintf("stvmm-firecracker@%s.service", id)

	obj := conn.Object("org.freedesktop.systemd1", "/org/freedesktop/systemd1")
	call := obj.Call("org.freedesktop.systemd1.Manager.StopUnit", 0, unit, "replace")
	if call.Err != nil {
		return call.Err
	}

	return nil
}

func setupFirecrackerNic(ctx context.Context, name string, mac net.HardwareAddr, ip net.IP, mask net.IPMask) error {
	err := writeNetworkdConfigs(name, mac, ip, mask)
	if err != nil {
		return err
	}

	conn, err := dbus.ConnectSystemBus()
	if err != nil {
		return err
	}
	defer conn.Close()

	err = reloadNetworkd(conn)
	if err != nil {
		return err
	}

	err = waitForLinkProperty(ctx, conn, name, "AdministrativeState", "configuring", false)
	if err != nil {
		return err
	}

	return nil
}

func removeFirecrackerNic(ctx context.Context, id string) error {
	netdevPath := fmt.Sprintf("/run/systemd/network/stvmm-%s.netdev", id)
	networkPath := fmt.Sprintf("/run/systemd/network/stvmm-%s.network", id)

	for _, path := range []string{netdevPath, networkPath} {
		err := os.Remove(path)
		if errors.Is(err, os.ErrNotExist) {
			zl.Ctx(ctx).Warn().Err(err).Str("path", path).Msg("networkd config not found")
			continue
		}
		if err != nil {
			return err
		}
	}

	conn, err := dbus.ConnectSystemBus()
	if err != nil {
		return err
	}
	defer conn.Close()

	err = reloadNetworkd(conn)
	if err != nil {
		return err
	}

	err = waitForLinkProperty(ctx, conn, id, "AdministrativeState", "unmanaged", true)
	if err != nil {
		return err
	}

	linkName := "stvmm-" + id
	link, err := netlink.LinkByName(linkName)
	if errors.As(err, new(netlink.LinkNotFoundError)) {
		zl.Ctx(ctx).Warn().Err(err).Str("link", linkName).Msg("link not found")
		return nil
	}
	if err != nil {
		return err
	}
	err = netlink.LinkDel(link)
	if err != nil {
		return err
	}

	return nil
}

func writeNetworkdConfigs(id string, mac net.HardwareAddr, ip net.IP, mask net.IPMask) error {
	netdevPath := fmt.Sprintf("/run/systemd/network/stvmm-%s.netdev", id)
	networkPath := fmt.Sprintf("/run/systemd/network/stvmm-%s.network", id)

	netdevFile, err := os.Create(netdevPath)
	if err != nil {
		return err
	}

	err = netdevTemplate.Execute(netdevFile, struct{ Id string }{id})
	if netdevFile.Close(); err != nil {
		return err
	}
	if err != nil {
		return err
	}

	networkFile, err := os.Create(networkPath)
	if err != nil {
		return err
	}

	ones, _ := mask.Size()
	err = networkTemplate.Execute(networkFile, struct {
		Id         string
		MacAddress string
		IpAddress  string
	}{id, mac.String(), fmt.Sprintf("%s/%d", ip, ones)})
	if networkFile.Close(); err != nil {
		return err
	}
	if err != nil {
		return err
	}

	return nil
}

func reloadNetworkd(conn *dbus.Conn) error {
	manager := conn.Object("org.freedesktop.network1", "/org/freedesktop/network1")
	return manager.Call("org.freedesktop.network1.Manager.Reload", 0).Err
}

// polls networkd for the link's property to be set to the expected value
func waitForLinkProperty(ctx context.Context, conn *dbus.Conn, id, property, value string, noLinkIsSuccess bool) error {
	linkName := "stvmm-" + id
	propertyName := "org.freedesktop.network1.Link." + property
	manager := conn.Object("org.freedesktop.network1", "/org/freedesktop/network1")

	var linkPath string
	var ifIndex uint32
	for {
		err := manager.Call("org.freedesktop.network1.Manager.GetLinkByName", 0, linkName).Store(&ifIndex, &linkPath)
		if err == nil {
			break
		}

		if noLinkIsSuccess {
			return nil
		}

		time.Sleep(1 * time.Second)
	}

	link := conn.Object("org.freedesktop.network1", dbus.ObjectPath(linkPath))
	check := func() error {
		op, err := link.GetProperty(propertyName)
		if err != nil {
			return err
		}

		fmt.Println(op.Value().(string))
		if op.Value().(string) == value {
			return nil
		}

		return errors.New("link not configured")
	}

	err := link.AddMatchSignal("org.freedesktop.DBus.Properties", "PropertiesChanged").Store()
	if err != nil {
		return err
	}

	c := make(chan *dbus.Signal, 10)
	conn.Signal(c)

	if check() == nil {
		return nil
	}

	zl.Ctx(ctx).Info().Str("link", linkName).Str("property", property).Str("value", value).Msg("Waiting for link property")

	for _ = range c {
		if check() == nil {
			return nil
		}

	}

	return nil
}
