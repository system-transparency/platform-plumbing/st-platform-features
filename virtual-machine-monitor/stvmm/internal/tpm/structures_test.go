package tpm

import (
	"bytes"
	"context"
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/x509"
	"errors"
	"reflect"
	"testing"

	test "system-transparency.org/ppg/stvmm/internal"
)

func TestEkCerts(t *testing.T) {
	ctx := context.Background()

	type pair struct {
		certNvindex uint32
		pubTemplate *Template
	}

	pairs := map[string]pair{
		"L-1": {EkCertificateIndexL1, &EkTemplateL1},
		"L-2": {EkCertificateIndexL2, &EkTemplateL2},
		"H-1": {EkCertificateIndexH1, &EkTemplateH1},
		"H-2": {EkCertificateIndexH2, &EkTemplateH2},
		"H-3": {EkCertificateIndexH3, &EkTemplateH3},
		"H-4": {EkCertificateIndexH4, &EkTemplateH4},
		"H-6": {EkCertificateIndexH6, &EkTemplateH6},
		"H-7": {EkCertificateIndexH7, &EkTemplateH7},
	}

	for name, p := range pairs {
		t.Run(name, func(t *testing.T) {
			dev := SelectTestTpm(t)
			if dev == nil {
				return
			}
			defer dev.Close()

			_ = Startup(ctx, dev, TPM_SU_CLEAR)

			maxBuf, err := TpmProperty(ctx, dev, uint32(TPM_PT_NV_BUFFER_MAX))
			test.NoErr(t, err)

			pub, _, err := NvReadPublic(ctx, dev, p.certNvindex)
			if errors.Is(err, ErrRcHandle) {
				t.Skip("EK not available")
				return
			}
			test.NoErr(t, err)

			t.Logf("cert size: %d\n", pub.DataSize)

			var der []byte
			for pub.DataSize > U16(len(der)) {
				sz := uint16(maxBuf)
				if sz > uint16(pub.DataSize)-uint16(len(der)) {
					sz = uint16(pub.DataSize) - uint16(len(der))
				}
				part, err := NvRead(ctx, dev, p.certNvindex, Password(""), p.certNvindex, sz, uint16(len(der)))
				if !test.NoErr(t, err) {
					return
				}
				der = append(der, part...)
			}

			if test.NoErr(t, err) {
				t.Logf("der: %x\n", der)
				var cert *x509.Certificate
				for {
					cert, err = x509.ParseCertificate(der)
					if err == nil {
						break
					}
					if der[len(der)-1] == 0xff {
						der = der[:len(der)-1]
					} else {
						test.NoErr(t, err)
						return
					}
				}

				t.Logf("cert: %v\n", cert.Subject)
				t.Logf("cert: %v\n", cert.Issuer)

				ek, ekpub, _, _, _, _, err := CreatePrimary(ctx, dev, Password(""), TPM_RH_ENDORSEMENT, "", *p.pubTemplate, Buffer{}, PcrSelection{})
				if test.NoErr(t, err) {
					defer func() { _ = FlushContext(ctx, dev, ek) }()
				}

				switch s := cert.PublicKey.(type) {
				case *rsa.PublicKey:
					test.IsEq(t, []byte(ekpub.RsaParameters.Unique), s.N.Bytes())
					exp := ekpub.RsaParameters.Exponent
					if exp == 0 {
						exp = 65537
					}
					test.IsEq(t, exp, U32(s.E))

				case *ecdsa.PublicKey:
					test.IsEq(t, []byte(ekpub.EccParameters.Unique.X), s.X.Bytes())
					test.IsEq(t, []byte(ekpub.EccParameters.Unique.Y), s.Y.Bytes())

				default:
					t.Fatalf("unsupported public key type: %T", cert.PublicKey)
				}
			}
		})
	}
}

func fuzzRoundtrip(t *testing.T, in wire, out wire) {
	buf := new(bytes.Buffer)
	err := encode(buf, in)
	if err != nil {
		t.Fatalf("encode failed: %v", err)
	}
	enc := append([]byte(nil), buf.Bytes()...)
	err = decode(buf, out)
	if err != nil {
		t.Fatalf("decode failed: %v", err)
	}

	if !reflect.DeepEqual(in, out) {
		t.Fatalf("not equal:\nin:  %#v\nout: %#v", in, out)
		return
	}

	if pub, ok := in.(*Public); ok {
		pub2, err := UnpackPublic(enc)
		if err != nil {
			t.Fatalf("DecodePublic failed: %v", err)
		}
		if !reflect.DeepEqual(pub, pub2) {
			t.Fatalf("not equal:\nin:  %#v\nout: %#v", pub, pub2)
			return
		}
		enc2, err := pub.Pack()
		if err != nil {
			t.Fatalf("Pack failed: %v", err)
		}
		if !bytes.Equal(enc, enc2) {
			t.Fatalf("not equal:\nenc:  %x\nenc2: %x", enc, enc2)
		}
	}
	if att, ok := in.(*Attest); ok {
		att2, err := UnpackAttest(enc)
		if err != nil {
			t.Fatalf("DecodeAttest failed: %v", err)
		}
		if !reflect.DeepEqual(att, att2) {
			t.Fatalf("not equal:\nin:  %#v\nout: %#v", att, att2)
		}
		enc2, err := att.Pack()
		if err != nil {
			t.Fatalf("Pack failed: %v", err)
		}
		if !bytes.Equal(enc, enc2) {
			t.Fatalf("not equal:\nenc:  %x\nenc2: %x", enc, enc2)
		}
	}
	if sig, ok := in.(*Signature); ok {
		sig2, err := UnpackSignature(enc)
		if err != nil {
			t.Fatalf("DecodeSignature failed: %v", err)
		}
		if !reflect.DeepEqual(sig, sig2) {
			t.Fatalf("not equal:\nin:  %#v\nout: %#v", sig, sig2)
		}
		enc2, err := sig.Pack()
		if err != nil {
			t.Fatalf("Pack failed: %v", err)
		}
		if !bytes.Equal(enc, enc2) {
			t.Fatalf("not equal:\nenc:  %x\nenc2: %x", enc, enc2)
		}
	}

	for i := 1; i < len(enc)-1; i++ {
		err = decode(bytes.NewReader(enc[0:i]), out)
		if err == nil {
			t.Logf("unexpected success")
		}
	}
}

func fuzzRoundtripFails(t *testing.T, in wire, out wire) {
	buf := new(bytes.Buffer)
	err := encode(buf, in)
	if err != nil {
		return
	}
	err = decode(buf, out)
	if err != nil {
		return
	}
	if reflect.DeepEqual(in, out) {
		t.Fatalf("equal:\nin:  %#v\nout: %#v", in, out)
	}
}

func FuzzPublicRsa(f *testing.F) {
	f.Add(int(TPM_ALG_RSA), int(TPM_ALG_SHA256),
		int(TPMA_OBJECT_RESTRICTED|TPMA_OBJECT_USERWITHAUTH), []byte{},
		int(TPM_ALG_AES), int(128), int(TPM_ALG_CFB),
		int(TPM_ALG_NULL), 0, 0,
		1024, 65537)
	f.Add(int(TPM_ALG_RSA), int(TPM_ALG_SHA256),
		int(TPMA_OBJECT_RESTRICTED|TPMA_OBJECT_USERWITHAUTH), []byte{},
		int(TPM_ALG_NULL), 0, int(TPM_ALG_NULL),
		int(TPM_ALG_NULL), 0, 0,
		1024, 65537)
	f.Add(int(TPM_ALG_RSA), int(TPM_ALG_SHA256),
		int(TPMA_OBJECT_RESTRICTED|TPMA_OBJECT_USERWITHAUTH), []byte{},
		int(TPM_ALG_NULL), 0, int(TPM_ALG_NULL),
		int(TPM_ALG_NULL), 0, 0,
		1024, 0)

	f.Fuzz(func(t *testing.T, ty, nameAlg, attrib int,
		authPolicy []byte, symdefAlg, symdefBits, symdefMode, scheme, schemeAlg,
		schemeCount, keyBits, exponent int) {
		pub := &Public{
			Type:             U16(ty),
			NameAlg:          U16(nameAlg),
			ObjectAttributes: U32(attrib),
			AuthPolicy:       Buffer(authPolicy),
		}
		pub.RsaParameters = &RsaParms{
			Symmetric: SymDefObject{
				Algorithm: U16(symdefAlg),
				KeyBits:   U16(symdefBits),
				Mode:      U16(symdefMode),
			},
			Scheme: Scheme{
				Scheme:  U16(scheme),
				HashAlg: U16(schemeAlg),
				Count:   U32(schemeCount),
			},
			KeyBits:  U16(keyBits),
			Exponent: U32(exponent),
			Unique:   make([]byte, 0),
		}
		var pub2 Public

		fuzzRoundtrip(t, pub, &pub2)
	})
}

func FuzzPublicRsaFailing(f *testing.F) {
	f.Add(int(TPM_ALG_RSA), int(TPM_ALG_SHA256),
		int(TPMA_OBJECT_RESTRICTED|TPMA_OBJECT_USERWITHAUTH), []byte{},
		int(TPM_ALG_ECDSA), int(128), int(TPM_ALG_CFB),
		int(TPM_ALG_NULL), 0, 0,
		1024, 65537)
	f.Add(int(TPM_ALG_RSA), int(TPM_ALG_SHA256),
		int(TPMA_OBJECT_RESTRICTED|TPMA_OBJECT_USERWITHAUTH), []byte{},
		int(TPM_ALG_NULL), 0, int(TPM_ALG_NULL),
		int(TPM_ALG_AES), 0, 0,
		1024, 65537)
	f.Add(int(TPM_ALG_AES), int(TPM_ALG_SHA256),
		int(TPMA_OBJECT_RESTRICTED|TPMA_OBJECT_USERWITHAUTH), []byte{},
		int(TPM_ALG_NULL), 0, int(TPM_ALG_NULL),
		int(TPM_ALG_AES), 0, 0,
		1024, 65537)

	f.Fuzz(func(t *testing.T, ty, nameAlg, attrib int,
		authPolicy []byte, symdefAlg, symdefBits, symdefMode, scheme, schemeAlg,
		schemeCount, keyBits, exponent int) {
		pub := &Public{
			Type:             U16(ty),
			NameAlg:          U16(nameAlg),
			ObjectAttributes: U32(attrib),
			AuthPolicy:       Buffer(authPolicy),
		}
		pub.RsaParameters = &RsaParms{
			Symmetric: SymDefObject{
				Algorithm: U16(symdefAlg),
				KeyBits:   U16(symdefBits),
				Mode:      U16(symdefMode),
			},
			Scheme: Scheme{
				Scheme:  U16(scheme),
				HashAlg: U16(schemeAlg),
				Count:   U32(schemeCount),
			},
			KeyBits:  U16(keyBits),
			Exponent: U32(exponent),
			Unique:   make([]byte, 0),
		}
		var pub2 Public

		fuzzRoundtripFails(t, pub, &pub2)
	})
}

func FuzzPublicEcc(f *testing.F) {
	f.Add(int(TPM_ALG_ECC), int(TPM_ALG_SHA256),
		int(TPMA_OBJECT_RESTRICTED|TPMA_OBJECT_USERWITHAUTH), []byte{},
		int(TPM_ALG_NULL), 0, int(TPM_ALG_NULL),
		int(TPM_ALG_NULL), 0, 0,
		int(TPM_ECC_NIST_P256),
		int(TPM_ALG_NULL), 0, 0,
		[]byte{0x01, 0x02, 0x03}, []byte{0x04, 0x05, 0x06})
	f.Add(int(TPM_ALG_ECC), int(TPM_ALG_SHA256),
		int(TPMA_OBJECT_RESTRICTED|TPMA_OBJECT_USERWITHAUTH), []byte{},
		int(TPM_ALG_AES), int(128), int(TPM_ALG_CFB),
		int(TPM_ALG_NULL), 0, 0,
		int(TPM_ECC_NIST_P256),
		int(TPM_ALG_KDF2), int(TPM_ALG_SHA256), 0,
		[]byte{0x01, 0x02, 0x03}, []byte{0x04, 0x05, 0x06})
	f.Add(int(TPM_ALG_ECC), int(TPM_ALG_SHA256),
		int(TPMA_OBJECT_RESTRICTED|TPMA_OBJECT_USERWITHAUTH), []byte{},
		int(TPM_ALG_AES), int(128), int(TPM_ALG_CFB),
		int(TPM_ALG_NULL), 0, 0,
		int(TPM_ECC_NIST_P256),
		int(TPM_ALG_KDF1_SP800_56A), int(TPM_ALG_SHA256), 0,
		[]byte{0x01, 0x02, 0x03}, []byte{0x04, 0x05, 0x06})
	f.Add(int(TPM_ALG_ECC), int(TPM_ALG_SHA256),
		int(TPMA_OBJECT_RESTRICTED|TPMA_OBJECT_USERWITHAUTH), []byte{},
		int(TPM_ALG_NULL), 0, int(TPM_ALG_NULL),
		int(TPM_ALG_ECDSA), int(TPM_ALG_SHA256), 0,
		int(TPM_ECC_NIST_P256),
		int(TPM_ALG_KDF1_SP800_56A), int(TPM_ALG_SHA256), 0,
		[]byte{0x01, 0x02, 0x03}, []byte{0x04, 0x05, 0x06})

	f.Fuzz(func(t *testing.T, ty, nameAlg, attrib int,
		authPolicy []byte, symdefAlg, symdefBits, symdefMode, scheme, schemeAlg,
		schemeCount, curve, kdfScheme, kdfHashAlg, kdfCount int, x, y []byte) {
		pub := &Public{
			Type:             U16(ty),
			NameAlg:          U16(nameAlg),
			ObjectAttributes: U32(attrib),
			AuthPolicy:       Buffer(authPolicy),
		}
		pub.EccParameters = &EccParms{
			Symmetric: SymDefObject{
				Algorithm: U16(symdefAlg),
				KeyBits:   U16(symdefBits),
				Mode:      U16(symdefMode),
			},
			Scheme: Scheme{
				Scheme:  U16(scheme),
				HashAlg: U16(schemeAlg),
				Count:   U32(schemeCount),
			},
			CurveID: U16(curve),
			KDF: Scheme{
				Scheme:  U16(kdfScheme),
				HashAlg: U16(kdfHashAlg),
				Count:   U32(kdfCount),
			},
			Unique: EccPoint{
				X: x,
				Y: y,
			},
		}
		var pub2 Public

		fuzzRoundtrip(t, pub, &pub2)
	})
}

func FuzzPublicKeyExtraction(f *testing.F) {
	f.Add(int(TPM_ALG_RSA), 65537, 0, []byte{0x01, 0x02, 0x03}, []byte{}, []byte{})
	f.Add(int(TPM_ALG_RSA), 0, 0, []byte{0x01, 0x02, 0x03}, []byte{}, []byte{})
	f.Add(int(TPM_ALG_ECC), 0, int(TPM_ECC_NIST_P256), []byte{}, []byte{0x01, 0x02, 0x03}, []byte{0x04, 0x05, 0x06})
	f.Add(int(TPM_ALG_ECC), 0, int(TPM_ECC_NIST_P384), []byte{}, []byte{0x01, 0x02, 0x03}, []byte{0x04, 0x05, 0x06})
	f.Add(int(TPM_ALG_ECC), 0, int(TPM_ECC_NIST_P521), []byte{}, []byte{0x01, 0x02, 0x03}, []byte{0x04, 0x05, 0x06})

	f.Fuzz(func(t *testing.T, ty, e, curve int, n, x, y []byte) {
		pub := Public{
			Type:             U16(ty),
			NameAlg:          TPM_ALG_SHA256,
			ObjectAttributes: TPMA_OBJECT_RESTRICTED | TPMA_OBJECT_USERWITHAUTH,
			AuthPolicy:       nil,
		}
		if len(n) > 0 {
			pub.RsaParameters = &RsaParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_NULL,
				},
				Scheme: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				KeyBits:  U16(len(n) * 8),
				Exponent: U32(e),
				Unique:   n,
			}
		} else if len(x) > 0 {
			pub.EccParameters = &EccParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_NULL,
				},
				Scheme: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				CurveID: U16(curve),
				Unique: EccPoint{
					X: x,
					Y: y,
				},
			}
		}

		_, err := pub.PublicKey()
		if err != nil {
			t.Fatalf("PublicKey failed: %v", err)
		}
	})
}

func FuzzPublicKeyExtractionFail(f *testing.F) {
	f.Add(int(TPM_ALG_AES), 65537, 0, []byte{0x01, 0x02, 0x03}, []byte{}, []byte{})
	f.Add(int(TPM_ALG_RSA), 0, 0, []byte{}, []byte{}, []byte{})
	f.Add(int(TPM_ALG_ECC), 0, int(TPM_ALG_AES), []byte{}, []byte{0x01, 0x02, 0x03}, []byte{0x04, 0x05, 0x06})

	f.Fuzz(func(t *testing.T, ty, e, curve int, n, x, y []byte) {
		pub := Public{
			Type:             U16(ty),
			NameAlg:          TPM_ALG_SHA256,
			ObjectAttributes: TPMA_OBJECT_RESTRICTED | TPMA_OBJECT_USERWITHAUTH,
			AuthPolicy:       nil,
		}
		if len(n) > 0 {
			pub.RsaParameters = &RsaParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_NULL,
				},
				Scheme: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				KeyBits:  U16(len(n) * 8),
				Exponent: U32(e),
				Unique:   n,
			}
		} else if len(x) > 0 {
			pub.EccParameters = &EccParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_NULL,
				},
				Scheme: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				CurveID: U16(curve),
				Unique: EccPoint{
					X: x,
					Y: y,
				},
			}
		}

		_, err := pub.PublicKey()
		if err == nil {
			t.Fatalf("PublicKey succeeded")
		}
	})
}

func FuzzSymDefObj(f *testing.F) {
	f.Add(int(TPM_ALG_NULL), 0, int(TPM_ALG_NULL), false)
	f.Add(int(TPM_ALG_RSA), 128, int(TPM_ALG_CFB), false)
	f.Add(int(TPM_ALG_AES), 128, int(TPM_ALG_CFB), true)
	f.Add(int(TPM_ALG_AES), 256, int(TPM_ALG_CFB), true)

	f.Fuzz(func(t *testing.T, alg, bits, mode int, ok bool) {
		sym := SymDefObject{
			Algorithm: U16(alg),
			KeyBits:   U16(bits),
			Mode:      U16(mode),
		}

		_, err := sym.Cipher(make([]byte, bits/8), TPM_ALG_AES)
		if (err == nil) != ok {
			t.Fatalf("Cipher failed: %v", err)
		}
	})
}

func FuzzPcrSelection(f *testing.F) {
	f.Add(int(TPM_ALG_SHA1), []byte{0, 1, 2, 3}, int(TPM_ALG_SHA256), []byte{4, 5, 6, 7})
	f.Add(int(TPM_ALG_SHA1), []byte{21, 22}, -1, []byte{})
	f.Add(int(TPM_ALG_SHA1), []byte{100, 101}, -1, []byte{})

	f.Fuzz(func(t *testing.T, alg1 int, pcr1 []byte, alg2 int, pcr2 []byte) {
		pcrs := PcrSelection{}

		if alg1 > -1 {
			pcr := make([]int, len(pcr1))
			for i, v := range pcr1 {
				pcr[i] = int(v)
			}
			sel := PcrSelect{
				Hash: U16(alg1),
				Pcr:  pcr,
			}
			pcrs = append(pcrs, sel)
		}
		if alg2 > -1 {
			pcr := make([]int, len(pcr1))
			for i, v := range pcr2 {
				pcr[i] = int(v)
			}
			sel := PcrSelect{
				Hash: U16(alg2),
				Pcr:  pcr,
			}
			pcrs = append(pcrs, sel)
		}

		fuzzRoundtrip(t, &pcrs, &PcrSelection{})
	})
}

func FuzzTicket(f *testing.F) {
	f.Add(int(6), int(TPM_RH_ENDORSEMENT), []byte{0, 1, 2, 3})
	f.Add(int(42), int(TPM_RH_OWNER), []byte{})

	f.Fuzz(func(t *testing.T, tag, hierarchy int, digest []byte) {
		ticket := Ticket{
			Tag:       U16(tag),
			Hierarchy: U32(hierarchy),
			Digest:    Buffer(digest),
		}
		fuzzRoundtrip(t, &ticket, &Ticket{})
	})
}

func FuzzSensitiveCreate(f *testing.F) {
	f.Add([]byte{}, []byte{})
	f.Add([]byte{0, 1, 2, 3}, []byte{4, 5, 6, 7})
	f.Add([]byte{}, []byte{4, 5, 6, 7})
	f.Add([]byte{0, 1, 2, 3}, []byte{})

	f.Fuzz(func(t *testing.T, userAuth, data []byte) {
		create := SensitiveCreate{
			UserAuth: Buffer(userAuth),
			Data:     Buffer(data),
		}
		fuzzRoundtrip(t, &create, &SensitiveCreate{})
	})
}

func FuzzNvPublic(f *testing.F) {
	f.Add(int(0), int(TPM_ALG_SHA1), int(TPMA_NV_AUTHREAD), []byte{4, 5, 6, 7}, int(1024))
	f.Add(int(0x11223344), int(TPM_ALG_SHA256), 0, []byte{}, int(1024))

	f.Fuzz(func(t *testing.T, idx, nameAlg, attrib int, pol []byte, sz int) {
		nvpub := NvPublic{
			Index:      U32(idx),
			NameAlg:    U16(nameAlg),
			Attributes: U32(attrib),
			AuthPolicy: Buffer(pol),
			DataSize:   U16(sz),
		}
		fuzzRoundtrip(t, &nvpub, &NvPublic{})
	})
}

func FuzzCapabilityAlgs(f *testing.F) {
	f.Add(int(TPM_CAP_ALGS), int(TPM_ALG_AES), 0x11223344, int(TPM_ALG_SHA1), 0x556677, true)
	f.Add(int(TPM_CAP_ALGS), int(TPM_ALG_AES), 0x11223344, -1, 0, true)
	f.Add(int(TPM_CAP_ALGS), -1, 0, -1, 0, true)
	f.Add(int(TPM_CAP_ALGS), int(TPM_RH_ENDORSEMENT), 0x11223344, -1, 0, true)
	f.Add(int(TPM_CAP_HANDLES), int(TPM_ALG_AES), 0x11223344, int(TPM_ALG_SHA1), 0x556677, false)

	f.Fuzz(func(t *testing.T, capa, a1, p1, a2, p2 int, ok bool) {
		props := []AlgProperty{}
		if a1 > -1 {
			props = append(props, AlgProperty{
				Alg:        U16(a1),
				Properties: U32(p1),
			})
		}
		if a2 > -1 {
			props = append(props, AlgProperty{
				Alg:        U16(a2),
				Properties: U32(p2),
			})
		}
		capdata := CapabilityData{
			Capability: U32(capa),
			Algorithms: props,
		}

		if ok {
			fuzzRoundtrip(t, &capdata, &CapabilityData{})
		} else {
			fuzzRoundtripFails(t, &capdata, &CapabilityData{})
		}
	})
}

func FuzzCapabilityHandels(f *testing.F) {
	f.Add(int(TPM_CAP_HANDLES), 1, 2, true)
	f.Add(int(TPM_CAP_HANDLES), 1, -1, true)
	f.Add(int(TPM_CAP_HANDLES), -1, -1, true)
	f.Add(int(TPM_CAP_ALGS), 1, -1, false)

	f.Fuzz(func(t *testing.T, capa, h1, h2 int, ok bool) {
		handles := []U32{}
		if h1 > -1 {
			handles = append(handles, U32(h1))
		}
		if h2 > -1 {
			handles = append(handles, U32(h2))
		}
		capdata := CapabilityData{
			Capability: U32(capa),
			Handles:    handles,
		}

		if ok {
			fuzzRoundtrip(t, &capdata, &CapabilityData{})
		} else {
			fuzzRoundtripFails(t, &capdata, &CapabilityData{})
		}
	})
}

func FuzzCapabilityPcrs(f *testing.F) {
	f.Add(int(TPM_CAP_PCRS), int(TPM_ALG_SHA1), []byte{0, 1, 2, 3}, int(TPM_ALG_SHA256), []byte{4, 5, 6, 7}, true)
	f.Add(int(TPM_CAP_PCRS), int(TPM_ALG_SHA1), []byte{21, 22}, -1, []byte{}, true)
	f.Add(int(TPM_CAP_PCRS), -1, []byte{}, -1, []byte{}, true)
	f.Add(int(TPM_CAP_HANDLES), int(TPM_ALG_SHA1), []byte{21, 22}, -1, []byte{}, false)

	f.Fuzz(func(t *testing.T, capa, alg1 int, pcr1 []byte, alg2 int, pcr2 []byte, ok bool) {
		pcrs := PcrSelection{}
		if alg1 > -1 {
			pcr := make([]int, len(pcr1))
			for i, v := range pcr1 {
				pcr[i] = int(v)
			}
			sel := PcrSelect{
				Hash: U16(alg1),
				Pcr:  pcr,
			}
			pcrs = append(pcrs, sel)
		}
		if alg2 > -1 {
			pcr := make([]int, len(pcr1))
			for i, v := range pcr2 {
				pcr[i] = int(v)
			}
			sel := PcrSelect{
				Hash: U16(alg2),
				Pcr:  pcr,
			}
			pcrs = append(pcrs, sel)
		}
		capdata := CapabilityData{
			Capability: U32(capa),
			Pcrs:       pcrs,
		}

		if ok {
			fuzzRoundtrip(t, &capdata, &CapabilityData{})
		} else {
			fuzzRoundtripFails(t, &capdata, &CapabilityData{})
		}
	})

}

func FuzzCapabilityCommands(f *testing.F) {
	f.Add(int(TPM_CAP_COMMANDS), 1, 2, true)
	f.Add(int(TPM_CAP_COMMANDS), 1, -1, true)
	f.Add(int(TPM_CAP_COMMANDS), -1, -1, true)
	f.Add(int(TPM_CAP_ALGS), 1, -1, false)

	f.Fuzz(func(t *testing.T, capa, c1, c2 int, ok bool) {
		cmds := []U32{}
		if c1 > -1 {
			cmds = append(cmds, U32(c1))
		}
		if c2 > -1 {
			cmds = append(cmds, U32(c2))
		}
		capdata := CapabilityData{
			Capability: U32(capa),
			Commands:   cmds,
		}

		if ok {
			fuzzRoundtrip(t, &capdata, &CapabilityData{})
		} else {
			fuzzRoundtripFails(t, &capdata, &CapabilityData{})
		}
	})
}

func FuzzCapabilityCurves(f *testing.F) {
	f.Add(int(TPM_CAP_ECC_CURVES), 1, 2, true)
	f.Add(int(TPM_CAP_ECC_CURVES), 1, -1, true)
	f.Add(int(TPM_CAP_ECC_CURVES), -1, -1, true)
	f.Add(int(TPM_CAP_ALGS), 1, -1, false)

	f.Fuzz(func(t *testing.T, capa, c1, c2 int, ok bool) {
		cvs := []U16{}
		if c1 > -1 {
			cvs = append(cvs, U16(c1))
		}
		if c2 > -1 {
			cvs = append(cvs, U16(c2))
		}
		capdata := CapabilityData{
			Capability: U32(capa),
			EccCurves:  cvs,
		}

		if ok {
			fuzzRoundtrip(t, &capdata, &CapabilityData{})
		} else {
			fuzzRoundtripFails(t, &capdata, &CapabilityData{})
		}
	})
}

func FuzzCapabilityTpmProps(f *testing.F) {
	f.Add(int(TPM_CAP_TPM_PROPERTIES), 1, 1, 2, 2, true)
	f.Add(int(TPM_CAP_TPM_PROPERTIES), 1, 1, -1, 0, true)
	f.Add(int(TPM_CAP_TPM_PROPERTIES), -1, 0, -1, 0, true)
	f.Add(int(TPM_CAP_ALGS), 1, 1, -1, 0, false)
	f.Add(42, 1, 1, -1, 0, false)

	f.Fuzz(func(t *testing.T, capa, p1, v1, p2, v2 int, ok bool) {
		props := map[U32]U32{}
		if p1 > -1 {
			props[U32(p1)] = U32(v1)
		}
		if p2 > -1 {
			props[U32(p2)] = U32(v2)
		}
		capdata := CapabilityData{
			Capability:    U32(capa),
			TpmProperties: props,
		}

		if ok {
			fuzzRoundtrip(t, &capdata, &CapabilityData{})
		} else {
			fuzzRoundtripFails(t, &capdata, &CapabilityData{})
		}
	})
}
func FuzzDigestValues(f *testing.F) {
	f.Add(int(TPM_ALG_SHA1), make([]byte, 20), int(TPM_ALG_SHA256), make([]byte, 32), true)
	f.Add(int(TPM_ALG_SHA1), make([]byte, 20), -1, []byte{}, true)
	f.Add(-1, []byte{}, -1, []byte{}, true)
	f.Add(int(TPM_ALG_SHA1), []byte{1, 2, 3}, int(TPM_ALG_SHA256), []byte{4, 5, 6}, false)
	f.Add(int(TPM_ALG_AES), []byte{1, 2, 3}, int(TPM_ALG_SHA256), []byte{4, 5, 6}, false)
	f.Add(int(TPM_ALG_SHA384), make([]byte, 48), -1, []byte{}, true)
	f.Add(int(TPM_ALG_SHA512), make([]byte, 64), -1, []byte{}, true)
	f.Add(int(TPM_ALG_SM3_256), make([]byte, 32), -1, []byte{}, true)

	f.Fuzz(func(t *testing.T, alg1 int, data1 []byte, alg2 int, data2 []byte, ok bool) {
		dd := DigestValues{}
		if alg1 > -1 {
			dd = append(dd, Ha{
				Alg:    U16(alg1),
				Digest: Buffer(data1),
			})
		}
		if alg2 > -1 {
			dd = append(dd, Ha{
				Alg:    U16(alg2),
				Digest: Buffer(data2),
			})
		}

		if ok {
			fuzzRoundtrip(t, &dd, &DigestValues{})
		} else {
			fuzzRoundtripFails(t, &dd, &DigestValues{})
		}
	})
}

func FuzzDigests(f *testing.F) {
	f.Add([]byte{1, 2, 3}, []byte{4, 5, 6})
	f.Add([]byte{1, 2, 3}, []byte{})
	f.Add([]byte{1, 2, 3}, []byte(nil))
	f.Add([]byte(nil), []byte(nil))

	f.Fuzz(func(t *testing.T, data1, data2 []byte) {
		dd := Digests{}
		if data1 != nil {
			dd = append(dd, Buffer(data1))
		}
		if data2 != nil {
			dd = append(dd, Buffer(data2))
		}

		fuzzRoundtrip(t, &dd, &Digests{})
	})
}

func FuzzSignature(f *testing.F) {
	f.Add(int(TPM_ALG_ECDSA), int(TPM_ALG_SHA1), []byte{1, 2, 3}, []byte{4, 5, 6}, true)
	f.Add(int(TPM_ALG_ECDSA), int(TPM_ALG_SHA256), []byte{1, 2, 3}, []byte{4, 5, 6}, true)
	f.Add(int(TPM_ALG_RSA), int(TPM_ALG_SHA384), []byte{1, 2, 3}, []byte{4, 5, 6}, false)

	f.Fuzz(func(t *testing.T, alg, hash int, r, s []byte, ok bool) {
		sig := Signature{
			Algorithm: U16(alg),
			Ecc: &EccSignature{
				Hash: U16(hash),
				R:    r,
				S:    s,
			},
		}
		if ok {
			fuzzRoundtrip(t, &sig, &Signature{})
		} else {
			fuzzRoundtripFails(t, &sig, &Signature{})
		}
	})
}

func FuzzAttest(f *testing.F) {
	f.Add(int(TPM_GENERATED_VALUE), int(TPM_ST_ATTEST_QUOTE), int(TPM_ALG_SHA1), int(TPM_ALG_SHA256), []byte{0, 1, 2, 3}, []byte{4, 5, 6, 7}, true)
	f.Add(int(TPM_GENERATED_VALUE), int(TPM_ST_ATTEST_CERTIFY), int(TPM_ALG_SHA1), int(TPM_ALG_SHA256), []byte{0, 1, 2, 3}, []byte{4, 5, 6, 7}, false)

	f.Fuzz(func(t *testing.T, magic, ty, alg1, alg2 int, pcr1, pcr2 []byte, ok bool) {
		pcrs := PcrSelection{}
		if alg1 > -1 {
			pcr := make([]int, len(pcr1))
			for i, v := range pcr1 {
				pcr[i] = int(v)
			}
			sel := PcrSelect{
				Hash: U16(alg1),
				Pcr:  pcr,
			}
			pcrs = append(pcrs, sel)
		}
		if alg2 > -1 {
			pcr := make([]int, len(pcr1))
			for i, v := range pcr2 {
				pcr[i] = int(v)
			}
			sel := PcrSelect{
				Hash: U16(alg2),
				Pcr:  pcr,
			}
			pcrs = append(pcrs, sel)
		}
		att := Attest{
			Magic:  U32(magic),
			Type:   U16(ty),
			Signer: make([]byte, 32),
			Extra:  make([]byte, 32),
			Clock: ClockInfo{
				Clock:        0,
				ResetCount:   0,
				RestartCount: 0,
				Safe:         0,
			},
			FirmwareVersion: 0,
			Quote: &QuoteInfo{
				Select: pcrs,
				Digest: make([]byte, 32),
			},
		}
		if ok {
			fuzzRoundtrip(t, &att, &Attest{})
		} else {
			fuzzRoundtripFails(t, &att, &Attest{})
		}
	})
}

func FuzzIdObj(f *testing.F) {
	f.Add([]byte{1, 2, 3}, []byte{4, 5, 6})
	f.Add([]byte{1, 2, 3}, []byte{})
	f.Add([]byte{}, []byte{4, 5, 6})

	f.Fuzz(func(t *testing.T, hmac, encid []byte) {
		id := IdObject{
			IntegrityHmac: hmac,
			EncIdentity:   encid,
		}
		buf, err := id.Pack()
		if err != nil {
			t.Fatalf("Pack failed: %v", err)
		}
		id2, err := UnpackIdObject(buf)
		if err != nil {
			t.Fatalf("Unpack failed: %v", err)
		}
		if !reflect.DeepEqual(&id, id2) {
			t.Fatalf("not equal:\nin:  %#v\nout: %#v", id, id2)
		}
		buf2 := new(bytes.Buffer)
		err = encode(buf2, &id)
		if err != nil {
			t.Fatalf("encode failed: %v", err)
		}
	})
}
