package tpm

import (
	"bytes"
	"context"
	"io"
)

// TPM2_Startup (Part 3, Section 9.3): Needs to be sent once after power up to
// enable commands other than TPM2_SelfTest and TPM2_GetCapability.
//
// The mode parameter must be TPM_SU_CLEAR or TPM_SU_STATE. Under normal
// conditions the operating system will have sent this command before the
// application starts. This command is mostly useful for testing against a
// simulator.
func Startup(ctx context.Context, dev Device, mode U16) error {
	return tx(ctx, dev, TPM_CC_Startup, []wire{&mode}, nil, nil, nil)
}

// TPM2_CreatePrimary (Part 3, Section 24.1): Creates a Primary Key.
//
// The primaryHandle parameter must be a hierarchy handle, userAuth the
// AuthValue send via SensitiveCreate. The public parameter is the public area
// template. The outsideInfo parameter is used to provide a seed value for the
// creation process. The creationPCR parameter is used to record the PCR values
// used in the creation process.
//
// The function only supports creating RSA and ECC keys.
//
// The function returns the handle of the newly created key, the filled public
// area of the key, the creation data and the creation hash, and the creation
// ticket that can validate them.
func CreatePrimary(ctx context.Context, dev Device, primaryAuth Auth, primaryHandle U32, userAuth string, tmpl Template, outsideInfo Buffer, creationPCR []PcrSelect) (U32, Public, Buffer, Buffer, Ticket, Buffer, error) {
	var (
		outPublic, outCreationData, outCreationHash Buffer
		outCreationTicket                           Ticket
		outName                                     Buffer

		inPublic, inSensitiveCreate, inOutsideInfo Buffer
		inCreationPCR                              PcrSelection
	)

	buf := new(bytes.Buffer)
	err := tmpl.Public.encode(buf)
	if err != nil {
		return 0, Public{}, Buffer{}, Buffer{}, Ticket{}, Buffer{}, err
	}
	inPublic = Buffer(buf.Bytes())

	buf = new(bytes.Buffer)
	sens := &SensitiveCreate{UserAuth: Buffer(userAuth)}
	err = sens.encode(buf)
	if err != nil {
		return 0, Public{}, Buffer{}, Buffer{}, Ticket{}, Buffer{}, err
	}
	inSensitiveCreate = Buffer(buf.Bytes())

	req := []wire{
		&inSensitiveCreate,
		&inPublic,
		&inOutsideInfo,
		&inCreationPCR,
	}
	reqh := singleHandle(primaryHandle, primaryAuth)
	resph := numHandles(1)
	resp := []wire{
		&outPublic,
		&outCreationData,
		&outCreationHash,
		&outCreationTicket,
		&outName,
	}

	err = tx(ctx, dev, TPM_CC_CreatePrimary, req, reqh, resp, resph)
	if err != nil {
		return 0, Public{}, Buffer{}, Buffer{}, Ticket{}, Buffer{}, err
	}

	var outpub Public
	if err := decode(bytes.NewBuffer(outPublic), &outpub); err != nil {
		return 0, Public{}, Buffer{}, Buffer{}, Ticket{}, Buffer{}, err
	}

	return resph.handles[0], outpub, outCreationData, outCreationHash, outCreationTicket, outName, nil
}

// TPM2_FlushContext (Part 3, Section 28.4): Removes an object from the TPM
// memory.
//
// The handle parameter is the handle of the object to be removed it must be a
// Primary Object or a session,
func FlushContext(ctx context.Context, dev Device, handle U32) error {
	return tx(ctx, dev, TPM_CC_FlushContext, []wire{&handle}, nil, nil, nil)
}

// TPM2_NV_ReadPublic (Part 3, Section 31.6): Reads the public area of an NV
// index.
//
// The public area contains metadata about the NV index, such as its size and
// attribute bits.
//
// The commands requires the index parameter to be the handle of the NV index
// and returns the public area and the name of the index. No authentication is
// required.
func NvReadPublic(ctx context.Context, dev Device, index uint32) (NvPublic, Buffer, error) {
	var pub NvPublic
	var nvName, nvPublic Buffer

	req := []wire{(*U32)(&index)}
	resp := []wire{&nvPublic, &nvName}

	err := tx(ctx, dev, TPM_CC_NV_ReadPublic, req, nil, resp, nil)
	if err != nil {
		return NvPublic{}, Buffer{}, err
	}

	err = pub.decode(bytes.NewBuffer(nvPublic))
	return pub, nvName, err
}

// TPM2_NV_Read (Part 3, Section 31.31): Reads data from an NV index.
//
// The index parameter is the handle of the NV index, size and offset is the
// area to be read. It returns the data read from the index or an error.
func NvRead(ctx context.Context, dev Device, authidx uint32, a Auth, index uint32, size uint16, offset uint16) (Buffer, error) {
	var data Buffer

	req := []wire{(*U16)(&size), (*U16)(&offset)}
	reqh := &handleArea{handles: []U32{U32(authidx), U32(index)}, auth: []Auth{a, nil}}
	resp := []wire{&data}

	err := tx(ctx, dev, TPM_CC_NV_Read, req, reqh, resp, nil)
	return data, err
}

// TPM2_GetCapability (Part 3, Section 30.2): Queries the TPM for various kinds
// of information.
//
// The capability parameter must be one of the TPM_CAP_* constants, property
// depends on the capability and count is the number of values to request. The returned values are stored in the CapabilityData structure. Only one field of the structure will be filled, depending on the capability parameter.
//
// | Capability             | Property  | Return value
// |------------------------|-----------|-------------
// | TPM_CAP_ALGS           | TPM_ALG_* | `.Algorithms`: Implemented algorithms and their properties
// | TPM_CAP_HANDLES        | Handle    | `.Handles`: Handles currently in use
// | TPM_CAP_COMMANDS       | TPM_CC_*  | `.Commands`: Implemented commands and their attributes
// | TPM_CAP_TPM_PROPERTIES | TPM_PT_*  | `.TpmProperties`: Values of the requested properties.
// | TPM_CAP_ECC_CURVES     | TPM_ECC_* | `.EccCurves`: Implemented elliptic curves.
//
// The boolean return value indicates if there are more values available than
// count.
func GetCapability(ctx context.Context, dev Device, capability, property, count uint32) (CapabilityData, bool, error) {
	var moreData u8
	var outCap CapabilityData

	req := []wire{(*U32)(&capability), (*U32)(&property), (*U32)(&count)}
	resp := []wire{&moreData, &outCap}

	err := tx(ctx, dev, TPM_CC_GetCapability, req, nil, resp, nil)
	if err != nil {
		return CapabilityData{}, false, err
	}
	if outCap.Capability != U32(capability) {
		return CapabilityData{}, false, ErrFormat
	}
	return outCap, moreData != 0, err
}

func TpmProperty(ctx context.Context, dev Device, property uint32) (uint32, error) {
	value, _, err := GetCapability(ctx, dev, uint32(TPM_CAP_TPM_PROPERTIES), property, 1)
	if err != nil {
		return 0, err
	}
	if value.TpmProperties == nil {
		return 0, ErrPropertyNotFound
	}
	f, ok := value.TpmProperties[U32(property)]
	if !ok {
		return 0, ErrPropertyNotFound
	}
	return uint32(f), nil
}

// XXX
func PcrExtend(ctx context.Context, dev Device, pcrIndex U32, pcrAuth Auth, digests DigestValues) error {
	reqh := singleHandle(pcrIndex, pcrAuth)
	req := []wire{&digests}

	err := tx(ctx, dev, TPM_CC_PCR_Extend, req, reqh, nil, nil)
	return err
}

// TPM2_PCR_Read (Part 3, Section 22.4): Reads the current value of a set of PCR.
//
// The selections parameter is a list of PCR banks to read. The function
// returns the current value of the selected PCR banks. The returned selection
// may be a subset of the requested selection due to response size limitations.
func PcrRead(ctx context.Context, dev Device, selections PcrSelection) (U32, PcrSelection, Digests, error) {
	var (
		outUpdateCounter U32
		outSelection     PcrSelection
		outPcrValues     Digests
	)

	req := []wire{&selections}
	resp := []wire{&outUpdateCounter, &outSelection, &outPcrValues}

	err := tx(ctx, dev, TPM_CC_PCR_Read, req, nil, resp, nil)
	return outUpdateCounter, outSelection, outPcrValues, err
}

// TPM2_Quote (Part 3, Section 18.4): Creates a signed attestation of PCR values.
//
// The TPM will create a attestation structure containing an hash of all PCR
// banks selected in pcrSelection, and sign it with the key handle signHandle.
// The key has to be a restricted signing key. The scheme parameter specifies
// the signature scheme to use, or TPM_ALG_NULL to use the default scheme of
// the key. The qualifyingData parameter is a user-provided data to include in
// the attestation. The function returns the attestation structure and the
// signature.
func Quote(ctx context.Context, dev Device, signHandle U32, signAuth Auth, scheme Scheme, qualifyingData Buffer, pcrSelection PcrSelection) (Attest, Signature, error) {
	var (
		outQuoted    Attest
		outSignature Signature
		quotebuf     Buffer
	)

	req := []wire{&qualifyingData, &scheme, &pcrSelection}
	reqh := singleHandle(signHandle, signAuth)
	resp := []wire{&quotebuf, &outSignature}

	err := tx(ctx, dev, TPM_CC_Quote, req, reqh, resp, nil)
	if err != nil {
		return Attest{}, Signature{}, err
	}

	err = outQuoted.decode(bytes.NewBuffer(quotebuf))
	if err != nil {
		return Attest{}, Signature{}, err
	}

	return outQuoted, outSignature, nil
}

// TPM2_ActivateCredential (Part 3, Section 12.5): Decrypts a sercret associated with a loaded TPM key.
//
// The function decrypts the secret, called credential using a key in the TPM
// (keyHandle) iff another key, associated with the credential (activateHandle)
// is loaded in the TPM. The function returns the decrypted secret or an error.
//
// Credential activation is used to link an restricted decryption key, like the
// Endorsement Key to a arbitrary TPM key. The decrypted credential can be used
// by the activated key to proof this fact to someone else e.g. by having the
// credential be an X.509 certificate.
//
// Credentials can be encrypted using the Public.MakeCredential function.
func ActivateCredential(ctx context.Context, dev Device, activateHandle, keyHandle U32, activeAuth, keyAuth Auth, credBlob IdObject, secret Buffer) (Buffer, error) {
	var certInfo Buffer

	credbuf, err := pack(&credBlob)
	if err != nil {
		return Buffer{}, err
	}

	req := []wire{&credbuf, &secret}
	reqh := &handleArea{
		handles: []U32{activateHandle, keyHandle},
		auth:    []Auth{activeAuth, keyAuth},
	}
	resp := []wire{&certInfo}

	err = tx(ctx, dev, TPM_CC_ActivateCredential, req, reqh, resp, nil)
	return certInfo, err
}

// TPM2_StartAuthSession (Part 3, Section 11.1) starts an authentication session.
//
// This is a simplified version of the StartAuthSession command that only
// supports unbound, unsalted policy sessions. The authHash parameter specifies
// the hash algorithm to use for the session. The function returns the handle
// of the newly created session or an error.
//
// Sessions created by this function must be deleted with FlushContext when they
// are no longer needed. To use the session handle for authorization, it must be
func StartAuthSession(ctx context.Context, dev Device, authHash U16, random io.Reader) (U32, error) {
	h, err := HashAlgorithm(authHash)
	if err != nil {
		return 0, err
	}

	var (
		encryptedSalt, nonceCaller, nonceTPM Buffer
		symmetric                            SymDefObject
		sessionType                          u8
		tpmKey, bindKey                      U32
	)
	symmetric.Algorithm = TPM_ALG_NULL
	sessionType = TPM_SE_POLICY
	nonceCaller = Buffer(make([]byte, h.Size()))
	tpmKey = TPM_RH_NULL
	bindKey = TPM_RH_NULL

	_, err = io.ReadFull(random, nonceTPM)
	if err != nil {
		return 0, err
	}

	req := []wire{&nonceCaller, &encryptedSalt, &sessionType, &symmetric, &authHash}
	reqh := &handleArea{handles: []U32{tpmKey, bindKey}}
	resp := []wire{&nonceTPM}
	resph := numHandles(1)

	err = tx(ctx, dev, TPM_CC_StartAuthSession, req, reqh, resp, resph)
	if err != nil {
		return 0, err
	}
	return resph.handles[0], err
}

// TPM2_PolicySecret (Part 3, Section 23.4): Asserts the knowledge of the
// authValue of an object.
//
// The function updates the policy digest of sessionHandle if the called can
// prove knowledge the the authValue of authHandle. The nonceTPM parameter is
// the session's policy nonce, cpHashA limits the policy to a specific command,
// policyRef is the value that replaces the current policy digest, and
// expiration is the time when the policy session should expire.
//
// The function returns the timeout value and a ticket if expiration is not 0.
func PolicySecret(ctx context.Context, dev Device, authHandle U32, sessionHandle U32, handleAuth Auth, nonceTPM Buffer, cpHashA Buffer, policyRef Buffer, expiration U32) (Buffer, Ticket, error) {
	var timeout Buffer
	var ticket Ticket

	req := []wire{&nonceTPM, &cpHashA, &policyRef, &expiration}
	reqh := &handleArea{
		handles: []U32{authHandle, sessionHandle},
		auth:    []Auth{handleAuth, nil},
	}
	resp := []wire{&timeout, &ticket}

	err := tx(ctx, dev, TPM_CC_PolicySecret, req, reqh, resp, nil)
	return timeout, ticket, err
}

func PcrReset(ctx context.Context, dev Device, pcr U32, auth Auth) error {
	reqh := singleHandle(pcr, auth)

	err := tx(ctx, dev, TPM_CC_PCR_Reset, nil, reqh, nil, nil)
	return err
}
