package tpm

import (
	"bytes"
	"crypto"
	"crypto/aes"
	"crypto/cipher"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rsa"
	"io"
	"math/big"
	"sort"
)

// TPMT_PUBLIC (Part 2, Section 12.2): The public part of a Primary or Ordinary
// Object.
//
// The public part of an object contains it's type, Name Algorithm, attributes,
// policy digest, type-specific parameters and public key.
//
// This implementation only supports RSA and ECC keys.
type Public struct {
	Type             U16    // TPM_ALG_RSA or TPM_ALG_ECC
	NameAlg          U16    // TPM_ALG_* hash algorithm
	ObjectAttributes U32    // TPMA_OBJECT_* bits
	AuthPolicy       Buffer // Empty or NameAlg sized Buffer

	// Only one will be set.
	RsaParameters *RsaParms // Type == TPM_ALG_RSA
	EccParameters *EccParms // Type == TPM_ALG_ECC
}

func UnpackPublic(buf []byte) (*Public, error) {
	var p Public
	if err := decode(bytes.NewReader(buf), &p); err != nil {
		return nil, err
	}
	return &p, nil
}

var _ = wire(new(Public))

func (p *Public) encode(wr io.Writer) error {
	if err := encode(wr, &p.Type, &p.NameAlg, &p.ObjectAttributes, &p.AuthPolicy); err != nil {
		return err
	}
	if p.EccParameters != nil {
		return p.EccParameters.encode(wr)
	} else if p.RsaParameters != nil {
		return p.RsaParameters.encode(wr)
	}
	return nil
}

func (p *Public) decode(rd io.Reader) error {
	if err := decode(rd, &p.Type, &p.NameAlg, &p.ObjectAttributes, &p.AuthPolicy); err != nil {
		return err
	}

	switch p.Type {
	case TPM_ALG_RSA:
		p.RsaParameters = new(RsaParms)
		return p.RsaParameters.decode(rd)
	case TPM_ALG_ECC:
		p.EccParameters = new(EccParms)
		return p.EccParameters.decode(rd)
	default:
		return ErrUnsupported
	}
}

func (p *Public) PublicKey() (pub crypto.PublicKey, err error) {
	switch p.Type {
	case TPM_ALG_RSA:
		if p.RsaParameters == nil {
			return nil, ErrUnsupported
		}
		exp := p.RsaParameters.Exponent
		if exp == 0 {
			exp = 0x10001
		}
		pub = &rsa.PublicKey{
			N: new(big.Int).SetBytes(p.RsaParameters.Unique),
			E: int(exp),
		}
	case TPM_ALG_ECC:
		if p.EccParameters == nil {
			return nil, ErrUnsupported
		}
		cv, err := p.EccParameters.Curve()
		if err != nil {
			return nil, err
		}
		pub = &ecdsa.PublicKey{
			X:     new(big.Int).SetBytes(p.EccParameters.Unique.X),
			Y:     new(big.Int).SetBytes(p.EccParameters.Unique.Y),
			Curve: cv,
		}
	default:
		err = ErrUnsupported
	}

	return
}

func (p *Public) Pack() ([]byte, error) {
	var buf bytes.Buffer
	if err := p.encode(&buf); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

type Template struct {
	Public Public
}

// TPMS_ECC_POINT (Part 2, Section 11.2.5.2): A point on an elliptic curve.
type EccPoint struct {
	X Buffer
	Y Buffer
}

var _ = wire(new(EccPoint))

func (e *EccPoint) encode(wr io.Writer) error {
	return encode(wr, &e.X, &e.Y)
}

func (e *EccPoint) decode(rd io.Reader) error {
	return decode(rd, &e.X, &e.Y)
}

// TPMS_ECC_PARMS (Part 2, Section 12.2.3.6): Public ECC key parametes.
type EccParms struct {
	Symmetric SymDefObject
	Scheme    Scheme // Signature scheme
	CurveID   U16    // TPM_ECC_*
	KDF       Scheme
	Unique    EccPoint // Public key
}

var _ = wire(new(EccParms))

func (e *EccParms) encode(wr io.Writer) error {
	err := encode(wr, &e.Symmetric, &e.Scheme, &e.CurveID, &e.KDF, &e.Unique)
	return err
}

func (e *EccParms) decode(rd io.Reader) error {
	return decode(rd, &e.Symmetric, &e.Scheme, &e.CurveID, &e.KDF, &e.Unique)
}

func (e *EccParms) Curve() (elliptic.Curve, error) {
	switch e.CurveID {
	case TPM_ECC_NIST_P256:
		return elliptic.P256(), nil
	case TPM_ECC_NIST_P384:
		return elliptic.P384(), nil
	case TPM_ECC_NIST_P521:
		return elliptic.P521(), nil
	default:
		return nil, ErrUnsupported
	}
}

// TPMS_RSA_PARMS (Part 2, Section 12.2.3.5): Public RSA key parameters.
type RsaParms struct {
	Symmetric SymDefObject
	Scheme    Scheme // Signature scheme
	KeyBits   U16    // 2048, 3072, 4096
	Exponent  U32
	Unique    Buffer // Public key modulus
}

var _ = wire(new(RsaParms))

func (r *RsaParms) encode(wr io.Writer) error {
	return encode(wr, &r.Symmetric, &r.Scheme, &r.KeyBits, &r.Exponent, &r.Unique)
}

func (r *RsaParms) decode(rd io.Reader) error {
	err := decode(rd, &r.Symmetric, &r.Scheme, &r.KeyBits, &r.Exponent, &r.Unique)
	if err != nil {
		return err
	}
	return nil
}

// TPMT_SIG_SCHEME (Part 2, Section 11.2.1.5): Signature scheme.
// Depending on the context this is a TPMT_ECC_SCHEME or TPMT_RSA_SCHEME. The
// Count and HashAlg fields are only used for certain schemes.
type Scheme struct {
	Scheme  U16 // TPM_ALG_*
	HashAlg U16
	Count   U32
}

var _ = wire(new(Scheme))

func (k *Scheme) encode(wr io.Writer) error {
	if err := encode(wr, &k.Scheme); err != nil {
		return err
	}
	switch k.Scheme {
	case TPM_ALG_NULL:
		return nil
	case TPM_ALG_ECDSA:
		return encode(wr, &k.HashAlg)
	case TPM_ALG_KDF1_SP800_56A:
		return encode(wr, &k.HashAlg)
	case TPM_ALG_KDF2:
		return encode(wr, &k.HashAlg)
	default:
		return ErrUnsupported
	}
}

func (k *Scheme) decode(rd io.Reader) error {
	if err := decode(rd, &k.Scheme); err != nil {
		return err
	}
	switch k.Scheme {
	case TPM_ALG_NULL:
		return nil
	case TPM_ALG_ECDSA:
		return decode(rd, &k.HashAlg)
	case TPM_ALG_KDF1_SP800_56A:
		return decode(rd, &k.HashAlg)
	case TPM_ALG_KDF2:
		return decode(rd, &k.HashAlg)
	default:
		return ErrUnsupported
	}
}

// TPMT_SYM_DEF(_OBJECT) (Part 2, Section 11.1.6/7): Symmetric cipher
// definition.
// KeyBits and Mode fields are ony set if Algorithm is not TPM_ALG_NULL.
type SymDefObject struct {
	Algorithm U16 // TPM_ALG_*
	KeyBits   U16
	Mode      U16 // TPM_ALG_*
}

var _ = wire(new(SymDefObject))

func (k *SymDefObject) encode(wr io.Writer) error {
	if err := encode(wr, &k.Algorithm); err != nil {
		return err
	}
	switch k.Algorithm {
	case TPM_ALG_NULL:
		return nil
	case TPM_ALG_AES:
		return encode(wr, &k.KeyBits, &k.Mode)
	default:
		return ErrUnsupported
	}
}

func (k *SymDefObject) decode(rd io.Reader) error {
	if err := decode(rd, &k.Algorithm); err != nil {
		return err
	}
	switch k.Algorithm {
	case TPM_ALG_NULL:
		k.KeyBits = 0
		k.Mode = TPM_ALG_NULL
		return nil
	case TPM_ALG_AES:
		return decode(rd, &k.KeyBits, &k.Mode)
	default:
		return ErrUnsupported
	}
}

// Instantiates a new cipher.Stream for the symmetric cipher with the given key
// and IV.
func (k *SymDefObject) Cipher(key []byte, defaultAlg U16) (cipher.Block, error) {
	alg := k.Algorithm
	if alg == TPM_ALG_NULL {
		alg = defaultAlg
	}
	switch alg {
	case TPM_ALG_AES:
		return aes.NewCipher(key)
	default:
		return nil, ErrUnsupported
	}
}

// TPMS_PCR_SELECTION (Part 2, Section 10.6.2): A selection of a single bank of
// multiple PCR.
// Selects the "Hash" bank of all PCR in the "Pcr" slice.
type PcrSelect struct {
	Hash U16
	Pcr  []int
}

var _ = wire(new(PcrSelect))

func (p *PcrSelect) encode(wr io.Writer) error {
	// The selection is encoded as a bit field. Bit x is one if PCR x is
	// selected. The bit field minimum size is large enough to hold the largest
	// PCR index. This implementation hardcodes the value.
	bits := make([]byte, NumSupportedPcrs/8)
	if NumSupportedPcrs%8 != 0 {
		bits = append(bits, 0)
	}
	for _, p := range p.Pcr {
		if len(bits) < p/8 {
			bits = append(bits, make([]byte, p/8+1-len(bits))...)
		}
		bits[p/8] |= 1 << uint(p%8)
	}
	var sz = u8(len(bits))
	if err := encode(wr, &p.Hash, &sz); err != nil {
		return err
	}
	if _, err := wr.Write(bits); err != nil {
		return err
	}
	return nil
}

func (p *PcrSelect) decode(rd io.Reader) error {
	var sz u8
	if err := decode(rd, &p.Hash, &sz); err != nil {
		return err
	}
	bits := make([]byte, sz)
	if _, err := io.ReadFull(rd, bits); err != nil {
		return err
	}
	for i := 0; i < len(bits)*8; i++ {
		if bits[i/8]&(1<<uint(i%8)) != 0 {
			p.Pcr = append(p.Pcr, i)
		}
	}
	sort.Ints(p.Pcr)
	return nil
}

// TPML_PCR_SELECTION (Part 2, Section 10.6.1): A list of PCR selections.
// Used to select more than one PCR bank.
type PcrSelection []PcrSelect

var _ = wire(new(PcrSelection))

func (p *PcrSelection) encode(wr io.Writer) error {
	count := U32(len(*p))
	if err := encode(wr, &count); err != nil {
		return err
	}
	for _, sel := range *p {
		if err := encode(wr, &sel); err != nil {
			return err
		}
	}
	sort.Slice(*p, func(i, j int) bool {
		return (*p)[i].Hash < (*p)[j].Hash
	})
	return nil
}

func (p *PcrSelection) decode(rd io.Reader) error {
	var count U32
	if err := decode(rd, &count); err != nil {
		return err
	}
	*p = make(PcrSelection, count)
	for i := range *p {
		if err := decode(rd, &(*p)[i]); err != nil {
			return err
		}
	}
	return nil
}

// TPMT_TK_* (Part 2, Section 10.7): Ticket structures.
//
// A ticket is a symmetric signature of some data. A ticket is used to prove a
// specific TPM operation was performed. Tickets are returned by
// TPM2_CreatePrimary and TPM2_PolicySecret. This implementation supports
// decoding tickets but does not implement the commands to verify them.
type Ticket struct {
	Tag       U16
	Hierarchy U32
	Digest    Buffer
}

var _ = wire(new(Ticket))

func (t *Ticket) encode(wr io.Writer) error {
	return encode(wr, &t.Tag, &t.Hierarchy, &t.Digest)
}

func (t *Ticket) decode(rd io.Reader) error {
	return decode(rd, &t.Tag, &t.Hierarchy, &t.Digest)
}

// TPMS_SENSITIVE_CREATE (Part 2, Section 11.1.15) Sensitive data for object
// creation.
type SensitiveCreate struct {
	UserAuth Buffer // AuthValue of the new object
	Data     Buffer // Object's private key, encrypted using the parent's public key or empty.
}

var _ = wire(new(SensitiveCreate))

func (s *SensitiveCreate) encode(wr io.Writer) error {
	return encode(wr, &s.UserAuth, &s.Data)
}

func (s *SensitiveCreate) decode(rd io.Reader) error {
	return decode(rd, &s.UserAuth, &s.Data)
}

// TPMS_NV_PUBLIC (Part 2, Section 13.5) NV index metadata.
type NvPublic struct {
	Index      U32
	NameAlg    U16    // TPM_ALG_* hash algorithm
	Attributes U32    // TPMA_NV_* bits, not implemented
	AuthPolicy Buffer // Empty or NameAlg sized policy digest
	DataSize   U16    // Size of the NV index data in bytes
}

var _ = wire(new(NvPublic))

func (n *NvPublic) encode(wr io.Writer) error {
	return encode(wr, &n.Index, &n.NameAlg, &n.Attributes, &n.AuthPolicy, &n.DataSize)
}

func (n *NvPublic) decode(rd io.Reader) error {
	return decode(rd, &n.Index, &n.NameAlg, &n.Attributes, &n.AuthPolicy, &n.DataSize)
}

// TPMS_ALG_PROPERTY (Part 2, Section 12.2.4) Algorithm property bitmap.
type AlgProperty struct {
	Alg        U16 // TPM_ALG_*
	Properties U32 // TPMA_ALGORITHM_* bits, not implemented
}

// TPMS_CAPABILITY_DATA (Part 2, Section 10.10.2) TPM2_GetCapability response.
// Depending on the Capability field, one of the slices will be set.
type CapabilityData struct {
	Capability    U32           // TPM_CAP_*
	Handles       []U32         // Capability == TPM_CAP_HANDLES
	Pcrs          PcrSelection  // Capability == TPM_CAP_PCRS
	Algorithms    []AlgProperty // Capability == TPM_CAP_ALGS
	Commands      []U32         // Capability == TPM_CAP_COMMANDS
	TpmProperties map[U32]U32   // Capability == TPM_CAP_TPM_PROPERTIES
	EccCurves     []U16         // Capability == TPM_CAP_ECC_CURVES
}

var _ = wire(new(CapabilityData))

func (t *CapabilityData) encode(wr io.Writer) error {
	tpml := func(l []U32) error {
		var sz = U32(len(l))
		if err := encode(wr, &sz); err != nil {
			return err
		}
		for _, v := range l {
			if err := encode(wr, &v); err != nil {
				return err
			}
		}
		return nil
	}

	if err := encode(wr, &t.Capability); err != nil {
		return err
	}

	switch t.Capability {
	case TPM_CAP_ALGS:
		var sz = U32(len(t.Algorithms))
		if err := encode(wr, &sz); err != nil {
			return err
		}
		for _, v := range t.Algorithms {
			if err := encode(wr, &v.Alg, &v.Properties); err != nil {
				return err
			}
		}
		return nil
	case TPM_CAP_HANDLES:
		return tpml(t.Handles)
	case TPM_CAP_COMMANDS:
		return tpml(t.Commands)
	case TPM_CAP_TPM_PROPERTIES:
		var sz = U32(len(t.TpmProperties))
		if err := encode(wr, &sz); err != nil {
			return err
		}
		for k, v := range t.TpmProperties {
			if err := encode(wr, &k, &v); err != nil {
				return err
			}
		}
	case TPM_CAP_ECC_CURVES:
		var sz = U32(len(t.EccCurves))
		if err := encode(wr, &sz); err != nil {
			return err
		}
		for _, v := range t.EccCurves {
			if err := encode(wr, &v); err != nil {
				return err
			}
		}
		return nil
	case TPM_CAP_PCRS:
		return encode(wr, &t.Pcrs)
	default:
		return ErrUnsupported
	}
	return nil
}

func (t *CapabilityData) decode(rd io.Reader) error {
	tpml := func() ([]U32, error) {
		var sz U32
		if err := decode(rd, &sz); err != nil {
			return nil, err
		}

		v := make([]U32, sz)
		for i := U32(0); i < sz; i++ {
			if err := decode(rd, &v[i]); err != nil {
				return nil, err
			}
		}
		return v, nil
	}

	if err := decode(rd, &t.Capability); err != nil {
		return err
	}

	var err error
	switch t.Capability {
	case TPM_CAP_ALGS:
		var sz U32
		if err := decode(rd, &sz); err != nil {
			return err
		}
		t.Algorithms = make([]AlgProperty, sz)
		for i := U32(0); i < sz; i++ {
			if err := decode(rd, &t.Algorithms[i].Alg, &t.Algorithms[i].Properties); err != nil {
				return err
			}
		}
		return nil
	case TPM_CAP_HANDLES:
		t.Handles, err = tpml()
	case TPM_CAP_COMMANDS:
		t.Commands, err = tpml()
	case TPM_CAP_TPM_PROPERTIES:
		var sz U32
		if err := decode(rd, &sz); err != nil {
			return err
		}
		t.TpmProperties = make(map[U32]U32, sz)
		for i := U32(0); i < sz; i++ {
			var k, v U32
			if err := decode(rd, &k, &v); err != nil {
				return err
			}
			t.TpmProperties[k] = v
		}
		return nil
	case TPM_CAP_PCRS:
		return decode(rd, &t.Pcrs)
	case TPM_CAP_ECC_CURVES:
		var sz U32
		if err := decode(rd, &sz); err != nil {
			return err
		}
		t.EccCurves = make([]U16, sz)
		for i := U32(0); i < sz; i++ {
			if err := decode(rd, &t.EccCurves[i]); err != nil {
				return err
			}
		}
		return nil
	default:
		return ErrUnsupported
	}

	return err
}

// TPMT_HA (Part 2, Section 10.3.2) A digest of a hash algorithm.
type Ha struct {
	Alg    U16
	Digest []byte
}

var _ = wire(&Ha{})

func (b *Ha) decode(rd io.Reader) error {
	if err := decode(rd, &b.Alg); err != nil {
		return err
	}

	switch b.Alg {
	case TPM_ALG_SHA256:
		b.Digest = make([]byte, 32)
	case TPM_ALG_SHA1:
		b.Digest = make([]byte, 20)
	case TPM_ALG_SHA384:
		b.Digest = make([]byte, 48)
	case TPM_ALG_SHA512:
		b.Digest = make([]byte, 64)
	case TPM_ALG_SM3_256:
		b.Digest = make([]byte, 32)
	default:
		return ErrUnsupported
	}
	if _, err := io.ReadFull(rd, b.Digest); err != nil {
		return err
	}

	return nil
}

func (b *Ha) encode(wr io.Writer) error {
	if err := encode(wr, &b.Alg); err != nil {
		return err
	}
	if _, err := wr.Write(b.Digest); err != nil {
		return err
	}

	return nil
}

// TPML_DIGEST_VALUES (Part 2, Section 10.9.6) A list of digests.
type DigestValues []Ha

var _ = wire(new(DigestValues))

func (d *DigestValues) encode(wr io.Writer) error {
	var sz = U32(len(*d))
	if err := encode(wr, &sz); err != nil {
		return err
	}
	for _, ha := range *d {
		if err := encode(wr, &ha); err != nil {
			return err
		}
	}
	return nil
}

func (d *DigestValues) decode(rd io.Reader) error {
	var sz U32
	if err := decode(rd, &sz); err != nil {
		return err
	}
	*d = make(DigestValues, sz)
	for i := range *d {
		if err := decode(rd, &(*d)[i]); err != nil {
			return err
		}
	}
	return nil
}

// TPML_DIGEST (Part 2, Section 10.9.5) A list of digests with implicit hash
// algorithm.
type Digests []Buffer

var _ = wire(new(Digests))

func (d *Digests) encode(wr io.Writer) error {
	var sz = U32(len(*d))
	if err := encode(wr, &sz); err != nil {
		return err
	}
	for _, b := range *d {
		if err := encode(wr, &b); err != nil {
			return err
		}
	}
	return nil
}

func (d *Digests) decode(rd io.Reader) error {
	var sz U32
	if err := decode(rd, &sz); err != nil {
		return err
	}
	*d = make(Digests, sz)
	for i := range *d {
		if err := decode(rd, &(*d)[i]); err != nil {
			return err
		}
	}
	return nil
}

// TPMS_SIGNATURE_ECC (Part 2, Section 11.3.3) A ECDSA signature.
type EccSignature struct {
	Hash U16
	R    Buffer // packed TPMS_ECC_POINT
	S    Buffer // packed TPMS_ECC_POINT
}

var _ = wire(new(EccSignature))

func (e *EccSignature) encode(wr io.Writer) error {
	return encode(wr, &e.Hash, &e.R, &e.S)
}

func (e *EccSignature) decode(rd io.Reader) error {
	return decode(rd, &e.Hash, &e.R, &e.S)
}

// TPMT_SIGNATURE (Part 2, Section 11.3.4) A signature.
// This implementation only supports ECDSA signatures.
type Signature struct {
	Algorithm U16           // TPM_ALG_*
	Ecc       *EccSignature // Algorithm == TPM_ALG_ECDSA
}

var _ = wire(new(Signature))

func (s *Signature) encode(wr io.Writer) error {
	if err := encode(wr, &s.Algorithm); err != nil {
		return err
	}
	if s.Ecc != nil {
		return s.Ecc.encode(wr)
	}
	return nil
}

func (s *Signature) decode(rd io.Reader) error {
	if err := decode(rd, &s.Algorithm); err != nil {
		return err
	}
	switch s.Algorithm {
	case TPM_ALG_ECDSA:
		s.Ecc = new(EccSignature)
		return s.Ecc.decode(rd)
	default:
		return ErrUnsupported
	}
}

func (i *Signature) Pack() ([]byte, error) {
	var buf bytes.Buffer
	if err := i.encode(&buf); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func UnpackSignature(buf []byte) (*Signature, error) {
	var i Signature
	if err := i.decode(bytes.NewReader(buf)); err != nil {
		return nil, err
	}
	return &i, nil
}

// TPMS_CLOCK_INFO (Part 2, Section 10.11.1) A timestamp of the TPM internal
// clock.
type ClockInfo struct {
	Clock        u64 // TPM uptime in milliseconds
	ResetCount   U32 // Number of TPM resets
	RestartCount U32 // Number of TPM restarts
	Safe         u8  // The update was stored in NVRAM before the last shutdown
}

var _ = wire(new(ClockInfo))

func (c *ClockInfo) encode(wr io.Writer) error {
	return encode(wr, &c.Clock, &c.ResetCount, &c.RestartCount, &c.Safe)
}

func (c *ClockInfo) decode(rd io.Reader) error {
	return decode(rd, &c.Clock, &c.ResetCount, &c.RestartCount, &c.Safe)
}

// TPMS_QUOTE_INFO (Part 2, Section 10.12.4) A quote of PCR values.
type QuoteInfo struct {
	Select PcrSelection // Quoted PCR banks
	Digest Buffer       // Hash of the selected PCR values
}

var _ = wire(new(QuoteInfo))

func (q *QuoteInfo) encode(wr io.Writer) error {
	return encode(wr, &q.Select, &q.Digest)
}

func (q *QuoteInfo) decode(rd io.Reader) error {
	return decode(rd, &q.Select, &q.Digest)
}

// TPMT_ATTEST (Part 2, Section 10.12.12) An attestation structure.
// Includes the attested data and Type field plus some metadata.
// This implementation only supports TPM_ST_ATTEST_QUOTE (TPM2_Quote return
// value)
type Attest struct {
	Magic           U32       // TPM_GENERATED_VALUE
	Type            U16       // TPM_ST_ATTEST_*
	Signer          Buffer    // Name of the signing key
	Extra           Buffer    // Extra data send by the requestor
	Clock           ClockInfo // Timestamp of the attestation
	FirmwareVersion u64       // Firmware version of the TPM

	Quote *QuoteInfo // Type == TPM_ST_ATTEST_QUOTE
}

var _ = wire(new(Attest))

func (a *Attest) encode(wr io.Writer) error {
	if err := encode(wr, &a.Magic, &a.Type, &a.Signer, &a.Extra, &a.Clock, &a.FirmwareVersion); err != nil {
		return err
	}
	if a.Quote != nil {
		return a.Quote.encode(wr)
	}
	return nil
}

func (a *Attest) decode(rd io.Reader) error {
	if err := decode(rd, &a.Magic, &a.Type, &a.Signer, &a.Extra, &a.Clock, &a.FirmwareVersion); err != nil {
		return err
	}
	switch a.Type {
	case TPM_ST_ATTEST_QUOTE:
		a.Quote = new(QuoteInfo)
		return a.Quote.decode(rd)
	default:
		return ErrUnsupported
	}
}

func (i *Attest) Pack() ([]byte, error) {
	var buf bytes.Buffer
	if err := i.encode(&buf); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func UnpackAttest(buf []byte) (*Attest, error) {
	var i Attest
	if err := i.decode(bytes.NewReader(buf)); err != nil {
		return nil, err
	}
	return &i, nil
}

// TPMS_ID_OBJECT (Part 2, Section 12.4.2) An encrypted credential.
// Generated by (*Public).MakeCredential, consumed by ActivateCredential.
type IdObject struct {
	IntegrityHmac Buffer
	EncIdentity   raw
}

var _ = wire(new(IdObject))

func (i *IdObject) encode(wr io.Writer) error {
	return encode(wr, &i.IntegrityHmac, &i.EncIdentity)
}

func (i *IdObject) decode(rd io.Reader) error {
	return decode(rd, &i.IntegrityHmac, &i.EncIdentity)
}

func (i *IdObject) Pack() ([]byte, error) {
	var buf bytes.Buffer
	var encid = Buffer(i.EncIdentity)
	if err := encode(&buf, &i.IntegrityHmac, &encid); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func UnpackIdObject(buf []byte) (*IdObject, error) {
	var i IdObject
	var encid Buffer
	if err := decode(bytes.NewReader(buf), &i.IntegrityHmac, &encid); err != nil {
		return nil, err
	}
	i.EncIdentity = raw(encid)
	return &i, nil
}
