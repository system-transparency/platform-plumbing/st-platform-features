package tpm

import (
	"bytes"
	"context"
	"io"
	"reflect"
	"testing"
	"testing/quick"

	test "system-transparency.org/ppg/stvmm/internal"
)

type fixedSizeBuffer struct {
	data *bytes.Buffer
}

func (b *fixedSizeBuffer) Read(p []byte) (n int, err error) {
	return b.data.Read(p)
}

func (b *fixedSizeBuffer) Write(p []byte) (n int, err error) {
	avail := b.data.Cap() - b.data.Len()
	if avail < len(p) {
		return 0, io.ErrShortBuffer
	}
	return b.data.Write(p)
}

func TestBufferCodec(t *testing.T) {
	f := func(x []byte) bool {
		var wr bytes.Buffer

		buf1 := Buffer(x)
		err := (&buf1).encode(&wr)
		if err != nil {
			return false
		}
		if wr.Len() != 2+len(x) {
			t.Logf("wr.Len() = %d, want %d", wr.Len(), 2+len(x))
			return false
		}
		if !bytes.Equal(wr.Bytes()[2:], x) {
			t.Logf("wr.Bytes()[2:] = %v, want %v", wr.Bytes()[2:], x)
			return false
		}

		rd := bytes.NewBuffer(wr.Bytes())
		buf2 := Buffer{}
		err = (&buf2).decode(rd)
		if err != nil {
			return false
		}

		if !bytes.Equal(buf1, buf2) {
			t.Logf("buf1 = %v, buf2 = %v", buf1, buf2)
			return false
		}
		if !bytes.Equal(buf1, x) {
			t.Logf("buf1 = %v, x = %v", buf1, x)
			return false
		}
		return true
	}
	test.NoErr(t, quick.Check(f, nil))
}

func TestEncodeBufferError(t *testing.T) {
	var b bytes.Buffer
	// too large Buffer
	buf1 := make(Buffer, 65536)
	err := (&buf1).encode(&b)
	test.WantErr(t, err)

	// target Buffer too small for size field
	bb := fixedSizeBuffer{data: bytes.NewBuffer(make([]byte, 0, 1))}
	buf2 := Buffer{0}
	err = (&buf2).encode(&bb)
	test.WantErr(t, err)

	// target Buffer too small for data
	bb = fixedSizeBuffer{data: bytes.NewBuffer(make([]byte, 0, 2))}
	err = (&buf2).encode(&bb)
	test.WantErr(t, err)
}

func TestDecodeBufferError(t *testing.T) {
	cases := [][]byte{
		{0},
		{0, 1},
		{0, 2, 1},
		append([]byte{4, 1}, make([]byte, 1024)...),
	}

	for _, c := range cases {
		buf := new(Buffer)
		err := buf.decode(bytes.NewBuffer(c))
		test.WantErr(t, err)
	}
}

func TestAuthAreaCodec(t *testing.T) {
	f := func(pass string) bool {
		var b bytes.Buffer
		aa1 := Password(pass)
		err := aa1.request().encode(&b)
		if err != nil {
			t.Logf("err = %v", err)
			return false
		}
		b = *bytes.NewBuffer(b.Bytes()[4:])
		aa2 := new(authArea)
		err = aa2.decode(&b)
		if err != nil {
			t.Logf("err = %v", err)
			return false
		}
		if !bytes.Equal(aa2.AuthAck, []byte(pass)) {
			t.Logf("aa2.AuthAck = %v, want %v", aa2.AuthAck, []byte(pass))
			return false
		}
		return true
	}
	test.NoErr(t, quick.Check(f, nil))
}

func TestEncode(t *testing.T) {
	type pair struct {
		in  wire
		out []byte
	}
	v8 := u8(23)
	v16 := U16(1000)
	v32 := U32(100000)
	aa := authArea{Handle: 0x12345678, Nonce: Buffer{1, 2, 3}, SessionAttributes: 0x42, AuthAck: Buffer{4, 5, 6}}
	buf := Buffer{1, 2, 3}

	cases := []pair{
		{&v16, []byte{0x03, 0xe8}},
		{&v8, []byte{0x17}},
		{&v32, []byte{0x00, 0x01, 0x86, 0xa0}},
		{
			&aa,
			[]byte{0x12, 0x34, 0x56, 0x78, 0x00, 0x03, 0x01, 0x02, 0x03, 0x42, 0x00, 0x03, 0x04, 0x05, 0x06},
		},
		{&buf, []byte{0x00, 0x03, 0x01, 0x02, 0x03}},
	}

	for _, c := range cases {
		t.Run(reflect.TypeOf(c.in).String(), func(t *testing.T) {
			var b bytes.Buffer

			err := encode(&b, c.in)
			test.NoErr(t, err)

			if !bytes.Equal(b.Bytes(), c.out) {
				t.Errorf("got %v, want %v", b.Bytes(), c.out)
			}
		})
	}
}

func TestEncodeError(t *testing.T) {
	var b bytes.Buffer
	err := encode(&b, nil)
	test.WantErr(t, err)
}

func TestDecode(t *testing.T) {
	b := bytes.NewBuffer([]byte{23})
	var v1 u8
	err := decode(b, &v1)
	test.NoErr(t, err)
	test.IsEq(t, u8(23), v1)

	b = bytes.NewBuffer([]byte{3, 232})
	var v2 U16
	err = decode(b, &v2)
	test.NoErr(t, err)
	test.IsEq(t, U16(1000), v2)

	b = bytes.NewBuffer([]byte{0, 1, 134, 160})
	var v3 U32
	err = decode(b, &v3)
	test.NoErr(t, err)
	test.IsEq(t, U32(100000), v3)

	b = bytes.NewBuffer([]byte{0, 3, 1, 2, 3, 0x42, 0, 3, 4, 5, 6})
	var v4 authArea
	err = decode(b, &v4)
	test.NoErr(t, err)
	test.IsEq(t, v4.Nonce, Buffer{1, 2, 3})
	test.IsEq(t, v4.AuthAck, Buffer{4, 5, 6})
	test.IsEq(t, v4.SessionAttributes, u8(0x42))
	test.IsEq(t, v4.Handle, U32(0))

	b = bytes.NewBuffer([]byte{0, 3, 1, 2, 3})
	var v5 Buffer
	err = decode(b, &v5)
	test.NoErr(t, err)
	test.IsEq(t, v5, Buffer{1, 2, 3})

	b = bytes.NewBuffer([]byte{1, 2, 3, 4, 5, 6, 7})
	err = decode(b, &v1, &v2, &v3)
	test.NoErr(t, err)
	test.IsEq(t, v1, u8(1))
	test.IsEq(t, v2, U16(0x0203))
	test.IsEq(t, v3, U32(0x04050607))
}

func TestSend(t *testing.T) {
	var b bytes.Buffer
	pass := "pass"

	// no auth
	reqh := &handleArea{handles: []U32{1, 2}}
	val := U32(0x12345678)
	err := makeRequest(&b, TPM_CC_ActivateCredential, []wire{&val}, reqh)
	test.NoErr(t, err)
	expected := []byte{
		// tag
		0x80, 0x01,
		// command size
		0x00, 0x00, 0x00, 22,
		// command code
		0x00, 0x00, 0x01, 0x47,
		// handle
		0x00, 0x00, 0x00, 0x01,
		0x00, 0x00, 0x00, 0x02,
		// parameter
		0x12, 0x34, 0x56, 0x78,
	}
	test.IsEq(t, b.Bytes(), expected)

	// one auth handle
	b.Reset()
	reqh = &handleArea{handles: []U32{1, 2}, auth: []Auth{Password(pass), nil}}
	err = makeRequest(&b, TPM_CC_ActivateCredential, []wire{&val}, reqh)
	test.NoErr(t, err)
	expected = []byte{
		// tag
		0x80, 0x02,
		// command size
		0x00, 0x00, 0x00, 0x27,
		// command code
		0x00, 0x00, 0x01, 0x47,
		// handle
		0x00, 0x00, 0x00, 0x01,
		0x00, 0x00, 0x00, 0x02,
		// auth area size
		0x00, 0x00, 0x00, 13,
		// session handle
		0x40, 0x00, 0x00, 0x09,
		// nonce size
		0x00, 0x00,
		// session attributes
		0x01,
		// auth size
		0x00, 0x04,
		// auth
		0x70, 0x61, 0x73, 0x73,
		// parameter
		0x12, 0x34, 0x56, 0x78,
	}
	test.IsEq(t, b.Bytes(), expected)

	// two auth handles
	b.Reset()
	reqh = &handleArea{handles: []U32{1, 2}, auth: []Auth{Password(pass), Password(pass)}}
	err = makeRequest(&b, TPM_CC_ActivateCredential, []wire{&val}, reqh)
	test.NoErr(t, err)
	expected = []byte{
		// tag
		0x80, 0x02,
		// command size
		0x00, 0x00, 0x00, 52,
		// command code
		0x00, 0x00, 0x01, 0x47,
		// handle
		0x00, 0x00, 0x00, 0x01,
		0x00, 0x00, 0x00, 0x02,
		// auth area size
		0x00, 0x00, 0x00, 26,
		// 1st session handle
		0x40, 0x00, 0x00, 0x09,
		// 1st nonce size
		0x00, 0x00,
		// 1st session attributes
		0x01,
		// 1st auth size
		0x00, 0x04,
		// 1st auth
		0x70, 0x61, 0x73, 0x73,
		// 2nd session handle
		0x40, 0x00, 0x00, 0x09,
		// 2nd nonce size
		0x00, 0x00,
		// 2nd session attributes
		0x01,
		// 2nd auth size
		0x00, 0x04,
		// 2nd auth
		0x70, 0x61, 0x73, 0x73,
		// parameter
		0x12, 0x34, 0x56, 0x78,
	}
	test.IsEq(t, b.Bytes(), expected)
}

func TestCancel(t *testing.T) {
	url := test.GetTestTpmOrSkip(t)
	if url == "" {
		return
	}
	dev, err := OpenTPM(url)
	if test.NoErr(t, err) {
		defer dev.Close()
	}

	ctx, cancel := context.WithCancel(context.Background())
	cancel()

	err = Startup(ctx, dev, TPM_SU_CLEAR)
	test.IsEq(t, err, context.Canceled)
}
