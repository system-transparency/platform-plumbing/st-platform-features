package tpm

import (
	"crypto"
	"crypto/ecdh"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"testing"

	test "system-transparency.org/ppg/stvmm/internal"
)

func TestHashAlg(t *testing.T) {
	cases := map[string]U16{
		"SHA-1":   TPM_ALG_SHA1,
		"SHA-256": TPM_ALG_SHA256,
		"SHA-384": TPM_ALG_SHA384,
		"SHA-512": TPM_ALG_SHA512,
		"SM3_256": TPM_ALG_SM3_256,
		"AES":     TPM_ALG_AES,
		"NULL":    TPM_ALG_NULL,
	}

	for name, alg := range cases {
		t.Run(name, func(t *testing.T) {
			h, err := HashAlgorithm(alg)
			if name == "NULL" || name == "AES" || name == "SM3_256" {
				test.IsEq(t, err, ErrUnsupported)
			} else {
				test.NoErr(t, err)
				test.IsEq(t, name, h.String())
			}
		})
	}
}

func TestMkCredRsa(t *testing.T) {
	cases := []string{
		"normal",
		"wrong-type",
		"wrong-namealg",
		"wrong-pub",
	}

	for _, c := range cases {
		t.Run(c, func(t *testing.T) {
			priv, err := rsa.GenerateKey(rand.Reader, 2048)
			test.NoErr(t, err)

			pub := Public{
				Type:             TPM_ALG_RSA,
				NameAlg:          TPM_ALG_SHA256,
				ObjectAttributes: 0,
				AuthPolicy:       []byte{},
				RsaParameters: &RsaParms{
					KeyBits:  2048,
					Exponent: U32(priv.E),
					Unique:   priv.N.Bytes(),
				},
			}
			sec := []byte("secret")

			switch c {
			case "wrong-pub":
				priv2, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
				test.NoErr(t, err)

				pub.RsaParameters = nil
				pub.EccParameters = &EccParms{
					Symmetric: SymDefObject{},
					Scheme:    Scheme{},
					CurveID:   TPM_ECC_NIST_P256,
					KDF:       Scheme{},
					Unique: EccPoint{
						X: priv2.X.Bytes(),
						Y: priv2.Y.Bytes(),
					},
				}
				fallthrough
			case "wrong-type":
				pub.Type = TPM_ALG_ECC
			case "wrong-namealg":
				pub.NameAlg = TPM_ALG_SM3_256
			}

			cred, err := mkcredRSA(pub, sec, rand.Reader)
			if c == "normal" {
				test.NoErr(t, err)

				sec2, err := rsa.DecryptOAEP(sha256.New(), rand.Reader, priv, cred, identityLabel)
				test.NoErr(t, err)
				test.IsEq(t, sec, sec2)
			} else {
				test.WantErr(t, err)
			}
		})
	}
}

func TestMkCredEcc(t *testing.T) {
	cases := []string{
		"normal",
		"wrong-type",
		"wrong-namealg",
		"wrong-pub",
	}

	for _, c := range cases {
		t.Run(c, func(t *testing.T) {
			priv, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
			test.NoErr(t, err)

			pub := Public{
				Type:             TPM_ALG_ECC,
				NameAlg:          TPM_ALG_SHA256,
				ObjectAttributes: 0,
				AuthPolicy:       []byte{},
				EccParameters: &EccParms{
					CurveID: TPM_ECC_NIST_P256,
					Unique: EccPoint{
						X: priv.X.Bytes(),
						Y: priv.Y.Bytes(),
					},
				},
			}

			switch c {
			case "wrong-pub":
				priv2, err := rsa.GenerateKey(rand.Reader, 2048)
				test.NoErr(t, err)

				pub.EccParameters = nil
				pub.RsaParameters = &RsaParms{
					KeyBits:  2048,
					Exponent: U32(priv2.E),
					Unique:   priv2.N.Bytes(),
				}
				fallthrough
			case "wrong-type":
				pub.Type = TPM_ALG_RSA
			case "wrong-namealg":
				pub.NameAlg = TPM_ALG_SM3_256
			}

			cred, pnt, err := mkcredECC(pub, rand.Reader)
			if c == "normal" {
				test.NoErr(t, err)

				dhpub, err := ecdh.P256().NewPublicKey(append(append([]byte{4}, pnt.X...), pnt.Y...))
				test.NoErr(t, err)
				dhpriv, err := priv.ECDH()
				test.NoErr(t, err)
				shared, err := dhpriv.ECDH(dhpub)
				test.NoErr(t, err)

				cred2, err := kfde(crypto.SHA256, shared, identityLabel, pnt.X, pub.EccParameters.Unique.X, 256)
				test.NoErr(t, err)
				test.IsEq(t, cred2, []byte(cred))
			} else {
				test.WantErr(t, err)
			}
		})
	}

}

func TestComputeName(t *testing.T) {
	pub := Public{
		Type:             TPM_ALG_ECC,
		NameAlg:          TPM_ALG_SHA256,
		ObjectAttributes: 0,
		AuthPolicy:       make([]byte, 32),
		EccParameters: &EccParms{
			Symmetric: SymDefObject{
				Algorithm: TPM_ALG_NULL,
			},
			Scheme: Scheme{
				Scheme:  TPM_ALG_ECDSA,
				HashAlg: TPM_ALG_SHA256,
			},
			CurveID: TPM_ECC_NIST_P256,
			KDF: Scheme{
				Scheme:  TPM_ALG_KDF1_SP800_56A,
				HashAlg: TPM_ALG_SHA256,
			},
		},
	}
	nam, err := ComputeName(TPM_ALG_SHA256, &pub)
	test.NoErr(t, err)
	pubbuf, err := pub.Pack()
	test.NoErr(t, err)
	sum := sha256.Sum256(pubbuf)
	test.IsEq(t, append([]byte{0, 11}, sum[:]...), []byte(nam))

	_, err = ComputeName(TPM_ALG_NULL, &pub)
	test.WantErr(t, err)

	hier := TPM_RH_ENDORSEMENT
	nam, err = ComputeName(TPM_ALG_SHA256, &hier)
	test.NoErr(t, err)
	test.IsEq(t, []byte{0x40, 0, 0, 0xb}, []byte(nam))

	_, err = ComputeName(TPM_ALG_SHA256, pub)
	test.WantErr(t, err)
}
