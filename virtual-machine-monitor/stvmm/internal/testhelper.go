package internal

import (
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func WantErr(t *testing.T, err error) bool {
	t.Helper()
	if err == nil {
		t.Error("expected error")
		return false
	}
	return true
}

func NoErr(t *testing.T, err error) bool {
	t.Helper()
	if err != nil {
		t.Error(err)
		return false
	}
	return true
}

func IsEq(t *testing.T, actual, expected interface{}) bool {
	t.Helper()
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf("got %v, want %v", actual, expected)
		return false
	}
	return true
}

func GetTestTpmOrSkip(t *testing.T) string {
	t.Helper()
	if url := os.Getenv("TEST_TPM"); url != "" {
		return url
	}
	if _, err := os.Stat(fmt.Sprintf("testdata/%s.tx", t.Name())); err != nil {
		t.Skip("no TPM available, set TEST_TPM to the TPM URL")
		return ""
	}
	return fmt.Sprintf("test:%s", t.Name())
}

func GetTestdataPath(t *testing.T) string {
	t.Helper()

	_, dir, _, _ := runtime.Caller(0)
	for {
		d := filepath.Join(dir, "testdata")
		st, err := os.Stat(d)
		if err == nil && st.IsDir() {
			return d
		}

    if next := filepath.Dir(dir); next == dir {
      break
    } else {
      dir = next
    }
	}

	t.Fatalf("testdata directory not found")
	return ""
}
