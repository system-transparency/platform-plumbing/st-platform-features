stvmm
=====

Runs OS packages in a virtual machine for improved security and management.

**stvmm** runs standard OS packages in a Firecracker microVM. OS packages are
booted directly without running **stboot** inside the microVM. The microVM can
be attested remotely to prove to third parties that the OS packages are running
in an **stvmm** microVM on a **stboot**'d system.

The **stvmm** project consists of two applications: the **stvmmd** daemon and
the **stv** command-line tool. The daemon provides a REST API to start new
microVMs, and the command-line tool is used to interact with the daemon.
Starting the Firecracker process and setting up the network is done by
**systemd** and **systemd-networkd**.

Running
-------

First, build the **stvmm** project:

```sh
go build -o stv ./cmd/stv
go build -o stvmmd ./cmd/stvmmd
cp stvmmd stv <stimages>/config/stvmm-hypervisor/overlay/vanilla/usr/sbin/stvmmd
```

Then build an OS package with **stvmmd** and its **systemd** unit files. The OS
package also needs to include Firecracker, **systemd-networkd**, and **dbus**.
The **stvmm-hypervisor** configuration in the **stimages** repository can be
used as a base for the OS package.

If you want to log into the hypervisor or guest, you need to set a root password:

```sh
# in stimages
echo "hypervisor password" > config/stvmm-hypervisor/pw.root
echo "guest password" > config/stvmm-guest/pw.root
```

In the stimages repository, first build the **stvmm-guest** OS package. The
package will need to be endorsed to prepare it for remote attestation. For
this, ra tool is needed that is in `remote-attestation/ra`.

```sh
# in st-platform-features/remote-attestation/ra
CGO_ENABLED=0 go build -o ra ./cmd/ra
cp ra <stimages>/config/stvmm-guest/overlay/vanilla/usr/sbin/ra
cp ra <stimages>/config/stvmm-hypervisor/overlay/vanilla/usr/sbin/ra
```

Then build the **firecracker** binary. We tested version 1.9.0.

```sh
git clone git@github.com:firecracker-microvm/firecracker.git -b v1.9.0
cd firecracker
tools/devtool build
toolchain="$(uname -m)-unknown-linux-musl"
cp build/cargo_target/${toolchain}/release/firecracker <stimages>/config/stvmm-hypervisor/overlay/vanilla/usr/sbin/firecracker
```

Finally, build the **stvmm-guest** kernel. The kernel configuration for version
6.2 is in this repository.

```sh
git clone git@github.com:torvalds/linux.git -b v6.2
cd linux
cp <st-platform-features>/virtual-machine-monitor/linux-6.2.config .config
KCFLAGS="-Wa,-mx86-used-note=no" make -j$(nproc) bzImage
cp vmlinux <stimages>/config/stvmm-guest/overlay/vanilla/vmlinuz
```

Now, build the **stvmm-guest** OS package:

```sh
# in stimages
make PATH=$PATH:<path to ra binary> \
     CONFIG=config/stvmm-guest \
     STMGR_VERSION=6cdcb68b924367a5b2894d9aa4f2bd93420327c8 \
     all
```

Then, copy the OS package to the hypervisor overlay, built, and boot the
stvmm-hypervisor OS package:

```sh
# in stimages
cp build/stimage.{json,zip,endorsement.bin} \
    config/stvmm-hypervisor/overlay/vanilla/var/lib/stvmm/
make PATH=$PATH:<path to ra binary> \
     CONFIG=config/stvmm-hypervisor \
     STMGR_VERSION=6cdcb68b924367a5b2894d9aa4f2bd93420327c8 \
     clean boot-stvmm
```

Once the OS package with the hypervisor is booted and the **stvmmd** service is
running, the **stv** command-line tool can be used to start a new microVM:

```sh
./stv --host <hypervisor IP>:8000 start <microVM name>
```

This will start a new microVM with the name `<microVM name>`, remotely attest
the new microVM, print its unique four-letter ID, and write the attestation
report to the file `quote.bin`.

You can verify that the microVM is running by checking its **systemd** unit
file:

```sh
# On the hypervisor
systemctl status stvmm-firecracker@<microVM ID>.service
```

The console output of the microVM is logged to the journal:

```sh
# On the hypervisor
journalctl -u stvmm-firecracker@<microVM ID>.service
```

Each microVM has a TUN device with a unique IP address that can be used to
connect to the VM. The name of the TUN device is `stvmm-<microVM ID>`.

To validate the attestation report, a modified version of the `ra` tool in
`remote-attestation/ra` in this repository can be used:

```sh
./ra quote -quote quote.bin \
    -ospkg <stimages>/config/stvmm-hypervisor/overlay/vanilla/var/lib/stvmm/stimage.endorsement.bin \
    -stboot <stimages>/build/stboot.endorsement.bin \
    -hypervisor <stimages>/build/stimage.endorsement.bin \
    -public <stimages>/contrib/endorsement-signing.pub
```

If the validation failes the `ra` tool will print the expected PCR values.
Running `tpm2_pcrread` on the hypervisor will show the actual PCR values. These
can be compared to find the differences. The most commons source of differences
is the platform endorsement. The one checked into the stimages repository is
for qemu with the edk2 shipped with Ubunutu and may not match the one used to
boot the hypervisor. To create a new platform endorsement run the platform
endorsement protocol described in the remote-attestation README. In short, the
following commands can be used:

```sh
# On the hypervisor
ra endorse -tpm /dev/tpmrm0
# scp the localhost.request.bin file out of the virtualized hypervisor.

# On the operator machine
ra endorse -platform localhost.request.bin -key <stimages>/contrib/endorsement-signing.key
# scp the localhost.challedge.bin file back to the hypervisor

# On the hypervisor
ra endorse -challenge localhost.challenge.bin -tpm /dev/tpmrm0
# scp the localhost.endorsement.bin file back to the operator machine

# On the operator machine
cp localhost.endorsement.bin <stimages>/config/stvmm-hypervisor/overlay/vanilla/var/lib/stvmm/platform.endorsement.bin
cp localhost.endorsement.bin <stimages>/config/stvmm-guest/overlay/vanilla/var/lib/stvmm/platform.endorsement.bin
```

After this, rebuild the stvmm-guest, copy the OS package to the hypervisor
overlay and rebuild the hypervisor OS package. The process is explained in
detail above.


Stopping the microVM is done with the `stop` command:

```sh
./stv --host <hypervisor IP>:8000 stop <microVM ID>
```

The microVM can ask the hypervisor for a new attestation report via the
guest-to-host communication channel. The channel is supported by the `ra` tool.
Creating a quote for the microVM and the hypervisor with the `ra quote` command
uses the **stvmm** TPM:

```sh
./ra quote -tpm stvmm -platform /var/lib/stvmm/platform.endorsement.bin -quote quote.bin
```

Verification is done the same way as for the attestation report of the
hypervisor.

Using Cloud Hypervisor
----------------------

Instead of using Firecracker, **stvmm** can use Cloud Hypervisor as the VMM.
The project includes a wrapper shell script that extracts the relevant
configuration from the Firecracker configuration and converts them to Cloud
Hypervisor arguments. The script is called `fc-ch-wrapper.sh` and is located in
the same directory as this README. The wrapper can be tried out by modifying
the `stvmm-firecracker@.service` unit file in
`stimages/config/stvmm-hypervisor/overlay/vanilla/etc/systemd/system/`.

Architecture
------------

The **stvmmd** daemon has two responsibilities. First, generating the
Firecracker and **systemd-networkd** configuration files for new microVMs.
Second, measuring and quoting the microVM for remote attestation. Starting and
monitoring the Firecracker process is done by **systemd**.

A microVM does not run a copy of **stboot** but starts the OS package directly.
Because of this, the hypervisor verifies the OS package before starting the
microVM. The logic is identical to the **stboot** verification logic. In the
prototype, both the OS package and the certificate used to verify the OS
package are part of the hypervisor OS package
(`/var/lib/stvmm/root-certificate.pem`, `/var/lib/stvmm/stimage.json`, and
`/var/lib/stvmm/stimage.zip`).

The guest-to-host communication channel is HTTP REST over **AF_VSOCK**. The
guest can send HTTP messages to CID 3, port 1234. On the host side, the
messages are received by Firecracker and forwarded to a VM-specific Unix
socket. A dedicated **stvmmd** instance is started using **systemd** socket
activation to handle the messages. The **stvmmd** instance is started with the
`receive` command.

The measurements done by the hypervisor are stored in the TPM. This allows the
remote attestation quote to include both the hypervisor and the microVM
measurements. The TPM is used to sign the attestation report so that the
Attestation Identity Key (AIK) used is the same as the one used for signing the
remote attestation quote for the hypervisor. The hypervisor measures the
microVM's OS package descriptor and the archive in the same way **stboot**
does. The difference is that VM measurements are stored in PCR 23, which is
reset to all zeroes each time before the measurements are taken.
