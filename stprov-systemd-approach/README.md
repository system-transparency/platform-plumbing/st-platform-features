A systemd-based Provisioning OS Package
---------------------------------------

The goal is to develop a ospkg that provisions a machine for stboot using
components that are not developed by us to fill in gaps in the capabilities of
stprov. The tasks this ospkg should perform are:
    
    1. Configure the network in the common case.
    2. Allow an administrator to supply their own config in situations where
       (1) fails.
    3. Enroll custom UEFI Secure Boot keys as long as Setup Mode is enabled.
    4. Provision the device for stboot, that means
      a. Generate and persist a Host Configuration and Trust Policy, and
      b. Set stboot as the default boot loader for EFI.
    5. Allow administrators to run custom provisioning steps like formatting
       the disk.
    6. Reboot the system into stboot.
    
Optionally the ospkg should prepare the machine for local attestation. The
custom provisioning steps should be provided as Ansible Playbooks or Roles.
Interaction via the BMC serial or web console should be kept to a minimum. All
network connections to the machine being provisioned should be encrypted and
authenticated.

If the experiment goes well, we may consider extending stboot's UKI user space
with other tools too.

GitLab Milestone: https://git.glasklar.is/groups/system-transparency/platform-plumbing/-/milestones/1#tab-issues
