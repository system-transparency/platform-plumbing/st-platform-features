stprov Network Configuration Wizard 🧙
======================================

A text-based user interface that allows configuring a server's NICs. Supports
DHCP, static IP addresses and a manual mode for directly editiing the network
config. Uses systemd-networkd for the actual configuration.

Improvements
------------

- [ ] Prefill the static network config page with previous values.
- [ ] Auto-complete gateway based on IP and netmask.
- [ ] Don't show placeholder text on b/w terminals.
- [ ] set "PermitRootLogin yes" in sshd_config
- [ ] check for python3 binary
