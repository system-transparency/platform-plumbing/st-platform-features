package main

import (
	"strings"

	"github.com/charmbracelet/bubbles/key"
	"github.com/charmbracelet/bubbles/table"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/huh"
	"github.com/charmbracelet/lipgloss"
)

type devicesModel struct {
	focused     bool
	theme       *huh.Theme
	title       string
	description string

	link  string
	err   error
	model table.Model
}

func newDevicesModel() *devicesModel {
	return &devicesModel{
		focused:     false,
		theme:       huh.ThemeCharm(),
		title:       "",
		description: "",

		link:  "",
		err:   nil,
		model: table.New(table.WithFocused(false)),
	}
}

func (m *devicesModel) Init() tea.Cmd {
	return nil
}

func (m *devicesModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	// no interaction
	return m, nil
}

func (m *devicesModel) View() string {
	var sb strings.Builder

	styles := m.theme.Focused
	if !m.focused {
		styles = m.theme.Blurred
	}

	if m.title != "" {
		sb.WriteString(styles.Title.Render(m.title))
		sb.WriteString("\n")
	}
	if m.description != "" {
		sb.WriteString(styles.Description.Render(m.description))
		sb.WriteString("\n")
	}
	sb.WriteString(m.model.View())

	return sb.String()
}

func (m *devicesModel) Blur() tea.Cmd {
	m.focused = false
	m.model.Blur()
	return nil
}

func (m *devicesModel) Focus() tea.Cmd {
	m.focused = true
	m.model.Focus()
	return nil
}

func (m *devicesModel) Error() error {
	return m.err
}

func (m *devicesModel) Run() error {
	return huh.Run(m)
}

func (m *devicesModel) KeyBinds() []key.Binding {
	return []key.Binding{
		m.model.KeyMap.LineUp,
		m.model.KeyMap.LineDown,
		m.model.KeyMap.PageUp,
		m.model.KeyMap.PageDown,
		m.model.KeyMap.HalfPageUp,
		m.model.KeyMap.HalfPageDown,
		m.model.KeyMap.GotoTop,
		m.model.KeyMap.GotoBottom,
	}
}

func (m *devicesModel) WithTheme(theme *huh.Theme) huh.Field {
	m.theme = theme
	s := table.DefaultStyles()
	s.Header = lipgloss.NewStyle().
		Foreground(m.theme.Focused.Base.GetForeground()).
		BorderStyle(lipgloss.NormalBorder()).
		BorderForeground(m.theme.Focused.Base.GetBorderBottomForeground()).
		BorderBottom(true).
		Bold(true)
	s.Selected = lipgloss.NewStyle()
	s.Cell = m.theme.Focused.UnselectedOption
	m.model.SetStyles(s)
	return m
}

func (m *devicesModel) WithAccessible(b bool) huh.Field {
	return m
}

func (m *devicesModel) WithKeyMap(*huh.KeyMap) huh.Field {
	return m
}

func (m *devicesModel) WithWidth(w int) huh.Field {
	m.model.SetWidth(w)
	return m
}

func (m *devicesModel) WithTitle(s string) huh.Field {
	m.title = s
	return m
}

func (m *devicesModel) WithDescription(s string) huh.Field {
	m.description = s
	return m
}

func (m *devicesModel) GetKey() string {
	return ""
}

func (m *devicesModel) GetValue() any {
	return m.link
}

func (m *devicesModel) Title(s string) *devicesModel {
	m.title = s
	return m
}

func (m *devicesModel) Description(s string) *devicesModel {
	m.description = s
	return m
}

func (m *devicesModel) Links(l []link) *devicesModel {
	colWidths := []int{len("Name"), len("State"), len("IP Address"), len("Hardware Address")}

	for _, l := range l {
		n := l.name
		op := l.operational
		ip := l.ipAddress
		hw := l.hwAddress

		if len(n) > colWidths[0] {
			colWidths[0] = len(n)
		}
		if len(op) > colWidths[1] {
			colWidths[1] = len(op)
		}
		if len(ip) > colWidths[2] {
			colWidths[2] = len(ip)
		}
		if len(hw) > colWidths[3] {
			colWidths[3] = len(hw)
		}
	}

	m.model.SetColumns([]table.Column{
		{Title: "Name", Width: colWidths[0] + 1},
		{Title: "State", Width: colWidths[1] + 1},
		{Title: "IP Address", Width: colWidths[2] + 1},
		{Title: "Hardware Address", Width: colWidths[3] + 1},
	})
	rows := make([]table.Row, len(l))
	for i, l := range l {
		rows[i] = table.Row{l.name, l.operational, l.ipAddress, l.hwAddress}
	}
	m.model.SetRows(rows)
	m.model.SetHeight(len(l))

	return m
}
