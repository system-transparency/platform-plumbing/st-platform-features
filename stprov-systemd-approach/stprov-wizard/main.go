package main

import (
	"log"
	"os"
	"text/template"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/spf13/cobra"
)

var (
	configPath     = "/etc/systemd/network/99-stprov-wizard.network"
	hostConfigPath = "/tmp/stprov-wizard-hostconfig.json"
)

func writeTemplate(path string, t *template.Template, data interface{}) error {
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()

	return t.Execute(f, data)
}

var rootCmd = &cobra.Command{
	Use:   "stprov-wizard",
	Short: "A wizard to configure your network",
	RunE: func(cmd *cobra.Command, args []string) error {
		p := tea.NewProgram(defaultModel())

		go pollOpenSSH(p)
		go pollNetwork(p)
		go randomPasswd(p)

		_, err := p.Run()
		return err
	},
}

func main() {
	rootCmd.PersistentFlags().StringVar(&configPath, "config", configPath, "path to the network config file")

	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
