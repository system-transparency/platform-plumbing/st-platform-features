package main

import (
	"fmt"
	"text/template"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/huh"
)

var (
	dynamicNetworkdTemplate = template.Must(template.New("").Parse(`
[Match]
Name={{.Link}}

[Network]
DHCP=yes
`))
	dynamicHostconfigTemplate = template.Must(template.New("").Parse(`
{
  "network_mode": "dhcp",
  "network_interfaces": [{
    "interface_name": "{{.Link}}",
    "mac_address": "{{.MAC}}"
  }],
  "host_ip": null,
  "gateway": null,
  "dns": [],
  "identity": null,
  "authentication": null,
  "bonding_mode": null,
  "bond_name": null
}
`))
)

type dynamicModel struct {
	linkSelect *huh.Select[link]

	form tea.Model
}

func (m *dynamicModel) Init() tea.Cmd {
	var l link

	m.linkSelect = huh.NewSelect[link]().
		Title("Link to configure").
		Value(&l)

	m.form = huh.NewForm(huh.NewGroup(m.linkSelect))

	return m.form.Init()
}

func (m *dynamicModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			return m, closeMenu
		}
	case msgDevices:
		sel := m.linkSelect.GetKey()
		opts := []huh.Option[link]{}
		for _, l := range msg.links {
			label := fmt.Sprintf("%s (%s)", l.name, l.hwAddress)
			opts = append(opts, huh.NewOption[link](label, l))
		}
		m.linkSelect.Options(opts...)
		if m.linkSelect.Key(sel) == nil && len(msg.links) > 0 {
			m.linkSelect.Value(&msg.links[0])
		}
	}

	var cmd tea.Cmd
	m.form, cmd = m.form.Update(msg)

	if m.form.(*huh.Form).State == huh.StateCompleted {
		link := m.linkSelect.GetValue().(link)
		return m, tea.Batch(cmd, writeDynamicConfig(link.name, link.hwAddress))
	}

	return m, cmd
}

func (m *dynamicModel) View() string {
	return m.form.View()
}

func writeDynamicConfig(link, addr string) tea.Cmd {
	return func() tea.Msg {
		vals := map[string]interface{}{
			"Link": link,
			"MAC":  addr,
		}

		err := writeTemplate(configPath, dynamicNetworkdTemplate, vals)
		if err != nil {
			return msgConfig{err: err}
		}
		err = writeTemplate(hostConfigPath, dynamicHostconfigTemplate, vals)
		if err != nil {
			return msgConfig{err: err}
		}

		return msgConfig{path: configPath, err: err}
	}
}
