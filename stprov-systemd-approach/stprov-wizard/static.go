package main

import (
	"errors"
	"fmt"
	"net"
	"regexp"
	"strings"
	"text/template"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/huh"
)

var (
	isNumber               = regexp.MustCompile(`^\d+$`)
	staticNetworkdTemplate = template.Must(template.New("").Parse(`
[Match]
Name={{.Link}}

[Network]
Address={{.IP}}
Gateway={{.Gateway}}
{{- range .DNS}}
DNS={{.}}
{{- end }}
`))
	staticHostconfigTemplate = template.Must(template.New("").Parse(`
{
  "network_mode": "static",
  "host_ip": "{{.IP}}",
  "gateway": "{{.Gateway}}",
  "dns": [
{{$dns := .DNS}}
{{ range $index, $dns := .DNS}}
  {{if $index}},{{end}}
  "{{$dns}}"
{{end}}
  ],
  "network_interfaces": [{
    "interface_name": "{{.Link}}",
    "mac_address": "{{.MAC}}"
  }],
  "identity": null,
  "authentication": null,
  "bonding_mode": null,
  "bond_name": null
}
`))
)

type staticModel struct {
	linkSelect   *huh.Select[link]
	ipInput      *huh.Input
	maskInput    *huh.Input
	gatewayInput *huh.Input
	dnsInput     *huh.Input

	form tea.Model
}

func isValidIP(s string) error {
	if net.ParseIP(s) != nil {
		return nil
	} else {
		return errors.New("invalid IP address")
	}
}

func isValidMask(s string) error {
	if net.ParseIP(s) != nil || isNumber.MatchString(s) {
		return nil
	} else {
		return errors.New("invalid address mask/prefix")
	}
}

func isValidDNS(s string) error {
	ss := strings.Split(s, ",")
	empty := true
	for _, ip := range ss {
		ip = strings.TrimSpace(ip)
		if ip != "" {
			if net.ParseIP(ip) == nil {
				return errors.New("invalid DNS server address")
			} else {
				empty = false
			}
		}
	}
	if empty {
		return errors.New("no DNS server addresses specified")
	} else {
		return nil
	}
}

func (m *staticModel) Init() tea.Cmd {
	var l link
	var ip, mask, gateway, dns string

	m.linkSelect = huh.NewSelect[link]().
		Title("Link to configure").
		Value(&l)

	m.ipInput = huh.NewInput().
		Title("Link address").
		Value(&ip).
		Placeholder("1.2.3.4 or 2001:01::").
		Description("IPv4 or IPv6 address to assign.").
		Validate(isValidIP)

	m.maskInput = huh.NewInput().
		Title("Link address mask").
		Value(&mask).
		Placeholder("255.255.128.0 or 64").
		Description("IPv4 address mask or IPv6 prefix.").
		Validate(isValidMask)

	m.gatewayInput = huh.NewInput().
		Title("Gateway IPv4 or IPv6 address").
		Value(&gateway).
		Placeholder("1.2.3.4 or 2001:01::").
		Description("IPv4 or IPv6 address of the default gateway.").
		Validate(isValidIP)

	m.dnsInput = huh.NewInput().
		Title("DNS server adddresses").
		Value(&dns).
		Placeholder("1.2.3.4 or 2001:01::").
		Description("Comma separated list of DNS server addresses.").
		Validate(isValidDNS)

	m.form = huh.NewForm(
		huh.NewGroup(m.linkSelect, m.ipInput, m.maskInput, m.gatewayInput, m.dnsInput),
	)

	return m.form.Init()
}

func (m *staticModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			return m, closeMenu
		case "tab":
			return m, m.form.(*huh.Form).NextField()
		case "shift+tab":
			return m, m.form.(*huh.Form).PrevField()
		}
	case msgDevices:
		sel := m.linkSelect.GetKey()
		opts := []huh.Option[link]{}
		for _, l := range msg.links {
			label := fmt.Sprintf("%s (%s)", l.name, l.hwAddress)
			opts = append(opts, huh.NewOption[link](label, l))
		}
		m.linkSelect.Options(opts...)
		if m.linkSelect.Key(sel) == nil && len(msg.links) > 0 {
			m.linkSelect.Value(&msg.links[0])
		}
	}

	var cmd tea.Cmd
	m.form, cmd = m.form.Update(msg)

	if m.form.(*huh.Form).State == huh.StateCompleted {
		link := m.linkSelect.GetValue().(link)
		ip := m.ipInput.GetValue().(string)
		mask := m.maskInput.GetValue().(string)
		gateway := m.gatewayInput.GetValue().(string)
		dns := m.dnsInput.GetValue().(string)

		ipnet := net.IPNet{
			IP:   net.ParseIP(ip),
			Mask: net.IPMask(net.ParseIP(mask)),
		}

		return m, tea.Batch(cmd, writeStaticConfig(link, ipnet.String(), gateway, dns))
	}

	// update prompts
	prompt := func(s any, val func(string) error) string {
		str := s.(string)
		if str == "" {
			return "> "
		} else if val(str) != nil {
			return "✘ "
		} else {
			return "✔ "
		}
	}
	m.ipInput.Prompt(prompt(m.ipInput.GetValue(), isValidIP))
	m.maskInput.Prompt(prompt(m.maskInput.GetValue(), isValidMask))
	m.gatewayInput.Prompt(prompt(m.gatewayInput.GetValue(), isValidIP))
	m.dnsInput.Prompt(prompt(m.dnsInput.GetValue(), isValidDNS))

	return m, cmd
}

func (m *staticModel) View() string {
	return m.form.View()
}

func writeStaticConfig(l link, ip, gateway, dnslist string) tea.Cmd {
	return func() tea.Msg {
		dns := []net.IP{}
		for _, s := range strings.Split(dnslist, ",") {
			s = strings.TrimSpace(s)
			if s != "" {
				dns = append(dns, net.ParseIP(s))
			}
		}

		vals := map[string]interface{}{
			"Link":    l.name,
			"MAC":     l.hwAddress,
			"IP":      ip,
			"Gateway": gateway,
			"DNS":     dns,
		}
		err := writeTemplate(configPath, staticNetworkdTemplate, vals)
		if err != nil {
			return msgConfig{err: err}
		}
		err = writeTemplate(hostConfigPath, staticHostconfigTemplate, vals)
		if err != nil {
			return msgConfig{err: err}
		}

		return msgConfig{err: err, path: configPath}
	}
}
