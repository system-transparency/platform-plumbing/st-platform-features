package main

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"net"
	"os/exec"
	"sort"
	"strings"
	"time"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/godbus/dbus/v5"
)

type msgSSH struct {
	active string
	fprs   []string
}

type msgDevices struct {
	links []link
}

func networkdReload() tea.Msg {
	conn, err := dbus.ConnectSystemBus()
	if err != nil {
		return err
	}
	defer conn.Close()

	manager := conn.Object("org.freedesktop.network1", "/org/freedesktop/network1")
	err = manager.Call("org.freedesktop.network1.Manager.Reload", 0).Err
	return err
}

func openSshActive() (string, error) {
	conn, err := dbus.ConnectSystemBus()
	if err != nil {
		return "", err
	}
	defer conn.Close()

	manager := conn.Object("org.freedesktop.systemd1", "/org/freedesktop/systemd1")

	var obj interface{}
	err = manager.Call("org.freedesktop.systemd1.Manager.GetUnit", 0, "ssh.service").Store(&obj)
	if err != nil {
		return "", err
	}

	unit := conn.Object("org.freedesktop.systemd1", obj.(dbus.ObjectPath))
	active, err := unit.GetProperty("org.freedesktop.systemd1.Unit.ActiveState")
	if err != nil {
		return "", err
	}

	return active.Value().(string), nil
}

func openSshFprs() ([]string, error) {
	cmd := exec.Command("/bin/sh", "-c", "ssh-keyscan localhost 2>/dev/null | ssh-keygen -lf -")
	out, err := cmd.Output()
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(out), "\n")
	fprs := []string{}
	for _, line := range lines {
		if strings.HasPrefix(line, "#") {
			continue
		}

		cols := strings.Split(line, " ")
		if len(cols) < 4 {
			continue
		}

		fprs = append(fprs, cols[1]+" "+cols[3])
	}

	sort.Strings(fprs)
	return fprs, nil
}

func pollOpenSSH(p *tea.Program) {
	for {
		active, err := openSshActive()
		if err != nil {
			p.Send(Fatal(err))
			return
		}

		fprs, err := openSshFprs()
		if err != nil {
			p.Send(Fatal(err))
			return
		}

		p.Send(msgSSH{active: active, fprs: fprs})
		time.Sleep(1 * time.Second)
	}
}

func pollNetwork(p *tea.Program) {
	conn, err := dbus.ConnectSystemBus()
	if err != nil {
		p.Send(Fatal(err))
		return
	}
	defer conn.Close()

	manager := conn.Object("org.freedesktop.network1", "/org/freedesktop/network1")

	for {
		var s [][]interface{}

		err = manager.Call("org.freedesktop.network1.Manager.ListLinks", 0).Store(&s)
		if err != nil {
			p.Send(Fatal(err))
			return
		}

		sort.Slice(s, func(i, j int) bool {
			return s[i][1].(string) < s[j][1].(string)
		})
		idx := sort.Search(len(s), func(i int) bool {
			return s[i][1].(string) >= "lo"
		})
		if idx < len(s) && s[idx][1].(string) == "lo" {
			s = append(s[:idx], s[idx+1:]...)
		}

		links := make([]link, len(s))

		for i, v := range s {
			links[i].name = v[1].(string)
			link := conn.Object("org.freedesktop.network1", v[2].(dbus.ObjectPath))

			op, err := link.GetProperty("org.freedesktop.network1.Link.OperationalState")
			if err != nil {
				p.Send(Fatal(err))
				return
			}
			links[i].operational = op.Value().(string)
		}

		is, err := net.Interfaces()
		if err != nil {
			p.Send(Fatal(err))
			return
		}

	outer:
		for _, v := range is {
			for j, l := range links {
				if v.Name == l.name {
					ad, err := v.Addrs()
					if err == nil && len(ad) > 0 {
						links[j].ipAddress = ad[0].String()
					}
					links[j].hwAddress = v.HardwareAddr.String()
					continue outer
				}
			}
		}

		p.Send(msgDevices{links})
		time.Sleep(1 * time.Second)
	}
}

type msgPasswd struct {
	passwd string
}

func randomPasswd(p *tea.Program) {
	r := make([]byte, 16)
	rand.Read(r)
	passwd := strings.TrimRight(base64.URLEncoding.EncodeToString(r), "=")

	cmd := exec.Command("/usr/sbin/chpasswd")
	cmd.Stdin = strings.NewReader("root:" + passwd + "\n")
	out, err := cmd.CombinedOutput()
	if err != nil {
		err = fmt.Errorf("failed to set root password: %s. %s", err.Error(), out)
		p.Send(Fatal(err))
		return
	}

	p.Send(msgPasswd{passwd: passwd})
}
