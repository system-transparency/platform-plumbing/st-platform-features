package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/huh"
	"github.com/charmbracelet/lipgloss"
)

type link struct {
	name        string
	operational string
	ipAddress   string
	hwAddress   string
}

type msgConfig struct {
	path string
	err  error
}

type msgError struct {
	err   error
	fatal bool
}

type editorFinishedMsg struct {
	err error
}

type model struct {
	err               error
	networkType       int
	networkConfigured bool
	networkWorking    bool
	openSshActive     string
	openSshFprs       []string
	links             []link
	passwd            string

	devicesModel *devicesModel
	staticModel  tea.Model
	dynamicModel tea.Model

	form tea.Model
}

func Fatal(err error) tea.Msg {
	return msgError{err: err, fatal: true}
}

func closeMenu() tea.Msg {
	return msgConfig{}
}

func openEditor() tea.Cmd {
	editor := os.Getenv("EDITOR")
	if editor == "" {
		editor = "nano"
	}
	c := exec.Command(editor, configPath) //nolint:gosec
	return tea.ExecProcess(c, func(err error) tea.Msg {
		return msgConfig{path: configPath, err: err}
	})
}

func defaultModel() model {
	var m model

	m.devicesModel = new(devicesModel).
		Links(nil)
	m.form = huh.NewForm(huh.NewGroup(m.devicesModel))

	return m
}

func (m model) Init() tea.Cmd {
	return m.form.Init()
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case msgError:
		m.err = msg.err
		if msg.fatal {
			fmt.Fprintln(os.Stderr, "Fatal error:", msg.err)
			return m, tea.Quit
		} else {
			return m, nil
		}

	case msgSSH:
		m.openSshActive = msg.active
		m.openSshFprs = msg.fprs
		return m, nil

	case msgPasswd:
		m.passwd = msg.passwd
		return m, nil

	case msgDevices:
		m.devicesModel.Links(msg.links)
		ready := false
		for _, l := range msg.links {
			switch l.operational {
			case "routable", "degraded", "enslaved":
				ready = true
			}
		}
		m.networkConfigured = ready

	case msgConfig:
		m.err = msg.err
		m.dynamicModel = nil
		m.staticModel = nil
		return m, networkdReload
	}

	if m.staticModel != nil {
		var cmd tea.Cmd
		m.staticModel, cmd = m.staticModel.Update(msg)
		return m, cmd
	}

	if m.dynamicModel != nil {
		var cmd tea.Cmd
		m.dynamicModel, cmd = m.dynamicModel.Update(msg)
		return m, cmd
	}

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			return m, tea.Quit

		case "s":
			var cmd1, cmd2 tea.Cmd
			me := new(staticModel)
			m.staticModel = me
			cmd1 = m.staticModel.Init()
			m.staticModel, cmd2 = m.staticModel.Update(msgDevices{links: m.links})
			return m, tea.Batch(cmd1, cmd2)

		case "d":
			var cmd1, cmd2 tea.Cmd
			me := new(dynamicModel)
			m.dynamicModel = me
			cmd1 = m.dynamicModel.Init()
			m.dynamicModel, cmd2 = m.dynamicModel.Update(msgDevices{links: m.links})
			return m, tea.Batch(cmd1, cmd2)

		case "m":
			os.Remove(hostConfigPath)
			return m, openEditor()
		}
	}

	if m.form != nil {
		var cmd tea.Cmd
		m.form, cmd = m.form.Update(msg)
		return m, cmd
	}

	return m, nil
}

func (m model) View() string {
	if m.staticModel != nil {
		return m.staticModel.View()
	}

	if m.dynamicModel != nil {
		return m.dynamicModel.View()
	}

	var sb strings.Builder

	style := huh.ThemeCharm().Focused
	keyStyle := style.Base.Copy().
		Bold(true).
		Width(15)
	listStyle := style.Base.Copy().
		PaddingLeft(4)
	successStyle := lipgloss.NewStyle().
		Foreground(lipgloss.Color("2"))
	errorStyle := lipgloss.NewStyle().
		Foreground(lipgloss.Color("1"))

	sb.WriteString(style.Title.Render("Network"))
	sb.WriteString("\n")
	sb.WriteString(style.Description.Width(80).Render("Press 's' to configure static IP settings, 'd' for DHCP, 'm' for manual configuration"))
	sb.WriteString("\n")

	sb.WriteString(keyStyle.Render("State: "))
	if m.networkConfigured {
		sb.WriteString(successStyle.Render("online"))
	} else {
		sb.WriteString(errorStyle.Render("offline"))
	}
	sb.WriteString("\n")

	if m.form != nil {
		sb.WriteString(keyStyle.Render("Links"))
		sb.WriteString("\n")
		sb.WriteString(listStyle.Render(m.form.View()))
	}
	sb.WriteString("\n")

	sb.WriteString(style.Title.Render("OpenSSH"))
	sb.WriteString("\n")
	sb.WriteString(style.Description.Render("Status of the OpenSSH server."))
	sb.WriteString("\n")

	sb.WriteString(keyStyle.Render("State: "))
	if m.openSshActive == "active" {
		sb.WriteString(successStyle.Render("active"))
	} else {
		sb.WriteString(errorStyle.Render(m.openSshActive))
	}
	sb.WriteString("\n")

	sb.WriteString(keyStyle.Render("Root passwd: "))
	sb.WriteString(m.passwd)
	sb.WriteString("\n")

	sb.WriteString(keyStyle.Render("Fingerprints"))
	sb.WriteString("\n")
	for _, fpr := range m.openSshFprs {
		sb.WriteString(listStyle.Render(fpr))
		sb.WriteString("\n")
	}

	if m.err != nil {
		sb.WriteString(style.ErrorMessage.Render(m.err.Error()))
		sb.WriteString("\n")
	}

	return sb.String()
}
