module system-transparency.org/ppg/stprov-predict

go 1.21.6

require (
	github.com/cavaliergopher/cpio v1.0.1
	github.com/diskfs/go-diskfs v1.4.0
	github.com/saferwall/pe v1.4.8
	github.com/sirupsen/logrus v1.9.0
	github.com/spf13/cobra v1.8.0
)

require (
	github.com/edsrzf/mmap-go v1.1.0 // indirect
	github.com/elliotwutingfeng/asciiset v0.0.0-20230602022725-51bbb787efab // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/pierrec/lz4/v4 v4.1.17 // indirect
	github.com/pkg/xattr v0.4.9 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/ulikunitz/xz v0.5.11 // indirect
	go.mozilla.org/pkcs7 v0.0.0-20210826202110-33d05740a352 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/text v0.7.0 // indirect
	gopkg.in/djherbis/times.v1 v1.3.0 // indirect
)
