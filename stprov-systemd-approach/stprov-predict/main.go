package main

import (
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

type pcrlock struct {
	Records []celRecord `json:"records"`
}

var (
	celPath    string
	stbootPath string
	descPath   string
	ouptutPath string

	rootCmd = &cobra.Command{
		Use:   "stprov-predict",
		Short: "Generate *.pcrlock files for a given stboot UKI",
		RunE: func(cmd *cobra.Command, args []string) error {
			log.SetLevel(log.DebugLevel)
			if !strings.HasSuffix(descPath, ".json") {
				return fmt.Errorf("OS package description file must end in .json")
			}
			return predict(celPath, stbootPath, descPath, ouptutPath)
		},
	}
)

func hashFile(p string, double bool) ([4][]byte, error) {
	fd, err := os.Open(p)
	if err != nil {
		return [4][]byte{}, err
	}
	defer fd.Close()
	h1 := sha1.New()
	h256 := sha256.New()
	h384 := sha512.New384()
	h512 := sha512.New()

	if _, err := io.Copy(h1, fd); err != nil {
		return [4][]byte{}, err
	}
	fd.Seek(0, 0)
	if _, err := io.Copy(h256, fd); err != nil {
		return [4][]byte{}, err
	}
	fd.Seek(0, 0)
	if _, err := io.Copy(h384, fd); err != nil {
		return [4][]byte{}, err
	}
	fd.Seek(0, 0)
	if _, err := io.Copy(h512, fd); err != nil {
		return [4][]byte{}, err
	}

	var s1 [20]byte
	var s256 [32]byte
	var s384 [48]byte
	var s512 [64]byte
	if double {
		s1 = sha1.Sum(h1.Sum(nil))
		s256 = sha256.Sum256(h256.Sum(nil))
		s384 = sha512.Sum384(h384.Sum(nil))
		s512 = sha512.Sum512(h512.Sum(nil))
	} else {
		copy(s1[:], h1.Sum(nil))
		copy(s256[:], h256.Sum(nil))
		copy(s384[:], h384.Sum(nil))
		copy(s512[:], h512.Sum(nil))
	}

	return [4][]byte{s1[:], s256[:], s384[:], s512[:]}, nil
}
func writePreSeparatorPredictions(outpath string, p *predictions) (string, error) {
	lock := pcrlock{
		Records: []celRecord{
			{
				PCR: 4,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: p.UkiAuthentihash[0]},
					{HashAlg: celAlgSHA256, Digest: p.UkiAuthentihash[1]},
					{HashAlg: celAlgSHA384, Digest: p.UkiAuthentihash[2]},
					{HashAlg: celAlgSHA512, Digest: p.UkiAuthentihash[3]},
				},
			},
		},
	}

	fname := fmt.Sprintf("%x.pcrlock", p.File[0:8])
	dname := path.Join(outpath, "251-efi-boot-services-application.pcrlock.d")
	os.MkdirAll(dname, 0755)
	out := path.Join(dname, fname)

	lockfd, err := os.Create(out)
	if err != nil {
		return "", err
	}
	defer lockfd.Close()
	return out, json.NewEncoder(lockfd).Encode(lock)
}

func writePostSeparatorPredictions(outpath string, p *predictions) (string, error) {
	lock := pcrlock{
		Records: []celRecord{
			{
				PCR: 4,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: p.LinuxAuthentihash[0]},
					{HashAlg: celAlgSHA256, Digest: p.LinuxAuthentihash[1]},
					{HashAlg: celAlgSHA384, Digest: p.LinuxAuthentihash[2]},
					{HashAlg: celAlgSHA512, Digest: p.LinuxAuthentihash[3]},
				},
			},
		},
	}

	fname := fmt.Sprintf("%x.pcrlock", p.File[0:8])
	dname := path.Join(outpath, "600-efi-boot-services-application.pcrlock.d")
	os.MkdirAll(dname, 0755)
	out := path.Join(dname, fname)

	lockfd, err := os.Create(out)
	if err != nil {
		return "", err
	}
	defer lockfd.Close()
	return out, json.NewEncoder(lockfd).Encode(lock)
}

func writeKernelCmdlinePredictions(outpath string, p *predictions) (string, error) {
	lock := pcrlock{
		Records: []celRecord{

			{
				PCR: 9,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: p.CmdlineUTF16[0]},
					{HashAlg: celAlgSHA256, Digest: p.CmdlineUTF16[1]},
					{HashAlg: celAlgSHA384, Digest: p.CmdlineUTF16[2]},
					{HashAlg: celAlgSHA512, Digest: p.CmdlineUTF16[3]},
				},
			},
		},
	}

	fname := fmt.Sprintf("%x.pcrlock", p.File[0:8])
	dname := path.Join(outpath, "710-kernel-cmdline.pcrlock.d")
	os.MkdirAll(dname, 0755)
	out := path.Join(dname, fname)

	lockfd, err := os.Create(out)
	if err != nil {
		return "", err
	}
	defer lockfd.Close()
	return out, json.NewEncoder(lockfd).Encode(lock)
}

func writeKernelInitrdPredictions(outpath string, p *predictions) (string, error) {
	lock := pcrlock{
		Records: []celRecord{
			{
				PCR: 9,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: p.Initrd[0]},
					{HashAlg: celAlgSHA256, Digest: p.Initrd[1]},
					{HashAlg: celAlgSHA384, Digest: p.Initrd[2]},
					{HashAlg: celAlgSHA512, Digest: p.Initrd[3]},
				},
			},
		},
	}

	fname := fmt.Sprintf("%x.pcrlock", p.File[0:8])
	dname := path.Join(outpath, "720-kernel-initrd.pcrlock.d")
	os.MkdirAll(dname, 0755)
	out := path.Join(dname, fname)

	lockfd, err := os.Create(out)
	if err != nil {
		return "", err
	}
	defer lockfd.Close()
	return out, json.NewEncoder(lockfd).Encode(lock)
}

func writeStubPredictions(outpath string, p *predictions) (string, error) {
	lock := pcrlock{
		Records: []celRecord{
			{
				PCR: 11,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: linuxName[:][0]},
					{HashAlg: celAlgSHA256, Digest: linuxName[:][1]},
					{HashAlg: celAlgSHA384, Digest: linuxName[:][2]},
					{HashAlg: celAlgSHA512, Digest: linuxName[:][3]},
				},
			}, {
				PCR: 11,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: p.Linux[0]},
					{HashAlg: celAlgSHA256, Digest: p.Linux[1]},
					{HashAlg: celAlgSHA384, Digest: p.Linux[2]},
					{HashAlg: celAlgSHA512, Digest: p.Linux[3]},
				},
			}, {
				PCR: 11,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: osrelName[:][0]},
					{HashAlg: celAlgSHA256, Digest: osrelName[:][1]},
					{HashAlg: celAlgSHA384, Digest: osrelName[:][2]},
					{HashAlg: celAlgSHA512, Digest: osrelName[:][3]},
				},
			}, {
				PCR: 11,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: p.Osrel[0]},
					{HashAlg: celAlgSHA256, Digest: p.Osrel[1]},
					{HashAlg: celAlgSHA384, Digest: p.Osrel[2]},
					{HashAlg: celAlgSHA512, Digest: p.Osrel[3]},
				},
			}, {
				PCR: 11,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: cmdlineName[0]},
					{HashAlg: celAlgSHA256, Digest: cmdlineName[1]},
					{HashAlg: celAlgSHA384, Digest: cmdlineName[2]},
					{HashAlg: celAlgSHA512, Digest: cmdlineName[3]},
				},
			}, {
				PCR: 11,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: p.Cmdline[0]},
					{HashAlg: celAlgSHA256, Digest: p.Cmdline[1]},
					{HashAlg: celAlgSHA384, Digest: p.Cmdline[2]},
					{HashAlg: celAlgSHA512, Digest: p.Cmdline[3]},
				},
			}, {
				PCR: 11,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: initrdName[0]},
					{HashAlg: celAlgSHA256, Digest: initrdName[1]},
					{HashAlg: celAlgSHA384, Digest: initrdName[2]},
					{HashAlg: celAlgSHA512, Digest: initrdName[3]},
				},
			}, {
				PCR: 11,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: p.Initrd[0]},
					{HashAlg: celAlgSHA256, Digest: p.Initrd[1]},
					{HashAlg: celAlgSHA384, Digest: p.Initrd[2]},
					{HashAlg: celAlgSHA512, Digest: p.Initrd[3]},
				},
			}, /* {
				PCR: 11,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: unameName[0]},
					{HashAlg: celAlgSHA256, Digest: unameName[1]},
					{HashAlg: celAlgSHA384, Digest: unameName[2]},
					{HashAlg: celAlgSHA512, Digest: unameName[3]},
				},
			}, {
				PCR: 11,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: p.Uname[0]},
					{HashAlg: celAlgSHA256, Digest: p.Uname[1]},
					{HashAlg: celAlgSHA384, Digest: p.Uname[2]},
					{HashAlg: celAlgSHA512, Digest: p.Uname[3]},
				},
			}, {
				PCR: 11,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: sbatName[0]},
					{HashAlg: celAlgSHA256, Digest: sbatName[1]},
					{HashAlg: celAlgSHA384, Digest: sbatName[2]},
					{HashAlg: celAlgSHA512, Digest: sbatName[3]},
				},
			}, {
				PCR: 11,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: p.Sbat[0]},
					{HashAlg: celAlgSHA256, Digest: p.Sbat[1]},
					{HashAlg: celAlgSHA384, Digest: p.Sbat[2]},
					{HashAlg: celAlgSHA512, Digest: p.Sbat[3]},
				},
			},*/
		},
	}

	fname := fmt.Sprintf("%x.pcrlock", p.File[0:8])
	dname := path.Join(outpath, "351-systemd-stub.pcrlock.d")
	os.MkdirAll(dname, 0755)
	out := path.Join(dname, fname)

	lockfd, err := os.Create(out)
	if err != nil {
		return "", err
	}
	defer lockfd.Close()
	return out, json.NewEncoder(lockfd).Encode(lock)
}

func writeStbootPredictions(outpath, descpath, archpath string, p *predictions) (string, error) {
	ospkgdesc, err := hashFile(descpath, false)
	if err != nil {
		return "", err
	}
	ospkgarch, err := hashFile(archpath, true)
	if err != nil {
		return "", err
	}

	empty1 := sha1.Sum(nil)
	empty256 := sha256.Sum256(nil)
	empty384 := sha512.Sum384(nil)
	empty512 := sha512.Sum512(nil)

	lock := pcrlock{
		Records: []celRecord{
			{
				PCR: 12,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: ospkgarch[0]},
					{HashAlg: celAlgSHA256, Digest: ospkgarch[1]},
					{HashAlg: celAlgSHA384, Digest: ospkgarch[2]},
					{HashAlg: celAlgSHA512, Digest: ospkgarch[3]},
				},
			}, {
				PCR: 12,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: ospkgdesc[0]},
					{HashAlg: celAlgSHA256, Digest: ospkgdesc[1]},
					{HashAlg: celAlgSHA384, Digest: ospkgdesc[2]},
					{HashAlg: celAlgSHA512, Digest: ospkgdesc[3]},
				},
			}, {
				PCR: 13,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: p.SecurityConfig[0]},
					{HashAlg: celAlgSHA256, Digest: p.SecurityConfig[1]},
					{HashAlg: celAlgSHA384, Digest: p.SecurityConfig[2]},
					{HashAlg: celAlgSHA512, Digest: p.SecurityConfig[3]},
				},
			}, {
				PCR: 13,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: p.SigningRoot[0]},
					{HashAlg: celAlgSHA256, Digest: p.SigningRoot[1]},
					{HashAlg: celAlgSHA384, Digest: p.SigningRoot[2]},
					{HashAlg: celAlgSHA512, Digest: p.SigningRoot[3]},
				},
			}, {
				PCR: 13,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: p.HttpsRoots[0]},
					{HashAlg: celAlgSHA256, Digest: p.HttpsRoots[1]},
					{HashAlg: celAlgSHA384, Digest: p.HttpsRoots[2]},
					{HashAlg: celAlgSHA512, Digest: p.HttpsRoots[3]},
				},
			}, {
				PCR: 14,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: empty1[:]},
					{HashAlg: celAlgSHA256, Digest: empty256[:]},
					{HashAlg: celAlgSHA384, Digest: empty384[:]},
					{HashAlg: celAlgSHA512, Digest: empty512[:]},
				},
			}, {
				PCR: 14,
				Digests: []celDigest{
					{HashAlg: celAlgSHA1, Digest: empty1[:]},
					{HashAlg: celAlgSHA256, Digest: empty256[:]},
					{HashAlg: celAlgSHA384, Digest: empty384[:]},
					{HashAlg: celAlgSHA512, Digest: empty512[:]},
				},
			},
		},
	}

	fname := fmt.Sprintf("%x.pcrlock", p.File[0:8])
	dname := path.Join(outpath, "730-stboot.pcrlock.d")
	os.MkdirAll(dname, 0755)
	out := path.Join(dname, fname)

	lockfd, err := os.Create(out)
	if err != nil {
		return "", err
	}
	defer lockfd.Close()
	return out, json.NewEncoder(lockfd).Encode(lock)
}

func writeSystemdUserspaceEventlog(outpath, descpath, archpath string, p *predictions) (string, error) {
	ospkgdesc, err := hashFile(descpath, false)
	if err != nil {
		return "", err
	}
	ospkgarch, err := hashFile(archpath, true)
	if err != nil {
		return "", err
	}

	empty1 := sha1.Sum(nil)
	empty256 := sha256.Sum256(nil)
	//empty384 := sha512.Sum384(nil)
	//empty512 := sha512.Sum512(nil)

	log := []celRecord{
		{
			PCR: 12,
			Digests: []celDigest{
				{HashAlg: celAlgSHA1, Digest: ospkgarch[0]},
				{HashAlg: celAlgSHA256, Digest: ospkgarch[1]},
				//{HashAlg: celAlgSHA384, Digest: ospkgarch[2]},
				//{HashAlg: celAlgSHA512, Digest: ospkgarch[3]},
			},
			ContentType: "systemd",
			Content: &celContent{
				String: "stboot: OS package archive",
			},
		}, {
			PCR: 12,
			Digests: []celDigest{
				{HashAlg: celAlgSHA1, Digest: ospkgdesc[0]},
				{HashAlg: celAlgSHA256, Digest: ospkgdesc[1]},
				//{HashAlg: celAlgSHA384, Digest: ospkgdesc[2]},
				//{HashAlg: celAlgSHA512, Digest: ospkgdesc[3]},
			},
			ContentType: "systemd",
			Content: &celContent{
				String: "stboot: OS package description",
			},
		}, {
			PCR: 13,
			Digests: []celDigest{
				{HashAlg: celAlgSHA1, Digest: p.SecurityConfig[0]},
				{HashAlg: celAlgSHA256, Digest: p.SecurityConfig[1]},
				//{HashAlg: celAlgSHA384, Digest: p.SecurityConfig[2]},
				//{HashAlg: celAlgSHA512, Digest: p.SecurityConfig[3]},
			},
			ContentType: "systemd",
			Content: &celContent{
				String: "stboot: Trust policy",
			},
		}, {
			PCR: 13,
			Digests: []celDigest{
				{HashAlg: celAlgSHA1, Digest: p.SigningRoot[0]},
				{HashAlg: celAlgSHA256, Digest: p.SigningRoot[1]},
				//{HashAlg: celAlgSHA384, Digest: p.SigningRoot[2]},
				//{HashAlg: celAlgSHA512, Digest: p.SigningRoot[3]},
			},
			ContentType: "systemd",
			Content: &celContent{
				String: "stboot: OS package signing root",
			},
		}, {
			PCR: 13,
			Digests: []celDigest{
				{HashAlg: celAlgSHA1, Digest: p.HttpsRoots[0]},
				{HashAlg: celAlgSHA256, Digest: p.HttpsRoots[1]},
				//{HashAlg: celAlgSHA384, Digest: p.HttpsRoots[2]},
				//{HashAlg: celAlgSHA512, Digest: p.HttpsRoots[3]},
			},
			ContentType: "systemd",
			Content: &celContent{
				String: "stboot: HTTPS roots",
			},
		}, {
			PCR: 14,
			Digests: []celDigest{
				{HashAlg: celAlgSHA1, Digest: empty1[:]},
				{HashAlg: celAlgSHA256, Digest: empty256[:]},
				//{HashAlg: celAlgSHA384, Digest: empty384[:]},
				//{HashAlg: celAlgSHA512, Digest: empty512[:]},
			},
			ContentType: "systemd",
			Content: &celContent{
				String: "stboot: UX Identity",
			},
		}, {
			PCR: 14,
			Digests: []celDigest{
				{HashAlg: celAlgSHA1, Digest: empty1[:]},
				{HashAlg: celAlgSHA256, Digest: empty256[:]},
				//{HashAlg: celAlgSHA384, Digest: empty384[:]},
				//{HashAlg: celAlgSHA512, Digest: empty512[:]},
			},
			ContentType: "systemd",
			Content: &celContent{
				String: "stboot: Data channel",
			},
		},
	}

	fname := fmt.Sprintf("%x-tpm2-measure.log", p.File[0:8])
	out := path.Join(outpath, fname)

	logfd, err := os.Create(out)
	if err != nil {
		return "", err
	}
	defer logfd.Close()
	dec := json.NewEncoder(logfd)

	for _, r := range log {
		// ascii record separator
		logfd.WriteString("\x1e")

		err := dec.Encode(r)
		if err != nil {
			return "", err
		}
	}

	return out, nil
}

func predict(celPath, stbootPath, descPath, outputPath string) error {
	celfd, err := os.Open(celPath)
	if err != nil {
		return err
	}
	defer celfd.Close()

	var celRecords []celRecord
	if err := json.NewDecoder(celfd).Decode(&celRecords); err != nil {
		return err
	}

	err = sanityCheck(celRecords)
	if err != nil {
		return err
	}
	log.Info("Event log looks good")

	p, err := predictStboot(stbootPath)
	if err != nil {
		return err
	}

	log.Debugf("stboot Authentihash: %x", p.UkiAuthentihash[1])
	log.Debugf("Linux Authentihash: %x", p.LinuxAuthentihash[1])
	log.Debugf("Linux: %x", p.Linux[1])
	log.Debugf("Initrd: %x", p.Initrd[1])
	log.Debugf("OS release: %x", p.Osrel[1])
	log.Debugf("Command line: %x", p.Cmdline[1])
	log.Debugf("Command line (UTF-16): %x", p.CmdlineUTF16[1])

	prespcrlock, err := writePreSeparatorPredictions(outputPath, p)
	if err != nil {
		return err
	}
	log.Infof("Generated %s", prespcrlock)

	stubpcrlock, err := writeStubPredictions(outputPath, p)
	if err != nil {
		return err
	}
	log.Infof("Generated %s", stubpcrlock)

	postspcrlock, err := writePostSeparatorPredictions(outputPath, p)
	if err != nil {
		return err
	}
	log.Infof("Generated %s", postspcrlock)

	kernelcmdline, err := writeKernelCmdlinePredictions(outputPath, p)
	if err != nil {
		return err
	}
	log.Infof("Generated %s", kernelcmdline)

	kernelinitrd, err := writeKernelInitrdPredictions(outputPath, p)
	if err != nil {
		return err
	}
	log.Infof("Generated %s", kernelinitrd)

	archPath := descPath[:len(descPath)-len(".json")] + ".zip"
	stbootpcrlock, err := writeStbootPredictions(outputPath, descPath, archPath, p)
	if err != nil {
		return err
	}
	log.Infof("Generated %s", stbootpcrlock)

	measurelog, err := writeSystemdUserspaceEventlog(outputPath, descPath, archPath, p)
	if err != nil {
		return err
	}
	log.Infof("Generated %s", measurelog)

	return nil
}

func main() {
	rootCmd.PersistentFlags().StringVarP(&celPath, "cel", "c", "", "Path to CEL file")
	rootCmd.PersistentFlags().StringVarP(&stbootPath, "stboot", "s", "", "Path to stboot UKI file")
	rootCmd.PersistentFlags().StringVarP(&descPath, "ospkg", "p", "", "Path to OS package description file. Must end in .json")
	rootCmd.PersistentFlags().StringVarP(&ouptutPath, "output", "o", "./", "Path to output directory")
	rootCmd.MarkPersistentFlagRequired("cel")
	rootCmd.MarkPersistentFlagRequired("stboot")

	if err := rootCmd.Execute(); err != nil {
		log.Error(err)
		os.Exit(1)
	}
}
