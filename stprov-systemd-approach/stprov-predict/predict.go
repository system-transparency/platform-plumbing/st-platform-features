package main

import (
	"bytes"
	"compress/gzip"
	"crypto"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
	"unicode/utf16"

	"github.com/cavaliergopher/cpio"
	"github.com/diskfs/go-diskfs"
	saferwall "github.com/saferwall/pe"
	log "github.com/sirupsen/logrus"
)

// XXX: copied from system-transparency/stboot/trust
type trustPolicy struct {
	SignatureThreshold int    `json:"ospkg_signature_threshold"`
	FetchMethod        string `json:"ospkg_fetch_method"`
}

var (
	stmgrVfatRegexp = regexp.MustCompile("STMGR.*_VFAT")
)

func decodePEM(pemBytes []byte) ([]*x509.Certificate, error) {
	var certs []*x509.Certificate

	for len(pemBytes) > 0 {
		block, rest := pem.Decode(pemBytes)
		if block == nil {
			break
		}

		if block.Type != "CERTIFICATE" {
			pemBytes = rest

			continue
		}

		cert, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			return nil, err
		}

		certs = append(certs, cert)
		pemBytes = rest
	}

	if len(certs) == 0 {
		return nil, errors.New("no PEM block of type CERTIFICATE found")
	}

	return certs, nil
}
func isUKI(path string) bool {
	fd, err := os.Open(path)
	if err != nil {
		return false
	}
	defer fd.Close()

	var magic [2]byte
	if _, err := io.ReadFull(fd, magic[:]); err != nil {
		return false
	}
	fd.Close()

	return magic[0] == 'M' && magic[1] == 'Z'
}

func loadUkiFromIso(path string) ([]byte, error) {
	isoDisk, err := diskfs.Open(path)
	if err != nil {
		return nil, err
	}

	isoFs, err := isoDisk.GetFilesystem(0)
	if err != nil {
		return nil, err
	}

	fatList, err := isoFs.ReadDir("VFAT")
	if err != nil {
		return nil, err
	}

	fatPath := ""
	for _, f := range fatList {
		if stmgrVfatRegexp.MatchString(f.Name()) {
			fatPath = fmt.Sprintf("VFAT/%s", f.Name())
			break
		}
	}
	if fatPath == "" {
		return nil, fmt.Errorf("%s has no embedded FAT", path)
	}

	fatCopy, err := os.CreateTemp("", "stmgr.*_vfat")
	if err != nil {
		return nil, err
	}

	defer fatCopy.Close()
	defer os.Remove(fatCopy.Name())

	fatSrc, err := isoFs.OpenFile(fatPath, os.O_RDONLY)
	if err != nil {
		return nil, err
	}

	_, err = io.Copy(fatCopy, fatSrc)
	if err != nil {
		return nil, err
	}

	fatSrc.Close()

	fatDisk, err := diskfs.Open(fatCopy.Name())
	if err != nil {
		return nil, err
	}

	fatFs, err := fatDisk.GetFilesystem(0)
	if err != nil {
		return nil, err
	}

	stbootUki, err := fatFs.OpenFile("/EFI/BOOT/BOOTX64.EFI", os.O_RDONLY)
	if err != nil {
		return nil, err
	}
	defer stbootUki.Close()

	return io.ReadAll(stbootUki)
}

func parsePE(buf []byte) (*saferwall.File, error) {
	f, err := saferwall.NewBytes(buf, &saferwall.Options{})
	if err != nil {
		return nil, err
	}
	err = f.Parse()
	if err != nil {
		return nil, err
	}

	return f, nil
}

// sha 1, 256, 384, 512
type predictions struct {
	File              []byte
	UkiAuthentihash   [4][]byte
	LinuxAuthentihash [4][]byte
	Linux             [4][]byte
	Osrel             [4][]byte
	Cmdline           [4][]byte
	CmdlineUTF16      [4][]byte
	Initrd            [4][]byte
	Uname             [4][]byte
	Sbat              [4][]byte
	SigningRoot       [4][]byte
	SecurityConfig    [4][]byte
	HttpsRoots        [4][]byte
}

func predictStboot(path string) (*predictions, error) {
	var stbootuki []byte
	var err error
	var ret predictions

	h := sha256.New()
	fd, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer fd.Close()
	if _, err := io.Copy(h, fd); err != nil {
		return nil, err
	}
	ret.File = h.Sum(nil)

	if isUKI(path) {
		stbootuki, err = os.ReadFile(path)
		if err != nil {
			return nil, err
		}
	} else {
		stbootuki, err = loadUkiFromIso(path)
		if err != nil {
			return nil, err
		}
	}

	stbootpe, err := parsePE(stbootuki)
	if err != nil {
		return nil, err
	}

	ret.UkiAuthentihash = [4][]byte{
		stbootpe.AuthentihashExt(crypto.SHA1.New())[0],
		stbootpe.AuthentihashExt(crypto.SHA256.New())[0],
		stbootpe.AuthentihashExt(crypto.SHA384.New())[0],
		stbootpe.AuthentihashExt(crypto.SHA512.New())[0],
	}

	for _, sec := range stbootpe.Sections {
		start := sec.Header.PointerToRawData
		end := start + sec.Header.VirtualSize
		sum1 := sha1.Sum(stbootuki[start:end])
		sum256 := sha256.Sum256(stbootuki[start:end])
		sum384 := sha512.Sum384(stbootuki[start:end])
		sum512 := sha512.Sum512(stbootuki[start:end])

		switch fmt.Sprintf("%s", sec.Header.Name) {
		case ".linux\x00\x00":
			linuxpe, err := parsePE(stbootuki[start:end])
			if err != nil {
				return nil, err
			}
			ret.LinuxAuthentihash = [4][]byte{
				linuxpe.AuthentihashExt(crypto.SHA1.New())[0],
				linuxpe.AuthentihashExt(crypto.SHA256.New())[0],
				linuxpe.AuthentihashExt(crypto.SHA384.New())[0],
				linuxpe.AuthentihashExt(crypto.SHA512.New())[0],
			}
			ret.Linux = [4][]byte{sum1[:], sum256[:], sum384[:], sum512[:]}
		case ".osrel\x00\x00":
			ret.Osrel = [4][]byte{sum1[:], sum256[:], sum384[:], sum512[:]}
		case ".cmdline":
			ret.Cmdline = [4][]byte{sum1[:], sum256[:], sum384[:], sum512[:]}
			line := strings.TrimSpace(string(stbootuki[start:end]))
			buf := []byte{}
			for _, v := range utf16.Encode([]rune(line)) {
				buf = append(buf, byte(v), byte(v>>8))
			}
			buf = append(buf, 0, 0)
			zum1 := sha1.Sum(buf)
			zum256 := sha256.Sum256(buf)
			zum384 := sha512.Sum384(buf)
			zum512 := sha512.Sum512(buf)
			ret.CmdlineUTF16 = [4][]byte{zum1[:], zum256[:], zum384[:], zum512[:]}
		case ".initrd\x00":
			ret.Initrd = [4][]byte{sum1[:], sum256[:], sum384[:], sum512[:]}
			buf := bytes.NewBuffer(stbootuki[start:end])

			for buf.Len() > 0 {
				rdgz, err := gzip.NewReader(buf)
				if err != nil {
					return nil, err
				}
				rdgz.Multistream(false)
				rd := cpio.NewReader(rdgz)

				var h *cpio.Header
				for h, err = rd.Next(); err == nil; h, err = rd.Next() {
					contents, err := io.ReadAll(rd)
					if err != nil {
						return nil, err
					}

					switch h.Name {
					case "etc/ssl/certs/isrgrootx1.pem":
						certs, err := decodePEM(contents)
						if err != nil {
							return nil, err
						}
						var roots []byte
						for _, cert := range certs {
							roots = append(roots, cert.Raw...)
						}
						sum1 := sha1.Sum(roots)
						sum256 := sha256.Sum256(roots)
						sum384 := sha512.Sum384(roots)
						sum512 := sha512.Sum512(roots)

						ret.HttpsRoots = [4][]byte{sum1[:], sum256[:], sum384[:], sum512[:]}
						log.Debug("found http roots")

					case "etc/trust_policy/ospkg_signing_root.pem":
						blk, _ := pem.Decode(contents)
						cert, err := x509.ParseCertificate(blk.Bytes)
						if err != nil {
							return nil, err
						}
						sum1 := sha1.Sum(cert.Raw)
						sum256 := sha256.Sum256(cert.Raw)
						sum384 := sha512.Sum384(cert.Raw)
						sum512 := sha512.Sum512(cert.Raw)

						ret.SigningRoot = [4][]byte{sum1[:], sum256[:], sum384[:], sum512[:]}
						log.Debugf("found signing root")

					case "etc/trust_policy/trust_policy.json":
						var trustPolicy trustPolicy
						err := json.Unmarshal(contents, &trustPolicy)
						if err != nil {
							return nil, err
						}
						contents, err = json.Marshal(trustPolicy)
						if err != nil {
							return nil, err
						}
						sum1 := sha1.Sum(contents)
						sum256 := sha256.Sum256(contents)
						sum384 := sha512.Sum384(contents)
						sum512 := sha512.Sum512(contents)

						ret.SecurityConfig = [4][]byte{sum1[:], sum256[:], sum384[:], sum512[:]}
						log.Debugf("found security config")
					}
				}
				io.Copy(io.Discard, rdgz)
			}

		case ".uname\x00\x00":
			ret.Uname = [4][]byte{sum1[:], sum256[:], sum384[:], sum512[:]}
		case ".sbat\x00\x00\x00":
			ret.Sbat = [4][]byte{sum1[:], sum256[:], sum384[:], sum512[:]}
		}
	}

	return &ret, nil
}
