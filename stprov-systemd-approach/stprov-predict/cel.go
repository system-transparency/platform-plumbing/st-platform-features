package main

import (
	"bytes"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"fmt"

	log "github.com/sirupsen/logrus"
)

type celAlg int

var (
	celAlgSHA1   celAlg = 0
	celAlgSHA256 celAlg = 1
	celAlgSHA384 celAlg = 2
	celAlgSHA512 celAlg = 3
)

func (a celAlg) String() string {
	switch a {
	case celAlgSHA1:
		return "sha1"
	case celAlgSHA256:
		return "sha256"
	case celAlgSHA384:
		return "sha384"
	case celAlgSHA512:
		return "sha512"
	default:
		return "unknown"
	}
}

func (a celAlg) MarshalJSON() ([]byte, error) {
	return []byte(`"` + a.String() + `"`), nil
}

func (a *celAlg) UnmarshalJSON(b []byte) error {
	switch string(b) {
	case `"sha1"`:
		*a = celAlgSHA1
	case `"sha256"`:
		*a = celAlgSHA256
	case `"sha384"`:
		*a = celAlgSHA384
	case `"sha512"`:
		*a = celAlgSHA512
	default:
		return fmt.Errorf("unknown algorithm: %s", string(b))
	}
	return nil
}

type celHash []byte

func (h celHash) String() string {
	return hex.EncodeToString(h)
}

func (h celHash) MarshalJSON() ([]byte, error) {
	return []byte(`"` + h.String() + `"`), nil
}

func (h *celHash) UnmarshalJSON(b []byte) error {
	var err error
	*h, err = hex.DecodeString(string(b[1 : len(b)-1]))
	return err
}

type celDigest struct {
	HashAlg celAlg  `json:"hashAlg"`
	Digest  celHash `json:"digest"`
}

type celContent struct {
	EventType string `json:"event_type,omitempty"`
	EventData string `json:"event_data,omitempty"`
	String    string `json:"string,omitempty"`
}

type celRecord struct {
	PCR         int         `json:"pcr"`
	Recnum      *int        `json:"recnum,omitempty"`
	Digests     []celDigest `json:"digests"`
	ContentType string      `json:"content_type,omitempty"`
	Content     *celContent `json:"content,omitempty"`
}

func hashAll(data string) [4][]byte {
	h1 := sha1.Sum([]byte(data))
	h256 := sha256.Sum256([]byte(data))
	h384 := sha512.Sum384([]byte(data))
	h512 := sha512.Sum512([]byte(data))

	return [4][]byte{h1[:], h256[:], h384[:], h512[:]}
}

var (
	linuxName   = hashAll(".linux\x00")
	initrdName  = hashAll(".initrd\x00")
	cmdlineName = hashAll(".cmdline\x00")
	osrelName   = hashAll(".osrel\x00")
	unameName   = hashAll(".uname\x00")
	sbatName    = hashAll(".sbat\x00")
)

func matches(ds []celDigest, hash [4][]byte) bool {
	hits := 0
outer:
	for _, h := range hash {
		for _, d := range ds {
			if bytes.Equal(d.Digest, h) {
				hits++
				continue outer
			}
		}
	}
	return len(ds) == hits
}

func sanityCheck(celRecords []celRecord) error {
	state := "fw"

	for _, r := range celRecords {
		if r.Content == nil {
			state = "stboot"
			continue
		}
		switch r.Content.EventType {

		// UEFI boot app executed.
		case "EV_EFI_BOOT_SERVICES_APPLICATION":
			if r.PCR != 4 {
				return fmt.Errorf("unexpected pcr index")
			}
			switch state {
			case "fw":
				state = "uki"
				log.Debugf("found stboot uki authentihash\n")
			case "uki":
				state = "stboot"
				log.Debugf("found stboot linux authentihash\n")
			default:
				return fmt.Errorf("too many applications")
			}

		// ExitBootServices()
		case "EV_EFI_ACTION":
			switch r.Content.EventData {
			case "Exit Boot Services Returned with Success":
				if r.PCR != 5 {
					return fmt.Errorf("unexpected pcr index")
				}
				switch state {
				case "uki":
					state = "stboot"
				case "stboot":
				default:
					return fmt.Errorf("unexpected state")
				}
			}

			// systemd-stub measurements
		case "EV_IPL":
			switch state {
			case "uki":
				if matches(r.Digests, linuxName) {
					state = "uki:linux"
				} else if matches(r.Digests, initrdName) {
					state = "uki:initrd"
				} else if matches(r.Digests, cmdlineName) {
					state = "uki:cmdline"
				} else if matches(r.Digests, osrelName) {
					state = "uki:osrel"
				} else if matches(r.Digests, unameName) {
					state = "uki:uname"
				} else if matches(r.Digests, sbatName) {
					state = "uki:sbat"
				}
			case "uki:linux":
				log.Debugf("found %s\n", state)
				state = "uki"
			case "uki:initrd":
				log.Debugf("found %s\n", state)
				state = "uki"
			case "uki:cmdline":
				log.Debugf("found %s\n", state)
				state = "uki"
			case "uki:osrel":
				log.Debugf("found %s\n", state)
				state = "uki"
			case "uki:uname":
				log.Debugf("found %s\n", state)
				state = "uki"
			case "uki:sbat":
				log.Debugf("found %s\n", state)
				state = "uki"
			}
		}
	}

	if state != "stboot" {
		return fmt.Errorf("missing measurements. final state: %s", state)
	}

	return nil
}
