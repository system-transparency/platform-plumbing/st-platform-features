#!/bin/bash

set -euo pipefail

# Change to the script directory
cd "${0%/*}"

# keys folder not empty
if [[ -n "$(ls -A keys)" ]]; then
    echo "Keys folder not empty, are you sure to replace it? (y/N)"
    read confirm
    if [[ "$confirm" == [yY] ]]; then
        rm -rf keys
    fi
fi

mkdir -p keys

if [[ ! -n "$(ls -A keys)" ]]; then
  # generate new self-signed ed25519 CA certificate
  openssl req -x509 \
    -newkey ed25519 \
    -keyout keys/ospkg-ca.key \
    -out keys/ospkg-ca.crt \
    -days 36500 \
    -nodes -subj "/CN=stboot ospkg CA"

  # generate Secure Boot keys
  openssl req -x509 \
    -newkey rsa:4096 \
    -keyout keys/KEK.key \
    -out keys/KEK.pem \
    -days 36500 \
    -nodes -subj "/CN=stboot KEK"

  openssl req -x509 \
    -newkey rsa:4096 \
    -keyout keys/db.key \
    -out keys/db.pem \
    -days 36500 \
    -nodes -subj "/CN=stboot signing key"

  # generate three ospkg signing certificates, signed by the CA
  for i in {1..3}; do
    openssl req -x509 \
      -newkey ed25519 \
      -keyout keys/key$i.key \
      -CA keys/ospkg-ca.crt \
      -CAkey keys/ospkg-ca.key \
      -out keys/key$i.crt \
      -days 36500 \
      -nodes -subj "/CN=ospkg signing key no.$i"
  done

  echo "New KEK and db keys generated in keys folder."
fi

rm -rf stboot-signed.efi.uki stboot-signed.efi.uki.signed

# ospkg CA certificate to stboot
cpiodir=$(mktemp -d -t cpio-XXXXXXXXXX)
cpiofile=$(mktemp -t cpio-XXXXXXXXXX)
initrd=$(mktemp -t initrd-XXXXXXXXXX)

mkdir -p $cpiodir/etc/trust_policy/
cp keys/ospkg-ca.crt $cpiodir/etc/trust_policy/ospkg_signing_root.pem
(cd $cpiodir && find . | cpio -o -H newc) | gzip > $cpiofile
cat prebuilt/stboot-initramfs.cpio.gz $cpiofile > $initrd

# build stboot EFI application
prebuilt/stmgr.amd64 uki create \
  -cert keys/db.pem \
  -key keys/db.key \
  -format uki \
  -initramfs $initrd \
  -kernel prebuilt/bzImage \
  -cmdline "console=ttyS1,115200n8 console=tty0" \
  -out stboot-signed.efi

rm -rf $cpiodir $cpiofile $initrd stboot-signed.efi.uki

echo "stboot EFI application signed with the generate key: stboot-signed.efi.uki.signed"
echo "The stboot requires two valid signatures on a ospkg to boot it."
