#!/bin/env python3

import os
import sys
import argparse
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--prototype',
                        dest='prototype',
                        help='Prototype host configuration file',
                        default='/tmp/stprov-wizard-hostconfig.json')
    parser.add_argument('-o', '--output',
                        dest='output',
                        help='Output final host configuration file',
                        default='host_configuration.json')
    parser.add_argument('-P', '--pointer',
                        dest='pointer',
                        help='OS package point, i.e. the URL to the OS package that is fetched by stboot',
                        required=True)
    ns = parser.parse_args()
    
    with open(ns.prototype) as f:
        hostcfg = json.load(f)
    hostcfg['ospkg_pointer'] = ns.pointer
    with open(ns.output, 'w') as f:
        json.dump(hostcfg, f)

if __name__ == '__main__':
    main()
