## Setup

1. Generate db keypair
2. Generate ospkg root signing key
3. Generate n ospkg signing keys, certify with root
4. Generate stboot initramfs with root in trust policy
5. Sign stboot UKI with db pair

## Per Server
  
1. Enable setup mode
2. Boot stprov UKI
3. Generate and backup device specific PK and KEK pairs
4. Sign db with KEK
5. Deploy keyset and host config
Reboot
6. (opt) Enable secure boot and deployed mode
Reboot
7. Boot into stboot

## Per ospkg

1. Sign kernel with db key
2. Sign ospkg with n signature keys
