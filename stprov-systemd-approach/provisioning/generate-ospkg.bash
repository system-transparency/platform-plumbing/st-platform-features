#!/bin/bash

set -euo pipefail

# Change to the script directory
cd "${0%/*}"

if [[ ! -n "$(ls -A keys)" ]]; then
    echo "Keys folder is empty. Run generate-keyset.bash first."
    exit 1
fi

# generate-ospkg.bash <kernel> <initramfs> <zip-url> <output>
if [[ $# -lt 4 ]]; then
    echo "Usage: generate-ospkg.bash <kernel> <initramfs> <zip-url> <output>"
    exit 1
fi

kernel=$1
initramfs=$2
zip_url=$3
output=$4

# sign the kernel so stboot can kexec() it
signed_kernel=$(mktemp -d -t kernel-XXXXXXXXXX)
osslsigncode sign \
  -spc keys/db.pem \
  -key keys/db.key \
  -in $kernel \
  -out $signed_kernel/vmlinuz

prebuilt/stmgr.amd64 ospkg create \
  -cmdline "console=ttyS1,115200n8 console=tty0 rw rdinit=/lib/systemd/systemd" \
  -kernel $signed_kernel/vmlinuz \
  -initramfs $initramfs \
  -out $output \
  -url $zip_url

rm -fr $signed_kernel

echo "Generated $output. Still needs to be signed with stmgr.
To sign it, run:
  prebuilt/stmgr.amd64 ospkg sign -cert <CERT> -key <KEY> -ospkg $output"
