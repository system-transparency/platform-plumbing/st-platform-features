# Prerequisites

1) Install Ansible and Git on your system.
2) Clone this repository.
3) Run the following command to install the required roles:
```
ansible-galaxy collection install git+https://github.com/QueraTeam/ansible-github.git
```
4) Install the OS packages required by the roles:
```
apt install python3-jmespath
```

# Execution

1) Set the root password in the inventory.yml file.
2) Run the following command to execute the playbook in the QEMU virtual machine:
```
ansible-playbook -i inventory.yml qemu.yml
``` 

# Demo

Edit the OS package fetch URL in demo.yml and generate-ospkg.bash.

```bash
./generate-secureboot-keyset.bash
./generate-ospkg.bash
./prebuilt/stmgr.amd64 ospkg sign -key ... -cert ... -ospkg test-ospkg.json
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i <SERVER-IP>, -k demo.yml
```
