#!/bin/sh

set -eu

sudo ip l add vm-net type bridge
sudo ip l set vm-net up promisc on
sudo ip tuntap add dev vm-nic mode tap
sudo ip l set vm-nic up promisc on
sudo ip l set vm-nic master vm-net
sudo ip a add 10.42.23.1/24 broadcast 10.42.23.255 dev vm-net
touch ./dhcpd.leases

# Check if udhcpcd exists
if ! command -v udhcpd > /dev/null; then
    echo "udhcpd not found, executing busybox version"
    sudo busybox udhcpd ./udhcpd.conf
else
sudo udhcpd ./udhcpd.conf
fi