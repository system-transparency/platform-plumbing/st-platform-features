#!/bin/bash

# Check if Python 3 is installed
if command -v python3 &> /dev/null
then
    echo "Python 3 is installed."
else
    echo "Python 3 is not installed."
fi

# Setup python virtual environment
python3 -m venv python-venv

# Activate the virtual environment
source python-venv/bin/activate

# Install the required python packages
pip install virt-firmware

# Copy OVMF_VARS.fd to the current directory
cp -f "$EDK2_OVMF_PATH/OVMF_VARS.fd" $(pwd)/OVMF_VARS.fd

# Create keys directory
sb_keys_temp_dir=$(mktemp -d)

# Random GUID
uuidgen --random > $sb_keys_temp_dir/GUID.txt

# Create pk key
openssl req -newkey rsa:4096 -nodes -keyout $sb_keys_temp_dir/PK.key -new -x509 -sha256 -days 3650 -subj "/CN=my Platform Key/" -out $sb_keys_temp_dir/PK.crt

# Create kek key
openssl req -newkey rsa:4096 -nodes -keyout $sb_keys_temp_dir/KEK.key -new -x509 -sha256 -days 3650 -subj "/CN=my Key Exchange Key/" -out $sb_keys_temp_dir/KEK.crt

# Create db key
\openssl req -newkey rsa:4096 -nodes -keyout $sb_keys_temp_dir/db.key -new -x509 -sha256 -days 3650 -subj "/CN=my Image Key/" -out $sb_keys_temp_dir/db.crt

# Patch ovmf to allow for secure boot
virt-fw-vars --inplace $(pwd)/OVMF_VARS.fd --sb --set-pk 7fa6a5bd-68b7-4b8a-87e7-365a7c2bbb29 $sb_keys_temp_dir/PK.crt --add-kek 7fa6a5bd-68b7-4b8a-87e7-365a7c2bbb29 $sb_keys_temp_dir/KEK.crt --add-db 7fa6a5bd-68b7-4b8a-87e7-365a7c2bbb29 $sb_keys_temp_dir/db.crt
virt-fw-vars -i $(pwd)/OVMF_VARS.fd --output-json -

# Deactivate the virtual environment
deactivate

# Check for sbsigntools
if command -v sbsign &> /dev/null
then
    echo "sbsign is installed."
else
    echo "sbsign is not installed."
    exit 1
fi

# Sign the EFI binary
sbsign --key $sb_keys_temp_dir/db.key --cert $sb_keys_temp_dir/db.crt --output $QEMU_IMAGE $QEMU_IMAGE