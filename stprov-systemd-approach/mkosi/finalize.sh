#!/bin/bash

set -xe

read -r -d '' SSHD_CONFIG << EOF || true
Port 22
AddressFamily any
ListenAddress 0.0.0.0
ListenAddress ::
Protocol 2
SyslogFacility AUTH
LogLevel INFO
LoginGraceTime 2m
PermitRootLogin yes
StrictModes yes
MaxAuthTries 6
MaxSessions 10
PasswordAuthentication yes
PermitEmptyPasswords no
EOF

# Write startup command into root .bash_login
mkdir -p $DESTDIR/root
echo "clear" >> $DESTDIR/root/.bash_login
echo "echo \"Starting stprov-wizard...\"" >> $DESTDIR/root/.bash_login
echo "stprov-wizard" >> $DESTDIR/root/.bash_login

# Write SSHD config
mkdir -p $DESTDIR/etc/ssh/sshd_config.d
echo "$SSHD_CONFIG" > $DESTDIR/etc/ssh/sshd_config.d/stprov.conf