#!/bin/bash

set -eu

# Change to the script directory
cd "${0%/*}"

# Docker image and container configurations
IMAGE_NAME="debian"
IMAGE_TAG="trixie"
DOCKERFILE_PATH="."  # Path to your Dockerfile
FILE_TO_COPY="/root/image.efi"  # File in the container to copy
LINUX_PACKAGES="linux-image-generic,systemd-boot,systemd,nano,bash,coreutils,systemd-resolved,cryptsetup,iputils-ping,dbus,less,sbsigntool,mokutil,tpm2-tools,python3,ca-certificates,systemd-sysv,/work/src/host/stprov-wizard.deb"
KERNEL_COMMAND_LINE="net.ifnames=0"

if [ $# -eq 0 ]; then
    # Set a default value if the argument is not provided
    TIMEZONE="UTC"
    HOST_DESTINATION_PATH="$(pwd)/stprov.efi"
else
    # Use the provided command-line argument as the variable value
    TIMEZONE="$1"
    HOST_DESTINATION_PATH="$2"
fi

# Stop all running containers that match the specified name and tag
echo "Stopping all running Docker containers matching $IMAGE_NAME:$IMAGE_TAG..."
docker ps --format '{{.ID}}\t{{.Image}}' | grep "$IMAGE_NAME:$IMAGE_TAG" | cut -f1 | xargs -r docker kill

# Check if there was an error stopping the containers
if [ $? -ne 0 ]; then
    echo "There was an error stopping the containers."
    exit 1
fi

# List all images (filter with grep if needed)
echo "Available Docker images:"
docker images

# Confirm with the user
read -p "Do you want to remove images related to '$IMAGE_NAME'? (y/N) " confirm

if [[ "$confirm" == [yY] ]]; then
    # Get IDs of images related to the specified name
    IMAGE_IDS=$(docker images | grep "$IMAGE_NAME" | awk '{print $3}')

    # Remove the images
    if [ ! -z "$IMAGE_IDS" ]; then
        echo "Removing images..."
        docker rmi -f $IMAGE_IDS
    else
        echo "No images found for '$IMAGE_NAME'."
    fi
else
    echo "Image removal canceled."
fi

# Build the Docker image
echo "Building Docker image..."
docker build -t $IMAGE_NAME:$IMAGE_TAG $DOCKERFILE_PATH

# Check if build was successful
if [ $? -ne 0 ]; then
    echo "Docker image build failed, exiting."
    exit 1
fi

# Run the Docker container and get its ID
echo "Running Docker container..."
CONTAINER_ID=$(docker run -dt --privileged $IMAGE_NAME:$IMAGE_TAG)

# Fix overlayfs in docker for mkosi
docker exec -t $CONTAINER_ID mount -t tmpfs tmpfs /var/tmp

# Execute mkosi build command
docker exec -t $CONTAINER_ID mkosi -t uki --architecture x86-64 --autologin --hostname stprov --timezone $TIMEZONE --bootable --bootloader uki --with-network --bios-bootloader none --tools-tree default -p $LINUX_PACKAGES --force --build-sources-ephemeral=true --build-sources=/root:host --output-dir /root --kernel-command-line-extra $KERNEL_COMMAND_LINE --build-script=/root/finalize.sh

# Check if the container is running
if [ -z "$CONTAINER_ID" ]; then
    echo "Failed to run the container, exiting."
    exit 1
fi

echo "Container ID: $CONTAINER_ID"

# Copy the file from the Docker container to the host
echo "Copying file from container to host..."
docker cp "$CONTAINER_ID:$FILE_TO_COPY" "$HOST_DESTINATION_PATH"

# Check if the copy was successful
if [ -f "$HOST_DESTINATION_PATH" ]; then
    echo "File copied successfully to $HOST_DESTINATION_PATH"
else
    echo "Error: File could not be copied."
    exit 1
fi

# Optional: Stop and remove the container after the file is copied
echo "Stopping and removing the container..."
docker stop $CONTAINER_ID
docker rm $CONTAINER_ID

echo "Script completed."
