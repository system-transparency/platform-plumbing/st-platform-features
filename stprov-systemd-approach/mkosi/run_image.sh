#!/bin/bash

set -eu

# Change to the script directory
cd "${0%/*}"

# Define the path to the QEMU binary
QEMU_BIN=$(which qemu-system-x86_64)
SWTPM_BIN=$(which swtpm)
SWTPM_SETUP_BIN=$(which swtpm_setup)

function fresh_ovmf_vars() {
    # Copy the file to the destination
    cp "$(pwd)/prebuilt/OVMF_VARS.fd" "$(pwd)/OVMF_VARS.fd"

    # Check if copy was successful
    if [ $? -eq 0 ]; then
        echo "File copied successfully."
    else
        echo "Error in copying file."
    fi

    # Check if Python 3 is installed
    if command -v python3 &> /dev/null
    then
        echo "Python 3 is installed."
    else
        echo "Python 3 is not installed."
    fi

    # Setup python virtual environment
    python3 -m venv python-venv

    # Activate the virtual environment
    source python-venv/bin/activate

    # Install the required python packages
    pip install virt-firmware

    virt-fw-vars --inplace $(pwd)/OVMF_VARS.fd --set-true SetupMode-8be4df61-93ca-11d2-aa0d-00e098032b8c

    virt-fw-vars --inplace $(pwd)/OVMF_VARS.fd --set-false SecureBoot-8be4df61-93ca-11d2-aa0d-00e098032b8c

    virt-fw-vars -i $(pwd)/OVMF_VARS.fd --output-json -

    # Deactivate the virtual environment
    deactivate
}

function create_harddrive() {
    # If not already present, create a directory to store the hard drive image
    if [ ! -f "$(pwd)/harddrive.img" ]; then
        # Create a 10GB hard drive image
        qemu-img create -f qcow2 $(pwd)/harddrive.img 150M
    fi

    # Check if keyfile exists
    if [ ! -f "$(pwd)/keyfile" ]; then
        # Create a keyfile
        dd if=/dev/random of=$(pwd)/keyfile bs=1 count=512
    fi

    # Check if stprov.efi exists
    if [ ! -f "$(pwd)/stprov.efi" ]; then
        # Copy the file to the destination
        cp "$QEMU_IMAGE" "$(pwd)/stprov.efi"
    fi
}

# Cleanup from previous runs
rm -rf $(pwd)/GUID.txt python-venv swtpm-localca

# Define the path to the disk image (replace with your image file)
if [ $# -eq 0 ]; then
    # Set a default value if the argument is not provided
    CREATE_FRESH_OVMF_VARS=false
    QEMU_IMAGE="$(pwd)/stprov.efi"
else
    # Use the provided command-line argument as the variable value
    CREATE_FRESH_OVMF_VARS="$1"
    QEMU_IMAGE="$2"
fi

if [ "$CREATE_FRESH_OVMF_VARS" = true ] ; then
    fresh_ovmf_vars
fi

# Start the swtpm daemon in the background
tpm_temp_dir=$(mktemp -d)

# Create EK and other basics for swtpm to work. allowed to fail
mkdir $(pwd)/swtpm-localca
$SWTPM_SETUP_BIN --tpm2 --tpm-state $tpm_temp_dir --create-ek-cert --create-platform-cert --overwrite --config swtpm-setup.conf || true

# Launch swtpm
$SWTPM_BIN socket --tpm2 --tpmstate dir=$tpm_temp_dir --flags not-need-init --ctrl type=unixio,path=/tmp/swtpm.sock --log level=20 -td

# Sleep for a moment to allow swtpm to initialize
sleep 2

# Add any additional QEMU options you need
QEMU_OPTIONS=" \
  -machine q35,smm=on,accel=kvm 
  -global driver=cfi.pflash01,property=secure,value=on \
  -drive if=pflash,format=raw,unit=0,file="$(pwd)/prebuilt/OVMF_CODE.fd",readonly=on \
  -drive if=pflash,format=raw,unit=1,file="$(pwd)/OVMF_VARS.fd" \
  -nographic \
  -m 4096 \
  -enable-kvm \
  -net user \
  -netdev tap,id=n1,ifname=vm-nic,script=no,downscript=no \
  -device virtio-net-pci,netdev=n1 \
  -chardev socket,id=chrtpm,path=/tmp/swtpm.sock \
  -tpmdev emulator,id=tpm0,chardev=chrtpm \
  -device tpm-tis,tpmdev=tpm0 
  -drive file=$(pwd)/harddrive.img,format=qcow2 "

# Create a hard drive image
create_harddrive

# Start QEMU with the specified options
./net-up.sh
trap './net-down.sh' EXIT
$QEMU_BIN $QEMU_OPTIONS -kernel $QEMU_IMAGE