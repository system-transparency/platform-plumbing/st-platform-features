# OSPKG Kernel Module Signing

This project is about kernel module signing in a custom keys secure boot environment. Especially for the ST project it is important to load any type of standard Linux distribution inside the OSPKG. Therefore we can't control certificates and pre-compiled artifacts which are embedded into the Linux kernel and initrd images. Nowadays ever Linux distro comes with signed kernel modules and enabled lockdown mode because of security reasons. In order to still be able to load your own kernel modules  as part of the OS image. You will need to sign them in a way it can be loaded by the pre-built/signed kernel. 

## Goals / MVP

* Write consumable documentation for the core team which can help them to understand the problem we want to solve and give them an solution we believe would be the best. 
* Give them visuals and example tools how to achieve that.
* Test the solution on a real system before delivering the solution.

## Challenges

* None, most of the MVP is doable with standard tooling.

## The Solution

![](overview.png)

### Explanation

#### Scenario

OSPKG loading procedure contains `kernel`, `initrd` and `cmline`. Therefore we don't have any type of bootloader included itself into the OSPKG. I expect here that kernel is signed by a third-party and cross-signed with sigsum n of m signature scheme. The kernel itself includes a certificates for kernel driver signature verification. It is able to verify the kernel module signatures without using the MOK feature of the [SHIM](https://github.com/rhboot/shim/blob/main/MokVars.txt) or the [SHIM](https://github.com/rhboot/shim) embedded Linux distro certificate.

The following possible solution can be used as reference for later integration into ST.

#### Considerations

We should consider enrolling an extra key as part of the DB key store. This drops double use of the existing db key for secure boot and allows us to revoke kernel modules without revoking the certificate for the stboot path.

### Possible Solution 1

**Tools:** [sbctl](https://github.com/Foxboron/sbctl), [sign-file (linux kernel)](https://www.kernel.org/doc/html/v4.15/admin-guide/module-signing.html)

0. First of all our target system is running and we have already provisioned UEFI Secure Boot.

1. Now you want to enroll the kernel module key with **sbctl**.

   ```bash
   sbctl import-keys --db-cert kernel-module.pem --db-key kernel-module.key # import kernel cert and key
   sbctl enroll-keys -t -p db -a # append and enroll only db keys
   ```

2. We are signing our kernel module with **sign-file**.

   ```bash
   sign-file sha256 kernel-module.key kernel-module.pem kernel-module.ko signed-kernel-module.ko # signing
   modinfo signed-kernel-module.ko # further information
   ```

3. Done

These steps can be done even after stboot provisioning inside the OSPKG.

### Possible Solution 2

**Tools:** [stmgr](https://git.glasklar.is/system-transparency/core/stmgr) includes [sbctl](https://github.com/Foxboron/sbctl) code

Here we are going to integrate more code inside the stmgr application for signing the kernel module and generating the DB key.

This would ease the process for the end-user but also involves tooling he needs to include into the OSKPG as non-standard distro binary.