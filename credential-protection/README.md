# TPM Sealing for OS package-specific credentials

The project is about protecting OSPKG specific credentials with TPM and encryption. We will provide here an API or tooling for OSPKG's to maintain their credentials at rest securely.

## Goals / MVP

* Confidentiality of credentials at rest

* Encryption is bound to the platform state (executed code & signatures)

* Encryption is bound to the platform TPM and private key/s inside it.

* On updates

  1) UEFI Firmware/Data: New provisioning required

  2. UEFI Secure Boot options/keys: Automatic pre-calculation as part of tooling

  3. stboot bootloader + OSPKG: Automatic pre-calculation as part of tooling

  * stprov: No change required

* Encryption technology/algorithms are sane.

## Challenges

The most complicated thing is to provide pre-calculated values for the end-user to consume when he is doing updates for UEFI Secure Boot / stboot bootloader / OSPKG. Since sealing can involve data access to be lost when something goes wrong. We need to provide a sane and well tested implementation.

## Possible Solutions

![](overview.png)

We have multiple options to provide a feasible solution. Important consideration:

* The **storage** we use to store the credentials.
* The credential **sealing** which protects the volume master key from access when the platform & state is not valid.
* The **confidentiality** of the credential for data at rest protection

### Storage

#### 1. EFI Variables

We can use EFI variables to store our credentials. The maximum size limit here is 1MB. Aside from being available on any modern system without the need of extra storage it is quite interesting option since we involve here UEFI authenticated variables which can be used for write-protection of the credential storage. The data in this case is mutable for the OSPKG but requires to be signed.

#### 2. TPM NVRAM

Similar to UEFI but with less storage, the minimum size available defined in the specification for the TPM 2.0 is few hundreds of bytes. In practice, I have checked some of the TPM 2.0 chips and they have roughly 4KB free usable NV memory. A really nice feature of the TPM is also that the NV memory can be read protected aside from being write protected. The data in this case is mutable for the OSPKG but requires to be signed/or any other authentication provided by TPM.

#### 3. Local Storage

Local storage has no limitation but requires the availability of local storage which can be used. No protection mechanisms are available for this storage type except it's special storage designed for it. The data in this case is mutable for the OSPKG without restrictions.

#### 4. OSPKG / Network payload

Instead of storing the encrypted credential data on the target machine we pull it together with the OSPKG from some remote server. This has the benefit similar to the TPM NVRAM and EFI variables that we are storage independent from the target system. The data is write protected by default since the server just needs to serve it read-only. The data in this case is immutable for the OSPKG, we cannot change the data on the target machine.

### Sealing

#### 1. systemd-pcrlock

systemd-pcrlock is the first complete implementation of a PCR pre-calculation tooling. Even it's complete and concept seems interesting we figured out issue with the current implementation. In order to lock the PCR's used for sealing the tool uses a TPM NVRAM index which is protected by the TPM command `PolicyAuthorizeNV` which isn't implemented on all platforms. Also pre-calculation of PCR-4 (the executed efi binary) seems to be wrong in some cases. Since the tool is quite new it's understandable.

Quote from systemd code how their implementation works:

```c
/* Here's how this all works: after predicting all possible PCR values for next boot (with
 * alternatives) we'll calculate a policy from it as a combination of PolicyPCR + PolicyOR
 * expressions. This is then stored in an NV index. When a component of the boot process is changed a
 * new prediction is made and the NV index updated (which automatically invalidates any older
 * policies).
 *
 * Whenever we want to lock an encrypted object (for example FDE) against this policy, we'll use a
 * PolicyAuthorizeNV expression that pins the NV index in the policy, and permits access to any
 * policies matching the current NV index contents.
 *
 * We grant world-readable read access to the NV index. Write access is controlled by a PIN (which we
 * either generate locally or which the user can provide us with) which can also be used for
 * recovery. This PIN is sealed to the TPM and is locked via PolicyAuthorizeNV to the NV index it
 * protects (i.e. we dogfood 🌭 🐶 hard here). This means in order to update such a policy we need
 * the policy to pass.
 *
 * Information about the used NV Index, the SRK of the TPM, the sealed PIN and the current PCR
 * prediction data are stored in a JSON file in /var/lib/. In order to be able to unlock root disks
 * this data must be also copied to the ESP so that it is available to the initrd. The data is not
 * sensitive, as SRK and NV index are pinned by it, and the prediction data must match the NV index
 * to be useful. */
```

As a side note I can really recommend the tooling from a interface perspective. It uses json for data output and operates with a nice TUI on the command-line. Aside from that it fully integrates with cryptsetup and the TPM software stack.

It is important to mention that the tool was developed for end-users and not for servers at scale. Also the code complexity is quite high because it's simply complex but also written in C.

#### 2. From the scratch

This approach requires reimplementing a minimum of the following features:

* TPM Sealing
* TPM Unsealing
* PCR pre-calculation
  * ST components
  * UEFI Secure Boot
* VMK
  * Creation
  * Recovery & Backup

If we are implementing it from scratch we can adapt it perfectly for our use case but would also involve additional maintenance later on. Existing library doing PCR pre-calculation doesn't exists. There have been some work in one github repo but that was terminated when systemd-pcrlock popped up.

### Confidentiality

#### 1. cryptsetup

cryptsetup is one of the most prominent tooling solutions in the Linux ecosystem for disk encryption. The use of LUKS2 specification for disk encryption have been proofed to resilient to a lot of government attacker and hackers. cryptsetup offers features like TPM support, reencryption, FIDO2, and more. It provides multiple slots for recovery and access. It's one of the most adopted solutions out there for Linux based disk encryption

**Limitation comes in our use case with the disk size**. The minimum disk we can create needs to be more than 10MB, for integrity protected disks it's doubled size because of meta information.

cryptsetup provides through the linux-kernel device mapper / crypto implementation access as regular mounted block device. This comes handy for a lot of people but doesn't offer any on-demand proof if the systemd is still secure in a way that the executed code changed (Linux IMA + TPM). We would need to remount the device every time for file access which is possible but didn't mean to be used like this.

Also to mention is that cryptsetup supports all algorithms from the Linux kernel this includes salsa

#### 2. systemd-creds

systemd-creds is a tool which encrypts credentials per file base. It uses AES256-GCM to encrypt credentials with FIDO2 or TPM support. At the moment it lacks of the ability to use pre-calculated PCR's like pcrlock integration. It's a simple tool for credential management.

#### 3. custom encryption tooling

Here we are going to re-invent the wheel and introduce non-standard tooling. This can come again mentioned with benefits adopting it to our use case and also defining a custom API access we can control.

## Recommendation

After careful consideration, I recommend using the combination:

**Storage:** OSPKG / Network payload

**Sealing:** systemd-pcrlock

**Confidentiality:** cryptsetup

### Pros

* Flexible and machine independent storage. Works with modern deployment architectures. Includes write protection enforced by an external entity. Local storage option still exists.
* Thanks to systemd we are part of the community, don't need to maintain our own tooling. Also using standard system tools offers easier adoption through operators.
* Disk encryption is rock solid and well accepted world-wide by operators. We don't need to re-invent the wheel. Also the security is well proven, can also be combined with local storage.

### Cons

* No multiple machine storage and perfect forward secrecy out-of-the-box.
* We need to deal with the systemd community to get our modifications upstream.
* Size constrains, since we combine it here with network ospkg payload it can work out easily.
* We rely on existing Linux tooling with a bigger code base (big TCB)