stboot Platform Plumbing
========================

This repository contains the various projects the platform plumbing team
engaged in.

### Remote Attestation

Prototype remote attestation for st. Implements minimal viable remote
attestation for stboot in pure Go without any dependencies. The project comes
with extensive unit tests, a [demo
application](remote-attestation/ra/README.md) and a [protocol
documentation](remote-attestation/ra/documentation/protocol.md).

* Presented at the May'24 meetup: [notes](https://git.glasklar.is/system-transparency/platform-plumbing/pp-private/-/blob/main/may-2024-meetup/2024-05-28-st-remote-attestation-library.txt), [slides](https://git.glasklar.is/system-transparency/platform-plumbing/pp-private/-/blob/main/may-2024-meetup/2024-05-28-st-remote-attestation-library.pdf).
* Code: [remote-attestation/ra](remote-attestation/ra)
* Ask: Kai

### Credential Protection with TPM 2.0

Document evaluating possible ways of using the system's TPM to protect
credentials in a way that they're only accessible to the correct stboot/OS
package combination. No implementation is included, we need to decide which
route to go first.


* Document: [credential-protection](credential-protection/README.md)
* Presentation about credential protection approaches at the Dec'23 meetup: [slides](https://git.glasklar.is/system-transparency/platform-plumbing/pp-private/-/blob/main/dec-2023-meetup/2023-12-12-stauth-v2.pdf?ref_type=heads)
* Ask: Philpp

### Prototype provisioning image using sd-networkd

Prototype provisioning image that uses systemd-networkd and a simple ncurses
TUI to simplify setting up the initial host configuration via BMC/SoL. Contains
the image build system and the TUI application.

* Presented at the Feb'24 meetup: [notes](https://git.glasklar.is/system-transparency/platform-plumbing/pp-private/-/blob/main/feb-2024-meetup/2024-02-13-provisioning-sb-server-with-stprov.txt?ref_type=heads).
* Demo: [stprov-systemd-approach/provisioning](stprov-systemd-approach/provisioning)
* Ask: Kai
 
### Prototype build system for a systemd-enabled stboot

Experiment trying to replace as much of stboot as possible with existing
applications like systemd and kexec tools. Considerable effort was spent to
make the resulting image as small as possible.

* Presented at the May'24 meetup: [notes](https://git.glasklar.is/system-transparency/platform-plumbing/pp-private/-/blob/main/may-2024-meetup/2024-05-30-stboot-systemd-prototype-v2.txt), [slides](https://git.glasklar.is/system-transparency/platform-plumbing/pp-private/-/blob/main/may-2024-meetup/2024-05-30-stboot-systemd-prototype-v2.pdf).
* Code: [prototype-systemd-stboot](prototype-systemd-stboot)
* Ask: Philipp

### Fallback mechanism for stboot

Proposal for a fallback mechanism for stboot that allows to reenter the
provisioning mode in case of a failed boot. Includes an attacker model and a
proposal for a solution. No implementation is included.

* Document: [prov-mode-proposal](prov-mode-proposal.md)
* Ask: Kai

### Evaluate two possible future OS package formats

Evaluation of two possible routes for the future OS package format: UKI and a
new, custom format. Motivated by the observation that UKIs may be too complex
for our use case.

* Presented at the Feb'24 meetup: [notes](https://git.glasklar.is/system-transparency/platform-plumbing/pp-private/-/blob/main/feb-2024-meetup/2024-02-06-future-of-ospkg.txt?ref_type=heads)
* Goals: [ospkg-format-evaluation](ospkg-format-evaluation/README.md)
* Overview: [ospkg](ospkg-format-evaluation/ospkg.md)
* UKI evaluation: [uki-format](ospkg-format-evaluation/uki-format.md)
* Custom format evaluation: [custom-format](ospkg-format-evaluation/custom-format.md)
* Ask: Jens

### Linux kernel module signing for UEFI Secure Boot

Practical tutorial how to sign Linux kernel modules for UEFI Secure Boot
enabled systems. Contains cli snippets and a visual overview of the process. Is
required because st recommends custom Secure Boot keys.

* Presented at the Apr'24 meetup: [notes](https://git.glasklar.is/system-transparency/platform-plumbing/pp-private/-/blob/main/apr-2024-meetup/2024-04-10-philips-plumbing.txt), [slides](https://git.glasklar.is/system-transparency/platform-plumbing/pp-private/-/blob/main/apr-2024-meetup/2024-04-10-philipps-plumbing-kernel-module-signing.pdf)
* Document: [kernel-module-signing](kernel-module-signing)
* Ask: Philipp

### Running st on coreboot'd hardware

Document how to run st on hardware booted with coreboot. Contains a
step-by-step guide and build configuration on how to build st for a qemu and
SiFive HiFive Unmatched board.

* Document: [coreboot-support](coreboot-support/README.md)
* Qemu: [coreboot-support/qemu-x86](coreboot-support/qemu-x86/guide.md)
* SiFive HiFive Unmatched: [coreboot-support/riscv-unmached](coreboot-support/riscv-unmached/guide.md)
* Ask: Jens

### Running OS packages in virtual machines

Prototype of stvmm that allows to run OS packages in virtual machines that
support remote attestation. Includes a daemon that provides a REST API to start
Firecracker microVMs from OS packages.

* Document: [virtual-machine-monitor](virtual-machine-monitor/README.md)
* Presented at the Nov'24 meetup: [slides](https://git.glasklar.is/system-transparency/platform-plumbing/pp-private/-/blob/main/nov-2024-meetup/2024-11-04-meetup-An_introduction_into_stvmm.pdf)
* Ask: Philipp, Kai
