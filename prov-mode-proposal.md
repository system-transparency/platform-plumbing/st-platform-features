Fallback To Provisioning Mode
=============================

This proposal describes a mechanism for a provisioned stboot-enabled server to
fall back to provisioning mode.

## Problem

A provisioned server may need to be reprovisioned if its provisioning data like
the OS package URL are wrong or outdated. Under normal operation, deleting the
provisioning data would cause stboot to reenter provisioning mode, but because
System Transparency recommends custom Secure Boot keys, running another
operating system to delete the provisioning data can be difficult.

## Attacker Model

In the absence of remote attestation, an attacker which can make stboot enter
provisioning mode can force the machine to run any OS package that passes the
stboot Trust Policy checks. The Trust Policy itself is protected by Secure
Boot, which can only be circumvented by an attacker with either

1. physical access to the machine and it's UEFI flash memory, or
2. access to the UEFI menu either by physical access or via a remote management
   interface like BMC.

Because System Transparency does not recommend an UEFI menu password, an
attacker who's able to trigger keystrokes is not in the attacker model.

## Proposal

After starting but before the host configuration is loaded, stboot waits 5
seconds for the user to press a key. If that happends stboot follows the same
code path as if not host configuration was found.

The provisioning OS package may need to be modified to handle an existing host
configuration by deleting or overwriting it.

The scheme could be improved by adding a counter in a EFI variable that is
incremented every time the stboot starts and decremented before handing off
control to a verified and loaded OS package (immediatly before the call to
kexec()). stboot would only display the prompt if the counter is above some
threshold (e.g. 3). Keep in mind that the key press still needs to be required,
otherwise the attacker who has control over the network could trigger the
fallback by blocking the OS package download.

## Discussion

The provisioning OS package is included in the stboot image and thus protected
by Secure Boot. Triggering the key press can only be done by someone with
physical access to the machine or access to remote management interfaces like
BMC.

This proposal assumes no remote attestation is being done. If remote
attestation is added, the attacker model can be relaxed because now a change to
the OS package booted on a machine can be detected.
