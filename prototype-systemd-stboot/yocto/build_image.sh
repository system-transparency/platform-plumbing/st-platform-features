#!/bin/bash
SCRIPT_DIR=$(dirname $(readlink -f $0))
POKY_DIR="$SCRIPT_DIR/poky"
MACHINE_TARGET="qemux86-64"

# Change to the poky directory
cd $POKY_DIR

# Initialize the build environment
source $(pwd)/oe-init-build-env

# Copy local.conf yocto configuration file
cp $SCRIPT_DIR/config/local.conf $SCRIPT_DIR/poky/build/conf/
echo -e "\nMACHINE = \"${MACHINE_TARGET}\"" >> $SCRIPT_DIR/poky/build/conf/local.conf

# Add the stboot layer to the bblayers.conf file
bitbake-layers add-layer ../meta-stboot

# Build the image
bitbake stboot-image

# List installed packages
bitbake -g stboot-image && cat pn-buildlist | grep -ve "native" | sort | uniq