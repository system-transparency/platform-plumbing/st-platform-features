#!/bin/bash
SCRIPT_DIR=$(dirname $(readlink -f $0))
POKY_DIR="$SCRIPT_DIR/poky"
MACHINE_TARGET="qemux86-64"

# Check for tools to exist
if ! command -v checksec &> /dev/null
then
    echo "checksec could not be found"
    exit
fi

# Change to the poky directory
cd $POKY_DIR

# Initialize the build environment
source $(pwd)/oe-init-build-env

clear

# Create tmp directory
mkdir -p $SCRIPT_DIR/tmp

# Unpack the cpio.xz stboot-image
cd $SCRIPT_DIR/tmp
unxz -c $SCRIPT_DIR/poky/build/tmp/deploy/images/qemux86-64/stboot-image-qemux86-64.rootfs.cpio.xz | cpio -idmv

# Check the stboot-image for hardening
checksec --dir=$SCRIPT_DIR/tmp --libcfile=$SCRIPT_DIR/tmp/usr/lib/libc.so

# Check kernel configuration
checksec --kernel=$SCRIPT_DIR/poky/build/tmp/deploy/images/qemux86-64/bzImage

# Remove the tmp directory
rm -rf $SCRIPT_DIR/tmp