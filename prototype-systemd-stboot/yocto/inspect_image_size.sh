#!/bin/bash
SCRIPT_DIR=$(dirname $(readlink -f $0))
POKY_DIR="$SCRIPT_DIR/poky"
MACHINE_TARGET="qemux86-64"

# Check for tools to exist
if ! command -v ncdu &> /dev/null
then
    echo "ncdu could not be found"
    exit
fi

# Change to the poky directory
cd $POKY_DIR

# Initialize the build environment
source $(pwd)/oe-init-build-env

clear

# Create tmp directory
mkdir -p $SCRIPT_DIR/tmp

# Unpack the cpio.xz stboot-image
cd $SCRIPT_DIR/tmp
unxz -c $SCRIPT_DIR/poky/build/tmp/deploy/images/qemux86-64/stboot-image-qemux86-64.rootfs.cpio.xz | cpio -idmv

# Show the image size
ncdu $SCRIPT_DIR/tmp

# Remove the tmp directory
rm -rf $SCRIPT_DIR/tmp