#!/bin/bash
SCRIPT_DIR=$(dirname $(readlink -f $0))
POKY_DIR="$SCRIPT_DIR/poky"
MACHINE_TARGET="qemux86_64"

# Change to the poky directory
cd $POKY_DIR

# Initialize the build environment
source $(pwd)/oe-init-build-env

# Remove the existing build artifacts
rm -rf $SCRIPT_DIR/poky/build-st

# Export the build logs to the SCRIPT_DIR
export OEQA_DEBUGGING_SAVED_OUTPUT="${SCRIPT_DIR}"

clear

# Self test for reproducibility of all stboot-image artifacts, ATTENTION: This test may take a long time and requires previous stboot-image build
oe-selftest -r reproducible.ReproducibleTests.test_reproducible_builds
