Yocto-Based stboot Image
========================

This directory contains the build scripts for a minimal stboot image. The image
is built using the Yocto build system and is based on the OpenEmbedded provided
Poky GNU/Linux distribution.

Building
--------

First, make sure the `poky` submodule is checked out.

```bash
git submodule update --init poky
```

Then, the ELF application containing LinuxBoot and stboot can be build. Because
Yocto builds the complete system including all dependencies from source this
can take up to a few hours and consumes upwards to 40GB of disk space.

Start the build with the following command:

```bash
./build_image.sh
```

Once the build is finished the image can be run in qemu using the following command:

```bash
./run_image.sh
```

After booting, stboot can be started like this:

```bash
stboot-systemd
```


