#!/bin/bash
SCRIPT_DIR=$(dirname $(readlink -f $0))
POKY_DIR="$SCRIPT_DIR/poky"
MACHINE_TARGET="qemux86_64"

# Check for tools to exist
if ! command -v grype &> /dev/null
then
    echo "grype could not be found"
    exit
fi

if ! command -v syft &> /dev/null
then
    echo "syft could not be found"
    exit
fi

# Change to the poky directory
cd $POKY_DIR

# Initialize the build environment
source $(pwd)/oe-init-build-env

clear

# Genrate SPDX for the stboot-image
syft $SCRIPT_DIR/poky/build/tmp/deploy/images/qemux86-64/stboot-image-qemux86-64.rootfs.spdx.tar.zst -o spdx-json=$SCRIPT_DIR/stboot-image-qemux86-64.rootfs.spdx.json

# Check SBOM for vulnerabilities in artifacts
grype sbom:$SCRIPT_DIR/stboot-image-qemux86-64.rootfs.spdx.json

# Remove the SPDX file
rm $SCRIPT_DIR/stboot-image-qemux86-64.rootfs.spdx.json