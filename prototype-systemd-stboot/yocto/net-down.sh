#!/bin/sh

set -eu

if [ -f dhcpd.pid ]; then
    sudo kill $(cat dhcpd.pid) || true
    rm -f dhcpd.pid dhcpd.leases
fi
sudo ip l del vm-nic || true
sudo ip l del vm-net || true
