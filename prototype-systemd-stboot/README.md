# Prototype a systemd + stboot system

## 1st Phase: Initial PoC

In this project we will try to replace all stboot components with existing systemd technologies when they make sense. The goal is to deduct if we are able to reduce the footprint of the stboot code base when combined with systemd.

### Details

For now stboot is a single Golang application that does everything. Previously we build a prototype installer OS package that contained a whole Debian base system and used various systemd components to set up the network and install a host_configuration file. The task of this work package is to build something similar for the stboot UKI. It would contain a minimal systemd installation, kexec-tools and the stboot Golang application. systemd would be PID 1 and start stboot after setting up the network. stboot would only fetch and verify the OS package and then return. systemd afterwards would use kexec tools to boot into the OS package. The main goals of this work package are to

- evaluate whether network setup, stboot and kexec can be easily scheduled using systemd Units, and
- develop a build setup that allows building the most minimal initramfs containing only some systemd components and stboot.

### Motivation

Doing anything in stboot requires a lot of effort put into developing Golang code and testing it. It also increases the maintainance burden. For non core tasks like setting up the network or preparing disks it may be smarter to use existing tools like systemd. This allow us to concentrate on the unique parts of System Transparency. Considering that systemd is part of most Linux distributions and likely part of the OS package the increase in attack surface is outweighed by the lowered maintenance burden.

### Goals / MVP

* Try to replace stboot components with systemd
  * OSPKG fetching, verification and execution
  * Trustpolicy fetching/parsing
  * Passing metadata to OSPKG
  * Hostconfig fetching/parsing
  * TPM, EFI API access
  * Kernel kexec
  * Network setup
    * Static, DHCP, DNS, Bonding
  * u-root init
* Test new prototype in qemu
* Test new prototype on real hardware

### Challenges

Custom data format parsing might be limited to replace with existing tooling / code from the systemd ecosystem

### Outcome

![](stboot-overview.png)

#### Summary

- I didn’t want to spend trying out multiple build systems in the beginning and just start to deliver fast.
  - I have re-used our previous code, scripts for setting up everything with mkosi.
  - In between of development I had to switch from the debian mkosi base to fedora because of issues with debian
  - I didn’t implement all detailed functionality but the core which executes an ospkg with existing stboot flow
- I have replaced the most suitable components like
  - Network Setup
  - Kexec
  - TPM Measurements
  - Recovery/Init
- I wrote a tool which uses code from stboot to be invoked by systemd start which does
  - Cryptographic operations
  - Data structure parsing (hostconfig, trustpolicy)
  - Execution of the tools in the right order
- I didn’t touch the following components
  - Metadata
  - Extended measurements (RA is still not ready)
  - Logging

#### Components

- **systemd-networkd** is used a replacement for network setup
  - Offers a dbus api but with limited capabilities changing the network stack
  - Using configuration files to configure all needed capabilities
    - DHCP, static, DNS, Bonding
  - networkctl for cmdline management
- **Another Option: NetworkManager**
  - Used mostly in client platforms like laptops
  - Probably same amount of overhead as systemd but less suitable
  - API + configuration files, just with more API features
- **Another option: Linux userspace tools**
  - Putting loose ends together (dhclient, ip, ifconfig…)
- **systemd-measure:** Only measures UKI into a specific systemd defined PCR’s
- **systemd-pcrextend:** More flexible but only extend PCR’s with machine ID or filesystemd UUID
  - Would need more code to work as measurement tooling
- **tpm2-tools:** Does any thinkable tpm operation on the cmdline
  - works as expected for the documentation. Used by trammel hudson as well for remote attestation for safeboot.dev
  - Allows us to do a required command as shell out to the tools.
- **systemd-kexec, kexec-tools:** Works like a charm to execute ospkg artifacts
  - systemd kexec only works together with kexec-tools.
  - manually loading artifacts via kexec works
  - automatically loading is only possible when BLS with systemd boot is used
- **systemd/init:** This can be done through systemd service files
  - Since we already use systemd for networking there is no extra footprint
  - We can daemonize and isolate the execution of specific components
  - Recovery can be done also as part of a systemd service/unit file

#### Recommendation

- systemd tool suite still lacks important ready-to-use functionality for TPM operation and kexec.
- systemd is meant to be configured through text files not via API
- The biggest profit we gain using systemd is **init/recovery** and **networking** functionality
  - IMHO it doesn’t make sense to do networking and init/recovery stuff when this has been solved already
- **kexec-tools** might be a better idea than using u-root kexec implementation
  - Even if the u-root implementation is maintained it still lacks feature completeness and validation through the kernel community
  - u-root also brings a huge code base in again we can’t control so well

## 2nd Phase: Component & Size Reduction

In this project we try to reduce the amount of components in the target initramfs image to run the previous PoC's with a minimum of components and a small TCB.

### Details

I have tested multiple ways to generate a minimal initramfs with systemd. The best option I have found is to use the yocto build system which comes with a necessary tools we need. Aside of tools requirements, [yocto](https://docs.yoctoproject.org/index.html) supports musl libc for static builds, SBOM generation, reproducible builds by default and systemd with musl libc compability.

In order to utilize yocto for our use case we need to create our own minimal build target for stboot. Therefore I will fork the project and introduce a stboot branch on a release version.

Further more, we have to modify build plans like systemd and kmod to reduce it to minimum tcb possible.

### Motivation

In order to make the 1st phase more practical we are trying to reduce the amount of components built into the image. This enables us to

* Load the image as LinuxBoot ready for use in the firmware image (SPI flash, small size environments)

* Use a small TCB and a hardened initramfs
* Keep control of the components we built in
* Still use systemd for networking and other components which are useful

### Goals / MVP

* Minimal image containing at least systemd-networkd and kexec-tools at minimum
* Hardened initramfs with systemd init
* Custom target for the build system we want to use
* Functional stboot image with mini stboot implementation of phase 1

### Challenges

* systemd is know to be huge and isn't easy to build in a reduced size environment. It is also known that systemd doesn't support static builds and any other libc than glibc

### Outcome

TBD
