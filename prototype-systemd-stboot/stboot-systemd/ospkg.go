package main

import (
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path"

	"system-transparency.org/stboot/ospkg"
	"system-transparency.org/stboot/trust"
)

const pcrIndex = 7

func fetchOSPackage(URL string) (*string, error) {
	tmpURL, err := url.Parse(URL)
	if err != nil {
		return nil, err
	}
	fileName := path.Base(tmpURL.Path)
	filePath := fmt.Sprintf("/tmp/%s", fileName)
	out, err := os.Create(filePath)
	if err != nil {
		return nil, err
	}
	defer out.Close()
	resp, err := http.Get(URL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return nil, err
	}
	return &filePath, nil
}

func FetchVerifyMeasureLoadOSPackage(tp *trust.Policy, URL string) error {
	descriptorFile, err := fetchOSPackage(URL)
	if err != nil {
		return err
	}
	descriptorData, err := os.ReadFile(*descriptorFile)
	if err != nil {
		return err
	}
	descriptor, err := ospkg.DescriptorFromBytes(descriptorData)
	if err != nil {
		return err
	}
	if err := descriptor.Validate(); err != nil {
		return err
	}
	oszip, err := fetchOSPackage(descriptor.PkgURL)
	if err != nil {
		return err
	}
	oszipData, err := os.ReadFile(*oszip)
	if err != nil {
		return err
	}
	ospkackage, err := ospkg.NewOSPackage(oszipData, descriptorData)
	if err != nil {
		return err
	}
	signingRootData, err := os.ReadFile(SigningRootFile)
	if err != nil {
		return err
	}
	block, _ := pem.Decode(signingRootData)
	if block == nil {
		return fmt.Errorf("failed to decode pem block")
	}
	signingRootCert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return err
	}
	found, valid, err := ospkackage.Verify(signingRootCert)
	if err != nil {
		return err
	}
	if tp.SignatureThreshold < found {
		return fmt.Errorf("signature threshold not met")
	}
	if tp.SignatureThreshold != valid {
		return fmt.Errorf("signature not valid")
	}
	if err := MeasureData(pcrIndex, oszipData); err != nil {
		return err
	}
	toKexec, err := ospkackage.LinuxImage()
	if err != nil {
		return err
	}
	return Execute(toKexec.Kernel, toKexec.Initrd, toKexec.Cmdline)
}
