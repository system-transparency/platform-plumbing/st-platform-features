package main

import (
	"net"
	"os"
	"testing"

	"github.com/vishvananda/netlink"
	"system-transparency.org/stboot/host"
	"system-transparency.org/stboot/ospkg"
	"system-transparency.org/stboot/trust"
)

var (
	ipAddrModeStatic  = host.IPStatic
	ipAddrModeDHCP    = host.IPDynamic
	gateway           = net.ParseIP("192.168.1.1")
	dns1              = net.ParseIP("8.8.8.8")
	dns2              = net.ParseIP("4.4.4.4")
	dnsServers        = []*net.IP{&dns1, &dns2}
	iface1            = "eth0"
	iface2            = "eth1"
	networkInterfaces = []*host.NetworkInterface{
		{
			InterfaceName: &iface1,
			MACAddress:    &net.HardwareAddr{0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff},
		},
		{
			InterfaceName: &iface2,
			MACAddress:    &net.HardwareAddr{0xab, 0xcd, 0xef, 0xfa, 0xeb, 0xdc},
		},
	}
	bondName = "bond0"
	ospkgURL = "http://niche.systems/file.zip"
	testData = []struct {
		mockHostConfig  host.Config
		mockTrustPolicy trust.Policy
	}{
		{
			mockHostConfig: host.Config{
				IPAddrMode:        &ipAddrModeDHCP,
				HostIP:            nil,
				DefaultGateway:    nil,
				DNSServer:         nil,
				NetworkInterfaces: nil,
				OSPkgPointer:      &ospkgURL,
				BondingMode:       host.BondingUnset,
				BondName:          nil,
			},
			mockTrustPolicy: trust.Policy{
				SignatureThreshold: 2,
				FetchMethod:        ospkg.FetchFromNetwork,
			},
		},
		{
			mockHostConfig: host.Config{
				IPAddrMode:        &ipAddrModeDHCP,
				HostIP:            nil,
				DefaultGateway:    nil,
				DNSServer:         nil,
				NetworkInterfaces: &networkInterfaces,
				OSPkgPointer:      &ospkgURL,
				BondingMode:       host.BondingActiveBackup,
				BondName:          &bondName,
			},
			mockTrustPolicy: trust.Policy{
				SignatureThreshold: 2,
				FetchMethod:        ospkg.FetchFromNetwork,
			},
		},
		{
			mockHostConfig: host.Config{
				IPAddrMode: &ipAddrModeStatic,
				HostIP: &netlink.Addr{
					IPNet: &net.IPNet{
						IP:   net.ParseIP("192.168.1.100"),
						Mask: net.CIDRMask(24, 32),
					},
				},
				DefaultGateway:    &gateway,
				DNSServer:         &dnsServers,
				NetworkInterfaces: &networkInterfaces,
				OSPkgPointer:      &ospkgURL,
				BondingMode:       host.BondingActiveBackup,
				BondName:          &bondName,
			},
			mockTrustPolicy: trust.Policy{
				SignatureThreshold: 2,
				FetchMethod:        ospkg.FetchFromNetwork,
			},
		},
		{
			mockHostConfig: host.Config{
				IPAddrMode: &ipAddrModeStatic,
				HostIP: &netlink.Addr{
					IPNet: &net.IPNet{
						IP:   net.ParseIP("192.168.1.100"),
						Mask: net.CIDRMask(24, 32),
					},
				},
				DefaultGateway:    &gateway,
				DNSServer:         &dnsServers,
				NetworkInterfaces: nil,
				OSPkgPointer:      &ospkgURL,
				BondingMode:       host.BondingUnset,
				BondName:          nil,
			},
			mockTrustPolicy: trust.Policy{
				SignatureThreshold: 2,
				FetchMethod:        ospkg.FetchFromNetwork,
			},
		},
	}
)

func Test_NetworkSetup(t *testing.T) {
	systemdNetworkDir := "network"
	os.Mkdir(systemdNetworkDir, 0755)
	for _, data := range testData {
		dir, err := os.MkdirTemp(systemdNetworkDir, "network")
		if err != nil {
			t.Error(err)
		}
		err = ConfigureNetwork(dir, data.mockHostConfig)
		if err != nil {
			t.Error(err)
		}
	}
}
