module git.glasklar.is/zaolin/stboot-systemd

go 1.21

require (
	github.com/godbus/dbus/v5 v5.1.0
	github.com/sergeymakinen/go-systemdconf v1.0.0
	github.com/vishvananda/netlink v1.2.1-beta.2
	system-transparency.org/stboot v0.4.0
)

require (
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/google/go-tpm v0.9.1-0.20230914180155-ee6cbcd136f8 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/klauspost/compress v1.17.8 // indirect
	github.com/pierrec/lz4/v4 v4.1.21 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/u-root/u-root v0.14.0 // indirect
	github.com/u-root/uio v0.0.0-20240224005618-d2acac8f3701 // indirect
	github.com/ulikunitz/xz v0.5.12 // indirect
	github.com/vishvananda/netns v0.0.4 // indirect
	golang.org/x/exp v0.0.0-20240506185415-9bf2ced13842 // indirect
	golang.org/x/sys v0.20.0 // indirect
)
