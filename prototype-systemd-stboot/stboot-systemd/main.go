package main

import (
	"fmt"

	"system-transparency.org/stboot/ospkg"
)

/*
This is a minimal stboot implementation which mostly uses systemd to boot the system.
It is a simple example of how to use the stboot package to create a bootable system.
It is not intended to be a complete implementation, but rather a starting point for
building a more complete system.
*/
func main() {
	fmt.Println("Kai's stboot implementation")

	systemdNetworkDir := "/etc/systemd/network"

	hostConfig, err := ReadHostConfig()
	if err != nil {
		fmt.Printf("Failed to read host config: %v\n", err)
		panic(err)
	}
	trustPolicy, err := ReadTrustPolicy()
	if err != nil {
		fmt.Printf("Failed to read trust policy: %v\n", err)
		panic(err)
	}
	if trustPolicy.FetchMethod == ospkg.FetchFromNetwork {
		if err := ConfigureNetwork(systemdNetworkDir, *hostConfig); err != nil {
			fmt.Printf("Failed to configure network: %v\n", err)
			panic(err)
		}
		if err := ReloadNetworkConfig(); err != nil {
			fmt.Printf("Failed to reload network config: %v\n", err)
			panic(err)
		}
	}
	if err := FetchVerifyMeasureLoadOSPackage(trustPolicy, *hostConfig.OSPkgPointer); err != nil {
		fmt.Printf("Failed to fetch, verify, measure, and load OS package: %v\n", err)
		panic(err)
	}
}
