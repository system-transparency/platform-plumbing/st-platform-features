package main

import (
	"fmt"
	"os"
	"time"

	"github.com/godbus/dbus/v5"
	"github.com/sergeymakinen/go-systemdconf"
	"github.com/sergeymakinen/go-systemdconf/network"

	"system-transparency.org/stboot/host"
)

func ConfigureNetwork(targetDir string, hc host.Config) error {
	networkFile := network.NetworkFile{}
	// Check for bonding mode first
	if hc.BondingMode != host.BondingUnset {
		netdevFile := network.NetdevFile{}
		netdevFile.NetDev = network.NetdevNetDevSection{
			Name: systemdconf.Value{*hc.BondName},
			Kind: systemdconf.Value{"bond"},
		}
		var bondMode string
		switch hc.BondingMode {
		case host.BondingActiveBackup:
			bondMode = "active-backup"
		case host.BondingBalanceRR:
			bondMode = "balance-rr"
		case host.BondingBalanceXOR:
			bondMode = "balance-xor"
		case host.BondingBroadcast:
			bondMode = "broadcast"
		case host.Bonding8023AD:
			bondMode = "802.3ad"
		case host.BondingBalanceTLB:
			bondMode = "balance-tlb"
		case host.BondingBalanceALB:
			bondMode = "balance-alb"
		default:
			return fmt.Errorf("invalid bonding mode: %v", hc.BondingMode)
		}
		netdevFile.Bond = network.NetdevBondSection{
			Mode:                  systemdconf.Value{bondMode},
			PrimaryReselectPolicy: systemdconf.Value{"always"},
			MIIMonitorSec:         systemdconf.Value{"1s"},
		}
		netDevFileData, err := systemdconf.Marshal(&netdevFile)
		if err != nil {
			return err
		}
		path := fmt.Sprintf("10-%s.netdev", *hc.BondName)
		err = os.WriteFile(targetDir+"/"+path, netDevFileData, 0644)
		if err != nil {
			return err
		}
		for _, iface := range *hc.NetworkInterfaces {
			// Combine bond and physical interfaces
			networkFile2 := network.NetworkFile{}
			networkFile2.Match = network.NetworkMatchSection{
				Name: systemdconf.Value{*iface.InterfaceName},
			}
			networkFile2.Network = network.NetworkNetworkSection{
				Bond:         systemdconf.Value{*hc.BondName},
				PrimarySlave: systemdconf.Value{"true"},
			}
			networkFile2Data, err := systemdconf.Marshal(&networkFile2)
			if err != nil {
				return err
			}
			path = fmt.Sprintf("20-%s.network", *iface.InterfaceName)
			err = os.WriteFile(targetDir+"/"+path, networkFile2Data, 0644)
			if err != nil {
				return err
			}
		}
		// Match bond interface
		matchSection := network.NetworkMatchSection{
			Name: systemdconf.Value{*hc.BondName},
		}
		networkFile.Match = matchSection
	} else {
		// Match physical interface
		if hc.NetworkInterfaces != nil {
			for _, iface := range *hc.NetworkInterfaces {
				matchSection := network.NetworkMatchSection{
					Name: systemdconf.Value{*iface.InterfaceName},
				}
				networkFile.Match = matchSection
				break
			}
		}
	}
	networkSection := network.NetworkNetworkSection{}
	switch *hc.IPAddrMode {
	case host.IPDynamic:
		networkSection.DHCP = systemdconf.Value{"yes"}
	case host.IPStatic:
		ip := hc.HostIP.IP.String()
		netmask := hc.HostIP.IPNet.Mask.String()
		gateway := hc.DefaultGateway.String()
		networkSection.Address = systemdconf.Value{ip + "/" + netmask}
		networkSection.Gateway = systemdconf.Value{gateway}
		var dnsServers []string
		for _, dns := range *hc.DNSServer {
			dnsServers = append(dnsServers, dns.String())
			networkSection.DNS = systemdconf.Value{dnsServers[0]}
		}
	}
	networkFile.Network = networkSection
	data, err := systemdconf.Marshal(&networkFile)
	if err != nil {
		return err
	}
	path := fmt.Sprintf("10-%s.network", networkFile.Match.Name.String())
	err = os.WriteFile(targetDir+"/"+path, data, 0644)
	if err != nil {
		return err
	}
	return nil
}

func ReloadNetworkConfig() error {
	if err := networkdReload(); err != nil {
		return err
	}
	if err := pollNetwork(); err != nil {
		return err
	}
	return nil
}

func networkdReload() error {
	conn, err := dbus.ConnectSystemBus()
	if err != nil {
		return err
	}
	defer conn.Close()

	manager := conn.Object("org.freedesktop.network1", "/org/freedesktop/network1")
	err = manager.Call("org.freedesktop.network1.Manager.Reload", 0).Err
	return err
}

func pollNetwork() error {
	conn, err := dbus.ConnectSystemBus()
	if err != nil {
		return err
	}
	defer conn.Close()

	manager := conn.Object("org.freedesktop.network1", "/org/freedesktop/network1")

	for {
		time.Sleep(1 * time.Second)

		op, err := manager.GetProperty("org.freedesktop.network1.Manager.OperationalState")
		if err != nil {
			return err
		}
		on, err := manager.GetProperty("org.freedesktop.network1.Manager.OnlineState")
		if err != nil {
			return err
		}

		if op.Value().(string) == "routable" && on.Value().(string) == "online" {
			break
		}
	}

	return nil
}
