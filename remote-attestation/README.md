Remote Attestation for stboot
=============================

The this project prototypes a remote attestation feature for stboot-enabled
servers that allows anyone to verify that:

- the server is owned by a particular entity,
- was provisioned according to a human-readable protocol,
- runs a firmware stack that has been marked as trusted by the operator, and
- has booted an OS package that is trusted by the operator.

To accomplish this, we use TPM 2.0 measurements and signatures. The project
implements the necessary tooling for operators of stboot-enabled server fleets
to provide measurements to third parties as well as software to verify these
measurements and signatures.

The implementation can be found in the `ra` directory, and the demo application
in the `racmd` directory. The `ra/documentation` directory contains the
protocol documentation and system design.
