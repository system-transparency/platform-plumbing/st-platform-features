module system-transparency.org/ppg/racmd

go 1.21.6

require system-transparency.org/ppg/ra v0.0.0-00010101000000-000000000000

replace system-transparency.org/ppg/ra => ../ra
