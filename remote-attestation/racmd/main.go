package main

import (
	"context"
	"fmt"
	"os"

	"system-transparency.org/ppg/ra"
	"system-transparency.org/ppg/ra/internal/tpm"
)

func main() {
	ctx := context.Background()
	dev, err := tpm.OpenTPM("/dev/tpmrm0")
	if err != nil {
		panic(err)
	}

	tpm.Startup(dev, tpm.TPM_SU_CLEAR)

	ekcerts, err := ra.usableEndorsementKeys(ctx, dev, "")
	if err != nil {
		panic(err)
	}

	certs, err := ra.vendorRoots()
	if err != nil {
		panic(err)
	}

	for i, ekcert := range ekcerts {
		_, err := ra.VerifyEndorsementCertificate(ekcert.Certificate, certs, false)
		state := "OK"
		if err != nil {
			state = err.Error()
		}

		fmt.Printf("%d: %s %s\n", i, ekcert.Certificate.SerialNumber.String(), state)
		err = os.WriteFile(fmt.Sprintf("ekcert-%d.der", i), ekcert.Certificate.Raw, 0644)
		if err != nil {
			panic(err)
		}
	}
}
