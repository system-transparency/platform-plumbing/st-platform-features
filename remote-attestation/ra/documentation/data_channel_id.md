Discussion on Gitlab: [https://git.glasklar.is/system-transparency/project/documentation/-/issues/7]

Keys, Certificates, and Identities
----------------------------------

Device Identity: This is a human-readable, lifetime identity of the device.
Examples include "nl1.mullvad.net" or "Kai's Macbook".

POK: This is the Platform Owner's signing key. It possesses a certificate
issued by the organization's root certificate that identifies them as the PO,
possibly using the RDN "OU=System Transparency PO" or a custom EKU.

AIK: The Attestation Identity Key (a TCG term) is used to sign the TPM quote.
It is generated in the device's TPM and certified by the POK. The certificate's
Subject Common Name would be the device identity string, and the extended key
usage would be a custom OID for System Transparency AIK.

DCI: The Data Channel Identity Key (a term I prefer) includes multiple keys of
different types for various protocols (Wireguard, TLS/OpenVPN, SSH). Each key
is certified by the POK, with the Subject Common Name set to the device
identity. The extended key usage would be System Transparency DCI for X (again,
Wireguard, TLS, SSH, etc.).

Utilizing the X.509 Subject and EKU fields for the above certificates implies
that a verifier doesn't need further information to select and trust the right
public key.

In this system, no direct link between the DCIs and the AIK exists, and the PO
can add or remove DCIs without involving the TPM. This also means that the AIK
can be removed entirely for TPM-less devices or remote attestation scenarios
where there is no strict AIK.

Binding DCIs to the TPM
-----------------------

The AIK's private key always resides in the TPM, while the DCIs do not need to,
in order for the scheme to function. If we want to establish this binding, we
could realize it in several ways.

We could generate the DCIs on or off the device and seal them to the device's
TPM and expected PCR. The keys would be accessible to a specific set of
firmware/stboot/ospkg combinations chosen by the PO. The set can be changed by
the PO at any time, making the DCIs static.

Alternatively, instead of sealing the keys, they could be generated in the TPM
and remain resident there. This way, if an attacker gains access to the device,
they would only acquire a signing oracle but not access to the private keys
themselves. Once the vulnerability is addressed by deploying new
firmware/stboot/ospkg and rebooting, the oracle is removed. Technically, the
keys wouldn't need to be revoked (although best practice still suggests doing
so). The downside of this approach is that the (relatively slow) TPM would need
to be involved in every connection request. However, this can be mitigated by
not using the TPM-resident DCI directly, but by using it to certify ephemeral
(e.g., 1-hour validity) sub DCIs. This approach only works for cryptographic
algorithms implemented by the TPM: PSS, OAEP, PKCS#1.5, ECDSA, and ECDH over
NIST P-* curves.

Dynamic DCI
-----------

If we want DCIs to be generated randomly during each boot, the PO would
necessarily have to recertify them each time. The TPM could be involved in two
ways.

The first way is to generate the DCI as TPM-resident keys (as above) in the
NULL hierarchy. Keys generated in that hierarchy are different after each
reboot. The rest of the scheme, including its advantages and disadvantages, is
the same as the static TPM-resident keys described above.

The second approach would be to generate the keys in stboot on the host and
measure the result. For each DCI, stboot would measure its public key, the
creation time (or sigsum head), and a signature of the time by the specific DCI
to prove not only generation but also possession.
