package ra

import (
	"bytes"
	"crypto"
	"encoding/hex"
	"fmt"
	"os"
	"strings"
	"testing"

	test "system-transparency.org/ppg/ra/internal"
)

func TestPlatformFw(t *testing.T) {
	cases := []string{
		"dl160.eventlog",
		"h12ssl.eventlog",
		"m50cyp.eventlog",
		"r340.eventlog",
		"sr630.eventlog",
	}

	for _, c := range cases {
		t.Run(c, func(t *testing.T) {
			fd, err := os.Open("testdata/" + c)
			if test.NoErr(t, err) {
				defer fd.Close()
			}

			records, err := transcodePcClientEventlog(fd)
			test.NoErr(t, err)

			pltfw, err := extractPlatformFirmware(records)
			test.NoErr(t, err)

			t.Logf("Platform Firmware: %#v\n", pltfw)
		})
	}
}

func TestAuthentihash(t *testing.T) {
	buf, err := os.ReadFile("testdata/putty.exe")
	test.NoErr(t, err)

	pe, err := parsePe(buf, crypto.SHA256)
	test.NoErr(t, err)

	expect, err := hex.DecodeString("8be7d65593b0fff2e8b29004640261b8a0d4fcc651a14cd0b8b702b7928f8ee0")
	test.NoErr(t, err)

	t.Logf("%x\n", pe.Authentihash)
	test.IsEq(t, bytes.Equal(pe.Authentihash, expect), true)
}

func TestCreateStbootMeasuremens(t *testing.T) {
	cases := []string{
		"stboot-0.3.5.efi",
		"stboot-0.3.6.efi",
	}

	for _, c := range cases {
		t.Run(c, func(t *testing.T) {
			buf, err := os.ReadFile(fmt.Sprintf("testdata/%s", c))
			test.NoErr(t, err)

			_, err = createSystemdStubMeasurements(buf)
			test.NoErr(t, err)
		})
	}
}

func TestTemplatizeLogs(t *testing.T) {
	logs := []string{
		"testdata/m50cyp.eventlog",
		"testdata/dl160.eventlog",
		"testdata/h12ssl.eventlog",
		"testdata/r340.eventlog",
		"testdata/sr630.eventlog",
	}
	for _, p := range logs {
		t.Run(p, func(t *testing.T) {
			fd, err := os.Open(p)
			if test.NoErr(t, err) {
				defer fd.Close()
			}

			log, err := transcodePcClientEventlog(fd)
			test.NoErr(t, err)

			for _, l := range log {
				t.Logf(l.String())
			}

			pltfw, err := extractPlatformFirmware(log)
			test.NoErr(t, err)

			test.IsEq(t, len(pltfw.Firmware) > 0, true)

			t.Logf("DRTM init: %x\n", pltfw.DRTMInit)
			t.Logf("Firmware: ")
			for _, l := range pltfw.Firmware {
				t.Logf(l.String())
			}
			t.Logf("Inter ebs:")
			for _, l := range pltfw.InterExitBootServices {
				t.Logf(l.String())
			}

		})
	}
}

func TestPeSections(t *testing.T) {
	t.Run("putty.exe", func(t *testing.T) {
		secs := map[string][]int{
			".text":  {0x400, 0x9cc26},
			".rdata": {0x9ce00, 0xc8e14},
			".data":  {0xc9000, 0xcf198},
			".pdata": {0xc9c00, 0xcf48c},
			".00cfg": {0xcf600, 0xcf610},
			".gfids": {0xcf800, 0xcf8a8},
			".rsrc":  {0xcfa00, 0x11aa30},
			".reloc": {0x11ac00, 0x11be70},
		}
		authentihash, err := hex.DecodeString("8be7d65593b0fff2e8b29004640261b8a0d4fcc651a14cd0b8b702b7928f8ee0")
		test.NoErr(t, err)

		buf, err := os.ReadFile("testdata/putty.exe")
		test.NoErr(t, err)

		pe, err := parsePe(buf, crypto.SHA256)
		test.NoErr(t, err)

		test.IsEq(t, len(pe.Sections), len(secs))
		for _, s := range pe.Sections {
			expect, ok := secs[strings.TrimRight(s.Name, "\x00")]
			if test.IsEq(t, ok, true) {
				test.IsEq(t, s.Start, expect[0])
				test.IsEq(t, s.End, expect[1])
			}
		}

		test.IsEq(t, pe.Authentihash, authentihash)
	})

	t.Run("stboot-0.3.6.efi", func(t *testing.T) {
		secs := map[string][]int{
			".text":    {0x340, 0xc310},
			".reloc":   {0xc340, 0xc34c},
			".data":    {0xc540, 0xf9d8},
			".dynamic": {0xfb40, 0xfc40},
			".rela":    {0xfd40, 0x10c70},
			".dynsym":  {0x10d40, 0x10d58},
			".sdmagic": {0x10f40, 0x10f74},
			".osrel":   {0x11140, 0x11197},
			".cmdline": {0x11340, 0x11354},
			".initrd":  {0x11540, 0x6096eb},
			".linux":   {0x609740, 0x16bd140},
		}
		authentihash, err := hex.DecodeString("aec0c57a69fa58d77b95c35a671143268d41025d00c8e77b681036a84b3550eb")
		test.NoErr(t, err)

		buf, err := os.ReadFile("testdata/stboot-0.3.6.efi")
		test.NoErr(t, err)

		pe, err := parsePe(buf, crypto.SHA256)
		test.NoErr(t, err)

		test.IsEq(t, len(pe.Sections), len(secs))
		for _, s := range pe.Sections {
			expect, ok := secs[strings.TrimRight(s.Name, "\x00")]
			if test.IsEq(t, ok, true) {
				test.IsEq(t, s.Start, expect[0])
				test.IsEq(t, s.End, expect[1])
			}
		}

		test.IsEq(t, pe.Authentihash, authentihash)
	})
}
