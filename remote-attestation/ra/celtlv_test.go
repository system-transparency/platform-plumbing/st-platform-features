package ra

import (
	"bytes"
	"crypto/rand"
	"reflect"
	"testing"
	"time"

	test "system-transparency.org/ppg/ra/internal"
)

func TestCelEncode(t *testing.T) {
	var buf bytes.Buffer
	ver := version{
		Major: 1,
		Minor: 2,
	}
	err := ver.encode(&buf)
	test.NoErr(t, err)
}

func TestRecordCodec(t *testing.T) {
	now := time.Now().UTC()
	now = time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second(), 0, time.UTC)
	tr := byte(0x42)
	recs := []record{
		{
			Recnum: 110,
			Pcr:    10,
			Digests: []digest{
				{Alg: sha1Alg, Digest: make([]byte, 20)},
				{Alg: sha256Alg, Digest: make([]byte, 32)},
				{Alg: sha384Alg, Digest: make([]byte, 48)},
				{Alg: sha512Alg, Digest: make([]byte, 64)},
			},
			Content: content{
				Cel: &cel{
					Version: &version{
						Major: 42,
						Minor: 23,
					},
				},
			},
		},
		{
			Recnum: 110,
			Pcr:    10,
			Digests: []digest{
				{Alg: sha512Alg, Digest: make([]byte, 64)},
			},
			Content: content{
				Cel: &cel{
					FirmwareEnd: true,
				},
			},
		},
		{
			Recnum: 110,
			Pcr:    10,
			Digests: []digest{
				{Alg: sha512Alg, Digest: make([]byte, 64)},
			},
			Content: content{
				Cel: &cel{
					Timestamp: &now,
				},
			},
		},
		{
			Recnum: 110,
			Pcr:    10,
			Digests: []digest{
				{Alg: sha512Alg, Digest: make([]byte, 64)},
			},
			Content: content{
				Cel: &cel{
					Transition: &tr,
				},
			},
		},
		{
			Recnum: 110,
			Pcr:    10,
			Digests: []digest{
				{Alg: sha512Alg, Digest: make([]byte, 64)},
			},
			Content: content{
				PcClientStd: &pcclient{
					Type: 0x80000011,
					Data: make([]byte, 42),
				},
			},
		},
		{
			Recnum: 110,
			Pcr:    10,
			Digests: []digest{
				{Alg: sha512Alg, Digest: make([]byte, 64)},
			},
			Content: content{
				Stboot: &stboot{
					Measurement: stbootMeasurement,
				},
			},
		},
	}

	for i, r := range recs {
		var buf bytes.Buffer
		err := r.encode(&buf)
		test.NoErr(t, err)

		var r2 record
		err = r2.decode(&buf)
		test.NoErr(t, err)

		if !test.IsEq(t, reflect.DeepEqual(&r, &r2), true) {
			t.Logf("test case #%d", i)
			t.Logf("r = %#v, r2 = %#v", r, r2)
			t.Fail()
		}
	}
}

func FuzzRecnumCodecError(f *testing.F) {
	f.Add([]byte{0, 0, 0, 0, 4, 1, 2, 3, 4}, true)
	f.Add([]byte{0, 0, 0, 0, 4, 1, 2, 3}, false)
	f.Add([]byte{0, 0, 0, 0, 3, 1, 2, 3}, false)
	f.Add([]byte{1, 0, 0, 0, 3, 1, 2, 3}, false)
	f.Add([]byte{1, 0, 0, 0}, false)
	f.Add([]byte{}, false)

	f.Fuzz(func(t *testing.T, data []byte, outcome bool) {
		val := new(recnum)
		buf := bytes.NewBuffer(data)
		err := val.decode(buf)
		if outcome {
			test.NoErr(t, err)
		} else {
			test.WantErr(t, err)
		}
	})
}

func FuzzPcrCodecError(f *testing.F) {
	f.Add([]byte{1, 0, 0, 0, 4, 1, 2, 3, 4}, true)
	f.Add([]byte{1, 0, 0, 0, 4, 1, 2, 3}, false)
	f.Add([]byte{1, 0, 0, 0, 3, 1, 2, 3}, false)
	f.Add([]byte{0, 0, 0, 0, 3, 1, 2, 3}, false)
	f.Add([]byte{0, 0, 0, 0}, false)
	f.Add([]byte{}, false)

	f.Fuzz(func(t *testing.T, data []byte, outcome bool) {
		val := new(pcr)
		buf := bytes.NewBuffer(data)
		err := val.decode(buf)
		if outcome {
			test.NoErr(t, err)
		} else {
			test.WantErr(t, err)
		}
	})
}

func FuzzDigestCodecError(f *testing.F) {
	dgst := make([]byte, 20)
	rand.Read(dgst)

	f.Add(append([]byte{sha1Alg, 0, 0, 0, 20}, dgst...), true)
	f.Add(append([]byte{sha1Alg, 0, 0, 0, 20}, dgst[:19]...), false)
	f.Add([]byte{sha1Alg, 0, 0, 0, 3, 1, 2, 3}, true)
	f.Add([]byte{sha1Alg, 0, 0, 0, 20, 1, 2, 3}, false)
	f.Add([]byte{1, 0, 0, 0}, false)
	f.Add([]byte{}, false)

	f.Fuzz(func(t *testing.T, data []byte, outcome bool) {
		val := new(digest)
		buf := bytes.NewBuffer(data)
		err := val.decode(buf)
		if outcome {
			test.NoErr(t, err)
		} else {
			test.WantErr(t, err)
		}
	})
}

func FuzzDigestsCodecError(f *testing.F) {
	f.Add([]byte{3, 0, 0, 0, 8, sha1Alg, 0, 0, 0, 3, 1, 2, 3}, true)
	f.Add([]byte{3, 0, 0, 0, 7, sha1Alg, 0, 0, 0, 3, 1, 2, 3}, false)
	f.Add([]byte{4, 0, 0, 0, 0}, false)
	f.Add([]byte{3, 0, 0, 0}, false)
	f.Add([]byte{}, false)

	f.Fuzz(func(t *testing.T, data []byte, outcome bool) {
		val := new(digests)
		buf := bytes.NewBuffer(data)
		err := val.decode(buf)
		if outcome {
			test.NoErr(t, err)
		} else {
			test.WantErr(t, err)
		}
	})
}

func FuzzContentCodecError(f *testing.F) {
	f.Add([]byte{
		4, 0, 0, 0, 23, // content cel
		1, 0, 0, 0, 18, // cel version
		0, 0, 0, 0, 2, 1, 2, // major version
		1, 0, 0, 0, 2, 3, 4, // minor version
	}, true)
	f.Add([]byte{
		4, 0, 0, 0, 23, // content cel
		0, 0, 0, 0, 18, // cel version
		0, 0, 0, 0, 2, 1, 2, // major version
		1, 0, 0, 0, 2, 3, 4, // minor version
	}, false)
	f.Add([]byte{
		4, 0, 0, 0, 23, // content cel
		1, 0, 0, 0, 18, // cel version
		1, 0, 0, 0, 2, 1, 2, // major version
		1, 0, 0, 0, 1, 3, 4, // minor version
	}, false)
	f.Add([]byte{
		4, 0, 0, 0, 23, // content cel
		1, 0, 0, 0, 18, // cel version
		0, 0, 0, 0, 1, 1, 2, // major version
		1, 0, 0, 0, 2, 3, 4, // minor version
	}, false)
	f.Add([]byte{
		4, 0, 0, 0, 23, // content cel
		1, 0, 0, 0, 18, // cel version
		0, 0, 0, 0, 2, 1, 2, // major version
		2, 0, 0, 0, 2, 3, 4, // minor version
	}, false)

	f.Add([]byte{
		4, 0, 0, 0, 5, // content cel
		2, 0, 0, 0, 0, // cel firmware end
	}, true)

	f.Add([]byte{
		4, 0, 0, 0, 13, // content cel
		80, 0, 0, 0, 8, 1, 2, 3, 4, 5, 6, 7, 8, // cel timestamp
	}, true)
	f.Add([]byte{
		4, 0, 0, 0, 13, // content cel
		80, 0, 0, 0, 7, 1, 2, 3, 4, 5, 6, 7, 8, // cel timestamp
	}, false)

	f.Add([]byte{
		4, 0, 0, 0, 6, // content cel
		81, 0, 0, 0, 1, 99, // cel transition
	}, true)

	f.Add([]byte{
		5, 0, 0, 0, 23, // pcclient
		0, 0, 0, 0, 4, 0x80, 0, 0, 1, // event type
		1, 0, 0, 0, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, // event data
	}, true)
	f.Add([]byte{
		5, 0, 0, 0, 23, // pcclient
		0, 0, 0, 0, 4, 0x80, 0, 0, 1, // event type
		1, 0, 0, 0, 0, // event data
	}, true)
	f.Add([]byte{
		5, 0, 0, 0, 23, // pcclient
		1, 0, 0, 0, 4, 0x80, 0, 0, 1, // event type
		1, 0, 0, 0, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, // event data
	}, false)
	f.Add([]byte{
		5, 0, 0, 0, 23, // pcclient
		0, 0, 0, 0, 4, 0x80, 0, 0, 1, // event type
		2, 0, 0, 0, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, // event data
	}, false)

	f.Add([]byte{
		42, 0, 0, 0, 9, // stboot
		42, 0, 0, 0, 4, 1, 2, 3, 4, // measurement
	}, true)
	f.Add([]byte{
		42, 0, 0, 0, 9, // stboot
		41, 0, 0, 0, 4, 1, 2, 3, 4, // measurement
	}, false)
	f.Add([]byte{
		42, 0, 0, 0, 9, // stboot
		42, 0, 0, 0, 3, 1, 2, 3, 4, // measurement
	}, false)

	f.Add([]byte{
		43, 0, 0, 0, 0, // empty content
	}, true)

	f.Add([]byte{
		44, 0, 0, 0, 0, // unknown content
	}, false)

	f.Add([]byte{}, false)
	f.Add([]byte{
		42, 0, 0, 0, 9, // stboot
	}, false)
	f.Add([]byte{
		5, 0, 0, 0, 23, // pcclient
		0, 0, 0, 0, 4, 0x80, 0, 0, 1, // event type
	}, false)
	f.Add([]byte{
		5, 0, 0, 0, 23, // pcclient
	}, false)
	f.Add([]byte{
		4, 0, 0, 0, 6, // content cel
		81, 0, 0, 0, 1,
	}, false)
	f.Add([]byte{
		4, 0, 0, 0, 6, // content cel
	}, false)
	f.Add([]byte{
		4, 0, 0, 0, 23, // content cel
		1, 0, 0, 0, 18, // cel version
		0, 0, 0, 0, 2, 1, 2, // major version
	}, false)
	f.Add([]byte{
		4, 0, 0, 0, 23, // content cel
		1, 0, 0, 0, 18, // cel version
	}, false)
	f.Add([]byte{
		4, 0, 0, 0, 23, // content cel
		1, 0, 0, 0, 18, // cel version
		0, 0, 0, 0, 2, 1,
	}, false)
	f.Add([]byte{
		5, 0, 0, 0, 23, // pcclient
		0, 0, 0, 0, 4, 0x80, 0, 0,
	}, false)

	f.Fuzz(func(t *testing.T, data []byte, outcome bool) {
		val := new(content)
		buf := bytes.NewBuffer(data)
		err := val.decode(buf)
		if outcome {
			test.NoErr(t, err)
		} else {
			test.WantErr(t, err)
		}
	})
}

func FuzzRecordCodecError(f *testing.F) {
	f.Add([]byte{
		0, 0, 0, 0, 4, 1, 2, 3, 4, // recnum
		1, 0, 0, 0, 4, 1, 2, 3, 4, // pcr
		3, 0, 0, 0, 8, sha1Alg, 0, 0, 0, 3, 1, 2, 3, //digests
		4, 0, 0, 0, 23, // content cel
		1, 0, 0, 0, 18, // cel version
		0, 0, 0, 0, 2, 1, 2, // major version
		1, 0, 0, 0, 2, 3, 4, // minor version
	}, true)
	f.Add([]byte{
		0, 0, 0, 0, 4, 1, 2, 3, 4, // recnum
		1, 0, 0, 0, 4, 1, 2, 3, 4, // pcr
		4, 0, 0, 0, 8, sha1Alg, 0, 0, 0, 3, 1, 2, 3, //digests
	}, false)
	f.Add([]byte{
		0, 0, 0, 0, 4, 1, 2, 3, 4, // recnum
		2, 0, 0, 0, 4, 1, 2, 3, 4, // pcr
	}, false)
	f.Add([]byte{
		1, 0, 0, 0, 4, 1, 2, 3, 4, // recnum
	}, false)

	f.Fuzz(func(t *testing.T, data []byte, outcome bool) {
		val := new(record)
		buf := bytes.NewBuffer(data)
		err := val.decode(buf)
		if outcome {
			test.NoErr(t, err)
		} else {
			test.WantErr(t, err)
		}
	})
}
