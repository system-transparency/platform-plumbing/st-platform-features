package ra

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"strings"
	"time"
)

const (
	sha1Alg   = 4
	sha256Alg = 11
	sha384Alg = 12
	sha512Alg = 13
	sm2Alg    = 27
	sm3Alg    = 18

	stbootMeasurement = 1
)

var (
	// Unsupported event log entry.
	ErrUnsupported = errors.New("unsupported")

	tcgSpecIdEvent = []byte("Spec ID Event03")

	eventTypes = map[uint32]string{
		// Legacy BIOS
		0x00000000: "EV_PREBOOT_CERT",
		0x00000001: "EV_POST_CODE",
		0x00000002: "EV_UNUSED",
		0x00000003: "EV_NO_ACTION",
		0x00000004: "EV_SEPARATOR",
		0x00000005: "EV_ACTION",
		0x00000006: "EV_EVENT_TAG",
		0x00000007: "EV_S_CRTM_CONTENTS",
		0x00000008: "EV_S_CRTM_VERSION",
		0x00000009: "EV_CPU_MICROCODE",
		0x0000000a: "EV_PLATFORM_CONFIG_FLAGS",
		0x0000000b: "EV_TABLE_OF_DEVICES",
		0x0000000c: "EV_COMPACT_HASH",
		0x0000000d: "EV_IPL",
		0x0000000e: "EV_IPL_PARTITION_DATA",
		0x0000000f: "EV_NONHOST_CODE",
		0x00000010: "EV_NONHOST_CONFIG",
		0x00000011: "EV_NONHOST_INFO",
		0x00000012: "EV_OMIT_BOOT_DEVICE_EVENTS",

		// EFI
		0x80000001: "EV_EFI_VARIABLE_DRIVER_CONFIG",
		0x80000002: "EV_EFI_VARIABLE_BOOT",
		0x80000003: "EV_EFI_BOOT_SERVICES_APPLICATION",
		0x80000004: "EV_EFI_BOOT_SERVICES_DRIVER",
		0x80000005: "EV_EFI_RUNTIME_SERVICES_DRIVER",
		0x80000006: "EV_EFI_GPT_EVENT",
		0x80000007: "EV_EFI_ACTION",
		0x80000008: "EV_EFI_PLATFORM_FIRMWARE_BLOB",
		0x80000009: "EV_EFI_HANDOFF_TABLES",
		0x8000000a: "EV_EFI_PLATFORM_FIRMWARE_BLOB2",
		0x8000000b: "EV_EFI_HANDOFF_TABLES2",
		0x8000000c: "EV_EFI_VARIABLE_BOOT2",
		0x80000010: "EV_EFI_HCRTM_EVENT",
		0x800000e0: "EV_EFI_VARIABLE_AUTHORITY",
		0x800000e1: "EV_EFI_SPDM_FIRMWARE_BLOB",
		0x800000e2: "EV_EFI_SPDM_FIRMWARE_CONFIG",
	}
)

func encodetl(wr io.Writer, tag uint8, length uint32) error {
	if _, err := wr.Write([]byte{tag}); err != nil {
		return err
	}
	return binary.Write(wr, binary.BigEndian, length)
}

func decodetl(rd io.Reader, tag uint8, length uint32) error {
	t, l, err := peektl(rd)
	if err != nil {
		return err
	}
	if t != tag {
		return ErrUnsupported
	}
	if l != length {
		return ErrUnsupported
	}
	return nil
}

func peektl(rd io.Reader) (uint8, uint32, error) {
	var tag uint8
	if err := binary.Read(rd, binary.BigEndian, &tag); err != nil {
		return 0, 0, err
	}
	var length uint32
	if err := binary.Read(rd, binary.BigEndian, &length); err != nil {
		return 0, 0, err
	}
	return tag, length, nil
}

type recnum uint32

func (r *recnum) encode(wr io.Writer) error {
	if err := encodetl(wr, 0, 4); err != nil {
		return err
	}
	return binary.Write(wr, binary.BigEndian, uint32(*r))
}

func (r *recnum) decode(rd io.Reader) error {
	if err := decodetl(rd, 0, 4); err != nil {
		return err
	}
	return binary.Read(rd, binary.BigEndian, (*uint32)(r))
}

type pcr uint32

func (p *pcr) encode(wr io.Writer) error {
	if err := encodetl(wr, 1, 4); err != nil {
		return err
	}
	return binary.Write(wr, binary.BigEndian, *p)
}

func (p *pcr) decode(rd io.Reader) error {
	if err := decodetl(rd, 1, 4); err != nil {
		return err
	}
	return binary.Read(rd, binary.BigEndian, p)
}

type digest struct {
	Alg    int
	Digest []byte
}

func (d *digest) encode(wr io.Writer) error {
	if err := encodetl(wr, uint8(d.Alg), uint32(len(d.Digest))); err != nil {
		return err
	}
	_, err := wr.Write(d.Digest)
	return err
}

func (d *digest) decode(rd io.Reader) error {
	t, l, err := peektl(rd)
	if err != nil {
		return err
	}
	d.Alg = int(t)
	d.Digest = make([]byte, l)
	_, err = io.ReadFull(rd, d.Digest)
	return err
}

type digests []digest

func (d *digests) encode(wr io.Writer) error {
	var buf bytes.Buffer
	for _, dd := range *d {
		if err := dd.encode(&buf); err != nil {
			return err
		}
	}

	if err := encodetl(wr, 3, uint32(buf.Len())); err != nil {
		return err
	}
	_, err := wr.Write(buf.Bytes())
	return err
}

func (d *digests) decode(rd io.Reader) error {
	t, l, err := peektl(rd)
	if err != nil {
		return err
	}
	if t != 3 {
		return ErrUnsupported
	}
	lrd := io.LimitReader(rd, int64(l))
	for {
		var dd digest
		if err := dd.decode(lrd); err != nil {
			if errors.Is(err, io.EOF) {
				return nil
			}
			return err
		}
		*d = append(*d, dd)
	}
}

type content struct {
	Cel         *cel
	PcClientStd *pcclient
	Stboot      *stboot
}

func (c *content) encode(wr io.Writer) error {
	var buf bytes.Buffer
	var err error
	var tag uint8
	if c.Cel != nil {
		tag = 4
		err = c.Cel.encode(&buf)
	} else if c.PcClientStd != nil {
		tag = 5
		err = c.PcClientStd.encode(&buf)
	} else if c.Stboot != nil {
		tag = 42
		err = c.Stboot.encode(&buf)
	} else {
		tag = 43
	}
	if err != nil {
		return err
	}

	if err := encodetl(wr, tag, uint32(buf.Len())); err != nil {
		return err
	}
	_, err = wr.Write(buf.Bytes())
	return err
}

func (c *content) decode(rd io.Reader) error {
	t, l, err := peektl(rd)
	if err != nil {
		return err
	}
	lrd := io.LimitReader(rd, int64(l))
	switch t {
	case 4:
		c.Cel = new(cel)
		return c.Cel.decode(lrd)
	case 5:
		c.PcClientStd = new(pcclient)
		return c.PcClientStd.decode(lrd)
	case 42:
		c.Stboot = new(stboot)
		return c.Stboot.decode(lrd)
	case 43:
		return nil
	default:
		return ErrUnsupported
	}
}

type cel struct {
	Version     *version
	FirmwareEnd bool
	Timestamp   *time.Time
	Transition  *uint8
}

func (c *cel) encode(wr io.Writer) error {
	var buf bytes.Buffer
	var err error
	var tag uint8
	if c.Version != nil {
		tag = 1
		err = c.Version.encode(&buf)
	} else if c.FirmwareEnd {
		tag = 2
	} else if c.Timestamp != nil {
		tag = 80
		ts := uint64(c.Timestamp.Unix())
		err = binary.Write(&buf, binary.BigEndian, &ts)
	} else if c.Transition != nil {
		tag = 81
		err = buf.WriteByte(*c.Transition)
	} else {
		return ErrUnsupported
	}
	if err != nil {
		return err
	}

	if err := encodetl(wr, tag, uint32(buf.Len())); err != nil {
		return err
	}
	_, err = wr.Write(buf.Bytes())
	return err
}

func (c *cel) decode(rd io.Reader) error {
	t, l, err := peektl(rd)
	if err != nil {
		return err
	}
	lrd := io.LimitReader(rd, int64(l))
	switch t {
	case 1:
		c.Version = new(version)
		return c.Version.decode(lrd)
	case 2:
		c.FirmwareEnd = true
	case 80:
		var ts uint64
		if err := binary.Read(lrd, binary.BigEndian, &ts); err != nil {
			return err
		}
		t := time.Unix(int64(ts), 0).UTC()
		c.Timestamp = &t
	case 81:
		b := make([]byte, 1)
		if _, err := io.ReadFull(lrd, b); err != nil {
			return err
		}
		c.Transition = &b[0]
	default:
		return ErrUnsupported
	}
	return nil
}

type version struct {
	Major, Minor uint16
}

func (v *version) encode(wr io.Writer) error {
	if err := encodetl(wr, 0, 2); err != nil {
		return err
	}
	if err := binary.Write(wr, binary.BigEndian, v.Major); err != nil {
		return err
	}
	if err := encodetl(wr, 1, 2); err != nil {
		return err
	}
	return binary.Write(wr, binary.BigEndian, v.Minor)
}

func (v *version) decode(rd io.Reader) error {
	if err := decodetl(rd, 0, 2); err != nil {
		return err
	}
	if err := binary.Read(rd, binary.BigEndian, &v.Major); err != nil {
		return err
	}
	if err := decodetl(rd, 1, 2); err != nil {
		return err
	}
	return binary.Read(rd, binary.BigEndian, &v.Minor)
}

type stboot struct {
	Measurement uint32
}

func (p *stboot) encode(wr io.Writer) error {
	if err := encodetl(wr, 42, 4); err != nil {
		return err
	}
	return binary.Write(wr, binary.BigEndian, p.Measurement)
}

func (p *stboot) decode(rd io.Reader) error {
	if err := decodetl(rd, 42, 4); err != nil {
		return err
	}
	return binary.Read(rd, binary.BigEndian, &p.Measurement)
}

type pcclient struct {
	Type uint32
	Data []byte
}

func (p *pcclient) encode(wr io.Writer) error {
	if err := encodetl(wr, 0, 4); err != nil {
		return err
	}
	if err := binary.Write(wr, binary.BigEndian, p.Type); err != nil {
		return err
	}
	if err := encodetl(wr, 1, uint32(len(p.Data))); err != nil {
		return err
	}
	_, err := wr.Write(p.Data)
	return err
}

func (p *pcclient) decode(rd io.Reader) error {
	if err := decodetl(rd, 0, 4); err != nil {
		return err
	}
	if err := binary.Read(rd, binary.BigEndian, &p.Type); err != nil {
		return err
	}
	t, l, err := peektl(rd)
	if err != nil {
		return err
	}
	if t != 1 {
		return ErrUnsupported
	}
	if l > 0 {
		p.Data = make([]byte, l)
		_, err = io.ReadFull(rd, p.Data)
		return err
	}
	return nil
}

type record struct {
	Recnum  recnum
	Pcr     pcr
	Digests digests
	Content content
}

func (r *record) encode(wr io.Writer) error {
	if err := r.Recnum.encode(wr); err != nil {
		return err
	}
	if err := r.Pcr.encode(wr); err != nil {
		return err
	}
	if err := r.Digests.encode(wr); err != nil {
		return err
	}
	return r.Content.encode(wr)
}

func (r *record) decode(rd io.Reader) error {
	if err := r.Recnum.decode(rd); err != nil {
		return err
	}
	if err := r.Pcr.decode(rd); err != nil {
		return err
	}
	if err := r.Digests.decode(rd); err != nil {
		return err
	}
	return r.Content.decode(rd)
}

func (r *record) String() string {
	if std := r.Content.PcClientStd; std != nil {
		data := std.Data
		if len(data) > 32 {
			data = data[:32]
		}
		mapascii := func(r rune) rune {
			if r < 32 || r > 126 {
				return '.'
			} else {
				return r
			}
		}
		str := strings.Map(mapascii, string(data))
		ty, ok := eventTypes[std.Type]
		if !ok {
			ty = fmt.Sprintf("%08x", std.Type)
		}
		return fmt.Sprintf("%3d: PCR[%02d] %s %x (%s) %x", r.Recnum, r.Pcr, ty, data, str, sha256Digest(r.Digests))
	} else {
		return fmt.Sprintf("%3d: PCR[%02d] %08x", r.Recnum, r.Pcr, r.Digests[0].Digest)
	}
}

func encodeRecords(wr io.Writer, r []record) error {
	for _, rec := range r {
		if err := rec.encode(wr); err != nil {
			return err
		}
	}
	return nil
}
