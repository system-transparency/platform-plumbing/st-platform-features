package ra

import (
	"bytes"
	"crypto"
	"crypto/sha256"
	"encoding/binary"
	"errors"
	"io"
	"strings"
	"unicode/utf16"
)

const (
	// Event log entry types. See TCG PC Client Platform TPM Profile
	// Specification for TPM 2.0 version 1.05, rev 23.
	efiBootServicesApplication = 0x80000003
	efiHandoffTables           = 0x80000009
	efiHandoffTables2          = 0x8000000b
	efiAction                  = 0x80000007
	noAction                   = 0x00000003
	separator                  = 0x4
	taggedEvent                = 0x6
	initialProgramLoader       = 0xd
)

var (
	// TXT startup event
	startupLocality = []byte("StartupLocality")
	// Spec ID event wrapping the TPM 2.0 event log
	specIdEvent = []byte("Spec ID Event03")

	// EFI boot services application EFI action string.
	exitBootServices       = []byte("Exit Boot Services Invocation")
	exitBootServicesReturn = []byte("Exit Boot Services Returned with Success")

	// Linux drivers/firmware/efi/libstub/efi-stub-helper.c and efistub.h
	linuxLoadOptions       = 0x8f3b22ed
	linuxLoadOptionsString = []byte("LOADED_IMAGE::LoadOptions\x00")
	linuxInitrd            = 0x8f3b22ec
	linuxInitrdString      = []byte("Linux initrd\x00")

	// Magic values prefixing the endorsement data before signing. Makes sure
	// attackers cannot reuse signatures.
	stbootEndorsementMagic = [4]byte{'S', 'T', 'B', 'E'}
	ospkgEndorsementMagic  = [4]byte{'O', 'S', 'P', 'E'}

	// section names and their measuring order are defined in
	// src/fundamentals/uki.{c,h} in the systemd source. we're using v255 here.
	ukiSectionOrder = map[string]int{
		".linux":   0,
		".initrd":  1,
		".cmdline": 2,
		".osrel":   3,
		".splash":  4,
		".dtb":     5,
		".uname":   6,
		".sbat":    7,
		".pcrsig":  8,
		".pcrpkey": 9,
	}

	// strings measured by systemd during boot. defined in the manpage of
	// systemd-pcrphase.service(8).
	systemdInitrdPcrphases = []string{
		"enter-initrd",
		"leave-initrd",
	}
	systemdReadyPcrphases = []string{
		"sysinit",
		"ready",
	}

	// Invalid event log sequence.
	ErrInvalidState = errors.New("invalid state")
	// Invalid EFI application. Either stboot or the embedded Linux kernel.
	ErrInvalidPE = errors.New("invalid PE file")
	// Invalid stboot UKI sections.
	ErrInvalidStub = errors.New("invalid stboot stub")
)

func sha256Digest(ds digests) []byte {
	for _, d := range ds {
		if d.Alg == sha256Alg {
			return d.Digest
		}
	}
	return nil
}

type section struct {
	Name  string
	Start int
	End   int
}

type peFile struct {
	Sections     []section
	Authentihash []byte
}

func parsePe(pecoff []byte, alg crypto.Hash) (*peFile, error) {
	rd16 := func(off int) (uint16, error) {
		if off+2 > len(pecoff) {
			return 0, ErrInvalidPE
		}
		return uint16(pecoff[off]) | uint16(pecoff[off+1])<<8, nil
	}

	rd32 := func(off int) (uint32, error) {
		if off+4 > len(pecoff) {
			return 0, ErrInvalidPE
		}
		return uint32(pecoff[off]) | uint32(pecoff[off+1])<<8 | uint32(pecoff[off+2])<<16 | uint32(pecoff[off+3])<<24, nil
	}

	// PE/COFF file signature
	sigOffset, err := rd32(0x3c)
	if err != nil {
		return nil, err
	}
	peSignature, err := rd32(int(sigOffset))
	if err != nil {
		return nil, err
	}
	if peSignature != 0x4550 {
		return nil, ErrInvalidPE
	}

	// PE/COFF and optional header size
	peCoffOffset := int(sigOffset) + 4
	optHeaderSz, err := rd16(peCoffOffset + 16)
	if err != nil {
		return nil, err
	}
	if optHeaderSz < 68 {
		return nil, ErrInvalidPE
	}
	sectionCount, err := rd16(peCoffOffset + 2)
	if err != nil {
		return nil, err
	}

	// Optional header and it's magic number
	optHeaderOffset := peCoffOffset + 20
	optHeaderMagic, err := rd16(optHeaderOffset)
	if err != nil {
		return nil, err
	}

	// attribute certificate's entry in the data directory
	certDirectoryEntry := optHeaderOffset
	sectionHeaderOffset := optHeaderOffset
	switch optHeaderMagic {
	case 0x20b:
		certDirectoryEntry += 144
		sectionHeaderOffset += 232 + 8
	case 0x10b:
		certDirectoryEntry += 128
		sectionHeaderOffset += 216 + 8
	default:
		return nil, ErrInvalidPE
	}

	// attribute certificate
	certOffset, err := rd32(certDirectoryEntry)
	if err != nil {
		return nil, err
	}
	certSz, err := rd32(certDirectoryEntry + 4)
	if err != nil {
		return nil, err
	}

	sections := make([]section, 0, sectionCount)
	for sectionNo := 0; sectionNo < int(sectionCount); sectionNo += 1 {
		pointer := sectionHeaderOffset + sectionNo*40
		name := bytes.TrimRight(pecoff[pointer:pointer+8], "\x00")
		// PointerToRawData
		start, err := rd32(pointer + 20)
		if err != nil {
			return nil, err
		}
		// VirtualSize
		sz, err := rd32(pointer + 8)
		if err != nil {
			return nil, err
		}

		sec := section{
			Name:  string(name),
			Start: int(start),
			End:   int(start + sz),
		}

		sections = append(sections, sec)
	}

	// hash up to the checksum field
	// skip checksum, hash up to the certificate table entry in data directory
	// skip certificate table entry, hash up to the end of the file headers
	// hash sections ordered by disk offset, skip all empty sections
	// hash all data after the sections before the end of the file

	headerChecksumOffset := optHeaderOffset + 64

	h := alg.New()
	h.Write(pecoff[0:headerChecksumOffset])
	h.Write(pecoff[headerChecksumOffset+4 : certDirectoryEntry])
	if certOffset > 0 {
		h.Write(pecoff[certDirectoryEntry+8 : certOffset])
		h.Write(pecoff[certOffset+certSz:])
	} else {
		h.Write(pecoff[certDirectoryEntry+8:])
	}

	pf := peFile{
		Authentihash: h.Sum(nil),
		Sections:     sections,
	}

	return &pf, nil
}

type platformFirmware struct {
	DRTMInit              []byte
	Firmware              []record
	HandoffTables         []record
	InterExitBootServices []record
}

func (pf *platformFirmware) pack(wr io.Writer) error {
	// DRMT init
	if err := binary.Write(wr, binary.LittleEndian, uint32(len(pf.DRTMInit))); err != nil {
		return err
	}
	if _, err := wr.Write(pf.DRTMInit); err != nil {
		return err
	}

	// Pre 1st UEFI Boot Application. we assume it's stboot
	var fw bytes.Buffer
	if err := encodeRecords(&fw, pf.Firmware); err != nil {
		return err
	}
	if err := binary.Write(wr, binary.LittleEndian, uint32(fw.Len())); err != nil {
		return err
	}
	if _, err := wr.Write(fw.Bytes()); err != nil {
		return err
	}

	// Handoff tables that my be repeated before each boot application load event
	var tbls bytes.Buffer
	if err := encodeRecords(&tbls, pf.HandoffTables); err != nil {
		return err
	}
	if err := binary.Write(wr, binary.LittleEndian, uint32(tbls.Len())); err != nil {
		return err
	}
	if _, err := wr.Write(tbls.Bytes()); err != nil {
		return err
	}

	// Measurements between ExitBootServices and ExitBootServices return
	var ebs bytes.Buffer
	if err := encodeRecords(&ebs, pf.InterExitBootServices); err != nil {
		return err
	}
	if err := binary.Write(wr, binary.LittleEndian, uint32(ebs.Len())); err != nil {
		return err
	}
	if _, err := wr.Write(ebs.Bytes()); err != nil {
		return err
	}

	return nil
}

func unpackPlatformFirmware(rd io.Reader) (*platformFirmware, error) {
	var pf platformFirmware

	var sz uint32
	if err := binary.Read(rd, binary.LittleEndian, &sz); err != nil {
		return nil, err
	}
	pf.DRTMInit = make([]byte, sz)
	if _, err := io.ReadFull(rd, pf.DRTMInit); err != nil {
		return nil, err
	}

	if err := binary.Read(rd, binary.LittleEndian, &sz); err != nil {
		return nil, err
	}
	fw := make([]byte, sz)
	if _, err := io.ReadFull(rd, fw); err != nil {
		return nil, err
	}
	r := bytes.NewReader(fw)
	for r.Len() > 0 {
		var rec record
		if err := rec.decode(r); err != nil {
			return nil, err
		}
		pf.Firmware = append(pf.Firmware, rec)
	}

	if err := binary.Read(rd, binary.LittleEndian, &sz); err != nil {
		return nil, err
	}
	tbls := make([]byte, sz)
	if _, err := io.ReadFull(rd, tbls); err != nil {
		return nil, err
	}
	r = bytes.NewReader(tbls)
	for r.Len() > 0 {
		var rec record
		if err := rec.decode(r); err != nil {
			return nil, err
		}
		pf.HandoffTables = append(pf.HandoffTables, rec)
	}

	if err := binary.Read(rd, binary.LittleEndian, &sz); err != nil {
		return nil, err
	}
	ebs := make([]byte, sz)
	if _, err := io.ReadFull(rd, ebs); err != nil {
		return nil, err
	}
	r = bytes.NewReader(ebs)
	for r.Len() > 0 {
		var rec record
		if err := rec.decode(r); err != nil {
			return nil, err
		}
		pf.InterExitBootServices = append(pf.InterExitBootServices, rec)
	}

	return &pf, nil
}

// DRTM init sequence
func platformInit(ev record, ret *platformFirmware) string {
	std := ev.Content.PcClientStd
	var data []byte
	if std != nil {
		data = std.Data
	}

	if std.Type == noAction {
		// skip the spec id event
		specid := ev.Pcr == 0 && bytes.HasPrefix(data, specIdEvent)
		if specid {
			return "init"
		}

		// DRTM init: "StartupLocality" + uint16(locality)
		drtm_init := ev.Pcr == 0 && len(data) == len(startupLocality)+2 &&
			bytes.HasPrefix(data, startupLocality)
		if drtm_init {
			ret.DRTMInit = make([]byte, 32)
			ret.DRTMInit[30] = data[len(data)-2]
			ret.DRTMInit[31] = data[len(data)-1]
			return "uefi"
		}
	} else {
		if d := sha256Digest(ev.Digests); d != nil {
			ret.Firmware = append(ret.Firmware, record{
				Pcr: ev.Pcr,
				Digests: []digest{
					{
						Alg:    sha256Alg,
						Digest: d,
					},
				},
				Content: ev.Content,
			})
		}
	}

	return "uefi"
}

// Separator
func platformUefi(ev record, ret *platformFirmware) string {
	std := ev.Content.PcClientStd

	if d := sha256Digest(ev.Digests); d != nil {
		ret.Firmware = append(ret.Firmware, record{
			Pcr: ev.Pcr,
			Digests: []digest{
				{
					Alg:    sha256Alg,
					Digest: d,
				},
			},
			Content: ev.Content,
		})
	}

	sep := ev.Pcr == 4 && std.Type == separator
	if sep {
		return "preboot"
	}

	return "uefi"
}

// First UEFI boot app or EFI action "Calling EFI Application from Boot Option"
func platformPreBoot(ev record, ret *platformFirmware) string {
	std := ev.Content.PcClientStd

	bootsvcapp := ev.Pcr == 4 && std.Type == efiBootServicesApplication
	if bootsvcapp {
		return "boot"
	}

	if d := sha256Digest(ev.Digests); d != nil {
		rec := record{
			Pcr: ev.Pcr,
			Digests: []digest{
				{
					Alg:    sha256Alg,
					Digest: d,
				},
			},
			Content: ev.Content,
		}
		ret.Firmware = append(ret.Firmware, rec)

		handofftbl := ev.Pcr == 1 && (std.Type == efiHandoffTables || std.Type == efiHandoffTables2)
		if handofftbl {
			ret.HandoffTables = append(ret.HandoffTables, rec)
		}
	}

	return "preboot"
}

// stboot measurements
func platformEbs(ev record, ret *platformFirmware) string {
	std := ev.Content.PcClientStd

	if d := sha256Digest(ev.Digests); d != nil {
		ret.InterExitBootServices = append(ret.InterExitBootServices, record{
			Pcr: ev.Pcr,
			Digests: []digest{
				{
					Alg:    sha256Alg,
					Digest: d,
				},
			},
			Content: ev.Content,
		})
	}

	// exit boot services return
	ebs := ev.Pcr == 5 && std.Type == efiAction &&
		bytes.Equal(std.Data, exitBootServicesReturn)
	if ebs {
		return "postboot"
	}

	return "ebs"
}

func platformBoot(ev record, ret *platformFirmware) string {
	std := ev.Content.PcClientStd

	// exit boot services
	ebs := ev.Pcr == 5 && std.Type == efiAction &&
		bytes.Equal(std.Data, exitBootServices)
	if ebs {
		ret.InterExitBootServices = append(ret.InterExitBootServices, record{
			Pcr: ev.Pcr,
			Digests: []digest{
				{
					Alg:    sha256Alg,
					Digest: sha256Digest(ev.Digests),
				},
			},
			Content: ev.Content,
		})

		return "ebs"
	}

	return "boot"
}

func endorsePlatformFirmware(log []record, s Signer) ([]byte, []byte, error) {
	pltfw, err := extractPlatformFirmware(log)
	if err != nil {
		return nil, nil, err
	}

	var buf bytes.Buffer

	if err := pltfw.pack(&buf); err != nil {
		return nil, nil, err
	}

	sig, err := s.Sign(buf.Bytes())
	return buf.Bytes(), sig, err
}

func extractPlatformFirmware(log []record) (*platformFirmware, error) {
	// mandatory pre separator events for each pcr

	var pltfw platformFirmware

	state := "init"

	for _, ev := range log {
		switch state {
		case "init":
			// drtm init sequence
			state = platformInit(ev, &pltfw)

		case "uefi":
			// separator
			state = platformUefi(ev, &pltfw)

		case "preboot":
			// 1st boot services app
			state = platformPreBoot(ev, &pltfw)

		case "boot":
			// exit boot services
			state = platformBoot(ev, &pltfw)

		case "ebs": // -> inter ebs measurements
			// exit boot services return
			state = platformEbs(ev, &pltfw)

		case "postboot":

		default:
			return nil, ErrInvalidState
		}
	}

	switch state {
	case "boot":
		// no exit boot services logged
		fallthrough

	case "postboot":
		return &pltfw, nil

	default:
		return nil, ErrInvalidState
	}
}

// Signed OS package measurements. This data structure contains the TPM
// measurements influenced by the OS package fetched and booted by stboot.
// This is one of the three endorsements required for verifying a [Quote].
type OsPackageEndorsement struct {
	OsPkgMeasurements  []byte
	StbootMeasurements []byte
	Signature          []byte
}

// Options for [EndorseOsPackage].
type OsPackageOptions struct {
	// For the systemd related measurements see TPM2_PCR_MEASUREMENTS.md in the
	// systemd source.

	// Include systemd-pcrphrase.server measurements "enter-initrd" and
	// "leave-initrd".
	SystemdInitrdPhases bool
	// Include systemd-pcrphrase.server measurements "sysinit" and "ready".
	SystemdReadyPhases bool
	// Measure the systemd machine-id like systemd-pcrmanager.service.
	SystemdMachineID []byte
}

// Creates an OS package endorsement. The endorsement contains the TPM
// measurements influenced by the OS package. The endorsement is created by
// analyzing the OS package archive .zip file and JSON descriptor. The result
// is signed using the provided signer.
func EndorseOsPackage(desc, archive []byte, s Signer, opts *OsPackageOptions) (*OsPackageEndorsement, error) {
	if opts == nil {
		opts = new(OsPackageOptions)
	}

	m1 := createStbootDetailMeasurements(desc, archive)
	var stbootbuf bytes.Buffer
	err := encodeRecords(&stbootbuf, m1)
	if err != nil {
		return nil, err
	}

	m2 := createSystemdMeasurements(opts.SystemdInitrdPhases, opts.SystemdReadyPhases, opts.SystemdMachineID)
	var ospkgbuf bytes.Buffer
	err = encodeRecords(&ospkgbuf, m2)
	if err != nil {
		return nil, err
	}

	ret := OsPackageEndorsement{
		StbootMeasurements: stbootbuf.Bytes(),
		OsPkgMeasurements:  ospkgbuf.Bytes(),
	}
	msg := encodeByteArrays(ospkgEndorsementMagic, ret.StbootMeasurements, ret.OsPkgMeasurements)
	ret.Signature, err = s.Sign(msg)

	return &ret, err
}

// Stboot endorsement. This data structure contains the TPM measurements that
// are the result of booting a particular stboot binary. This is one of the
// three endorsements required for verifying a [Quote].
type StbootEndorsement struct {
	// Measurements of the systemd-stub embedded in the stboot UKI.
	StubMeasurements []byte
	// EFI boot services application measurements of the stboot binary's
	// Authenticode hash.
	StbootLoadEvent []byte
	// Measurements that stboot makes of it's compiled-in trust policy, root CA
	// and HTTPS roots.
	StbootMeasurements []byte
	// Signature of the endorsement data.
	Signature []byte
}

// Options for [EndorseStboot].
type StbootOptions struct {
	// Include measurements the Linux kernel makes of it's initrd and cmdline.
	// This should be enable if the kernel compile into stboot has this enabled.
	LinuxMeasures bool
}

// Creates a stboot endorsement. The endorsement is generated from the stboot
// EFI application and the compiled-in trust policy, root CA and HTTPS roots.
// The result is signed using the provided signer. The opts parameter can be
// used to further tune the endorsement. If nil, default options are used.
func EndorseStboot(efiapp, rootCa, httpsRoots, trustPolicy, uxid, datachannel []byte, s Signer, opts *StbootOptions) (*StbootEndorsement, error) {
	if opts == nil {
		opts = new(StbootOptions)
	}

	stub, err := createSystemdStubMeasurements(efiapp)
	if err != nil {
		return nil, err
	}
	var stubbuf bytes.Buffer
	err = encodeRecords(&stubbuf, stub)
	if err != nil {
		return nil, err
	}

	load, err := createStbootLoadEvent(efiapp)
	if err != nil {
		return nil, err
	}
	if opts.LinuxMeasures {
		linux, err := createLinuxMeasurements(efiapp)
		if err != nil {
			return nil, err
		}
		load = append(load, linux...)
	}
	var loadbuf bytes.Buffer
	err = encodeRecords(&loadbuf, load)
	if err != nil {
		return nil, err
	}

	stboot := append([]record{}, createStbootAuthorityMeasurements(trustPolicy, rootCa, httpsRoots)...)
	stboot = append(stboot, createStbootIdentityMeasurements(uxid, datachannel)...)
	var stbootbuf bytes.Buffer
	err = encodeRecords(&stbootbuf, stboot)
	if err != nil {
		return nil, err
	}

	ret := StbootEndorsement{
		StubMeasurements:   stubbuf.Bytes(),
		StbootLoadEvent:    loadbuf.Bytes(),
		StbootMeasurements: stbootbuf.Bytes(),
	}
	msg := encodeByteArrays(
		stbootEndorsementMagic, ret.StubMeasurements, ret.StbootLoadEvent, ret.StbootMeasurements)
	ret.Signature, err = s.Sign(msg)

	return &ret, err
}

func createLinuxMeasurements(efiapp []byte) ([]record, error) {
	bootx64pe, err := parsePe(efiapp, crypto.SHA256)
	if err != nil {
		return nil, err
	}

	var initrdsum, cmdsum [32]byte

	for _, sec := range bootx64pe.Sections {
		switch sec.Name {
		case ".initrd":
			initrdsum = sha256.Sum256(efiapp[sec.Start:sec.End])

			// st-stub converts the kernel cmdline into UTF-16 after stripping all whitespace.
		case ".cmdline":
			buf := new(bytes.Buffer)
			cmdline := strings.TrimSpace(string(efiapp[sec.Start:sec.End]))
			for _, r := range cmdline {
				for _, wchar := range utf16.Encode([]rune{r}) {
					if err := binary.Write(buf, binary.LittleEndian, wchar); err != nil {
						return nil, err
					}
				}
			}
			cmdsum = sha256.Sum256(append(buf.Bytes(), 0, 0))
		}
	}

	var recs = make([]record, 0, 2)

	// PCR[09] Tagged event
	loadoptevbuf := new(bytes.Buffer)
	if err := binary.Write(loadoptevbuf, binary.LittleEndian, uint32(linuxLoadOptions)); err != nil {
		return nil, err
	}
	if err := binary.Write(loadoptevbuf, binary.LittleEndian, uint32(len(linuxLoadOptionsString))); err != nil {
		return nil, err
	}
	if _, err := loadoptevbuf.Write(linuxLoadOptionsString); err != nil {
		return nil, err
	}
	recs = append(recs, record{
		Pcr: 9,
		Digests: []digest{
			{
				Alg:    sha256Alg,
				Digest: cmdsum[:],
			},
		},
		Content: content{
			PcClientStd: &pcclient{
				Type: taggedEvent,
				Data: loadoptevbuf.Bytes(),
			},
		},
	})

	// PCR[09] Tagged event
	initrdevbuf := new(bytes.Buffer)
	if err := binary.Write(initrdevbuf, binary.LittleEndian, uint32(linuxInitrd)); err != nil {
		return nil, err
	}
	if err := binary.Write(initrdevbuf, binary.LittleEndian, uint32(len(linuxInitrdString))); err != nil {
		return nil, err
	}
	if _, err := initrdevbuf.Write(linuxInitrdString); err != nil {
		return nil, err
	}
	recs = append(recs, record{
		Pcr: 9,
		Digests: []digest{
			{
				Alg:    sha256Alg,
				Digest: initrdsum[:],
			},
		},
		Content: content{
			PcClientStd: &pcclient{
				Type: taggedEvent,
				Data: initrdevbuf.Bytes(),
			},
		},
	})

	return recs, nil
}

func createSystemdStubMeasurements(efiapp []byte) ([]record, error) {
	bootx64pe, err := parsePe(efiapp, crypto.SHA256)
	if err != nil {
		return nil, err
	}

	recs := make([]record, 0, 2+2*len(ukiSectionOrder))

	// PCR[04] UEFI Boot Services Application load event of the whole stboot EFI app
	recs = append(recs, record{
		Pcr: 4,
		Digests: []digest{
			{
				Alg:    sha256Alg,
				Digest: bootx64pe.Authentihash,
			},
		},
		Content: content{
			PcClientStd: &pcclient{
				Type: efiBootServicesApplication,
			},
		},
	})

	var to_measure = make([]section, 0, len(ukiSectionOrder))

	for _, sec := range bootx64pe.Sections {
		if _, ok := ukiSectionOrder[sec.Name]; !ok {
			continue
		}

		if sec.Name == ".linux" {
			to_measure = append([]section{sec}, to_measure...)
		} else {
			to_measure = append(to_measure, sec)
		}
	}

	for _, sec := range to_measure {
		// PCR[11]: Measure the ACSII section name
		ascii := sha256.Sum256(append([]byte(sec.Name), 0))
		recs = append(recs, record{
			Pcr: 11,
			Digests: []digest{
				{
					Alg:    sha256Alg,
					Digest: ascii[:],
				},
			},
			Content: content{
				PcClientStd: &pcclient{
					Type: initialProgramLoader,
					Data: []byte(sec.Name),
				},
			},
		})

		// PCR[11]: Measure the section contents
		sum := sha256.Sum256(efiapp[sec.Start:sec.End])
		recs = append(recs, record{
			Pcr: 11,
			Digests: []digest{
				{
					Alg:    sha256Alg,
					Digest: sum[:],
				},
			},
			Content: content{
				PcClientStd: &pcclient{
					Type: initialProgramLoader,
					Data: []byte(sec.Name),
				},
			},
		})
	}

	return recs, nil
}

func createStbootLoadEvent(efiapp []byte) ([]record, error) {
	bootx64pe, err := parsePe(efiapp, crypto.SHA256)
	if err != nil {
		return nil, err
	}

	for _, sec := range bootx64pe.Sections {
		if sec.Name != ".linux" {
			continue
		}

		linux, err := parsePe(efiapp[sec.Start:sec.End], crypto.SHA256)
		if err != nil {
			return nil, err
		}

		// PCR[04]: UEFI Boot Services Application load event of the embedded Linux
		rec := record{
			Pcr: 4,
			Digests: []digest{
				{
					Alg:    sha256Alg,
					Digest: linux.Authentihash,
				},
			},
			Content: content{
				PcClientStd: &pcclient{
					Type: efiBootServicesApplication,
				},
			},
		}
		return []record{rec}, nil
	}

	return nil, ErrInvalidStub
}

func createStbootDetailMeasurements(desc, archive []byte) []record {
	descsum := sha256.Sum256(desc)
	zipsum := sha256.Sum256(archive)
	zipsum = sha256.Sum256(zipsum[:])
	recs := []record{
		{
			Pcr: 12,
			Digests: []digest{
				{
					Digest: zipsum[:],
					Alg:    sha256Alg,
				},
			},
		},
		{
			Pcr: 12,
			Digests: []digest{
				{
					Alg:    sha256Alg,
					Digest: descsum[:],
				},
			},
		},
	}

	return recs
}

func createStbootAuthorityMeasurements(trustPolicy, rootCa, httpsRoots []byte) []record {
	tpsum := sha256.Sum256(trustPolicy)
	rootsum := sha256.Sum256(rootCa)
	httpsum := sha256.Sum256(httpsRoots)
	recs := []record{
		{
			Pcr: 13,
			Digests: []digest{
				{
					Digest: tpsum[:],
					Alg:    sha256Alg,
				},
			},
		},
		{
			Pcr: 13,
			Digests: []digest{
				{
					Alg:    sha256Alg,
					Digest: rootsum[:],
				},
			},
		},
		{
			Pcr: 13,
			Digests: []digest{
				{
					Alg:    sha256Alg,
					Digest: httpsum[:],
				},
			},
		},
	}

	return recs
}

func createStbootIdentityMeasurements(uxid, datachannel []byte) []record {
	uxsum := sha256.Sum256(uxid)
	dcsum := sha256.Sum256(datachannel)
	recs := []record{
		{
			Pcr: 14,
			Digests: []digest{
				{
					Digest: uxsum[:],
					Alg:    sha256Alg,
				},
			},
		},
		{
			Pcr: 14,
			Digests: []digest{
				{
					Alg:    sha256Alg,
					Digest: dcsum[:],
				},
			},
		},
	}

	return recs
}

func createSystemdMeasurements(sdinitrd, sdready bool, machineid []byte) []record {
	recs := make([]record, 0, len(systemdReadyPcrphases)+len(systemdInitrdPcrphases)+1)

	// PCR[11]: systemd-pcrphase.service measurements
	var phases []string
	if sdinitrd {
		phases = append(phases, systemdInitrdPcrphases...)
	}
	if sdready {
		phases = append(phases, systemdReadyPcrphases...)
	}
	for _, phase := range phases {
		sum := sha256.Sum256([]byte(phase))
		recs = append(recs,
			record{
				Pcr: 11,
				Digests: []digest{
					{
						Alg:    sha256Alg,
						Digest: sum[:],
					},
				},
			})
	}

	// XXX: systemd-gpt-auto-generator
	// systemd-pcrfs-root.service measurements
	// systemd-pcrfs@.service measurements for /var and maybe others that have x-systemd.pcrfs mount option

	if len(machineid) > 0 {
		// PCR[15]: systemd-pcrmachine.service measurements
		mid := sha256.Sum256(machineid)
		recs = append(recs,
			record{
				Pcr: 15,
				Digests: []digest{
					{
						Alg:    sha256Alg,
						Digest: mid[:],
					},
				},
			})
	}

	return recs
}
