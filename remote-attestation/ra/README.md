# Remote Attestation for System Transparency

The goal of this project is to implement remote attestation for stboot-enabled
servers that allows anyone to verify that:

- The server is owned by a particular entity,
- Was provisioned according to a human-readable protocol,
- Runs a firmware stack that has been marked as trusted by the operator, and
- Has booted an OS package and stboot that is trusted by the operator.

To accomplish this, it uses signed TPM 2.0 measurements called endorsements.
The library contains functions to create and sign endorsements, to generate TPM
2.0 quotes, and to verify the quotes against the endorsements.

## Quick Start

Before doing anything, make sure you have git LFS installed and checkout the
blobs managed by git LFS.

```bash
git lfs install
git lfs pull
```

The project contains a demo application that can generate all necessary keys,
endorsements, and quotes, as well as verify the quotes against the
endorsements. The application can be built with:

```bash
CGO_ENABLED=0 go build -o ra ./cmd
```

The endorsements we're about to create are signed with a private key, which is
created like this:

```bash
# writes key and key.pub into the current directory
./ra key -new
```

We need to create three endorsements: one for the UEFI firmware, one for the OS
package, and one for the stboot UKI. The OS package endorsement is created from
the archive and descriptor file.

```bash
# reads stimage.json & stimage.zip and writes ospkg.endorsement.bin
./ra endorse -ospkg stimage.json -key key
```

The stboot endorsement is created from the EFI application and the OS package
signing key CA, HTTPS root CA, and trust policy that are compiled into the
stboot binary. Replace the placeholders with the actual files.

```bash
# reads stboot.efi, root-cert.pem, isrgrootx1.pem, trust-policy.json
# writes stboot.endorsement.bin
./ra endorse -stboot stboot.efi -root-ca root-cert.pem \
    -https-roots isrgrootx1.pem -trust-policy trust-policy.json -key key
```

The third and last endorsement is for the UEFI firmware. It is created by
executing the enrollment protocol between the operator and the stboot-enabled
server. The first step is to generate an enrollment request on the server.

```bash
# on the server after copying the ra binary over
# writes localhost.request.bin
./ra endorse -tpm /dev/tpmrm0
```

The request file is then used to generate an enrollment challenge on the
operator's machine.

```bash 
# on the operator's machine after copying the localhost.request.bin file over
# writes localhost.challenge.bin
./ra endorse -platform localhost.request.bin -key key
```

The challenge file is then decrypted on the server using its TPM to retrieve
the endorsement.

```bash
# on the server after copying the localhost.challenge.bin file over
# writes localhost.endorsement.bin
./ra endorse -tpm /dev/tpmrm0 -challenge localhost.challenge.bin
```

Now that we have all three endorsements, we can generate a quote and do the
actual remote attestation.

```bash
# on the server, writes localhost.quote.bin
./ra quote -tpm /dev/tpmrm0 -endorsement localhost.endorsement.bin
```

The quote can be verified on the operator's machine using the OS package and
stboot endorsements, as well as the public half of the key used to sign the
endorsements.

```bash
# on the operator's machine after copying the localhost.quote.bin file over
ra quote -quote localhost.quote.bin -ospkg ospkg.endorsement.bin \
    -stboot stboot.endorsement.bin -public key.pub
```

The command will either run successfully or print a message detailing the
expected vs. received TPM 2.0 measurements. If the measurements match, the
server booted the OS package and stboot that the endorsements were created
from. As the endorsements were signed by the operator, anyone can do this
verification as long as there is an authenticated channel to exchange the
public half of the endorsement signing key pair.

## Hacking

The project has extensive unit and system tests. They can be run using the
usual `go test` command.

```bash
go test ./...
```

This tests the library functions against prerecorded TPM 2.0 messages and
responses. To test against a real or emulated TPM, the tests can be run with
the `TEST_TPM` environment variable set to the path of the TPM device or the
host and port of the TPM simulator. The project comes with a Dockerfile to
build a container that runs the swtpm TPM 2.0 simulator.

```bash
# build and run the swtpm container
(cd docker && docker build -t swtpm .)
docker run -d -p 2322:2321 swtpm

# run the test suite
TEST_TPM=localhost:2322 go test ./...

# alternatively, run the tests against a real TPM
TEST_TPM=/dev/tpmrm0 go test ./...
```

Adding `TEST_RECORD=1` will record the TPM 2.0 messages and responses to the
`testdata` directory. This is useful to add new tests or to debug failing
tests.

```bash
TEST_RECORD=1 TEST_TPM=localhost:2322 go test ./...
```

The project has its public API documented using godoc. The documentation can be
viewed by running godoc and opening the browser at `localhost:6060`.

```bash
godoc -http :6060
```

A detailed description of the project's architecture and an overview of the
attestation protocols can be found in the
[protocol documentation](./documentation/protocol.md).

In order to verify the TPM's Endorsement Key Certificate, the project needs to
have a list of trusted root certificates. The list is in the `certificates`
directory and contains a list of root and intermediate CA certificates compiled
by Microsoft. The latest version can be downloaded from the
[Microsoft website](https://go.microsoft.com/fwlink/?linkid=2097925). The .cab
file can be extracted using the `cabextract` tool.

## Status

| Machine | Status | Notes |
|---------|--------|-------|
| Supermicro X11SCH-LN4F | Not working | The X11SCH's SMBIOS table that is measured during boot contains volatile data. |
| QEMU | Working | |
| Lenovo SR630 V2 | Working | |
| Supermicro H12SSL-i | Working | |
| HPE ProLiant DL160 Gen10 | Working | Needs a firmware update to U31 v3.10 |
