package ra

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"math/big"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"strings"
	"testing"
	"time"

	test "system-transparency.org/ppg/ra/internal"
)

func TestRootCerts(t *testing.T) {
	_, err := vendorRoots()
	test.NoErr(t, err)
}

func TestValidateEkCert(t *testing.T) {
	eks := []string{
		"ekcert-0-dl160.der",
		"ekcert-1-dl160.der",
		"ekcert-0-h12ssl.der",
		"ekcert-1-h12ssl.der",
		"ekcert-0-m50cyp.der",
		"ekcert-1-m50cyp.der",
		"ekcert-0-r340.der",
		"ekcert-0-sr630.der",
		"ekcert-1-sr630.der",
	}

	for _, p := range eks {
		t.Run(p, func(t *testing.T) {
			buf, err := os.ReadFile(path.Join("testdata", p))
			test.NoErr(t, err)

			ekcert, err := x509.ParseCertificate(buf)
			test.NoErr(t, err)

			certs, err := vendorRoots()
			test.NoErr(t, err)

			_, err = verifyEndorsementCertificate(ekcert, certs, false)
			test.NoErr(t, err)
		})
	}
}

func TestCheckEkCrl(t *testing.T) {
	eks := []string{
		"ekcert-0-dl160.der",
		"ekcert-1-dl160.der",
		"ekcert-0-h12ssl.der",
		"ekcert-1-h12ssl.der",
		"ekcert-0-m50cyp.der",
		"ekcert-1-m50cyp.der",
		"ekcert-0-r340.der",
		"ekcert-0-sr630.der",
		"ekcert-1-sr630.der",
	}

	for _, p := range eks {
		t.Run(p, func(t *testing.T) {
			buf, err := os.ReadFile(path.Join("testdata", p))
			test.NoErr(t, err)

			ekcert, err := x509.ParseCertificate(buf)
			test.NoErr(t, err)

			certs, err := vendorRoots()
			test.NoErr(t, err)

			ek, err := verifyEndorsementCertificate(ekcert, certs, false)
			test.NoErr(t, err)

			err = checkRevokation(context.Background(), nil, ek)
			test.NoErr(t, err)
		})
	}
}

func TestNegativeEkTests(t *testing.T) {
	cases := []string{
		"normal",
		"noCa",
		"brokenSig",
		"selfSigned",
		"noKeyUsage",
		"noEku",
		"noSan",
		"noSda",
		"caBitSet",
	}

	for _, c := range cases {
		t.Run(c, func(t *testing.T) {
			cakey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
			test.NoErr(t, err)

			ca := &x509.Certificate{
				SerialNumber:          big.NewInt(1),
				Subject:               pkix.Name{CommonName: "Test CA"},
				NotBefore:             time.Now(),
				NotAfter:              time.Now().Add(time.Hour),
				KeyUsage:              x509.KeyUsageCertSign,
				IsCA:                  true,
				BasicConstraintsValid: true,
				MaxPathLen:            1,
				SubjectKeyId:          []byte{1, 2, 3, 4},
				PublicKey:             cakey.Public(),
				PublicKeyAlgorithm:    x509.ECDSA,
			}
			caCert, err := x509.CreateCertificate(rand.Reader, ca, ca, &cakey.PublicKey, cakey)
			test.NoErr(t, err)
			ca, err = x509.ParseCertificate(caCert)
			test.NoErr(t, err)

			ekkey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
			test.NoErr(t, err)

			// build Subject Alternative Name extension
			sanNames := pkix.RDNSequence{
				{
					{Type: tcgAttributeTpmManufacturer, Value: "id:11223344"},
					{Type: tcgAttributeTpmModel, Value: "Test TPM Model"},
					{Type: tcgAttributeTpmVersion, Value: "id:00230042"},
				},
			}
			namebuf, err := asn1.Marshal(sanNames)
			test.NoErr(t, err)
			dirNames := []asn1.RawValue{
				{Tag: directoryName, Bytes: namebuf},
			}
			dirbuf, err := asn1.Marshal(dirNames)
			test.NoErr(t, err)
			sanExt := pkix.Extension{
				Id:       x509SubjectAltNameExt,
				Critical: false,
				Value:    dirbuf,
			}

			// build Subject Directory Attributes extension
			tpmSpec := tpmSpecification{
				Family:   "2.0",
				Level:    1,
				Revision: 123,
			}
			specbuf, err := asn1.Marshal(tpmSpec)
			test.NoErr(t, err)
			specattr := pkix.AttributeTypeAndValue{
				Type:  tcgAttributeTpmSpecification,
				Value: specbuf,
			}
			secAssert := tpmSecurityAssertions{
				Version:                         1,
				FieldUpgradeable:                false,
				EkGenerationType:                0,
				EkGenerationLocation:            0,
				EkCertificateGenerationLocation: 0,
				CcInfo:                          commonCriteriaMeasures{},
				FipsLevel:                       fipsLevel{},
				Iso9000Certified:                false,
				Iso9000Uri:                      "",
			}
			secbuf, err := asn1.Marshal(secAssert)
			test.NoErr(t, err)
			secattr := pkix.AttributeTypeAndValue{
				Type:  tcgAttributeTpmSecurityAssertions,
				Value: secbuf,
			}
			sdaEnt := []pkix.AttributeTypeAndValue{specattr, secattr}
			sdaBuf, err := asn1.Marshal(sdaEnt)
			test.NoErr(t, err)
			sdaExt := pkix.Extension{
				Id:       x509SubjectDirAttribExt,
				Critical: false,
				Value:    sdaBuf,
			}

			// build Extended Key Usage extension
			tcgEku := []asn1.ObjectIdentifier{tcgKpEKCertificate}
			ekuBuf, err := asn1.Marshal(tcgEku)
			test.NoErr(t, err)
			ekuExt := pkix.Extension{
				Id:       x509ExtKeyUsageExt,
				Critical: true,
				Value:    ekuBuf,
			}

			ek := &x509.Certificate{
				SerialNumber:          big.NewInt(2),
				Subject:               pkix.Name{CommonName: "Test EK"},
				Issuer:                ca.Subject,
				NotBefore:             time.Now(),
				NotAfter:              time.Now().Add(time.Hour),
				KeyUsage:              x509.KeyUsageKeyAgreement,
				ExtraExtensions:       []pkix.Extension{sanExt, sdaExt, ekuExt},
				IsCA:                  false,
				BasicConstraintsValid: true,
				MaxPathLen:            0,
				SubjectKeyId:          []byte{1, 2, 3, 4},
				PublicKey:             ekkey.Public(),
				PublicKeyAlgorithm:    x509.ECDSA,
			}
			var ekCert []byte
			if c == "noKeyUsage" {
				ek.KeyUsage = x509.KeyUsageDigitalSignature
			}
			if c == "noEku" {
				ek.ExtraExtensions = []pkix.Extension{sanExt, sdaExt}
			}
			if c == "noSan" {
				ek.ExtraExtensions = []pkix.Extension{sdaExt, ekuExt}
			}
			if c == "noSda" {
				ek.ExtraExtensions = []pkix.Extension{sanExt, ekuExt}
			}
			if c == "caBitSet" {
				ek.IsCA = true
				ek.BasicConstraintsValid = true
			}
			if c == "selfSigned" {
				ek.Issuer = ek.Subject
				ekCert, err = x509.CreateCertificate(rand.Reader, ek, ek, &ekkey.PublicKey, ekkey)
			} else {
				ekCert, err = x509.CreateCertificate(rand.Reader, ek, ca, &cakey.PublicKey, cakey)
			}
			test.NoErr(t, err)
			ek, err = x509.ParseCertificate(ekCert)
			test.NoErr(t, err)

			if c == "brokenSig" {
				ek.Signature[0] ^= 0xff
			}

			pool := x509.NewCertPool()
			if c != "noCa" {
				pool.AddCert(ca)
			}

			_, err = verifyEndorsementCertificate(ek, pool, false)
			if c == "normal" {
				test.NoErr(t, err)
			} else {
				test.WantErr(t, err)
			}
		})
	}
}

func TestRevokationCheck(t *testing.T) {
	ctx := context.Background()
	_, tmpl, priv := fakeEccEk(t, rand.Reader)
	tmpl.KeyUsage = x509.KeyUsageCRLSign
	tmpl.AuthorityKeyId = tmpl.SubjectKeyId
	tmpl.IsCA = true

	der, err := x509.CreateCertificate(rand.Reader, &tmpl, &tmpl, tmpl.PublicKey, priv)
	test.NoErr(t, err)

	cert, err := x509.ParseCertificate(der)
	test.NoErr(t, err)

	ek := &endorsementCertificate{
		CAs:         []*x509.Certificate{cert},
		Certificate: cert,
	}

	cases := []string{
		"normal",
		"non-200",
		"wrong-content-type",
		"empty-body",
		"invalid-crl",
		"invalid-sig",
		"invalid-url",
		"invalid-scheme",
		"no-authority",
		"normal-no-crls",
		"normal-other-sn",
		"revoked",
	}

	for _, c := range cases {
		t.Run(c, func(t *testing.T) {
			crl := x509.RevocationList{
				Number: big.NewInt(1),
			}

			var usepriv interface{} = priv
			switch c {
			case "normal-other-sn":
				crl.RevokedCertificateEntries = []x509.RevocationListEntry{
					{
						SerialNumber:   big.NewInt(42),
						RevocationTime: time.Now(),
						ReasonCode:     1,
					},
				}
			case "revoked":
				crl.RevokedCertificateEntries = []x509.RevocationListEntry{
					{
						SerialNumber:   cert.SerialNumber,
						RevocationTime: time.Now(),
						ReasonCode:     1,
					},
				}
			case "invalid-sig":
				usepriv, err = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
				test.NoErr(t, err)
			}

			dercrl, err := x509.CreateRevocationList(rand.Reader, &crl, ek.Certificate, usepriv.(*ecdsa.PrivateKey))
			test.NoErr(t, err)

			srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				switch c {
				default:
					fallthrough
				case "normal":
					w.Header().Add("Content-Type", "application/pkix-crl")
					w.Write(dercrl)
				case "non-200":
					w.WriteHeader(http.StatusNotFound)
				case "wrong-content-type":
					w.Header().Add("Content-Type", "text/plain")
					w.Write(dercrl)
				case "empty-body":
					w.Header().Add("Content-Type", "application/pkix-crl")
					w.Write(nil)
				case "invalid-crl":
					w.Header().Add("Content-Type", "application/pkix-crl")
					w.Write(dercrl[:len(dercrl)-1])
				}
			}))
			defer srv.Close()

			switch c {
			case "invalid-url":
				ek.Certificate.CRLDistributionPoints = []string{"http://example.com#%1"}
			case "invalid-scheme":
				ek.Certificate.CRLDistributionPoints = []string{"ftp://example.com"}
			case "normal-no-crls":
				ek.Certificate.CRLDistributionPoints = nil
			case "no-authority":
				ek.CAs = nil
			default:
				cert.CRLDistributionPoints = []string{srv.URL}
				ek.CAs = []*x509.Certificate{cert}
			}
			err = checkRevokation(ctx, srv.Client(), ek)
			t.Logf("%s\n", err)

			switch c {
			case "revoked":
				test.IsEq(t, err, ErrRevoked)
			default:
				if strings.HasPrefix(c, "normal") {
					test.NoErr(t, err)
				} else {
					test.WantErr(t, err)
				}
			}
		})
	}
}

func TestVerifySan(t *testing.T) {
	cases := []string{
		"normal",
		"no-san",
		"no-dirnames",
		"noid-manuf",
		"noid-version",
		"normal-known-vendor",
		"dup-manuf",
		"dup-model",
		"dup-version",
		"nonnum-manuf",
		"nonnum-version",
		"nonstr-model",
	}

	for _, c := range cases {
		t.Run(c, func(t *testing.T) {
			// build Subject Alternative Name extension
			sanNames := pkix.RDNSequence{
				{
					{Type: tcgAttributeTpmManufacturer, Value: "id:11223344"},
					{Type: tcgAttributeTpmModel, Value: "Test TPM Model"},
					{Type: tcgAttributeTpmVersion, Value: "id:00230042"},
				},
			}

			switch c {
			case "noid-manuf":
				sanNames[0][0].Value = "11223344"
			case "normal-known-vendor":
				sanNames[0][0].Value = "id:414d4400"
			case "nonnum-manuf":
				sanNames[0][0].Value = "id:xyz"
			case "dup-manuf":
				sanNames[0] = append(sanNames[0], pkix.AttributeTypeAndValue{
					Type:  tcgAttributeTpmManufacturer,
					Value: "id:11223344",
				})
			case "noid-version":
				sanNames[0][2].Value = "00230042"
			case "nonnum-version":
				sanNames[0][2].Value = "id:xyz"
			case "dup-version":
				sanNames[0] = append(sanNames[0], pkix.AttributeTypeAndValue{
					Type:  tcgAttributeTpmVersion,
					Value: "id:11223344",
				})
			case "dup-model":
				sanNames[0] = append(sanNames[0], pkix.AttributeTypeAndValue{
					Type:  tcgAttributeTpmModel,
					Value: "Test TPM Model",
				})
			case "nonstr-model":
				sanNames[0][1].Value = 42
			}

			namebuf, err := asn1.Marshal(sanNames)
			test.NoErr(t, err)
			dirNames := []asn1.RawValue{
				{Tag: directoryName, Bytes: namebuf},
			}

			if c == "no-dirnames" {
				dirNames[0].Tag = 0
			}

			dirbuf, err := asn1.Marshal(dirNames)
			test.NoErr(t, err)
			sanExt := pkix.Extension{
				Id:       x509SubjectAltNameExt,
				Critical: false,
				Value:    dirbuf,
			}
			_, tmpl, priv := fakeEccEk(t, rand.Reader)

			switch c {
			case "no-san":
				tmpl.ExtraExtensions = nil
			default:
				tmpl.ExtraExtensions = []pkix.Extension{sanExt}
			}

			der, err := x509.CreateCertificate(rand.Reader, &tmpl, &tmpl, tmpl.PublicKey, priv)
			test.NoErr(t, err)
			cert, err := x509.ParseCertificate(der)
			test.NoErr(t, err)

			var ek endorsementCertificate
			err = verifySan(cert, &ek)
			if strings.HasPrefix(c, "normal") {
				test.NoErr(t, err)
				test.IsEq(t, ek.Model, "Test TPM Model")
				test.IsEq(t, ek.Version[0], uint16(0x23))
				test.IsEq(t, ek.Version[1], uint16(0x42))
			} else {
				test.WantErr(t, err)
			}
		})
	}
}

func TestVerifySda(t *testing.T) {
	cases := []string{
		"normal",
		"no-sda",
		"invalid-sda",
		"invalid-spec",
		"invalid-sec",
		"dup-spec",
		"dup-sec",
	}

	for _, c := range cases {
		t.Run(c, func(t *testing.T) {
			// build Subject Directory Attributes extension
			tpmSpec := tpmSpecification{
				Family:   "2.0",
				Level:    1,
				Revision: 123,
			}
			specbuf, err := asn1.Marshal(tpmSpec)
			test.NoErr(t, err)
			specattr := pkix.AttributeTypeAndValue{
				Type:  tcgAttributeTpmSpecification,
				Value: specbuf,
			}
			secAssert := tpmSecurityAssertions{
				Version:                         1,
				FieldUpgradeable:                false,
				EkGenerationType:                0,
				EkGenerationLocation:            0,
				EkCertificateGenerationLocation: 0,
				CcInfo:                          commonCriteriaMeasures{},
				FipsLevel:                       fipsLevel{},
				Iso9000Certified:                false,
				Iso9000Uri:                      "",
			}
			secbuf, err := asn1.Marshal(secAssert)
			test.NoErr(t, err)
			secattr := pkix.AttributeTypeAndValue{
				Type:  tcgAttributeTpmSecurityAssertions,
				Value: secbuf,
			}
			sdaEnt := []pkix.AttributeTypeAndValue{specattr, secattr}

			switch c {
			case "no-spec":
				sdaEnt = sdaEnt[1:]
			case "no-sec":
				sdaEnt = sdaEnt[:1]
			case "dup-spec":
				sdaEnt = append(sdaEnt, specattr)
			case "dup-sec":
				sdaEnt = append(sdaEnt, secattr)
			case "invalid-spec":
				sdaEnt[0].Value = []byte{0xff}
			case "invalid-sec":
				sdaEnt[1].Value = []byte{0xff}
			}

			sdaBuf, err := asn1.Marshal(sdaEnt)
			test.NoErr(t, err)
			sdaExt := pkix.Extension{
				Id:       x509SubjectDirAttribExt,
				Critical: false,
				Value:    sdaBuf,
			}
			_, tmpl, priv := fakeEccEk(t, rand.Reader)

			switch c {
			case "no-sda":
				tmpl.ExtraExtensions = nil
			case "invalid-sda":
				sdaExt.Value = []byte{0xff}
				fallthrough
			default:
				tmpl.ExtraExtensions = []pkix.Extension{sdaExt}
			}

			der, err := x509.CreateCertificate(rand.Reader, &tmpl, &tmpl, tmpl.PublicKey, priv)
			test.NoErr(t, err)
			cert, err := x509.ParseCertificate(der)
			test.NoErr(t, err)

			var ek endorsementCertificate
			err = verifySda(cert, &ek)
			if strings.HasPrefix(c, "normal") {
				test.NoErr(t, err)
				test.IsEq(t, *ek.Specification, tpmSpec)
				test.IsEq(t, *ek.SecurityAssertions, secAssert)
			} else {
				test.WantErr(t, err)
			}
		})
	}
}
