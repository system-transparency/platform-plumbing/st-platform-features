package ra

import (
	"bytes"
	"context"
	"crypto/x509"
	"crypto/x509/pkix"
	"embed"
	"encoding/asn1"
	"encoding/pem"
	"errors"
	"io"
	"net/http"
	"net/url"
	"path"
	"sort"
	"strconv"
	"strings"

	"system-transparency.org/ppg/ra/internal/tpm"
)

const (
	directoryName = 4
	crlMimeType   = "application/pkix-crl"
)

var (
	//go:embed certificates
	certificateFiles embed.FS
)

var (
	// Certificate is revoked
	ErrRevoked = errors.New("revoked")
	// No CRL could be fetched or parsed
	ErrNoValidCrl = errors.New("no CRL could be processed")
	// Subject Alternative Name cannot be processed or is missing TCG attributes
	ErrInvalidSanExt = errors.New("invalid SAN extension")
	// Subject Directory Attributes cannot be processed
	ErrInvalidSdaExt = errors.New("invalid SDA extension")

	// EK certificate's AuthorityKeyId is missing
	ErrAuthorityKeyId = errors.New("no authority key identifier")
	// Certificate chain is too short
	ErrCertificateChain = errors.New("invalid certificate chain")

	// Basic Constraints, key usage, or EKU wrong
	ErrInvalidCertificateUsage = errors.New("invalid certificate usage attributes")

	// SAN extensions
	tcgAttributeTpmManufacturer = asn1.ObjectIdentifier{2, 23, 133, 2, 1}
	tcgAttributeTpmModel        = asn1.ObjectIdentifier{2, 23, 133, 2, 2}
	tcgAttributeTpmVersion      = asn1.ObjectIdentifier{2, 23, 133, 2, 3}

	// SDA extensions
	tcgAttributeTpmSpecification      = asn1.ObjectIdentifier{2, 23, 133, 2, 16}
	tcgAttributeTpmSecurityAssertions = asn1.ObjectIdentifier{2, 23, 133, 2, 18}

	// EKU extensions
	tcgKpEKCertificate          = asn1.ObjectIdentifier{2, 23, 133, 8, 1}
	tcgKpPlatformCertificate    = asn1.ObjectIdentifier{2, 23, 133, 8, 2}
	tcgKpAIKCertificate         = asn1.ObjectIdentifier{2, 23, 133, 8, 3}
	tcgKpPlatformKeyCertificate = asn1.ObjectIdentifier{2, 23, 133, 8, 4}
	msCertSrv                   = asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 311, 21, 36}
	msPlutonUnknown             = asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 311, 122, 2}

	// X.509 extensions
	x509SubjectAltNameExt   = asn1.ObjectIdentifier{2, 5, 29, 17}
	x509SubjectDirAttribExt = asn1.ObjectIdentifier{2, 5, 29, 9}
	x509ExtKeyUsageExt      = asn1.ObjectIdentifier{2, 5, 29, 37}

	// Nuvoton ECC EK CA subject
	nuvotonEccEkCaSubject = "CN=Nuvoton TPM Root CA 2110,O=Nuvoton Technology Corporation,C=TW"
)

// Result of the EK certificate verification. Contains the parsed certificate
// and TCG specific attributes.
type endorsementCertificate struct {
	// Raw TCG Manufacturer ID
	ManufacturerId uint32
	// Translated TCG Manufacturer ID or empty string if unknown
	Manufacturer string
	// Manufacturer specific model name
	Model string
	// Manufacturer specific version
	Version [2]uint16

	Specification      *tpmSpecification
	SecurityAssertions *tpmSecurityAssertions

	// Certificate chain starting with the immediate issuer CA
	CAs []*x509.Certificate

	Certificate *x509.Certificate
}

// Load the TPM vendor root certificates from the embedded filesystem. The
// function verifies the structure and signatures of the certificates but
// ignores the expiration date.
func vendorRoots() (*x509.CertPool, error) {
	certificatePool := x509.NewCertPool()
	certs := make([]*x509.Certificate, 0)
	files, err := certificateFiles.ReadDir("certificates")
	if err != nil {
		return nil, err
	}
	for _, file := range files {
		if !file.IsDir() {
			buf, err := certificateFiles.ReadFile(path.Join("certificates", file.Name()))
			if err != nil {
				return nil, err
			}
			// some are PEM encoded, some are DER encoded
			if strings.Contains(string(buf), "BEGIN CERTIFICATE") {
				blk, err := pem.Decode(buf)
				if err != nil {
					buf = blk.Bytes
				}
			}
			cert, err := x509.ParseCertificate(buf)
			if err != nil {
				return nil, err
			}
			certificatePool.AddCert(cert)
			certs = append(certs, cert)
		}
	}

	// verify certificates, handle unrecognized TCG EKUs
	knownKp := []asn1.ObjectIdentifier{
		tcgKpEKCertificate,
		tcgKpAIKCertificate,
		tcgKpPlatformCertificate,
		tcgKpPlatformKeyCertificate,
		msCertSrv,
		msPlutonUnknown,
	}
	opts := x509.VerifyOptions{Roots: certificatePool}
	for _, cert := range certs {
		// accept TCG EKU bits
		flt := cert.UnknownExtKeyUsage[:0]
		for _, ku := range cert.UnknownExtKeyUsage {
			var isTcgKp bool
			for _, kp := range knownKp {
				isTcgKp = isTcgKp || ku.Equal(kp)
			}
			if !isTcgKp {
				flt = append(flt, ku)
			}
		}
		cert.UnknownExtKeyUsage = flt

		_, err := cert.Verify(opts)
		var certerr x509.CertificateInvalidError
		// accept expired certificates
		if errors.As(err, &certerr) && certerr.Reason == x509.Expired {
			continue
		}
		if err != nil {
			return nil, err
		}
	}

	return certificatePool, nil
}

type attribute struct {
	Type  asn1.ObjectIdentifier
	Value asn1.RawValue
}

// The following definitions are from the TCG EK Credential Profile For TPM
// Family 2.0; Level 0 Version 2.5 Revision 2. Sections 3 and 4.

type tpmSpecification struct {
	Family   string
	Level    int
	Revision int
}

type tpmSecurityAssertions struct {
	Version                         int                    `asn1:"default:0"`
	FieldUpgradeable                bool                   `asn1:"default:false"`
	EkGenerationType                asn1.Enumerated        `asn1:"explicit,tag:0,optional"`
	EkGenerationLocation            asn1.Enumerated        `asn1:"explicit,tag:1,optional"`
	EkCertificateGenerationLocation asn1.Enumerated        `asn1:"explicit,tag:2,optional"`
	CcInfo                          commonCriteriaMeasures `asn1:"explicit,tag:3,optional"`
	FipsLevel                       fipsLevel              `asn1:"explicit,tag:4,optional"`
	Iso9000Certified                asn1.Flag              `asn1:"explicit,tag:5,optional,default:false"`
	Iso9000Uri                      string                 `asn1:"optional"`
}

type commonCriteriaMeasures struct {
	Version            string
	AssuranceLevel     asn1.Enumerated
	EvaluationStatus   asn1.Enumerated
	Plus               asn1.Flag             `asn1:"default:false"`
	StrengthOfFunction asn1.Enumerated       `asn1:"explicit,tag:0,optional"`
	ProfileOid         asn1.ObjectIdentifier `asn1:"explicit,tag:1,optional"`
	ProfileUri         uriReference          `asn1:"explicit,tag:2,optional"`
	TargetOid          asn1.ObjectIdentifier `asn1:"explicit,tag:3,optional"`
	TargetUri          uriReference          `asn1:"explicit,tag:4,optional"`
}

type fipsLevel struct {
	Version string
	Level   asn1.Enumerated
	Plus    asn1.Flag `asn1:"default:false"`
}

type uriReference struct {
	Uri           string
	HashAlgortihm asn1.ObjectIdentifier `asn1:"optional"`
	HashValue     []byte                `asn1:"optional"`
}

// finds the extension with the given OID in a sorted slice of extensions
func extension(cert *x509.Certificate, oid asn1.ObjectIdentifier) *pkix.Extension {
	str := oid.String()
	idx := sort.Search(len(cert.Extensions), func(i int) bool {
		return cert.Extensions[i].Id.String() >= str
	})
	if idx == len(cert.Extensions) || !cert.Extensions[idx].Id.Equal(oid) {
		return nil
	}
	return &cert.Extensions[idx]
}

// parse and verify the Subject Alternative Name extension. The extension
// contains TPM manufacturer ID, model, and version. See the TCG EK Credential
// Profile Section 4 for details. After processing, the SAN extension is
// removed from the list of unhandled critical extensions.
func verifySan(cert *x509.Certificate, ek *endorsementCertificate) error {
	// Subject Alternative Name
	sanext := extension(cert, x509SubjectAltNameExt)
	if sanext == nil {
		return ErrInvalidSanExt
	}

	var names []asn1.RawValue
	_, err := asn1.Unmarshal(sanext.Value, &names)
	if err != nil {
		return ErrInvalidSanExt
	}

	var manufacturer, model, version bool

	for _, name := range names {
		if name.Tag != directoryName {
			continue
		}

		var dnseq pkix.RDNSequence
		var dn pkix.Name

		if _, err := asn1.Unmarshal(name.Bytes, &dnseq); err != nil {
			return ErrInvalidSanExt
		}
		dn.FillFromRDNSequence(&dnseq)

		for _, n := range dn.Names {
			if n.Type.Equal(tcgAttributeTpmManufacturer) {
				if manufacturer {
					return ErrInvalidSanExt
				}
				manufacturer = true

				str, ok := n.Value.(string)
				if !ok || !strings.HasPrefix(str, "id:") {
					return ErrInvalidSanExt
				}
				mid, err := strconv.ParseUint(strings.TrimPrefix(str, "id:"), 16, 32)
				if err != nil {
					return err
				}
				ek.ManufacturerId = uint32(mid)
				if ven, ok := tpm.VendorIds[ek.ManufacturerId]; ok {
					ek.Manufacturer = ven
				}
			} else if n.Type.Equal(tcgAttributeTpmModel) {
				if model {
					return ErrInvalidSanExt
				}
				model = true

				m, ok := n.Value.(string)
				if !ok {
					return ErrInvalidSanExt
				}

				ek.Model = m
			} else if n.Type.Equal(tcgAttributeTpmVersion) {
				if version {
					return ErrInvalidSanExt
				}
				version = true

				str, ok := n.Value.(string)
				if !ok || !strings.HasPrefix(str, "id:") {
					return ErrInvalidSanExt
				}
				ver, err := strconv.ParseUint(strings.TrimPrefix(str, "id:"), 16, 32)
				if err != nil {
					return ErrInvalidSanExt
				}

				ek.Version[0] = uint16(ver >> 16)
				ek.Version[1] = uint16(ver)
			}
		}
	}

	if !manufacturer || !model || !version {
		return ErrInvalidSanExt
	}

	// remove SAN from unhandle crit ext
	filteredExt := cert.UnhandledCriticalExtensions[:0]
	for _, extid := range cert.UnhandledCriticalExtensions {
		if !extid.Equal(x509SubjectAltNameExt) {
			filteredExt = append(filteredExt, extid)
		}
	}
	cert.UnhandledCriticalExtensions = filteredExt

	return nil
}

// parse and verify the Subject Directory Attributes extension. The extension
// contains the TCG specification implemented by the TPM and what certification
// levels are met. See the TCG EK Credential Profile Section 3.1 for details.
func verifySda(cert *x509.Certificate, ek *endorsementCertificate) error {
	sdaext := extension(cert, x509SubjectDirAttribExt)
	if sdaext == nil {
		return ErrInvalidSdaExt
	}
	var sda []attribute
	_, err := asn1.Unmarshal(sdaext.Value, &sda)
	if err != nil {
		return ErrInvalidSdaExt
	}

	for _, attrib := range sda {
		if attrib.Type.Equal(tcgAttributeTpmSpecification) {
			if ek.Specification != nil {
				return ErrInvalidSdaExt
			}
			ek.Specification = new(tpmSpecification)
			_, err := asn1.Unmarshal(attrib.Value.Bytes, ek.Specification)
			if err != nil {
				return ErrInvalidSdaExt
			}
		} else if attrib.Type.Equal(tcgAttributeTpmSecurityAssertions) {
			if ek.SecurityAssertions != nil {
				return ErrInvalidSdaExt
			}
			ek.SecurityAssertions = new(tpmSecurityAssertions)
			_, err := asn1.Unmarshal(attrib.Value.Bytes, ek.SecurityAssertions)
			if err != nil {
				return ErrInvalidSdaExt
			}
		}
	}

	return nil
}

// Verifies the endorsement certificate of a TPM according to the TCG EK
// Credential Profile specification. It verifies the certificate contains the
// required Subject Alternative Name and Subject Directory Attributes
// extensions. The certificate must not be a CA certificate and must have the
// correct key usage and extended key usage attributes. The certificate must be
// signed by a trusted root CA. The function returns the parsed certificate and
// TCG specific attributes.
// The function accepts certificates signed by expired CAs.
func verifyEndorsementCertificate(ekcert *x509.Certificate, pool *x509.CertPool, allowUnknownCa bool) (*endorsementCertificate, error) {
	var ek endorsementCertificate
	ek.Certificate = ekcert

	// extension() requires a sorted slice
	sort.Slice(ekcert.Extensions, func(i, j int) bool {
		return ekcert.Extensions[i].Id.String() < ekcert.Extensions[j].Id.String()
	})

	// parse and verify the Subject Alternative Name extension extension
	if err := verifySan(ekcert, &ek); err != nil {
		return nil, err
	}

	// parse and verify the Subject Directory Attributes extension
	if err := verifySda(ekcert, &ek); err != nil {
		return nil, err
	}

	// basic constraints: CA: false
	if !ekcert.BasicConstraintsValid || ekcert.IsCA {
		return nil, ErrInvalidCertificateUsage
	}

	// authority key identifier
	if len(ekcert.AuthorityKeyId) == 0 {
		return nil, ErrAuthorityKeyId
	}

	// check extended key usage extension
	var hasEkKeyUsage bool
	for _, ku := range ekcert.UnknownExtKeyUsage {
		hasEkKeyUsage = hasEkKeyUsage || ku.Equal(tcgKpEKCertificate)
	}
	if !hasEkKeyUsage {
		return nil, ErrInvalidCertificateUsage
	}

	// verify signature and compute CA chain
	opts := x509.VerifyOptions{
		Roots: pool,
		KeyUsages: []x509.ExtKeyUsage{
			x509.ExtKeyUsageAny,
		},
	}
	chains, err := ekcert.Verify(opts)
	var unkauth x509.UnknownAuthorityError
	if err != nil && (!errors.As(err, &unkauth) || !allowUnknownCa) {
		return nil, err
	} else if err == nil {
		if len(chains) != 1 {
			return nil, ErrCertificateChain
		}
		ek.CAs = chains[0]
		// remove the ek itself from the chain
		if len(ek.CAs) > 1 {
			ek.CAs = ek.CAs[1:]
		}
	}

	// key usage. assumes the EK is a storage key
	switch ekcert.PublicKeyAlgorithm {
	case x509.RSA:
		if ekcert.KeyUsage&x509.KeyUsageKeyEncipherment == 0 {
			return nil, ErrInvalidCertificateUsage
		}
	case x509.ECDSA:
		// Nuvoton didn't read the EK Credential Profile spec
		if len(ek.CAs) > 0 && ek.CAs[0].Subject.String() == nuvotonEccEkCaSubject {
			if ekcert.KeyUsage&x509.KeyUsageKeyEncipherment == 0 {
				return nil, ErrInvalidCertificateUsage
			}
		} else {
			if ekcert.KeyUsage&x509.KeyUsageKeyAgreement == 0 {
				return nil, ErrInvalidCertificateUsage
			}
		}
	default:
		return nil, errors.New("unsupported public key algorithm")
	}

	return &ek, nil
}

// Downloads and verifies the CRL of the EK certificate. The function returns
// an error if the certificate is revoked or the CRL could not be fetched or
// processed.
// The `client` parameter can be used to pass a custom HTTP client. If `nil`, a
// default client is used.
// The `context` parameter can be used to cancel the request.
func checkRevokation(ctx context.Context, client *http.Client, ek *endorsementCertificate) error {
	if client == nil {
		client = http.DefaultClient
	}
	if len(ek.Certificate.CRLDistributionPoints) == 0 {
		return nil
	}

	// find the certificate that signed the CRL
	var authority *x509.Certificate
	for _, ca := range ek.CAs {
		if bytes.Equal(ca.SubjectKeyId, ek.Certificate.AuthorityKeyId) {
			authority = ca
			break
		}
	}
	if authority == nil {
		return ErrNoValidCrl
	}

	processed := false
	for _, crlstr := range ek.Certificate.CRLDistributionPoints {
		crlurl, err := url.Parse(crlstr)
		if err != nil {
			continue
		}
		if crlurl.Scheme != "http" && crlurl.Scheme != "https" {
			continue
		}

		// fetch the CRL
		req, err := http.NewRequestWithContext(ctx, "GET", crlstr, nil)
		if err != nil {
			continue
		}
		resp, err := client.Do(req)
		if err != nil {
			continue
		}
		defer resp.Body.Close()
		if resp.StatusCode != http.StatusOK {
			continue
		}
		if resp.Header.Get("Content-Type") != crlMimeType {
			continue
		}
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			continue
		}

		// parse and verify the CRL
		rl, err := x509.ParseRevocationList(body)
		if err != nil {
			continue
		}
		err = rl.CheckSignatureFrom(authority)
		if err != nil {
			continue
		}

		processed = true

		for _, r := range rl.RevokedCertificateEntries {
			if r.SerialNumber.Cmp(ek.Certificate.SerialNumber) == 0 {
				return ErrRevoked
			}
		}
	}

	if !processed {
		return ErrNoValidCrl
	} else {
		return nil
	}
}
