// Remote attestation for System Transparency enabled devices.
//
// This package provides a simple API for remotely attesting the integrity of a
// device booted using System Transparency's reference boot loader stboot.
// It requires a TPM 2.0 and UEFI on the device to be attested.
//
// Remote attestation involves two parties: the verifier and the prover. The
// prover creates a signed attestation structure called a quote and sends it to
// the verifier. The verifier can use the quote to check that that the prover
// state is part of some set of known good states. In the context of System
// Transparency, the prover is the System Transparency enabled device and the
// verifier the operator's machine. A known good state is one with unmodified
// Firmware code and settings and a known good OS package and stboot.
//
// To initiate a remote attestation, the device calls [Attest] to generate a
// quote. The operator verifies the quote using [Verify]. Both functions
// require endorsements. Endorsements are data structures contain enough
// information to recreate the TPM measurements influeced by the endorsed
// entity. Endorsements are signed by the operator or by an entity the operator
// trusts. This package has three types of endorsements: platform, OS package,
// and stboot. The latter two are created from the OS package and stboot
// artifacts respectively using [EndorseOsPackage] and [EndorseStboot]. The
// platform endorsement is created in a three step process called the
// enrollemnt protocol. It starts by creating an enrollemnt request on the
// device using [Request]. The resulting data structure is fed into
// [CreateChallenge]. This function is run off the device and requires the
// private signing key. The resulting [Challenge] structure is send back to the
// device and is decrypted with [Finish], yielding the platform enrollment.
package ra
