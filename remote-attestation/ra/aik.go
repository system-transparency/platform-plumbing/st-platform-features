package ra

import (
	"bytes"
	"context"
	"errors"

	"system-transparency.org/ppg/ra/internal/tpm"
)

var (
	// returned by CreateChallenge if the AIK was not generated from
	// defaultAikTemplate.
	ErrInvalidAik = errors.New("AIK does not match expected template")

	defaultAikTemplate = tpm.Template{
		Public: tpm.Public{
			Type:             tpm.TPM_ALG_ECC,
			NameAlg:          tpm.TPM_ALG_SHA256,
			ObjectAttributes: tpm.TPMA_OBJECT_RESTRICTED_SIGN,
			AuthPolicy:       make([]byte, 32),
			EccParameters: &tpm.EccParms{
				Symmetric: tpm.SymDefObject{
					Algorithm: tpm.TPM_ALG_NULL,
				},
				Scheme: tpm.Scheme{
					Scheme:  tpm.TPM_ALG_ECDSA,
					HashAlg: tpm.TPM_ALG_SHA256,
				},
				CurveID: tpm.TPM_ECC_NIST_P256,
				KDF: tpm.Scheme{
					Scheme: tpm.TPM_ALG_NULL,
				},
				Unique: tpm.EccPoint{},
			},
		},
	}
	defaultAikLabel = "ST-ECC-AIK-LABEL-V0"
)

func loadAttestationIdentityKey(ctx context.Context, dev tpm.Device, ownerAuth string) (tpm.U32, *tpm.Public, error) {
	h, pub, _, _, _, _, err := tpm.CreatePrimary(ctx, dev, tpm.Password(ownerAuth), tpm.TPM_RH_OWNER, "", defaultAikTemplate, []byte(defaultAikLabel), nil)
	if err != nil {
		return 0, nil, err
	}

	return h, &pub, nil
}

func deriveAttestationIdentityKey(ctx context.Context, dev tpm.Device, ownerAuth string) (*tpm.Public, error) {
	h, pub, err := loadAttestationIdentityKey(ctx, dev, ownerAuth)
	if err != nil {
		return nil, err
	}
	err = tpm.FlushContext(ctx, dev, h)

	return pub, err
}

func verifyAttestationIdentityKey(pub *tpm.Public) error {
	if pub.Type != tpm.TPM_ALG_ECC {
		return ErrInvalidAik
	}
	if pub.NameAlg != tpm.TPM_ALG_SHA256 {
		return ErrInvalidAik
	}
	if pub.ObjectAttributes != tpm.TPMA_OBJECT_RESTRICTED_SIGN {
		return ErrInvalidAik
	}
	if !bytes.Equal(pub.AuthPolicy, make([]byte, 32)) {
		return ErrInvalidAik
	}
	if pub.EccParameters == nil {
		return ErrInvalidAik
	}
	params := pub.EccParameters
	if params.Symmetric.Algorithm != tpm.TPM_ALG_NULL {
		return ErrInvalidAik
	}
	if params.Scheme.Scheme != tpm.TPM_ALG_ECDSA {
		return ErrInvalidAik
	}
	if params.Scheme.HashAlg != tpm.TPM_ALG_SHA256 {
		return ErrInvalidAik
	}
	if params.CurveID != tpm.TPM_ECC_NIST_P256 {
		return ErrInvalidAik
	}
	if params.KDF.Scheme != tpm.TPM_ALG_NULL {
		return ErrInvalidAik
	}
	if len(params.Unique.X) != 32 || len(params.Unique.Y) != 32 {
		return ErrInvalidAik
	}

	return nil
}
