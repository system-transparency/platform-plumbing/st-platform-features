package ra

import (
	"bytes"
	"context"
	"crypto/ecdsa"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"math/big"
	"sort"

	"system-transparency.org/ppg/ra/internal/tpm"
	"system-transparency.org/ppg/ra/internal/vsock"
)

var (
	// Failed to read PCR values off the TPM.
	ErrPcrRead = errors.New("error reading PCR")
	// The PCR values in the quote do not match the expected values.
	ErrPcrSelection = errors.New("invalid PCR selection")
	// The quote structure is invalid.
	ErrAttestationStructure = errors.New("attestation structure is invalid")
	// The signature or hash algorithm used in the quote is not supported.
	ErrAlgorithm = errors.New("unsupported algorithm")
	// The signature verification of the quote or endorsement failed.
	ErrSignature = errors.New("signature verification failed")

	// we skip PCR 10 as it is used for IMA, which is essentially random
	pcClientPcr = []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15,
		16, 17, 18, 19, 20, 21, 22, 23}

	// Initial PCR values for the client. TCG PC Client Platform TPM Profile
	// Specification for TPM 2.0 version 1.05 rev. 14
	pcClientInitial = map[int][]byte{
		0:  make([]byte, 32),
		1:  make([]byte, 32),
		2:  make([]byte, 32),
		3:  make([]byte, 32),
		4:  make([]byte, 32),
		5:  make([]byte, 32),
		6:  make([]byte, 32),
		7:  make([]byte, 32),
		8:  make([]byte, 32),
		9:  make([]byte, 32),
		10: make([]byte, 32),
		11: make([]byte, 32),
		12: make([]byte, 32),
		13: make([]byte, 32),
		14: make([]byte, 32),
		15: make([]byte, 32),
		16: make([]byte, 32),
		17: []byte("\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"),
		18: []byte("\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"),
		19: []byte("\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"),
		20: []byte("\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"),
		21: []byte("\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"),
		22: []byte("\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"),
		23: make([]byte, 32),
	}

	stbootPcr  = pcr(12)
	microVmPcr = pcr(23)
)

// Signed statement of the booted system code and data.
type Quote struct {
	// TPM2_ATTEST structure
	Attest []byte
	// TPM2_SIGNATURE structure
	Signature []byte

	// nil if Attest is called with Minimal = true
	// PCR values in the quote.
	Pcr []byte
	// CEL event log of the booted system.
	Cel []byte
}

// Detailed error information about an attestation failure. Returned by
// [Verify].
type ErrAttestation struct {
	// Detailed error.
	Err error
	// PCR values computed from the endorsements.
	ExpectedPcr map[int][]byte
	// PCR values received in the quote. Only set if [Attest] was called with
	// Minimal = false.
	ReceivedPcr map[int][]byte
	// Expected hash of the all PCR values.
	ExpectedHash []byte
	// Hash of the PCR values received in the quote.
	ReceivedHash []byte
}

func (e ErrAttestation) Error() string {
	return fmt.Sprintf("attestation failed: %v", e.Err)
}

// UINT32: 4
// TPML_PCR_SELECTION: 4 + alg_count * (2 + 1 + 3) = 10
// TPML_DIGEST: 4 + x * (2 + hash_size) = 4 + x * 34
// 18 + 34x

func readAllPcr(ctx context.Context, dev tpm.Device) (map[int][]byte, error) {
	todo := append([]int{}, pcClientPcr...)
	pcrs := make(map[int][]byte)

	for len(todo) > 0 {
		maxSel := (tpm.MaxResponseSize - 18) / 34
		if maxSel > len(todo) {
			maxSel = len(todo)
		}

		var sel tpm.PcrSelect
		sel.Hash = tpm.TPM_ALG_SHA256
		sel.Pcr = todo[:maxSel]

		_, outsel, digests, err := tpm.PcrRead(ctx, dev, []tpm.PcrSelect{sel})
		if err != nil {
			return nil, err
		}
		if len(outsel) != 1 {
			return nil, ErrPcrRead
		}
		if outsel[0].Hash != tpm.TPM_ALG_SHA256 {
			return nil, ErrPcrRead
		}

		for i, pcr := range outsel[0].Pcr {
			pcrs[pcr] = append([]byte{}, digests[i]...)
			i := sort.SearchInts(todo, pcr)
			if i < len(todo) && todo[i] == pcr {
				todo = append(todo[:i], todo[i+1:]...)
			} else {
				return nil, ErrPcrRead
			}
		}
	}

	return pcrs, nil
}

func generateQuote(ctx context.Context, dev tpm.Device, ownerAuth string, nonce []byte) (*tpm.Attest, *tpm.Signature, map[int][]byte, error) {
	digests, err := readAllPcr(ctx, dev)
	if err != nil {
		return nil, nil, nil, err
	}

	aikHandle, _, err := loadAttestationIdentityKey(ctx, dev, ownerAuth)
	if err != nil {
		return nil, nil, nil, err
	}
	defer func() { _ = tpm.FlushContext(ctx, dev, aikHandle) }()

	sch := tpm.Scheme{Scheme: tpm.TPM_ALG_NULL}
	sel := tpm.PcrSelect{
		Hash: tpm.TPM_ALG_SHA256,
		Pcr:  append([]int{}, pcClientPcr...),
	}
	attest, sig, err := tpm.Quote(ctx, dev, aikHandle, tpm.Password(ownerAuth), sch, nonce, []tpm.PcrSelect{sel})
	if err != nil {
		return nil, nil, nil, err
	}

	return &attest, &sig, digests, nil
}

// Options for the [Attest] function.
type AttestOptions struct {
	// Path to the TPM device node. If empty, the system TPM is used.
	DevicePath string
	// Auth Value of the TPM owner hierarchy. The default is the empty string.
	OwnerAuth string
	// Minimize to size of the quote structure. If true the quote will include
	// the full event log. This help with debugging attestation failures. The
	// event log will be retuned in the [ErrAttestation] error.
	MinimalQuote bool

	// Testing only

	// event log to use instead of the UEFI event log.
	eventlog []record
	device   tpm.Device
}

// Attest uses the device's TPM the generate a signed statement of the booted
// code and data, called a quote. The quote can then be verified using the
// [Verify] function.
//
// The function connects to the TPM and the context ctx can be used to cancel
// the operation. The nonce is a random value that is used to bind the quote to
// the request and pervent replay attacks. The options argument opts can be
// used to tune the behavior of the function. See [AttestOptions] for more
// information. If opts is nil, the default options are used.
func Attest(ctx context.Context, nonce []byte, opts *AttestOptions) (*Quote, error) {
	var dev tpm.Device
	var err error

	if opts == nil {
		opts = new(AttestOptions)
	}

	// connect to TPM
	devicePath := opts.DevicePath

	if opts.device != nil {
		dev = opts.device
	} else {
		if devicePath == vsock.DevicePath {
			quotebuf, err := vsock.Quote(ctx, nonce)
			if err != nil {
				return nil, err
			}

			var quote Quote
			var nonce []byte
			err = readSizePrefix(bytes.NewReader(quotebuf), &nonce, &quote.Attest, &quote.Signature)
			if err != nil {
				return nil, err
			}

			return &quote, nil
		} else if devicePath != "" {
			dev, err = tpm.OpenTPM(devicePath)
		} else {
			dev, _, err = findSystemTPM(ctx)
		}
		if err != nil {
			return nil, err
		}
		defer dev.Close()
	}

	attest, sig, pcr, err := generateQuote(ctx, dev, opts.OwnerAuth, nonce)
	if err != nil {
		return nil, err
	}
	dev.Close()

	var binlog []byte
	if len(opts.eventlog) > 0 {
		buf := new(bytes.Buffer)
		err = encodeRecords(buf, opts.eventlog)
		if err != nil {
			return nil, err
		}
		binlog = buf.Bytes()
	} else {
		binlog, err = collectEventlogs(ctx, devicePath)
		if err != nil {
			return nil, err
		}
	}

	var quote Quote

	if quote.Attest, err = attest.Pack(); err != nil {
		return nil, err
	}
	if quote.Signature, err = sig.Pack(); err != nil {
		return nil, err
	}

	if !opts.MinimalQuote {
		for _, p := range pcClientPcr {
			if d, ok := pcr[p]; ok {
				quote.Pcr = append(quote.Pcr, d...)
			}
		}

		quote.Cel = binlog
	}

	return &quote, nil
}

// compute AIK Qualified Name
func qualifiedNameOwnerHierarchy(pub *tpm.Public) ([]byte, error) {
	nam, err := tpm.ComputeName(pub.NameAlg, pub)
	if err != nil {
		return nil, err
	}
	h, err := tpm.HashAlgorithm(pub.NameAlg)
	if err != nil {
		return nil, err
	}
	hh := h.New()
	if err := binary.Write(hh, binary.BigEndian, tpm.TPM_RH_OWNER); err != nil {
		return nil, err
	}
	hh.Write(nam)
	qnam := append([]byte{byte(pub.NameAlg >> 8), byte(pub.NameAlg)}, hh.Sum(nil)...)

	return qnam, nil
}

func verifyAttest(a *tpm.Attest, aik *tpm.Public, nonce, expected []byte) error {
	if a.Magic != tpm.TPM_GENERATED_VALUE {
		return ErrAttestationStructure
	}
	if a.Type != tpm.TPM_ST_ATTEST_QUOTE {
		return ErrAttestationStructure
	}

	aikqname, err := qualifiedNameOwnerHierarchy(aik)
	if err != nil {
		return err
	}

	if !bytes.Equal(a.Signer, aikqname) {
		return ErrAttestationStructure
	}
	if !bytes.Equal(a.Extra, nonce) {
		return ErrAttestationStructure
	}

	q := a.Quote
	if q == nil {
		return ErrAttestationStructure
	}
	if len(q.Select) != 1 {
		return ErrPcrSelection
	}
	if q.Select[0].Hash != tpm.TPM_ALG_SHA256 {
		return ErrPcrSelection
	}
	if len(q.Select[0].Pcr) != len(pcClientPcr) {
		return ErrPcrSelection
	}
	for p, i := range q.Select[0].Pcr {
		if pcClientPcr[p] != i {
			return ErrPcrSelection
		}
	}
	if !bytes.Equal(q.Digest, expected) {
		return ErrPcrSelection
	}

	return nil
}

func verifySignature(attest []byte, sig *tpm.Signature, aik *tpm.Public) error {
	if sig.Algorithm != tpm.TPM_ALG_ECDSA {
		return ErrAlgorithm
	}
	sigecc := sig.Ecc
	if sigecc == nil {
		return ErrAlgorithm
	}

	var sum []byte
	switch sigecc.Hash {
	case tpm.TPM_ALG_SHA256:
		sum256 := sha256.Sum256(attest)
		sum = sum256[:]
	case tpm.TPM_ALG_SHA384:
		sum384 := sha512.Sum384(attest)
		sum = sum384[:]
	case tpm.TPM_ALG_SHA512:
		sum512 := sha512.Sum512(attest)
		sum = sum512[:]
	default:
		return ErrAlgorithm
	}

	aikpub, err := aik.PublicKey()
	if err != nil {
		return err
	}

	switch aikpub := aikpub.(type) {
	case *ecdsa.PublicKey:
		r := big.NewInt(0).SetBytes(sigecc.R)
		s := big.NewInt(0).SetBytes(sigecc.S)

		if ecdsa.Verify(aikpub, sum, r, s) {
			return nil
		} else {
			return ErrSignature
		}
	default:
		return ErrAlgorithm
	}
}

func computePcr(plat *PlatformEndorsement, ospkg *OsPackageEndorsement, stboot *StbootEndorsement, guest *OsPackageEndorsement) (map[int][]byte, []byte, error) {
	evlog, err := computeEvents(plat, ospkg, stboot, guest)
	if err != nil {
		return nil, nil, err
	}

	fw, err := unpackPlatformFirmware(bytes.NewReader(plat.PlatformFirmware))
	if err != nil {
		return nil, nil, err
	}

	pcr := make(map[int][]byte, len(pcClientPcr))
	for _, p := range pcClientPcr {
		pcr[p] = append([]byte{}, pcClientInitial[p]...)
	}

	// DRTM init sequence
	if len(fw.DRTMInit) > 0 {
		pcr[0] = fw.DRTMInit
	}

	for _, r := range evlog {
		p := pcr[int(r.Pcr)]

		for _, d := range r.Digests {
			if d.Alg != int(tpm.TPM_ALG_SHA256) {
				continue
			}
			sum := sha256.Sum256(append(p, d.Digest[:]...))
			pcr[int(r.Pcr)] = sum[:]
		}
	}

	expected := sha256.New()
	for _, i := range pcClientPcr {
		if p, ok := pcr[i]; ok {
			expected.Write(p)
		}
	}

	return pcr, expected.Sum(nil), nil
}

func computeEvents(plat *PlatformEndorsement, ospkg *OsPackageEndorsement, stboot *StbootEndorsement, guest *OsPackageEndorsement) ([]record, error) {
	fw, err := unpackPlatformFirmware(bytes.NewReader(plat.PlatformFirmware))
	if err != nil {
		return nil, err
	}

	log := fw.Firmware

	stublog, err := decodeCelEventlog(bytes.NewReader(stboot.StubMeasurements))
	if err != nil {
		return nil, err
	}
	log = append(log, stublog...)
	log = append(log, fw.HandoffTables...)
	stbootlog1, err := decodeCelEventlog(bytes.NewReader(ospkg.StbootMeasurements))
	if err != nil {
		return nil, err
	}
	log = append(log, stbootlog1...)
	load, err := decodeCelEventlog(bytes.NewReader(stboot.StbootLoadEvent))
	if err != nil {
		return nil, err
	}
	log = append(log, load...)

	stbootlog2, err := decodeCelEventlog(bytes.NewReader(stboot.StbootMeasurements))
	if err != nil {
		return nil, err
	}
	log = append(log, stbootlog2...)
	log = append(log, fw.InterExitBootServices...)
	ospkglog, err := decodeCelEventlog(bytes.NewReader(ospkg.OsPkgMeasurements))
	if err != nil {
		return nil, err
	}
	log = append(log, ospkglog...)

	if guest != nil {
		guestlog, err := decodeCelEventlog(bytes.NewReader(guest.StbootMeasurements))
		if err != nil {
			return nil, err
		}
		// move stboot detail measurements to PCR 23
		for _, ev := range guestlog {
			if ev.Pcr != stbootPcr {
				continue
			}
			ev.Pcr = microVmPcr
			log = append(log, ev)
		}
	}

	return log, nil
}

// Verify a quote against the provided endorsements.
//
// The verify functions requires the same nonce value that was used to generate
// the quote as well as the quote structure iself. This function will check
// that the device that generated the quote is running the firmware in platform
// and was with the boot loader in stboot and the OS package in ospkg. All
// three endorsements signature's are verified using the provided verifier v.
//
// On success, Verify returns nil. If the verification fails, it returns an
// error.
func Verify(
	nonce []byte, quote *Quote, platform *PlatformEndorsement,
	ospkg *OsPackageEndorsement, stboot *StbootEndorsement,
	guest *OsPackageEndorsement, v Verifier) error {
	if err := v.Verify(platform.PlatformFirmware, platform.FirmwareSignature); err != nil {
		return err
	}
	msg := encodeByteArrays(ospkgEndorsementMagic, ospkg.StbootMeasurements, ospkg.OsPkgMeasurements)
	if err := v.Verify(msg, ospkg.Signature); err != nil {
		return err
	}
	msg = encodeByteArrays(
		stbootEndorsementMagic, stboot.StubMeasurements, stboot.StbootLoadEvent, stboot.StbootMeasurements)
	if err := v.Verify(msg, stboot.Signature); err != nil {
		return err
	}
	if guest != nil {
		msg := encodeByteArrays(ospkgEndorsementMagic, guest.StbootMeasurements, guest.OsPkgMeasurements)
		if err := v.Verify(msg, guest.Signature); err != nil {
			return err
		}
	}

	pcr, expected, err := computePcr(platform, ospkg, stboot, guest)
	if err != nil {
		return err
	}

	aik, err := tpm.UnpackPublic(platform.AttestationIdentityKey)
	if err != nil {
		return err
	}
	if err := v.Verify(platform.AttestationIdentityKey, platform.AikSignature); err != nil {
		return err
	}

	attest, err := tpm.UnpackAttest(quote.Attest)
	if err != nil {
		return err
	}

	atterr := ErrAttestation{
		ExpectedPcr:  pcr,
		ExpectedHash: expected,
	}
	if attest.Quote != nil {
		atterr.ReceivedHash = attest.Quote.Digest
	}

	if err := verifyAttest(attest, aik, nonce, expected); err != nil {
		atterr.Err = err
		return atterr
	}

	sig, err := tpm.UnpackSignature(quote.Signature)
	if err != nil {
		atterr.Err = err
		return atterr
	}
	if err := verifySignature(quote.Attest, sig, aik); err != nil {
		atterr.Err = err
		return atterr
	}

	return nil
}

func readSizePrefix(rd io.Reader, data ...*[]byte) error {
	for _, d := range data {
		var size uint32
		err := binary.Read(rd, binary.LittleEndian, &size)
		if err != nil {
			return err
		}
		*d = make([]byte, size)
		_, err = io.ReadFull(rd, *d)
		if err != nil {
			return err
		}
	}

	return nil
}
