package ra

import (
	"bytes"
	"os"
	"testing"

	test "system-transparency.org/ppg/ra/internal"
)

func TestPcClientLog(t *testing.T) {
	logs := []string{
		"testdata/x11schln4f-910-0.3.5-small.eventlog",
		"testdata/h12ssl-0.3.5-small.eventlog",
		"testdata/sr630-0.3.5-small.eventlog",
		"testdata/dl160-0.3.5-small.eventlog",
	}
	for _, p := range logs {
		t.Run(p, func(t *testing.T) {
			fd, err := os.Open(p)
			if test.NoErr(t, err) {
				defer fd.Close()
			}

			log, err := decodePcClientLog(fd)
			test.NoErr(t, err)

			t.Logf("%d log entries\n", len(log))
		})
	}
}

func TestTranscodeLogs(t *testing.T) {
	logs := []string{
		"testdata/x11schln4f-910-0.3.5-small.eventlog",
		"testdata/h12ssl-0.3.5-small.eventlog",
		"testdata/sr630-0.3.5-small.eventlog",
		"testdata/dl160-0.3.5-small.eventlog",
	}
	for _, p := range logs {
		t.Run(p, func(t *testing.T) {
			fd, err := os.Open(p)
			if test.NoErr(t, err) {
				defer fd.Close()
			}

			log, err := transcodePcClientEventlog(fd)
			test.NoErr(t, err)

			t.Logf("CEL size: %d bytes\n", len(log))
		})
	}
}

func TestCelSpecPcclient(t *testing.T) {
	cel := []byte{
		0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00,
		0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x19, 0x04,
		0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x05, 0x00, 0x00, 0x00, 0x33, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00,
		0x00, 0x03, 0x01, 0x00, 0x00, 0x00, 0x25, 0x53, 0x70, 0x65, 0x63, 0x20,
		0x49, 0x44, 0x20, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x30, 0x33, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x02, 0x02, 0x00, 0x00, 0x00, 0x04,
		0x00, 0x14, 0x00, 0x0b, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x04, 0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00,
		0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x3e, 0x04, 0x00, 0x00, 0x00, 0x14,
		0xc4, 0x2f, 0xed, 0xad, 0x26, 0x82, 0x00, 0xcb, 0x1d, 0x15, 0xf9, 0x78,
		0x41, 0xc3, 0x44, 0xe7, 0x9d, 0xae, 0x33, 0x20, 0x0b, 0x00, 0x00, 0x00,
		0x20, 0xd4, 0x72, 0x0b, 0x40, 0x09, 0x43, 0x82, 0x13, 0xb8, 0x03, 0x56,
		0x80, 0x17, 0xf9, 0x03, 0x09, 0x3f, 0x6b, 0xea, 0x8a, 0xb4, 0x7d, 0x28,
		0x3d, 0xb3, 0x2b, 0x6e, 0xab, 0xed, 0xbb, 0xf1, 0x55, 0x05, 0x00, 0x00,
		0x00, 0x1e, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x08, 0x01,
		0x00, 0x00, 0x00, 0x10, 0x1e, 0xfb, 0x6b, 0x54, 0x0c, 0x1d, 0x55, 0x40,
		0xa4, 0xad, 0x4e, 0xf4, 0xbf, 0x17, 0xb8, 0x3a,
	}
	pcclient := []byte{
		0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x25, 0x00, 0x00, 0x00, 0x53, 0x70, 0x65, 0x63,
		0x20, 0x49, 0x44, 0x20, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x30, 0x33, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x02, 0x02, 0x00, 0x00, 0x00,
		0x04, 0x00, 0x14, 0x00, 0x0b, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x08, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x04, 0x00, 0xc4,
		0x2f, 0xed, 0xad, 0x26, 0x82, 0x00, 0xcb, 0x1d, 0x15, 0xf9, 0x78, 0x41,
		0xc3, 0x44, 0xe7, 0x9d, 0xae, 0x33, 0x20, 0x0b, 0x00, 0xd4, 0x72, 0x0b,
		0x40, 0x09, 0x43, 0x82, 0x13, 0xb8, 0x03, 0x56, 0x80, 0x17, 0xf9, 0x03,
		0x09, 0x3f, 0x6b, 0xea, 0x8a, 0xb4, 0x7d, 0x28, 0x3d, 0xb3, 0x2b, 0x6e,
		0xab, 0xed, 0xbb, 0xf1, 0x55, 0x10, 0x00, 0x00, 0x00, 0x1e, 0xfb, 0x6b,
		0x54, 0x0c, 0x1d, 0x55, 0x40, 0xa4, 0xad, 0x4e, 0xf4, 0xbf, 0x17, 0xb8,
		0x3a,
	}

	mycel, err := transcodePcClientEventlog(bytes.NewReader(pcclient))
	test.NoErr(t, err)

	buf := new(bytes.Buffer)
	err = encodeRecords(buf, mycel)
	test.NoErr(t, err)

	test.IsEq(t, buf.Bytes(), cel)
}

func TestDl160PostUpdate(t *testing.T) {
	fd, err := os.Open("testdata/dl160-postupdate.eventlog")
	if test.NoErr(t, err) {
		defer fd.Close()
	}

	log, err := transcodePcClientEventlog(fd)
	test.NoErr(t, err)

	for _, ev := range log {
		t.Logf("Event: %s\n", ev.String())
	}
}

func FuzzDecodeEv1Fails(f *testing.F) {
	// missing event payload
	f.Add([]byte{
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		1, 0, 0, 0,
	})
	// missing event payload size
	f.Add([]byte{
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0,
	})
	// missing event digest
	f.Add([]byte{
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	})
	// missing event type
	f.Add([]byte{
		0, 0, 0, 0,
		0, 0, 0,
	})
	// missing event pcr index
	f.Add([]byte{
		0, 0, 0,
	})
	// too large event payload
	f.Add([]byte{
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0xff, 0xff, 0xff, 0xff,
	})

	f.Fuzz(func(t *testing.T, data []byte) {
		_, err := decodeEv1(bytes.NewReader(data))
		test.WantErr(t, err)
	})
}

func FuzzDecodeEv2Fails(f *testing.F) {
	// missing event payload
	f.Add([]byte{
		0, 0, 0, 0, // pcr index
		0, 0, 0, 0, // type
		1, 0, 0, 0, // num digests
		4, 0, // hash alg
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // digest
		1, 0, 0, 0,
	})
	// missing event payload size
	f.Add([]byte{
		0, 0, 0, 0, // pcr index
		0, 0, 0, 0, // type
		1, 0, 0, 0, // num digests
		4, 0, // hash alg
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // digest
		0, 0, 0,
	})
	// too large event payload
	f.Add([]byte{
		0, 0, 0, 0, // pcr index
		0, 0, 0, 0, // type
		1, 0, 0, 0, // num digests
		4, 0, // hash alg
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // digest
		0xff, 0xff, 0xff, 0xff,
	})
	// missing event sha1 digest
	f.Add([]byte{
		0, 0, 0, 0, // pcr index
		0, 0, 0, 0, // type
		1, 0, 0, 0, // num digests
		4, 0, // hash alg
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // digest
	})
	// missing event sha256 digest
	f.Add([]byte{
		0, 0, 0, 0, // pcr index
		0, 0, 0, 0, // type
		1, 0, 0, 0, // num digests
		11, 0, // hash alg
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	})
	// missing event sha385 digest
	f.Add([]byte{
		0, 0, 0, 0, // pcr index
		0, 0, 0, 0, // type
		1, 0, 0, 0, // num digests
		12, 0, // hash alg
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	})
	// missing event sha512 digest
	f.Add([]byte{
		0, 0, 0, 0, // pcr index
		0, 0, 0, 0, // type
		1, 0, 0, 0, // num digests
		13, 0, // hash alg
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	})
	// unknown hash algorithm
	f.Add([]byte{
		0, 0, 0, 0, // pcr index
		0, 0, 0, 0, // type
		1, 0, 0, 0, // num digests
		42, 0, // hash alg
	})
	// too short hash algorithm
	f.Add([]byte{
		0, 0, 0, 0, // pcr index
		0, 0, 0, 0, // type
		1, 0, 0, 0, // num digests
		42, // hash alg
	})
	// missing num digests
	f.Add([]byte{
		0, 0, 0, 0, // pcr index
		0, 0, 0, 0, // type
		1, 0, 0, // num digests
	})
	// missing type
	f.Add([]byte{
		0, 0, 0, 0, // pcr index
		0, 0, 0, // type
	})
	// missing pcr index
	f.Add([]byte{
		0, 0, 0, // pcr index
	})

	f.Fuzz(func(t *testing.T, data []byte) {
		_, err := decodeEv2(bytes.NewReader(data))
		test.WantErr(t, err)
	})
}

func FuzzDecodeSpecId(f *testing.F) {
	// pcr index wrong
	f.Add(append([]byte{
		1, 0, 0, 0,
		3, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		15, 0, 0, 0,
	}, specIdEvent...))
	// type wrong
	f.Add(append([]byte{
		0, 0, 0, 0,
		2, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		15, 0, 0, 0,
	}, specIdEvent...))
	// event payload size wrong
	f.Add(append([]byte{
		0, 0, 0, 0,
		3, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		14, 0, 0, 0,
	}, specIdEvent[:14]...))
	// event payload wrong
	f.Add([]byte{
		0, 0, 0, 0,
		3, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		15, 0, 0, 0,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	})
	// event too short
	f.Add([]byte{
		0, 0, 0, 0,
		3, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		15, 0, 0, 0,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	})

	f.Fuzz(func(t *testing.T, data []byte) {
		_, err := decodePcClientLog(bytes.NewReader(data))
		test.WantErr(t, err)
	})
}
