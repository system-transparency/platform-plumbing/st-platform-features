//go:build linux
// +build linux

package vsock

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net"
	"net/http"
	"syscall"
	"time"

	"golang.org/x/sys/unix"
)

const (
	cidHypervisor = 2
	cidGuest      = 3
	stvmmTpmPort  = 1234
)

type vsockAddr struct {
	cid  uint64
	port uint32
}

func (va *vsockAddr) Network() string {
	return "vsock"
}

func (va *vsockAddr) String() string {
	return fmt.Sprintf("%d:%d", va.cid, va.port)
}

type vsockConn struct {
	fd int
}

func (vc *vsockConn) Close() error {
	return syscall.Close(vc.fd)
}

func (vc *vsockConn) Read(b []byte) (int, error) {
	return syscall.Read(vc.fd, b)
}

func (vc *vsockConn) Write(b []byte) (int, error) {
	return syscall.Write(vc.fd, b)
}

func (vc *vsockConn) LocalAddr() net.Addr {
	return &vsockAddr{cid: cidGuest, port: 0}
}

func (vc *vsockConn) RemoteAddr() net.Addr {
	return &vsockAddr{cid: cidHypervisor, port: stvmmTpmPort}
}

func (vc *vsockConn) SetDeadline(t time.Time) error {
	return syscall.ENOTSUP
}

func (vc *vsockConn) SetReadDeadline(t time.Time) error {
	return syscall.ENOTSUP
}

func (vc *vsockConn) SetWriteDeadline(t time.Time) error {
	return syscall.ENOTSUP
}

func Quote(ctx context.Context, nonce []byte) ([]byte, error) {
	fd, err := syscall.Socket(unix.AF_VSOCK, syscall.SOCK_STREAM, 0)
	if err != nil {
		return nil, err
	}

	sa := &unix.SockaddrVM{
		CID:  cidHypervisor,
		Port: stvmmTpmPort,
	}
	err = unix.Connect(fd, sa)
	if err != nil {
		return nil, err
	}

	client := &http.Client{
		Transport: &http.Transport{
			DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
				return &vsockConn{fd}, nil
			},
		},
	}

	req, err := http.NewRequest("POST", "http://stvmm.local/v1/quote", bytes.NewReader(nonce))
	if err != nil {
		return nil, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	return io.ReadAll(resp.Body)
}
