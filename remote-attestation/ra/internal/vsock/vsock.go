package vsock

import (
	"encoding/binary"
	"errors"
	"io"
)

const (
	DevicePath = "stvmm"
)

var (
	ErrNotImplemented = errors.New("not implemented")
)

// dup to avoid import cycle
func readFromReader(rd io.Reader, data ...*[]byte) error {
	for _, d := range data {
		var length uint32
		err := binary.Read(rd, binary.LittleEndian, &length)
		if err != nil {
			return err
		}
		*d = make([]byte, length)
		_, err = rd.Read(*d)
		if err != nil {
			return err
		}
	}

	return nil
}
