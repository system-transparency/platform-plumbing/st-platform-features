//go:build !linux
// +build !linux

package vsock

import (
	"system-transparency.org/ppg/ra/internal/tpm"
)

func OpenDevice() (tpm.Device, error) {
	return nil, ErrNotImplemented
}
