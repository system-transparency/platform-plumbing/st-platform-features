package tpm

import (
	"bytes"
	"crypto"
	"crypto/cipher"
	"crypto/ecdh"
	"crypto/ecdsa"
	"crypto/hmac"
	"crypto/rsa"
	"encoding/binary"
	"io"
	"testing"
)

// KDFe for ECDH (11.4.10.3): Key derivation function of ECC. This function is
// used to derive a symmetric key from a shared secret point's X coordinate
// during MakeCredential.
//
// The z parameter is the shared secret point's X coordinate. The use parameter
// a NULL terminated label string. The uinfo and vinfo parameters are
// additional information that is included in the key derivation process. The
// bits parameter is the number of bits to derive.
func kfde(h crypto.Hash, z, use, uinfo, vinfo []byte, bits int) ([]byte, error) {
	otherinfo := append([]byte{}, use...)
	otherinfo = append(otherinfo, uinfo...)
	otherinfo = append(otherinfo, vinfo...)
	out := new(bytes.Buffer)

	// digest_i := H(counter || Z || OtherInfo)
	var counter uint32 = 1
	for i := 0; i <= bits/8; i++ {
		hh := h.New()
		if err := binary.Write(hh, binary.BigEndian, counter); err != nil {
			return nil, err
		}
		if _, err := hh.Write(z); err != nil {
			return nil, err
		}
		if _, err := hh.Write(otherinfo); err != nil {
			return nil, err
		}
		out.Write(hh.Sum(nil))
		counter++
	}

	return out.Bytes()[:bits/8], nil
}

// Returns the crypto.Hash for the given TPM algorithm.
func HashAlgorithm(alg U16) (crypto.Hash, error) {
	switch alg {
	case TPM_ALG_SHA1:
		return crypto.SHA1, nil
	case TPM_ALG_SHA256:
		return crypto.SHA256, nil
	case TPM_ALG_SHA384:
		return crypto.SHA384, nil
	case TPM_ALG_SHA512:
		return crypto.SHA512, nil
	default:
		return 0, ErrUnsupported
	}
}

// B.10 Secret Sharing for RSA
// OAEP encryption of a symetric key secret using the public key of signer. The
// OAEP label is "IDENTITY\x00".
func mkcredRSA(signer Public, secret []byte, random io.Reader) (Buffer, error) {
	pub, err := signer.PublicKey()
	if err != nil {
		return nil, err
	}
	rsapub, ok := pub.(*rsa.PublicKey)
	if !ok {
		return nil, ErrUnsupported
	}
	h, err := HashAlgorithm(signer.NameAlg)
	if err != nil {
		return nil, err
	}

	ciphertext, err := rsa.EncryptOAEP(h.New(), random, rsapub, secret, identityLabel)
	return Buffer(ciphertext), err
}

// C.6 Secret Sharing for ECC
// ECDH key exchange between signer and an ephemeral point P. The shared secret
// point's X coordinate is used to derive a symmetric secret.
//
// The function returns the derived symmetric key and the ephemeral point P.
func mkcredECC(signer Public, random io.Reader) (Buffer, EccPoint, error) {
	// convert our (initiator) public key Q_s,V to an ecdh.PublicKey
	pub, err := signer.PublicKey()
	if err != nil {
		return nil, EccPoint{}, err
	}
	QsVecdsa, ok := pub.(*ecdsa.PublicKey)
	if !ok {
		return nil, EccPoint{}, ErrUnsupported
	}
	QsV, err := QsVecdsa.ECDH()
	if err != nil {
		return nil, EccPoint{}, err
	}

	// generate an ephemeral key pair d_e,U / Q_e,U
	var deU *ecdh.PrivateKey

	// By design, GenerateKey is not deterministic:
	// https://github.com/golang/go/issues/58637#issuecomment-1600627963
	// When testing, we'd like to have deterministic results. This is required
	// for the tests involving MakeCredential.
	//
	// The following code block securely generates a deterministic key pair. So
	// even if the initial check for test env fails, the result should be
	// production grade. EVEN THOUGH THIS IS NOT RECOMMENDED FOR PRODUCTION.
	if testing.Testing() {
		var detU []byte

		switch QsV.Curve() {
		case ecdh.P256():
			detU = make([]byte, 32)
		case ecdh.P384():
			detU = make([]byte, 48)
		case ecdh.P521():
			detU = make([]byte, 66)
		default:
			return nil, EccPoint{}, ErrUnsupported
		}

		// poor man's rejection sampling
		for i := 0; i < 1000; i++ {
			_, err = io.ReadFull(random, detU)
			if err != nil {
				return nil, EccPoint{}, err
			}
			deU, err = QsV.Curve().NewPrivateKey(detU)
			if err == nil {
				break
			}
		}
		if err != nil {
			return nil, EccPoint{}, err
		}
	} else {
		// binaries built with `go build` will use this branch
		deU, err = QsV.Curve().GenerateKey(random)
		if err != nil {
			return nil, EccPoint{}, err
		}
	}

	QeU := deU.PublicKey()

	// convert Q_e,U to a TPM2B_ECC_POINT
	// golang uses the uncompressed SEC-1 point format only
	QeUsec1 := QeU.Bytes()
	QeUx := QeUsec1[1 : (len(QeUsec1)-1)/2+1]
	QeUy := QeUsec1[1+len(QeUx):]
	QeUtpm := EccPoint{X: Buffer(QeUx), Y: Buffer(QeUy)}
	QsVx := QsVecdsa.X.FillBytes(make([]byte, len(QeUx)))

	// compute the shared point P = [d_e,U]Q_s,V
	// The function only returns the X coordinate of P
	Px, err := deU.ECDH(QsV)
	if err != nil {
		return nil, EccPoint{}, err
	}

	// derive the symmetric key from the shared point's X coordinate
	// KDFe(X coord of P, "IDENTITY\x00", X coord of Q_e,U, X coord of Q_s,V, h.Size()*8)
	h, err := HashAlgorithm(signer.NameAlg)
	if err != nil {
		return nil, EccPoint{}, err
	}
	k, err := kfde(h, Px, identityLabel, QeUx, QsVx, h.Size()*8)
	if err != nil {
		return nil, EccPoint{}, err
	}
	return Buffer(k), QeUtpm, nil
}

// Computes the Name of an TPM object (Part 1, Section 16).
//
// The name is the objects unique identifier and is computed by hashing the
// public area of the object with its Name Algorithm (SHA1, SHA256, etc.) and
// prepending the hash with the algorithm's numeric ID. For hierarchies the
// name is just the handle value.
//
// The function is implemented for Ordinary and Primary objects (*Public) and
// hierarchy handles (*U32).
func ComputeName(alg U16, obj any) (Buffer, error) {
	h, err := HashAlgorithm(alg)
	if err != nil {
		return nil, err
	}
	hh := h.New()

	switch o := obj.(type) {
	case *Public:
		err = o.encode(hh)
		if err != nil {
			return nil, err
		}
		algbuf, err := pack(&alg)
		if err != nil {
			return nil, err
		}
		return Buffer(append(algbuf, hh.Sum(nil)...)), err

	case *U32:
		buf := new(bytes.Buffer)
		err = o.encode(buf)
		if err != nil {
			return nil, err
		}
		return Buffer(buf.Bytes()), nil

	default:
		return nil, ErrUnsupported
	}
}

// KDFa (Part 1, Section 11.4.10.1): SP800-108 HMAC-based Key Derivation Function.
//
// This function is used to derive a symmetric key from a seed value. The TPM
// uses it to derive encryption and MAC keys for the crednetial protection
// among other things.
//
// The key parameter is the seed value. The use parameter is a NULL terminated
// label string, the ctxu and ctxv parameters are additional information that
// is included in the key derivation process. The bits parameter is the number
// of bits to derive.
func kdfa(alg U16, key []byte, use string, ctxu, ctxv []byte, bits int) ([]byte, error) {
	h, err := HashAlgorithm(alg)
	if err != nil {
		return nil, err
	}
	var out []byte
	var counter uint32

	// K(i) = HMAC(K_I, counter || use || ctxU || ctxV || bits)
	for len(out) <= bits/8 {
		counter++
		h := hmac.New(h.New, key)
		if err := binary.Write(h, binary.BigEndian, counter); err != nil {
			return nil, err
		}
		if _, err := h.Write([]byte(use)); err != nil {
			return nil, err
		}
		if _, err := h.Write(ctxu); err != nil {
			return nil, err
		}
		if _, err := h.Write(ctxv); err != nil {
			return nil, err
		}
		if err := binary.Write(h, binary.BigEndian, uint32(bits)); err != nil {
			return nil, err
		}
		out = append(out, h.Sum(nil)...)
	}

	return out[:bits/8], nil
}

// MakeCredential (Part 1, Section 24 and Part 3, Section 12.6): Encrypt a
// credential for later activation.
//
// The function encrypts the credential for this public key using the protector
// key. It returns the TPM2B_ID_OBJECT and TPM2B_ENCRYPTED_SECRET structures
// that are used in the ActivateCredential command.
//
// The function generates a random seed value that encrypts the credential
// (credentialBlob). The seed is encrypted using the protector key (secret).
func (p *Public) MakeCredential(protector Public, credential []byte, random io.Reader) (IdObject, Buffer, error) {
	var (
		seed, secret []byte
		err          error
		symdef       *SymDefObject
	)

	// Generate and encrypt the seed used for deriving the symmetric keys used to
	// encrypt the credential.
	switch protector.Type {
	case TPM_ALG_RSA:
		// B.10: OAEP
		symdef = &protector.RsaParameters.Symmetric
		seed = make([]byte, symdef.KeyBits/8)
		if _, err := io.ReadFull(random, seed); err != nil {
			return IdObject{}, nil, err
		}
		secret, err = mkcredRSA(protector, seed, random)
		if err != nil {
			return IdObject{}, nil, err
		}
	case TPM_ALG_ECC:
		// C.6: ECDH
		s, pnt, err := mkcredECC(protector, random)
		if err != nil {
			return IdObject{}, nil, err
		}
		seed = s
		secret, err = pack(&pnt)
		if err != nil {
			return IdObject{}, nil, err
		}
		symdef = &protector.EccParameters.Symmetric

	default:
		return IdObject{}, nil, ErrUnsupported
	}

	// The Name of the key this credential is meant for (activateHandle) is used
	// in the key derivation.
	myname, err := ComputeName(p.NameAlg, p)
	if err != nil {
		return IdObject{}, nil, err
	}

	protalg, err := HashAlgorithm(protector.NameAlg)
	if err != nil {
		return IdObject{}, nil, err
	}

	// Assumes AES CFB
	keysz := int(symdef.KeyBits) / 8
	if keysz == 0 {
		keysz = 16
	}
	ivsz := 16

	// Part 1, Section 24.4
	// derive the symmetric key material
	symkey, err := kdfa(protector.NameAlg, seed, "STORAGE\x00", myname, nil, keysz*8)
	if err != nil {
		return IdObject{}, nil, err
	}
	blk, err := symdef.Cipher(symkey, TPM_ALG_AES)
	if err != nil {
		return IdObject{}, nil, err
	}
	strm := cipher.NewCFBEncrypter(blk, make([]byte, ivsz))

	// pack the credential into a TPM2B
	credbuf := Buffer(credential)
	credential, err = pack(&credbuf)
	if err != nil {
		return IdObject{}, nil, err
	}

	// CFB encryption
	strm.XORKeyStream(credential, credential)

	// Part 1, Section 24.5
	hmackey, err := kdfa(protector.NameAlg, seed, "INTEGRITY\x00", nil, nil, protalg.Size()*8)
	if err != nil {
		return IdObject{}, nil, err
	}

	// HMAC(encrypted credential || name)
	h := hmac.New(protalg.New, hmackey)
	h.Write(credential)
	h.Write(myname)
	idhmac := h.Sum(nil)

	idobj := IdObject{
		IntegrityHmac: Buffer(idhmac),
		EncIdentity:   raw(credential),
	}

	return idobj, secret, nil
}
