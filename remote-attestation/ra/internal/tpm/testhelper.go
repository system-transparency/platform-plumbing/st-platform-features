package tpm

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"

	test "system-transparency.org/ppg/ra/internal"
)

type conversation struct {
	Request, Response []byte
}

type testDevice struct {
	t             *testing.T
	conversations []conversation
}

func newTestDevice(t *testing.T, path string) *testDevice {
	dev := new(testDevice)
	dev.t = t

	fd, err := os.Open(path)
	if test.NoErr(t, err) {
		defer fd.Close()

		err = gob.NewDecoder(fd).Decode(&dev.conversations)
		test.NoErr(t, err)
	}

	return dev
}

func (d *testDevice) send(req []byte) ([]byte, error) {
	c := d.conversations[0]
	d.conversations = d.conversations[1:]

	if !bytes.Equal(c.Request, req) {
		d.t.Errorf("got %x, want %x", req, c.Request)
	}

	return c.Response, nil
}

func (d *testDevice) Close() error {
	return nil
}

type recordingDevice struct {
	t             *testing.T
	device        Device
	conversations []conversation
}

func newRecordingDevice(t *testing.T, dev Device) *recordingDevice {
	return &recordingDevice{t: t, device: dev}
}

func (d *recordingDevice) send(req []byte) ([]byte, error) {
	resp, err := d.device.send(req)
	c := conversation{Request: req, Response: resp}
	d.conversations = append(d.conversations, c)
	return resp, err
}

func (d *recordingDevice) Close() error {
	if path := pathForTestdata(d.t); path != "" {
		err := d.store(path)
		test.NoErr(d.t, err)
	}
	return d.device.Close()
}

func (d *recordingDevice) store(path string) error {
	fd, err := os.Create(path)
	if err != nil {
		return err
	}
	defer fd.Close()

	err = gob.NewEncoder(fd).Encode(d.conversations)
	if err == nil {
		d.t.Logf("recorded TPM conversations to %s", filepath.Base(path))
	}
	return err
}

func pathForTestdata(t *testing.T) string {
	t.Helper()

	testdata := test.GetTestdataPath(t)
	filename := fmt.Sprintf("%s.tx.gob", strings.ReplaceAll(t.Name(), "/", "_"))
	return filepath.Join(testdata, filename)
}

func SelectTestTpm(t *testing.T) Device {
	t.Helper()

	// real TPM set in TEST_TPM
	if url := os.Getenv("TEST_TPM"); url != "" {
		dev, err := OpenTPM(url)
		test.NoErr(t, err)

		// record conversations for later?
		if os.Getenv("TEST_RECORD") != "" {
			return newRecordingDevice(t, dev)
		} else {
			return dev
		}
	}

	// replay recorded conversations
	if path := pathForTestdata(t); path != "" {
		return newTestDevice(t, path)
	}

	// no TPM available
	t.Skip("no TPM available, set TEST_TPM to the TPM URL")
	return nil
}
