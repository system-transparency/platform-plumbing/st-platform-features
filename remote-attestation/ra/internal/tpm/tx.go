package tpm

import (
	"bytes"
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"strings"
	"syscall"
	"time"
)

const (
	// TCG PC Client Platform TPM Profile Specification for TPM 2.0, version
	// 1.05, rev 14
	maxResponseTime = 6000 // 2 * TIMEOUT_B
	MaxResponseSize = 4096 // CRB max buffer is 3968
)

var (
	ErrTimeout = errors.New("timeout")
)

// Converts a TPM response code to an error.
func response(i U32) error {
	if i == TPM_RC_SUCCESS {
		return nil
	}
	if i&(1<<7) == 0 {
		return formatZero(i)
	} else {
		return formatOne(i)
	}
}

// Converts Format-Zero response codes (Part 2, Table 13).
func formatZero(i U32) error {
	if i&(1<<8) == 0 {
		return ErrTpm12Error
	} else if i&(1<<10) != 0 {
		return ErrVendorError
	} else if i&(1<<11) != 0 {
		// warnings
		switch i {
		case TPM_RC_CONTEXT_GAP:
			return ErrRcContextGap
		case TPM_RC_OBJECT_MEMORY:
			return ErrRcObjectMemory
		case TPM_RC_SESSION_MEMORY:
			return ErrRcSessionMemory
		case TPM_RC_MEMORY:
			return ErrRcMemory
		case TPM_RC_SESSION_HANDLES:
			return ErrRcSessionHandles
		case TPM_RC_OBJECT_HANDLES:
			return ErrRcObjectHandles
		case TPM_RC_LOCALITY:
			return ErrRcLocality
		case TPM_RC_YIELDED:
			return ErrRcYielded
		case TPM_RC_CANCELED:
			return ErrRcCanceled
		case TPM_RC_TESTING:
			return ErrRcTesting
		case TPM_RC_NV_RATE:
			return ErrRcNvRate
		case TPM_RC_LOCKOUT:
			return ErrRcLockout
		case TPM_RC_RETRY:
			return ErrRcRetry
		case TPM_RC_NV_UNAVAILABLE:
			return ErrRcNvUnavailable
		default:
			return ErrRcUnknown
		}
	} else {
		switch i {
		case TPM_RC_INITIALIZE:
			return ErrRcInitialize
		case TPM_RC_FAILURE:
			return ErrRcFailure
		case TPM_RC_SEQUENCE:
			return ErrRcSequence
		case TPM_RC_DISABLED:
			return ErrRcDisabled
		case TPM_RC_EXCLUSIVE:
			return ErrRcExclusive
		case TPM_RC_AUTH_TYPE:
			return ErrRcAuthType
		case TPM_RC_AUTH_MISSING:
			return ErrRcAuthMissing
		case TPM_RC_POLICY:
			return ErrRcPolicy
		case TPM_RC_PCR:
			return ErrRcPcr
		case TPM_RC_PCR_CHANGED:
			return ErrRcPcrChanged
		case TPM_RC_UPGRADE:
			return ErrRcUpgrade
		case TPM_RC_TOO_MANY_CONTEXTS:
			return ErrRcTooManyContext
		case TPM_RC_AUTH_UNAVAILABLE:
			return ErrRcAuthUnavailable
		case TPM_RC_REBOOT:
			return ErrRcReboot
		case TPM_RC_UNBALANCED:
			return ErrRcUnbalanced
		case TPM_RC_COMMAND_SIZE:
			return ErrRcCommandSize
		case TPM_RC_COMMAND_CODE:
			return ErrRcCommandCode
		case TPM_RC_AUTHSIZE:
			return ErrRcAuthsize
		case TPM_RC_AUTH_CONTEXT:
			return ErrRcAuthContext
		case TPM_RC_NV_RANGE:
			return ErrRcNvRange
		case TPM_RC_NV_SIZE:
			return ErrRcNvSize
		case TPM_RC_NV_LOCKED:
			return ErrRcNvLocked
		case TPM_RC_NV_AUTHORIZATION:
			return ErrRcNvAuthorization
		case TPM_RC_NV_UNINITIALIZED:
			return ErrRcNvUninitialized
		case TPM_RC_NV_SPACE:
			return ErrRcNvSpace
		case TPM_RC_NV_DEFINED:
			return ErrRcNvDefined
		case TPM_RC_BAD_CONTEXT:
			return ErrRcBadContext
		case TPM_RC_CPHASH:
			return ErrRcCphash
		case TPM_RC_PARENT:
			return ErrRcParent
		case TPM_RC_NEEDS_TEST:
			return ErrRcNeedsTest
		case TPM_RC_NO_RESULT:
			return ErrRcNoResult
		case TPM_RC_SENSITIVE:
			return ErrRcSensitive
		default:
			return ErrRcUnknown
		}
	}
}

// Converts Format-One response codes (Part 2, Table 14).
func formatOne(i U32) error {
	err := ErrRcUnknown
	switch i & 0b_1011_1111 {
	case TPM_RC_ASYMMETRIC:
		err = ErrRcAsymmetric
	case TPM_RC_ATTRIBUTES:
		err = ErrRcAttributes
	case TPM_RC_HASH:
		err = ErrRcHash
	case TPM_RC_VALUE:
		err = ErrRcValue
	case TPM_RC_HIERARCHY:
		err = ErrRcHierarchy
	case TPM_RC_KEY_SIZE:
		err = ErrRcKeySize
	case TPM_RC_MGF:
		err = ErrRcMgf
	case TPM_RC_MODE:
		err = ErrRcMode
	case TPM_RC_TYPE:
		err = ErrRcType
	case TPM_RC_HANDLE:
		err = ErrRcHandle
	case TPM_RC_KDF:
		err = ErrRcKdf
	case TPM_RC_RANGE:
		err = ErrRcRange
	case TPM_RC_AUTH_FAIL:
		err = ErrRcAuthFail
	case TPM_RC_NONCE:
		err = ErrRcNonce
	case TPM_RC_PP:
		err = ErrRcPp
	case TPM_RC_SCHEME:
		err = ErrRcScheme
	case TPM_RC_SIZE:
		err = ErrRcSize
	case TPM_RC_SYMMETRIC:
		err = ErrRcSymmetric
	case TPM_RC_TAG:
		err = ErrRcTag
	case TPM_RC_SELECTOR:
		err = ErrRcSelector
	case TPM_RC_INSUFFICIENT:
		err = ErrRcInsufficient
	case TPM_RC_SIGNATURE:
		err = ErrRcSignature
	case TPM_RC_KEY:
		err = ErrRcKey
	case TPM_RC_POLICY_FAIL:
		err = ErrRcPolicyFail
	case TPM_RC_INTEGRITY:
		err = ErrRcIntegrity
	case TPM_RC_TICKET:
		err = ErrRcTicket
	case TPM_RC_RESERVED_BITS:
		err = ErrRcReservedBits
	case TPM_RC_BAD_AUTH:
		err = ErrRcBadAuth
	case TPM_RC_EXPIRED:
		err = ErrRcExpired
	case TPM_RC_POLICY_CC:
		err = ErrRcPolicyCc
	case TPM_RC_BINDING:
		err = ErrRcBinding
	case TPM_RC_CURVE:
		err = ErrRcCurve
	case TPM_RC_ECC_POINT:
		err = ErrRcEccPoint
	}

	if i&(1<<6) != 0 {
		return fmt.Errorf("parameter %d: %w", (i>>8)&0b1111, err)
	} else if i&(1<<11) != 0 {
		return fmt.Errorf("session %d: %w", (i>>8)&0b111, err)
	} else {
		return fmt.Errorf("handle %d: %w", (i>>8)&0b111, err)
	}
}

// Something that can be used as an authorization value.
type Auth interface {
	// request returns the authorization area sent before the command.
	request() *authArea
	// response returns the authorization area expected after the resopnse.
	response() *authArea
}

// A password session. Must match the Object's AuthValue.
type Password string

func (a Password) request() *authArea {
	return &authArea{
		Handle:            TPM_RS_PW,
		Nonce:             Buffer{},
		SessionAttributes: CONTINUE_SESSION,
		AuthAck:           Buffer(a),
	}
}

func (a Password) response() *authArea {
	return &authArea{
		Nonce:             Buffer{},
		SessionAttributes: CONTINUE_SESSION,
		AuthAck:           Buffer{},
	}
}

// A policy session handle returned by StartAuthSession. The sessions policy
// digest must match the Object's AuthPolicy field.
type Policy uint32

func (a Policy) request() *authArea {
	return &authArea{
		Handle:            U32(a),
		Nonce:             Buffer{},
		SessionAttributes: CONTINUE_SESSION,
		AuthAck:           Buffer{},
	}
}

func (a Policy) response() *authArea {
	return &authArea{
		Nonce:             Buffer{},
		SessionAttributes: CONTINUE_SESSION,
		AuthAck:           Buffer{},
	}
}

// Something that can be sent/received over the TPM connection.
type wire interface {
	encode(io.Writer) error
	decode(io.Reader) error
}

// Encodes a sequence of TPM structures into a TPM2B Buffer.
func pack(w ...wire) (Buffer, error) {
	var b bytes.Buffer
	for _, w := range w {
		if err := w.encode(&b); err != nil {
			return nil, err
		}
	}
	return Buffer(b.Bytes()), nil
}

// Wrapper for single byte values (Part 2, 5.1).
type u8 uint8

var _ = wire(new(u8))

func (i *u8) decode(rd io.Reader) error {
	return binary.Read(rd, binary.BigEndian, i)
}

func (i *u8) encode(wr io.Writer) error {
	return binary.Write(wr, binary.BigEndian, i)
}

// Wrapper for big-endian, 16-bit values (Part 2, 5.1).
type U16 uint16

var _ = wire(new(U16))

func (i *U16) decode(rd io.Reader) error {
	return binary.Read(rd, binary.BigEndian, i)
}

func (i *U16) encode(wr io.Writer) error {
	return binary.Write(wr, binary.BigEndian, i)
}

// Wrapper for big-endian, 32-bit values (Part 2, 5.1).
type U32 uint32

var _ = wire(new(U32))

func (i *U32) decode(rd io.Reader) error {
	return binary.Read(rd, binary.BigEndian, i)
}

func (i *U32) encode(wr io.Writer) error {
	return binary.Write(wr, binary.BigEndian, i)
}

// Wrapper for big-endian, 64-bit values (Part 2, 5.1).
type u64 uint64

var _ = wire(new(u64))

func (i *u64) decode(rd io.Reader) error {
	return binary.Read(rd, binary.BigEndian, i)
}

func (i *u64) encode(wr io.Writer) error {
	return binary.Write(wr, binary.BigEndian, i)
}

// A TPM2B Buffer. Used for all TPM2B_* types.
type Buffer []byte

var _ = wire(&Buffer{})

func (b *Buffer) decode(rd io.Reader) error {
	var size U16

	if err := decode(rd, &size); err != nil {
		return err
	}

	*b = make(Buffer, size)
	if _, err := io.ReadFull(rd, *b); err != nil {
		return err
	}

	return nil
}

func (b *Buffer) encode(wr io.Writer) error {
	if len(*b) > 0xffff {
		return ErrFormat
	}
	size := U16(len(*b))
	if err := encode(wr, &size); err != nil {
		return err
	}
	if _, err := wr.Write(*b); err != nil {
		return err
	}

	return nil
}

// A uninterpreted buffer sent verbatim to the TPM. Cannot be used in a
// response.
type raw []byte

var _ = wire(&raw{})

func (b *raw) decode(rd io.Reader) error {
	return ErrRawDecode
}

func (b *raw) encode(wr io.Writer) error {
	_, err := wr.Write(*b)
	return err
}

// Authorization Area (Part 1, 18.6). Used to authenticate handles sent as part
// of a command.
type authArea struct {
	Handle            U32 // TPM_RS_PW or a policy session handle
	Nonce             Buffer
	SessionAttributes u8     // CONTINUE_SESSION
	AuthAck           Buffer // Auth/Ack Buffer depending on the direction
}

var _ = wire(&authArea{})

func (a *authArea) decode(rd io.Reader) error {
	return decode(rd, &a.Nonce, &a.SessionAttributes, &a.AuthAck)
}

func (a *authArea) encode(wr io.Writer) error {
	return encode(wr, &a.Handle, &a.Nonce, &a.SessionAttributes, &a.AuthAck)
}

// A set of handles and their associated authorizations. Handles that do not
// need authorization have nil auth.
type handleArea struct {
	handles []U32
	auth    []Auth
}

// Returns a handleArea with a single handle and optional authorization.
func singleHandle(h U32, a ...Auth) *handleArea {
	if len(a) == 0 {
		return &handleArea{handles: []U32{h}, auth: []Auth{nil}}
	} else {
		return &handleArea{handles: []U32{h}, auth: []Auth{a[0]}}
	}
}

// Returns a handleArea with a number of handles and no authorizations.
func numHandles(num int) *handleArea {
	return &handleArea{handles: make([]U32, num)}
}

// Encodes a sequence of TPM structures and sends them to the TPM.
func encode(wr io.Writer, p ...wire) error {
	for _, p := range p {
		if p == nil {
			return ErrNilPointer
		}

		if err := p.encode(wr); err != nil {
			return err
		}
	}
	return nil
}

// Decodes a sequence of TPM structures from the TPM response.
func decode(rd io.Reader, p ...wire) error {
	for _, p := range p {
		if p == nil {
			return ErrNilPointer
		}

		if err := p.decode(rd); err != nil {
			return err
		}
	}
	return nil
}

// Sends a TPM command to the TPM. The cmd parameter is the command code
// TPM_CC_*, req is the list of parameters to send, reqh is the list of handles
// to send.
// See Part 1, Section 18 for details
func makeRequest(wr io.Writer, cmd U32, req []wire, h *handleArea) error {
	var b bytes.Buffer
	err := encode(&b, req...)
	if err != nil {
		return err
	}
	var aa bytes.Buffer
	if h != nil {
		for _, p := range h.auth {
			if p == nil {
				continue
			}
			err = p.request().encode(&aa)
			if err != nil {
				return err
			}
		}
	}

	// 2 bytes for TPM_ST, 4 bytes for command size, 4 bytes for command code,
	// 4 bytes for each handle
	l := U32(b.Len()) + 2 + 4 + 4
	if h != nil {
		l += 4 * U32(len(h.handles))
	}
	if aa.Len() > 0 {
		// 4 bytes for auth area size
		l += 4 + U32(aa.Len())
	}

	// command tag
	if h != nil && len(h.auth) > 0 {
		err = binary.Write(wr, binary.BigEndian, TPM_ST_SESSIONS)
	} else {
		err = binary.Write(wr, binary.BigEndian, TPM_ST_NO_SESSIONS)
	}
	if err != nil {
		return err
	}

	// command size and code
	err = encode(wr, &l, &cmd)
	if err != nil {
		return err
	}

	// handles
	if h != nil {
		for _, h := range h.handles {
			err = encode(wr, &h)
			if err != nil {
				return err
			}
		}
	}

	// auth area
	if aa.Len() > 0 {
		err = binary.Write(wr, binary.BigEndian, uint32(aa.Len()))
		if err != nil {
			return err
		}
		_, err = wr.Write(aa.Bytes())
		if err != nil {
			return err
		}
	}

	// parameters
	_, err = wr.Write(b.Bytes())

	return err
}

// Receives a TPM response from the TPM. The h parameter is the handle area
// expected in the response, param is the list of parameters expected in the
// response.
// See Part 1, Section 18 for details
func recv(rd io.Reader, h *handleArea, param ...wire) error {
	var tag U16
	var respsz, resp, paramsz U32

	// tag, response size, response code
	err := decode(rd, &tag, &respsz, &resp)
	if err != nil {
		return err
	}

	if resp != TPM_RC_SUCCESS {
		return response(resp)
	}

	// handles
	if h != nil {
		for i := range h.handles {
			err = decode(rd, &h.handles[i])
			if err != nil {
				return err
			}
		}
	}

	// parameters
	if param == nil {
		return nil
	}

	if tag == TPM_ST_SESSIONS {
		err = decode(rd, &paramsz)
		if err != nil {
			return err
		}
		rd = io.LimitReader(rd, int64(paramsz))
	}

	err = decode(rd, param...)
	if err != nil {
		return err
	}

	// we don't implement HMAC sessions so no auth area to evaluate

	return nil
}

// Sends a TPM command and receives a TPM response. The cmd parameter is the
// command code TPM_CC_*, req is the list of parameters to send, reqh is the
// list of handles to send, resp is the list of parameters expected in the
// response, resph is the list of handles expected in the response.
func tx(ctx context.Context, dev Device, cmd U32, req []wire, reqh *handleArea, resp []wire, resph *handleArea) error {
	reqbuf := new(bytes.Buffer)
	err := makeRequest(reqbuf, cmd, req, reqh)
	if err != nil {
		return err
	}

	signal := make(chan error)
	go func() {
		defer close(signal)

		for i := 0; i < 3; i++ {
			respbuf, err := dev.send(reqbuf.Bytes())
			if err != nil {
				signal <- err
				return
			}
			err = recv(bytes.NewBuffer(respbuf), resph, resp...)
			if err != ErrRcRetry {
				signal <- err
				return
			}

			time.Sleep(time.Duration(i*100) * time.Millisecond)
		}
		signal <- ErrRcRetry
	}()

	select {
	case <-ctx.Done():
		return ctx.Err()
	case err := <-signal:
		return err
	}
}

type Device interface {
	send([]byte) ([]byte, error)
	Close() error
}

func OpenTPM(desc string) (Device, error) {
	if strings.Contains(desc, ":") {
		return OpenNet(desc)
	} else {
		return OpenFile(desc)
	}
}

func OpenNet(ipport string) (Device, error) {
	conn, err := net.Dial("tcp", ipport)
	if err != nil {
		return nil, err
	}
	return &defaultDevice{enableFrames: true, conn: conn}, nil
}

func OpenFile(path string) (Device, error) {
	fd, err := os.OpenFile(path, os.O_RDWR, 0600)
	if err != nil {
		return nil, err
	}
	return &defaultDevice{enableFrames: false, conn: fd}, nil
}

type defaultDevice struct {
	enableFrames bool
	conn         io.ReadWriteCloser
}

func waitForData(conn io.Reader) error {
	if fd, ok := conn.(*os.File); ok {
		epfd, err := syscall.EpollCreate1(0)
		if err != nil {
			return err
		}
		defer syscall.Close(epfd)

		readev := []syscall.EpollEvent{{Events: syscall.EPOLLIN}}
		err = syscall.EpollCtl(epfd, syscall.EPOLL_CTL_ADD, int(fd.Fd()), &readev[0])
		if err != nil {
			return err
		}

		for {
			evs, err := syscall.EpollWait(epfd, readev, maxResponseTime)
			if err == syscall.EINTR {
				continue
			}
			if err != nil {
				return err
			}
			if evs > 0 {
				return nil
			}
			return ErrTimeout
		}
	}
	return nil
}

func (d *defaultDevice) send(req []byte) ([]byte, error) {
	var err error
	var framesz U32

	if d.enableFrames {
		// swtpm frame: command, locality, request size
		cmd := TPM2_SEND_COMMAND
		loc := u8(1 << 3)
		framesz = U32(len(req))

		header, err := pack(&cmd, &loc, &framesz)
		if err != nil {
			return nil, err
		}

		req = append(header, req...)
	}

	for i := 2; i <= 2048; i *= 2 {
		_, err = d.conn.Write(req)
		if err == nil {
			break
		}
		time.Sleep(time.Millisecond * time.Duration(i))
	}
	if err != nil {
		return nil, err
	}

	err = waitForData(d.conn)
	if err != nil {
		return nil, err
	}

	resp := make([]byte, MaxResponseSize)
	respsz, err := d.conn.Read(resp)
	if err != nil {
		return nil, err
	}
	resp = resp[:respsz]

	if d.enableFrames {
		respbuf := bytes.NewBuffer(resp)

		// swtpm frame size, tag, response size, response code
		err = decode(respbuf, &framesz)
		if err != nil {
			return nil, err
		}

		frame := make([]byte, framesz)
		_, err = io.ReadFull(respbuf, frame)
		if err != nil {
			return nil, err
		}

		// swtpm acknolwedgement
		var ack U32
		err = decode(respbuf, &ack)
		if err != nil {
			return nil, err
		}

		if ack != 0 {
			return nil, fmt.Errorf("swtpm acknolwedgement: 0x%08x", ack)
		}

		return frame, nil
	}

	return resp, nil
}

func (d *defaultDevice) Close() error {
	return d.conn.Close()
}
