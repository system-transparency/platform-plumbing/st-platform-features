package tpm

import (
	"context"
	"crypto/sha256"
	"errors"
	mrand "math/rand"
	"testing"

	test "system-transparency.org/ppg/ra/internal"
)

func TestStartup(t *testing.T) {
	dev := SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()

	ctx := context.Background()
	err := Startup(ctx, dev, TPM_SU_CLEAR)
	if err != nil && err != ErrRcInitialize {
		t.Errorf("Startup: %v", err)
	}
}

func TestCreatePrimary(t *testing.T) {
	dev := SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()

	ctx := context.Background()
	_ = Startup(ctx, dev, TPM_SU_CLEAR)

	tmpl := Template{
		Public{
			Type:    TPM_ALG_ECC,
			NameAlg: TPM_ALG_SHA256,
			ObjectAttributes: TPMA_OBJECT_FIXEDTPM |
				TPMA_OBJECT_FIXEDPARENT |
				TPMA_OBJECT_SENSITIVEDATAORIGIN |
				TPMA_OBJECT_USERWITHAUTH |
				TPMA_OBJECT_NODA |
				TPMA_OBJECT_RESTRICTED |
				TPMA_OBJECT_DECRYPT,
			EccParameters: &EccParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_AES,
					KeyBits:   128,
					Mode:      TPM_ALG_CFB,
				},
				Scheme: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				CurveID: TPM_ECC_NIST_P256,
				KDF: Scheme{
					Scheme: TPM_ALG_NULL,
				},
			},
		},
	}

	h, _, _, _, _, _, err := CreatePrimary(ctx, dev, Password(""), TPM_RH_ENDORSEMENT, "", tmpl, Buffer{}, PcrSelection{})
	test.NoErr(t, err)

	err = FlushContext(ctx, dev, h)
	test.NoErr(t, err)
}

func TestNvRead(t *testing.T) {
	dev := SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()

	ctx := context.Background()
	_ = Startup(ctx, dev, TPM_SU_CLEAR)

	der, err := NvRead(ctx, dev, EkCertificateIndexL1, Password(""), EkCertificateIndexL1, 100, 0)
	test.NoErr(t, err)

	test.IsEq(t, len(der) > 0, true)
}

func TestGetCap(t *testing.T) {
	dev := SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()

	ctx := context.Background()
	_ = Startup(ctx, dev, TPM_SU_CLEAR)

	t.Run("FixedProps", func(t *testing.T) {
		caps, _, err := GetCapability(ctx, dev, uint32(TPM_CAP_TPM_PROPERTIES), uint32(TPM_PT_FAMILY_INDICATOR), 13)
		test.NoErr(t, err)

		for p, v := range caps.TpmProperties {
			switch p {
			case TPM_PT_FAMILY_INDICATOR:
				t.Logf("TPM_PT_FAMILY_INDICATOR: %x\n", v)
			case TPM_PT_LEVEL:
				t.Logf("TPM_PT_LEVEL: %x\n", v)
			case TPM_PT_REVISION:
				t.Logf("TPM_PT_REVISION: %x\n", v)
			case TPM_PT_DAY_OF_YEAR:
				t.Logf("TPM_PT_DAY_OF_YEAR: %x\n", v)
			case TPM_PT_YEAR:
				t.Logf("TPM_PT_YEAR: %x\n", v)
			case TPM_PT_MANUFACTURER:
				t.Logf("TPM_PT_MANUFACTURER: %x\n", v)
			case TPM_PT_VENDOR_STRING_1:
				t.Logf("TPM_PT_VENDOR_STRING_1: %x\n", v)
			case TPM_PT_VENDOR_STRING_2:
				t.Logf("TPM_PT_VENDOR_STRING_2: %x\n", v)
			case TPM_PT_VENDOR_STRING_3:
				t.Logf("TPM_PT_VENDOR_STRING_3: %x\n", v)
			case TPM_PT_VENDOR_STRING_4:
				t.Logf("TPM_PT_VENDOR_STRING_4: %x\n", v)
			case TPM_PT_VENDOR_TPM_TYPE:
				t.Logf("TPM_PT_VENDOR_TPM_TYPE: %x\n", v)
			case TPM_PT_FIRMWARE_VERSION_1:
				t.Logf("TPM_PT_FIRMWARE_VERSION_1: %x\n", v)
			case TPM_PT_FIRMWARE_VERSION_2:
				t.Logf("TPM_PT_FIRMWARE_VERSION_2: %x\n", v)
			}
		}
	})

	t.Run("Algs", func(t *testing.T) {
		caps, _, err := GetCapability(ctx, dev, uint32(TPM_CAP_ALGS), 0, 0x44)
		test.NoErr(t, err)

		for _, alg := range caps.Algorithms {
			t.Logf("Alg: %x\n", alg.Alg)
		}
	})

	t.Run("Commands", func(t *testing.T) {
		caps, _, err := GetCapability(ctx, dev, uint32(TPM_CAP_COMMANDS), 0x11f, 0x193-0x11f)
		test.NoErr(t, err)

		t.Logf("Commands: %v\n", caps.Commands)
	})

	t.Run("Curves", func(t *testing.T) {
		caps, _, err := GetCapability(ctx, dev, uint32(TPM_CAP_ECC_CURVES), 0, 0x20)
		test.NoErr(t, err)

		t.Logf("Curves: %v\n", caps.EccCurves)
	})
}

func TestPcrExtend(t *testing.T) {
	dev := SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()

	ctx := context.Background()
	_ = Startup(ctx, dev, TPM_SU_CLEAR)

	event := []byte("this is a test event")
	hash := sha256.Sum256(event)
	digest := Ha{Alg: TPM_ALG_SHA256, Digest: hash[:]}
	err := PcrExtend(ctx, dev, 0, Password(""), DigestValues{digest})
	test.NoErr(t, err)
}

func TestPcrRead(t *testing.T) {
	dev := SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()

	ctx := context.Background()
	_ = Startup(ctx, dev, TPM_SU_CLEAR)

	sha1sel := PcrSelect{Hash: TPM_ALG_SHA1, Pcr: []int{3, 4, 5, 6, 7}}
	sha256sel := PcrSelect{Hash: TPM_ALG_SHA256, Pcr: []int{0, 1, 2, 3, 4, 5}}

	updatecnt, sel, digests, err := PcrRead(ctx, dev, PcrSelection{sha1sel, sha256sel})
	test.NoErr(t, err)

	t.Logf("updatecnt: %d\n", updatecnt)

	k := 0
	for _, s := range sel {
		t.Logf("%x\n", s.Hash)
		for _, j := range s.Pcr {
			t.Logf("\tPCR[%d]: %x\n", j, digests[k])
			k++
		}
	}
}

func TestQuote(t *testing.T) {
	dev := SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()

	ctx := context.Background()
	_ = Startup(ctx, dev, TPM_SU_CLEAR)

	tmpl := Template{
		Public{
			Type:    TPM_ALG_ECC,
			NameAlg: TPM_ALG_SHA256,
			ObjectAttributes: TPMA_OBJECT_FIXEDTPM |
				TPMA_OBJECT_FIXEDPARENT |
				TPMA_OBJECT_SENSITIVEDATAORIGIN |
				TPMA_OBJECT_USERWITHAUTH |
				TPMA_OBJECT_NODA |
				TPMA_OBJECT_RESTRICTED |
				TPMA_OBJECT_SIGN,
			EccParameters: &EccParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_NULL,
				},
				Scheme: Scheme{
					Scheme:  TPM_ALG_ECDSA,
					HashAlg: TPM_ALG_SHA256,
				},
				CurveID: TPM_ECC_NIST_P256,
				KDF: Scheme{
					Scheme: TPM_ALG_NULL,
				},
			},
		},
	}

	h, _, _, _, _, _, err := CreatePrimary(ctx, dev, Password(""), TPM_RH_OWNER, "", tmpl, Buffer{}, PcrSelection{})
	if test.NoErr(t, err) {
		defer func() { _ = FlushContext(ctx, dev, h) }()
	}

	scheme := Scheme{Scheme: TPM_ALG_NULL}
	sel := PcrSelect{Hash: TPM_ALG_SHA256, Pcr: []int{0, 1, 2, 3, 4, 5}}
	quote, sig, err := Quote(ctx, dev, h, Password(""), scheme, nil, PcrSelection{sel})
	test.NoErr(t, err)

	t.Logf("quote: %#v\n", quote)
	t.Logf("sig: %#v\n", sig)
}

func TestActivateCred(t *testing.T) {
	tmplaik := Template{
		Public{
			Type:             TPM_ALG_ECC,
			NameAlg:          TPM_ALG_SHA256,
			ObjectAttributes: TPMA_OBJECT_RESTRICTED_SIGN,
			EccParameters: &EccParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_NULL,
				},
				Scheme: Scheme{
					Scheme:  TPM_ALG_ECDSA,
					HashAlg: TPM_ALG_SHA256,
				},
				CurveID: TPM_ECC_NIST_P256,
				KDF: Scheme{
					Scheme: TPM_ALG_NULL,
				},
			},
		},
	}

	ek := []struct {
		name string
		tmpl Template
	}{
		{"L1", EkTemplateL1},
		{"L2", EkTemplateL2},
		{"H1", EkTemplateH1},
		{"H2", EkTemplateH2},
		{"H3", EkTemplateH3},
		{"H4", EkTemplateH4},
		{"H6", EkTemplateH6},
		{"H7", EkTemplateH7},
	}
	for _, pair := range ek {
		ss := pair.name
		tmplek := pair.tmpl

		t.Run(ss, func(t *testing.T) {
			dev := SelectTestTpm(t)
			if dev == nil {
				return
			}
			defer dev.Close()

			rng := mrand.New(mrand.NewSource(42))
			ctx := context.Background()
			_ = Startup(ctx, dev, TPM_SU_CLEAR)

			haik, pubaik, _, _, _, _, err := CreatePrimary(ctx, dev, Password(""), TPM_RH_OWNER, "", tmplaik, Buffer{}, PcrSelection{})
			if test.NoErr(t, err) {
				defer func() { _ = FlushContext(ctx, dev, haik) }()
			}

			hek, pubek, _, _, _, _, err := CreatePrimary(ctx, dev, Password(""), TPM_RH_ENDORSEMENT, "", tmplek, Buffer{}, PcrSelection{})
			if errors.Is(err, ErrRcValue) || errors.Is(err, ErrRcHash) {
				t.Skip("EK not supported not available")
				return
			}
			if test.NoErr(t, err) {
				defer func() { _ = FlushContext(ctx, dev, hek) }()
			}

			credBlob, encryptedSecret, err := pubaik.MakeCredential(pubek, Buffer("secret"), rng)
			//t.Logf("credBlob: %#v\n", credBlob)
			//t.Logf("encryptedSecret (%d): %x\n", len(encryptedSecret), encryptedSecret)
			//t.Logf("err: %v\n", err)
			test.NoErr(t, err)

			var authSess Auth
			if pubek.ObjectAttributes&TPMA_OBJECT_USERWITHAUTH == 0 {
				nonce := Buffer(make([]byte, 32))
				_, err = rng.Read(nonce)
				test.NoErr(t, err)

				sess, err := StartAuthSession(ctx, dev, TPM_ALG_SHA256, rng)
				if test.NoErr(t, err) {
					defer func() { _ = FlushContext(ctx, dev, sess) }()
				}

				_, _, err = PolicySecret(ctx, dev, TPM_RH_ENDORSEMENT, sess, Password(""), Buffer{}, Buffer{}, Buffer{}, 0)
				test.NoErr(t, err)

				//t.Logf("using policy\n")
				authSess = Policy(sess)
			} else {
				//t.Logf("using password\n")
				authSess = Password("")
			}

			//t.Log("ActivateCredential")
			cred2, err := ActivateCredential(ctx, dev, haik, hek, Password(""), authSess, credBlob, encryptedSecret)
			if test.NoErr(t, err) {
				//t.Logf("credential: %s\n", cred2)
				test.IsEq(t, cred2, Buffer("secret"))
			}
		})
	}
}

func TestStartAuthSessions(t *testing.T) {
	dev := SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()
	rng := mrand.New(mrand.NewSource(42))

	tmplaik := Template{
		Public{
			Type:             TPM_ALG_ECC,
			NameAlg:          TPM_ALG_SHA256,
			ObjectAttributes: TPMA_OBJECT_RESTRICTED_SIGN,
			EccParameters: &EccParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_NULL,
				},
				Scheme: Scheme{
					Scheme:  TPM_ALG_ECDSA,
					HashAlg: TPM_ALG_SHA256,
				},
				CurveID: TPM_ECC_NIST_P256,
				KDF: Scheme{
					Scheme: TPM_ALG_NULL,
				},
			},
		},
	}
	ctx := context.Background()
	_ = Startup(ctx, dev, TPM_SU_CLEAR)

	// we need a policy to use this EK
	test.IsEq(t, EkTemplateL1.Public.ObjectAttributes&TPMA_OBJECT_USERWITHAUTH, U32(0))

	hek, pubek, _, _, _, _, err := CreatePrimary(ctx, dev, Password(""), TPM_RH_OWNER, "", EkTemplateL1, Buffer{}, PcrSelection{})
	if test.NoErr(t, err) {
		defer func() { _ = FlushContext(ctx, dev, hek) }()
	}

	haik, pubaik, _, _, _, _, err := CreatePrimary(ctx, dev, Password(""), TPM_RH_OWNER, "", tmplaik, Buffer{}, PcrSelection{})
	if test.NoErr(t, err) {
		defer func() { _ = FlushContext(ctx, dev, haik) }()
	}

	credBlob, encryptedSecret, err := pubaik.MakeCredential(pubek, Buffer("secret"), rng)
	test.NoErr(t, err)

	nonce := Buffer(make([]byte, 32))
	_, err = rng.Read(nonce)
	test.NoErr(t, err)

	sess, err := StartAuthSession(ctx, dev, TPM_ALG_SHA256, rng)
	if test.NoErr(t, err) {
		defer func() { _ = FlushContext(ctx, dev, sess) }()
	}

	_, _, err = PolicySecret(ctx, dev, TPM_RH_ENDORSEMENT, sess, Password(""), Buffer{}, Buffer{}, Buffer{}, 0)
	test.NoErr(t, err)

	cred2, err := ActivateCredential(ctx, dev, haik, hek, Password(""), Policy(sess), credBlob, encryptedSecret)
	if test.NoErr(t, err) {
		t.Logf("credential: %s\n", cred2)
		test.IsEq(t, cred2, Buffer("secret"))
	}
}

func TestTpmProp(t *testing.T) {
	props := []struct {
		name string
		prop U32
	}{
		{"TPM_PT_FAMILY_INDICATOR", TPM_PT_FAMILY_INDICATOR},
		{"TPM_PT_LEVEL", TPM_PT_LEVEL},
		{"TPM_PT_REVISION", TPM_PT_REVISION},
		{"TPM_PT_DAY_OF_YEAR", TPM_PT_DAY_OF_YEAR},
		{"TPM_PT_YEAR", TPM_PT_YEAR},
		{"TPM_PT_MANUFACTURER", TPM_PT_MANUFACTURER},
		{"TPM_PT_VENDOR_STRING_1", TPM_PT_VENDOR_STRING_1},
		{"TPM_PT_VENDOR_STRING_2", TPM_PT_VENDOR_STRING_2},
		{"TPM_PT_VENDOR_STRING_3", TPM_PT_VENDOR_STRING_3},
		{"TPM_PT_VENDOR_STRING_4", TPM_PT_VENDOR_STRING_4},
		{"TPM_PT_VENDOR_TPM_TYPE", TPM_PT_VENDOR_TPM_TYPE},
		{"TPM_PT_FIRMWARE_VERSION_1", TPM_PT_FIRMWARE_VERSION_1},
		{"TPM_PT_FIRMWARE_VERSION_2", TPM_PT_FIRMWARE_VERSION_2},
	}

	for _, pair := range props {
		t.Run(pair.name, func(t *testing.T) {
			dev := SelectTestTpm(t)
			if dev == nil {
				return
			}
			defer dev.Close()

			ctx := context.Background()
			_ = Startup(ctx, dev, TPM_SU_CLEAR)

			val, err := TpmProperty(ctx, dev, uint32(pair.prop))
			test.NoErr(t, err)

			t.Logf("%s: 0x%08x\n", pair.name, val)
		})
	}
}
