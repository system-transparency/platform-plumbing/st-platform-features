package tpm

import "errors"

const (
	// Command codes for the SWTPM frame protocol
	TPM2_SEND_COMMAND U32 = 8

	// Magic number used on TPM-generated, signed structures
	TPM_GENERATED_VALUE = 0xff544347

	// TPM 2.0 command tags
	TPM_ST_NO_SESSIONS U16 = 0x8001
	TPM_ST_SESSIONS    U16 = 0x8002

	// Password session handle
	TPM_RS_PW U32 = 0x40000009

	// Supported auth session attributes
	CONTINUE_SESSION u8 = 0x01

	// TPM handle types
	TPM_HT_NV_INDEX     u8 = 0x01
	TPM_HT_HMAC_SESSION u8 = 0x02

	// TPMT_ATTEST tags
	TPM_ST_ATTEST_NV            U16 = 0x8014
	TPM_ST_ATTEST_COMMAND_AUDIT U16 = 0x8015
	TPM_ST_ATTEST_SESSION_AUDIT U16 = 0x8016
	TPM_ST_ATTEST_CERTIFY       U16 = 0x8017
	TPM_ST_ATTEST_QUOTE         U16 = 0x8018
	TPM_ST_ATTEST_TIME          U16 = 0x8019
	TPM_ST_ATTEST_CREATION      U16 = 0x801a

	// Supported TPM 2.0 command codes
	TPM_CC_CreatePrimary      U32 = 0x00000131
	TPM_CC_FlushContext       U32 = 0x00000165
	TPM_CC_NV_Read            U32 = 0x0000014e
	TPM_CC_NV_ReadPublic      U32 = 0x00000169
	TPM_CC_ActivateCredential U32 = 0x00000147
	TPM_CC_GetCapability      U32 = 0x0000017a
	TPM_CC_PCR_Read           U32 = 0x0000017e
	TPM_CC_Quote              U32 = 0x00000158
	TPM_CC_PCR_Event          U32 = 0x0000013c
	TPM_CC_PCR_Extend         U32 = 0x00000182
	TPM_CC_Startup            U32 = 0x00000144
	TPM_CC_StartAuthSession   U32 = 0x00000176
	TPM_CC_PolicySecret       U32 = 0x00000151

	// TPM 2.0 response codes
	TPM_RC_SUCCESS U32 = 0x00000000

	// Format zero response codes
	TPM_RC_INITIALIZE        U32 = 0x100
	TPM_RC_FAILURE           U32 = 0x101
	TPM_RC_SEQUENCE          U32 = 0x103
	TPM_RC_DISABLED          U32 = 0x120
	TPM_RC_EXCLUSIVE         U32 = 0x121
	TPM_RC_AUTH_TYPE         U32 = 0x124
	TPM_RC_AUTH_MISSING      U32 = 0x125
	TPM_RC_POLICY            U32 = 0x126
	TPM_RC_PCR               U32 = 0x127
	TPM_RC_PCR_CHANGED       U32 = 0x128
	TPM_RC_UPGRADE           U32 = 0x12D
	TPM_RC_TOO_MANY_CONTEXTS U32 = 0x12E
	TPM_RC_AUTH_UNAVAILABLE  U32 = 0x12F
	TPM_RC_REBOOT            U32 = 0x130
	TPM_RC_UNBALANCED        U32 = 0x131
	TPM_RC_COMMAND_SIZE      U32 = 0x142
	TPM_RC_COMMAND_CODE      U32 = 0x143
	TPM_RC_AUTHSIZE          U32 = 0x144
	TPM_RC_AUTH_CONTEXT      U32 = 0x145
	TPM_RC_NV_RANGE          U32 = 0x146
	TPM_RC_NV_SIZE           U32 = 0x147
	TPM_RC_NV_LOCKED         U32 = 0x148
	TPM_RC_NV_AUTHORIZATION  U32 = 0x149
	TPM_RC_NV_UNINITIALIZED  U32 = 0x14A
	TPM_RC_NV_SPACE          U32 = 0x14B
	TPM_RC_NV_DEFINED        U32 = 0x14C
	TPM_RC_BAD_CONTEXT       U32 = 0x150
	TPM_RC_CPHASH            U32 = 0x151
	TPM_RC_PARENT            U32 = 0x152
	TPM_RC_NEEDS_TEST        U32 = 0x153
	TPM_RC_NO_RESULT         U32 = 0x154
	TPM_RC_SENSITIVE         U32 = 0x155

	// Format one response codes
	TPM_RC_ASYMMETRIC    U32 = 0x081
	TPM_RC_ATTRIBUTES    U32 = 0x082
	TPM_RC_HASH          U32 = 0x083
	TPM_RC_VALUE         U32 = 0x084
	TPM_RC_HIERARCHY     U32 = 0x085
	TPM_RC_KEY_SIZE      U32 = 0x087
	TPM_RC_MGF           U32 = 0x088
	TPM_RC_MODE          U32 = 0x089
	TPM_RC_TYPE          U32 = 0x08A
	TPM_RC_HANDLE        U32 = 0x08B
	TPM_RC_KDF           U32 = 0x08C
	TPM_RC_RANGE         U32 = 0x08D
	TPM_RC_AUTH_FAIL     U32 = 0x08E
	TPM_RC_NONCE         U32 = 0x08F
	TPM_RC_PP            U32 = 0x090
	TPM_RC_SCHEME        U32 = 0x092
	TPM_RC_SIZE          U32 = 0x095
	TPM_RC_SYMMETRIC     U32 = 0x096
	TPM_RC_TAG           U32 = 0x097
	TPM_RC_SELECTOR      U32 = 0x098
	TPM_RC_INSUFFICIENT  U32 = 0x09a
	TPM_RC_SIGNATURE     U32 = 0x09b
	TPM_RC_KEY           U32 = 0x09c
	TPM_RC_POLICY_FAIL   U32 = 0x09d
	TPM_RC_INTEGRITY     U32 = 0x09f
	TPM_RC_TICKET        U32 = 0x0A0
	TPM_RC_RESERVED_BITS U32 = 0x0A1
	TPM_RC_BAD_AUTH      U32 = 0x0A2
	TPM_RC_EXPIRED       U32 = 0x0A3
	TPM_RC_POLICY_CC     U32 = 0x0A4
	TPM_RC_BINDING       U32 = 0x0A5
	TPM_RC_CURVE         U32 = 0x0A6
	TPM_RC_ECC_POINT     U32 = 0x0A7

	// Warning response codes
	TPM_RC_CONTEXT_GAP     U32 = 0x901
	TPM_RC_OBJECT_MEMORY   U32 = 0x902
	TPM_RC_SESSION_MEMORY  U32 = 0x903
	TPM_RC_MEMORY          U32 = 0x904
	TPM_RC_SESSION_HANDLES U32 = 0x905
	TPM_RC_OBJECT_HANDLES  U32 = 0x906
	TPM_RC_LOCALITY        U32 = 0x907
	TPM_RC_YIELDED         U32 = 0x908
	TPM_RC_CANCELED        U32 = 0x909
	TPM_RC_TESTING         U32 = 0x90A
	TPM_RC_NV_RATE         U32 = 0x920
	TPM_RC_LOCKOUT         U32 = 0x921
	TPM_RC_RETRY           U32 = 0x922
	TPM_RC_NV_UNAVAILABLE  U32 = 0x923

	// TPM2_Startup modes
	TPM_SU_CLEAR U16 = 0x0000
	TPM_SU_STATE U16 = 0x0001

	// Hierarchy handles
	TPM_RH_ENDORSEMENT U32 = 0x4000000B
	TPM_RH_OWNER       U32 = 0x40000001
	TPM_RH_PLATFORM    U32 = 0x4000000C
	TPM_RH_NULL        U32 = 0x40000007

	// Supported TPM 2.0 algorithms
	TPM_ALG_RSA            U16 = 0x0001
	TPM_ALG_SHA1           U16 = 0x0004
	TPM_ALG_SHA256         U16 = 0x000B
	TPM_ALG_SHA384         U16 = 0x000c
	TPM_ALG_SHA512         U16 = 0x000d
	TPM_ALG_SM3_256        U16 = 0x0012
	TPM_ALG_AES            U16 = 0x0006
	TPM_ALG_CFB            U16 = 0x0043
	TPM_ALG_NULL           U16 = 0x0010
	TPM_ALG_ECDSA          U16 = 0x0018
	TPM_ALG_ECDH           U16 = 0x0019
	TPM_ALG_KDF1_SP800_56A U16 = 0x0020
	TPM_ALG_KDF2           U16 = 0x0021
	TPM_ALG_ECC            U16 = 0x0023

	// Supported TPM 2.0 ECC curves
	TPM_ECC_NIST_P256 U16 = 0x0003
	TPM_ECC_NIST_P384 U16 = 0x0004
	TPM_ECC_NIST_P521 U16 = 0x0005

	// TPM 2.0 session types
	TPM_SE_POLICY = 0x01
	TPM_SE_TRAIL  = 0x03

	// TPM 2.0 object attributes
	TPMA_OBJECT_FIXEDTPM            U32 = 0x00000002
	TPMA_OBJECT_STCLEAR             U32 = 0x00000004
	TPMA_OBJECT_FIXEDPARENT         U32 = 0x00000010
	TPMA_OBJECT_SENSITIVEDATAORIGIN U32 = 0x00000020
	TPMA_OBJECT_USERWITHAUTH        U32 = 0x00000040
	TPMA_OBJECT_ADMINWITHPOLICY     U32 = 0x00000080
	TPMA_OBJECT_NODA                U32 = 0x00000400
	TPMA_OBJECT_RESTRICTED          U32 = 0x00010000
	TPMA_OBJECT_DECRYPT             U32 = 0x00020000
	TPMA_OBJECT_SIGN                U32 = 0x00040000

	// TPM 2.0 object attribute shorthands
	TPMA_OBJECT_RESTRICTED_SIGN U32 = TPMA_OBJECT_FIXEDTPM |
		TPMA_OBJECT_FIXEDPARENT |
		TPMA_OBJECT_SENSITIVEDATAORIGIN |
		TPMA_OBJECT_USERWITHAUTH |
		TPMA_OBJECT_NODA |
		TPMA_OBJECT_RESTRICTED |
		TPMA_OBJECT_SIGN
	TPMA_OBJECT_RESTRICTED_DECRYPT U32 = TPMA_OBJECT_FIXEDTPM |
		TPMA_OBJECT_FIXEDPARENT |
		TPMA_OBJECT_SENSITIVEDATAORIGIN |
		TPMA_OBJECT_USERWITHAUTH |
		TPMA_OBJECT_NODA |
		TPMA_OBJECT_RESTRICTED |
		TPMA_OBJECT_DECRYPT
	TPMA_OBJECT_ENDORSEMENT_KEY U32 = TPMA_OBJECT_FIXEDTPM |
		TPMA_OBJECT_FIXEDPARENT |
		TPMA_OBJECT_SENSITIVEDATAORIGIN |
		TPMA_OBJECT_ADMINWITHPOLICY |
		TPMA_OBJECT_RESTRICTED |
		TPMA_OBJECT_DECRYPT
	TPMA_OBJECT_ENDORSEMENT_KEY_USERWITHAUTH U32 = TPMA_OBJECT_ENDORSEMENT_KEY |
		TPMA_OBJECT_USERWITHAUTH

	// TPM 2.0 NVRAM index attributes
	TPMA_NV_AUTHREAD   U32 = 1 << 18
	TPMA_NV_READLOCKED U32 = 1 << 28

	// Supported capabilities for TPM2_GetCapability
	TPM_CAP_ALGS           U32 = 0x00000000
	TPM_CAP_HANDLES        U32 = 0x00000001
	TPM_CAP_COMMANDS       U32 = 0x00000002
	TPM_CAP_PCRS           U32 = 0x00000005
	TPM_CAP_TPM_PROPERTIES U32 = 0x00000006
	TPM_CAP_ECC_CURVES     U32 = 0x00000008

	// Supported fixed TPM properties for TPM2_GetCapability(TPM_CAP_TPM_PROPERTIES)
	TPM_PT_FAMILY_INDICATOR   U32 = 0x00000100
	TPM_PT_LEVEL              U32 = 0x00000101
	TPM_PT_REVISION           U32 = 0x00000102
	TPM_PT_DAY_OF_YEAR        U32 = 0x00000103
	TPM_PT_YEAR               U32 = 0x00000104
	TPM_PT_MANUFACTURER       U32 = 0x00000105
	TPM_PT_VENDOR_STRING_1    U32 = 0x00000106
	TPM_PT_VENDOR_STRING_2    U32 = 0x00000107
	TPM_PT_VENDOR_STRING_3    U32 = 0x00000108
	TPM_PT_VENDOR_STRING_4    U32 = 0x00000109
	TPM_PT_VENDOR_TPM_TYPE    U32 = 0x0000010a
	TPM_PT_FIRMWARE_VERSION_1 U32 = 0x0000010b
	TPM_PT_FIRMWARE_VERSION_2 U32 = 0x0000010c
	TPM_PT_NV_BUFFER_MAX      U32 = 0x0000012c

	// 2.0 Familiy, returned by GetCapability(TPM_CAP_TPM_PROPERTIES, TPM_PT_FAMILY_INDICATOR)
	TPM_SPEC_FAMILY = 0x322e3000

	// Official Endorsement Key templates defined by the TCG EK Credential
	// Profile version 2.5, rev 2

	// RSA 2048 EK Certificate (L-1)
	EkCertificateIndexL1 = 0x01c00002
	// RSA 2048 EK Nonce
	EkNonceIndexL1 = 0x01c00003
	// RSA 2048 EK Template
	EkTemplateIndexL1 = 0x01c00004
	// ECC NIST P256 EK Certificate
	EkCertificateIndexL2 = 0x01c0000a
	// ECC NIST P256 EK Nonce
	EkNonceIndexL2 = 0x01c0000b
	// ECC NIST P256 EK Template
	EkTemplateIndexL2 = 0x01c0000c

	// RSA 2048 EK Certificate (H-1)
	EkCertificateIndexH1 = 0x01c00012
	// ECC NIST P256 EK Certificate (H-2)
	EkCertificateIndexH2 = 0x01c00014
	// ECC NIST P384 EK Certificate (H-3)
	EkCertificateIndexH3 = 0x01c00016
	// ECC NIST P521 EK Certificate (H-4)
	EkCertificateIndexH4 = 0x01c00018
	// RSA 3072 EK Certificate (H-6)
	EkCertificateIndexH6 = 0x01c0001c
	// RSA 4096 EK Certificate (H-7)
	EkCertificateIndexH7 = 0x01c0001e

	// Used for decoding TPMS_PCR_SELECT bit fields. See PcrSelect.encode() for
	// details.
	NumSupportedPcrs = 24
)

var (
	// Received encoding is wrong.
	ErrFormat = errors.New("invalid format")
	// wire instance is a nil pointer
	ErrNilPointer = errors.New("nil pointer")
	// raw instance given in decode().
	ErrRawDecode = errors.New("raw cannot be decoded")

	// Error values for TPM_RC_*. These are returned by the commands when the
	// corresponding response code is returned by the TPM.
	ErrRcInitialize      = errors.New("TPM not initialized")
	ErrRcFailure         = errors.New("TPM in failure mode")
	ErrRcSequence        = errors.New("improper use of sequence handle")
	ErrRcDisabled        = errors.New("command disabled")
	ErrRcExclusive       = errors.New("audit sequence requires exclusivity")
	ErrRcAuthType        = errors.New("authorization handle is not correct for command")
	ErrRcAuthMissing     = errors.New("command requires an authorization session for handle")
	ErrRcPolicy          = errors.New("policy failure in the authorization session")
	ErrRcPcr             = errors.New("PCR check fail")
	ErrRcPcrChanged      = errors.New("PCR have changed since checked")
	ErrRcUpgrade         = errors.New("TPM is in field upgrade mode")
	ErrRcTooManyContext  = errors.New("context ID counter is at maximum")
	ErrRcAuthUnavailable = errors.New("authValue or authPolicy is not available for selected entity")
	ErrRcReboot          = errors.New("a _TPM_Init and Startup(CLEAR) is required")
	ErrRcUnbalanced      = errors.New("the protection algorithms (hash and symmetric) are not reasonably balanced")
	ErrRcCommandSize     = errors.New("command commandSize value is inconsistent with contents of the command buffer")
	ErrRcCommandCode     = errors.New("command code not supported")
	ErrRcAuthsize        = errors.New("authorization size is out of range")
	ErrRcAuthContext     = errors.New("authorization session not allowed for provided context")
	ErrRcNvRange         = errors.New("NV offset + size is out of range")
	ErrRcNvSize          = errors.New("requested allocation size is larger than allowed")
	ErrRcNvLocked        = errors.New("NV access locked")
	ErrRcNvAuthorization = errors.New("NV access authorization fails")
	ErrRcNvUninitialized = errors.New("an NV Index is used before being initialized")
	ErrRcNvSpace         = errors.New("insufficient space for NV allocation")
	ErrRcNvDefined       = errors.New("NV index or persistent object already defined")
	ErrRcBadContext      = errors.New("context in TPM2_ContextLoad() is not valid")
	ErrRcCphash          = errors.New("cpHash value already set or not correct for use")
	ErrRcParent          = errors.New("handle for parent is not a valid parent")
	ErrRcNeedsTest       = errors.New("some function needs testing")
	ErrRcNoResult        = errors.New("returned when an internal function cannot process a request due to an unspecified problem")
	ErrRcSensitive       = errors.New("the sensitive area did not unmarshal correctly after decryption")

	ErrRcAsymmetric   = errors.New("asymmetric algorithm not supported or not correct")
	ErrRcAttributes   = errors.New("inconsistent attributes")
	ErrRcHash         = errors.New("hash algorithm not supported or not appropriate")
	ErrRcValue        = errors.New("value is out of range or is not correct for the context")
	ErrRcHierarchy    = errors.New("hierarchy is not enabled or is not correct for the use")
	ErrRcKeySize      = errors.New("key size is not supported")
	ErrRcMgf          = errors.New("mask generation function not supported")
	ErrRcMode         = errors.New("mode of operation not supported")
	ErrRcType         = errors.New("the type of the value is not appropriate for the use")
	ErrRcHandle       = errors.New("the handle is not correct for the use")
	ErrRcKdf          = errors.New("unsupported key derivation function or function not appropriate for use")
	ErrRcRange        = errors.New("value was out of allowed range")
	ErrRcAuthFail     = errors.New("the authorization HMAC check failed and DA counter incremented")
	ErrRcNonce        = errors.New("invalid nonce size or nonce value mismatch")
	ErrRcPp           = errors.New("authorization requires assertion of Physical Presence")
	ErrRcScheme       = errors.New("unsupported or incompatible scheme")
	ErrRcSize         = errors.New("structure is the wrong size")
	ErrRcSymmetric    = errors.New("unsupported symmetric algorithm or key size, or not appropriate for instance")
	ErrRcTag          = errors.New("incorrect structure tag")
	ErrRcSelector     = errors.New("union selector is incorrect")
	ErrRcInsufficient = errors.New("the TPM was unable to unmarshal a value because there were not enough bytes in the input buffer")
	ErrRcSignature    = errors.New("the signature is not valid")
	ErrRcKey          = errors.New("key fields are not compatible with the selected use")
	ErrRcPolicyFail   = errors.New("a policy check failed")
	ErrRcIntegrity    = errors.New("integrity check failed")
	ErrRcTicket       = errors.New("invalid ticket")
	ErrRcReservedBits = errors.New("reserved bits not set to zero as required")
	ErrRcBadAuth      = errors.New("authorization failure without DA implications")
	ErrRcExpired      = errors.New("the policy has expired")
	ErrRcPolicyCc     = errors.New("the commandCode in the policy is not the commandCode of the command")
	ErrRcBinding      = errors.New("public and sensitive portions of an object are not cryptographically bound")
	ErrRcCurve        = errors.New("curve not supported")
	ErrRcEccPoint     = errors.New("point is not on the required curve")

	ErrRcContextGap     = errors.New("gap for context ID is too large")
	ErrRcObjectMemory   = errors.New("out of memory for object contexts")
	ErrRcSessionMemory  = errors.New("out of memory for session contexts")
	ErrRcMemory         = errors.New("out of memory")
	ErrRcSessionHandles = errors.New("out of session handles")
	ErrRcObjectHandles  = errors.New("out of object handles")
	ErrRcLocality       = errors.New("bad locality")
	ErrRcYielded        = errors.New("command needs to be restarted")
	ErrRcCanceled       = errors.New("command was canceled")
	ErrRcTesting        = errors.New("TPM is in test mode")
	ErrRcNvRate         = errors.New("NV is not available because of rate limit")
	ErrRcLockout        = errors.New("authorizations for objects subject to DA protection are not allowed at this time because the TPM is in DA lockout mode")
	ErrRcRetry          = errors.New("the TPM was not able to start the command")
	ErrRcNvUnavailable  = errors.New("the command may require writing of NV and NV is not current accessible")

	ErrRcUnknown   = errors.New("unknown response code")
	ErrTpm12Error  = errors.New("TPM 1.2 error")
	ErrVendorError = errors.New("vendor specific error")

	// Low range Endorsement Key templates. Copied from TCG EK Credential Profile
	// version 2.5, rev 2

	policyA = Buffer{
		0x83, 0x71, 0x97, 0x67, 0x44, 0x84, 0xB3, 0xF8, 0x1A, 0x90, 0xCC, 0x8D,
		0x46, 0xA5, 0xD7, 0x24, 0xFD, 0x52, 0xD7, 0x6E, 0x06, 0x52, 0x0B, 0x64,
		0xF2, 0xA1, 0xDA, 0x1B, 0x33, 0x14, 0x69, 0xAA,
	}
	policyB256 = Buffer{
		0xCA, 0x3D, 0x0A, 0x99, 0xA2, 0xB9, 0x39, 0x06, 0xF7, 0xA3, 0x34, 0x24,
		0x14, 0xEF, 0xCF, 0xB3, 0xA3, 0x85, 0xD4, 0x4C, 0xD1, 0xFD, 0x45, 0x90,
		0x89, 0xD1, 0x9B, 0x50, 0x71, 0xC0, 0xB7, 0xA0,
	}
	policyB384 = Buffer{
		0xB2, 0x6E, 0x7D, 0x28, 0xD1, 0x1A, 0x50, 0xBC, 0x53, 0xD8, 0x82, 0xBC,
		0xF5, 0xFD, 0x3A, 0x1A, 0x07, 0x41, 0x48, 0xBB, 0x35, 0xD3, 0xB4, 0xE4,
		0xCB, 0x1C, 0x0A, 0xD9, 0xBD, 0xE4, 0x19, 0xCA, 0xCB, 0x47, 0xBA, 0x09,
		0x69, 0x96, 0x46, 0x15, 0x0F, 0x9F, 0xC0, 0x00, 0xF3, 0xF8, 0x0E, 0x12,
	}
	policyB512 = Buffer{
		0xB8, 0x22, 0x1C, 0xA6, 0x9E, 0x85, 0x50, 0xA4, 0x91, 0x4D, 0xE3, 0xFA,
		0xA6, 0xA1, 0x8C, 0x07, 0x2C, 0xC0, 0x12, 0x08, 0x07, 0x3A, 0x92, 0x8D,
		0x5D, 0x66, 0xD5, 0x9E, 0xF7, 0x9E, 0x49, 0xA4, 0x29, 0xC4, 0x1A, 0x6B,
		0x26, 0x95, 0x71, 0xD5, 0x7E, 0xDB, 0x25, 0xFB, 0xDB, 0x18, 0x38, 0x42,
		0x56, 0x08, 0xB4, 0x13, 0xCD, 0x61, 0x6A, 0x5F, 0x6D, 0xB5, 0xB6, 0x07,
		0x1A, 0xF9, 0x9B, 0xEA,
	}

	// L-1: RSA 2048 (Storage)
	EkTemplateL1 = Template{
		Public{
			Type:             TPM_ALG_RSA,
			NameAlg:          TPM_ALG_SHA256,
			ObjectAttributes: TPMA_OBJECT_ENDORSEMENT_KEY,
			AuthPolicy:       policyA,
			RsaParameters: &RsaParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_AES,
					KeyBits:   128,
					Mode:      TPM_ALG_CFB,
				},
				Scheme: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				KeyBits:  2048,
				Exponent: 0,
				Unique:   Buffer(make([]byte, 256)),
			},
		},
	}

	// L-2: ECC NIST P256 (Storage)
	EkTemplateL2 = Template{
		Public{
			Type:             TPM_ALG_ECC,
			NameAlg:          TPM_ALG_SHA256,
			ObjectAttributes: TPMA_OBJECT_ENDORSEMENT_KEY,
			AuthPolicy:       policyA,
			EccParameters: &EccParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_AES,
					KeyBits:   128,
					Mode:      TPM_ALG_CFB,
				},
				Scheme: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				CurveID: TPM_ECC_NIST_P256,
				KDF: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				Unique: EccPoint{
					X: Buffer(make([]byte, 32)),
					Y: Buffer(make([]byte, 32)),
				},
			},
		},
	}

	// High range Endorsement Key templates. Copied from TCG EK Credential
	// Profile version 2.5, rev 2

	// H-1: RSA 2048 (Storage)
	EkTemplateH1 = Template{
		Public{
			Type:             TPM_ALG_RSA,
			NameAlg:          TPM_ALG_SHA256,
			ObjectAttributes: TPMA_OBJECT_ENDORSEMENT_KEY_USERWITHAUTH,
			AuthPolicy:       policyB256,
			RsaParameters: &RsaParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_AES,
					KeyBits:   128,
					Mode:      TPM_ALG_CFB,
				},
				Scheme: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				KeyBits: 2048,
				Unique:  Buffer(nil),
			},
		},
	}

	// H-2: ECC NIST P256 (Storage)
	EkTemplateH2 = Template{
		Public{
			Type:             TPM_ALG_ECC,
			NameAlg:          TPM_ALG_SHA256,
			ObjectAttributes: TPMA_OBJECT_ENDORSEMENT_KEY_USERWITHAUTH,
			AuthPolicy:       policyB256,
			EccParameters: &EccParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_AES,
					KeyBits:   128,
					Mode:      TPM_ALG_CFB,
				},
				Scheme: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				KDF: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				CurveID: TPM_ECC_NIST_P256,
				Unique: EccPoint{
					X: Buffer(nil),
					Y: Buffer(nil),
				},
			},
		},
	}

	// H-3: ECC NIST P384 (Storage)
	EkTemplateH3 = Template{
		Public{
			Type:             TPM_ALG_ECC,
			NameAlg:          TPM_ALG_SHA384,
			ObjectAttributes: TPMA_OBJECT_ENDORSEMENT_KEY_USERWITHAUTH,
			AuthPolicy:       policyB384,
			EccParameters: &EccParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_AES,
					KeyBits:   256,
					Mode:      TPM_ALG_CFB,
				},
				Scheme: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				KDF: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				CurveID: TPM_ECC_NIST_P384,
				Unique: EccPoint{
					X: Buffer(nil),
					Y: Buffer(nil),
				},
			},
		},
	}

	/// H-4: ECC NIST P521 (Storage)
	EkTemplateH4 = Template{
		Public{
			Type:             TPM_ALG_ECC,
			NameAlg:          TPM_ALG_SHA512,
			ObjectAttributes: TPMA_OBJECT_ENDORSEMENT_KEY_USERWITHAUTH,
			AuthPolicy:       policyB512,
			EccParameters: &EccParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_AES,
					KeyBits:   256,
					Mode:      TPM_ALG_CFB,
				},
				Scheme: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				KDF: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				CurveID: TPM_ECC_NIST_P521,
				Unique: EccPoint{
					X: Buffer(nil),
					Y: Buffer(nil),
				},
			},
		},
	}

	// H-6: RSA 3072 (Storage)
	EkTemplateH6 = Template{
		Public{
			Type:             TPM_ALG_RSA,
			NameAlg:          TPM_ALG_SHA384,
			ObjectAttributes: TPMA_OBJECT_ENDORSEMENT_KEY_USERWITHAUTH,
			AuthPolicy:       policyB384,
			RsaParameters: &RsaParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_AES,
					KeyBits:   256,
					Mode:      TPM_ALG_CFB,
				},
				Scheme: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				KeyBits: 3072,
				Unique:  Buffer(nil),
			},
		},
	}

	// H-7: RSA 4096 (Storage)
	EkTemplateH7 = Template{
		Public{
			Type:             TPM_ALG_RSA,
			NameAlg:          TPM_ALG_SHA384,
			ObjectAttributes: TPMA_OBJECT_ENDORSEMENT_KEY_USERWITHAUTH,
			AuthPolicy:       policyB384,
			RsaParameters: &RsaParms{
				Symmetric: SymDefObject{
					Algorithm: TPM_ALG_AES,
					KeyBits:   256,
					Mode:      TPM_ALG_CFB,
				},
				Scheme: Scheme{
					Scheme: TPM_ALG_NULL,
				},
				KeyBits: 4096,
				Unique:  Buffer(nil),
			},
		},
	}

	// Structure variant or TPM result is not supported.
	ErrUnsupported = errors.New("unsupported")

	// Returned by TpmProperty
	ErrPropertyNotFound = errors.New("no such property")

	// Labels for various key derivation functions
	identityLabel = []byte("IDENTITY\x00")

	// TCG-registed vendor IDs: TCG TPM Vendor ID Registry Family 1.2 and 2.0
	// version 1.06, rev 0.94
	// Return values of GetCapability(TPM_CAP_TPM_PROPERTIES, TPM_PT_MANUFACTURER)
	VendorIds = map[uint32]string{
		0x414d4400: "AMD",
		0x414e5400: "Ant Group",
		0x41544d4c: "Atmel",
		0x4252434d: "Broadcom",
		0x4353434f: "Cisco",
		0x464c5953: "Flyslice Technologies",
		0x524f4343: "Fuzhou Rockchip",
		0x474f4f47: "Google",
		0x48504900: "HPI",
		0x48504500: "HPE",
		0x48495349: "Huawei",
		0x49424d00: "IBM",
		0x49465800: "Infineon",
		0x494e5443: "Intel",
		0x4c454e00: "Lenovo",
		0x4d534654: "Microsoft",
		0x4e534d20: "National Semiconductor",
		0x4e545a00: "Nationz",
		0x4e544300: "Nuvoton Technology",
		0x51434f4d: "Qualcomm",
		0x534d534e: "Samsung",
		0x534e5300: "Sinosun",
		0x534d5343: "SMSC",
		0x53544d20: "ST Microelectronics",
		0x54584e00: "Texas Instruments",
		0x57454300: "Winbond",
	}
)
