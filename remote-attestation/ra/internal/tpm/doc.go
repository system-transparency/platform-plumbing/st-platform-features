// Low level routines for encoding and decoding TPM 2.0 structures and commands.
//
// This package implements a subset of TCG TPM 2.0 specification rev 1.59. It
// handles the encoding and decoding of TPM 2.0 commands and responses, basic
// cryptographic operations, and low-level TPM device access.
//
// The package only implements what is strictly necessary for System
// Transparency's remote attestation and is not meant to used outside of the
// context of this project.
//
// The package implements the following TPM 2.0 commands:
//   - TPM2_CreatePrimary
//   - TPM2_FlushContext
//   - TPM2_NV_Read
//   - TPM2_NV_ReadPublic
//   - TPM2_ActivateCredential
//   - TPM2_GetCapability
//   - TPM2_PCR_Read
//   - TPM2_Quote
//   - TPM2_PCR_Event
//   - TPM2_Startup
//   - TPM2_StartAuthSession (only Policy sessions, no binding or salting)
//   - TPM2_PolicySecret
//
// The package only implments encoding and decoding of structures that are used
// in these commands.
//
// Each command is implemented as a function that takes the necessary arguments
// for that particual commmand. The function returns the response from the TPM
// or an error.No sanity checking or processing of inputs is done by the
// package. The caller is responsible for ensuring that the inputs are valid.
//
// For authentication each command takes an auth parameter. This parameter is
// either a password string or a handle to a policy session.
package tpm
