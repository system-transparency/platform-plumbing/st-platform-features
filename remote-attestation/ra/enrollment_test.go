package ra

import (
	"bytes"
	"context"
	"crypto"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	mrand "math/rand"
	"os"
	"strings"
	"testing"

	test "system-transparency.org/ppg/ra/internal"
	"system-transparency.org/ppg/ra/internal/tpm"
)

func TestListNvHandles(t *testing.T) {
	dev := tpm.SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()

	ctx := context.Background()
	_ = tpm.Startup(ctx, dev, tpm.TPM_SU_CLEAR)

	list, err := listNvIndexes(context.Background(), dev)
	test.NoErr(t, err)

	for _, idx := range list {
		switch idx {
		case tpm.EkCertificateIndexL1:
			t.Log("L-1 provisioned")
		case tpm.EkCertificateIndexL2:
			t.Log("L-2 provisioned")
		case tpm.EkCertificateIndexH1:
			t.Log("H-1 provisioned")
		case tpm.EkCertificateIndexH2:
			t.Log("H-2 provisioned")
		case tpm.EkCertificateIndexH3:
			t.Log("H-3 provisioned")
		case tpm.EkCertificateIndexH4:
			t.Log("H-4 provisioned")
		case tpm.EkCertificateIndexH6:
			t.Log("H-6 provisioned")
		case tpm.EkCertificateIndexH7:
			t.Log("H-7 provisioned")
		default:
			t.Logf("unknown index 0x%08x\n", idx)
		}
	}
}

func TestEkSelect(t *testing.T) {
	dev := tpm.SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()

	ctx := context.Background()

	_ = tpm.Startup(ctx, dev, tpm.TPM_SU_CLEAR)

	_, _, _, err := selectEndorsementKey(ctx, dev, "")
	test.NoErr(t, err)
}

func TestPrereq(t *testing.T) {
	dev := tpm.SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()

	ctx := context.Background()
	_ = tpm.Startup(ctx, dev, tpm.TPM_SU_CLEAR)

	_, err := checkModulePrerequisits(ctx, dev)
	test.NoErr(t, err)
}

func TestAikDeriv(t *testing.T) {
	dev := tpm.SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()

	ctx := context.Background()

	_ = tpm.Startup(ctx, dev, tpm.TPM_SU_CLEAR)

	_, err := deriveAttestationIdentityKey(ctx, dev, "")
	test.NoErr(t, err)
}

func TestRequestEnrollment(t *testing.T) {
	dev := tpm.SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()

	ctx := context.Background()

	binlog, err := os.ReadFile("testdata/x11schln4f-910-0.3.5-small.eventlog")
	test.NoErr(t, err)

	evlog, err := transcodePcClientEventlog(bytes.NewReader(binlog))
	test.NoErr(t, err)

	reqopts := RequestOptions{
		DevicePath: "test",
		eventlog:   evlog,
		device:     dev,
	}
	req, err := Request(ctx, &reqopts)
	test.NoErr(t, err)

	signer := hmacSigner{Key: []byte("testkey")}
	chopts := ChallengeOptions{
		AllowUnknownCa: true,
		random:         mrand.New(mrand.NewSource(42)),
	}
	challenge, err := CreateChallenge(ctx, req, &signer, &chopts)
	test.NoErr(t, err)

	fopts := FinishOptions{
		DevicePath: "test",
		Verifier:   &signer,
		device:     dev,
	}
	_, err = Finish(ctx, challenge, &fopts)
	test.NoErr(t, err)
}

func FuzzBytesCodec(f *testing.F) {
	f.Add(credentialMagic[:], []byte("test"), []byte("fuzz"), []byte("bar"))
	f.Add(credentialMagic[:], []byte{}, []byte{}, []byte{})
	f.Add(credentialMagic[:], make([]byte, 4096), []byte("fuzz"), []byte("bar"))

	f.Fuzz(func(t *testing.T, magic []byte, f1 []byte, f2 []byte, f3 []byte) {
		var m [4]byte
		copy(m[:], magic[:4])

		buf := bytes.NewReader(encodeByteArrays(m, f1, f2, f3))
		var f1d, f2d, f3d []byte
		err := decodeByteArrays(buf, m, &f1d, &f2d, &f3d)
		test.NoErr(t, err)

		test.IsEq(t, f1, f1d)
		test.IsEq(t, f2, f2d)
		test.IsEq(t, f3, f3d)
	})
}

func FuzzBytesCodecFail(f *testing.F) {
	// field too short
	f.Add(credentialMagic[:], 1, []byte{'C', 'R', 'E', 'D', 5, 0, 0, 0, 1, 2, 3, 4})
	// wrong magic
	f.Add(credentialMagic[:], 1, []byte{'c', 'R', 'E', 'D', 5, 0, 0, 0, 1, 2, 3, 4, 5})
	// trailing data
	f.Add(credentialMagic[:], 1, []byte{'C', 'R', 'E', 'D', 5, 0, 0, 0, 1, 2, 3, 4, 5, 't', 'r', 'a', 'i', 'l', 'i', 'n', 'g'})
	// too short magic
	f.Add(credentialMagic[:], 1, []byte{'C', 'R', 'E'})
	// too short field size
	f.Add(credentialMagic[:], 1, []byte{'C', 'R', 'E', 'D', 0, 0, 0})

	f.Fuzz(func(t *testing.T, magic []byte, numFields int, data []byte) {
		var m [4]byte
		copy(m[:], magic[:4])

		fields := make([]*[]byte, numFields)
		for i := 0; i < numFields; i++ {
			fields[i] = &[]byte{}
		}
		err := decodeByteArrays(bytes.NewBuffer(data), m, fields...)
		test.WantErr(t, err)
	})
}

func TestFindNvIdx(t *testing.T) {
	_, err := findEndorsementKeyTemplate(0x11223344)
	test.WantErr(t, err)
}

func TestEkCertPkCheck(t *testing.T) {
	cases := []string{
		"normal",
		"normal-padded",
		"is-ca",
		"mismatch-type",
		"no-params",
		"mismatch-key-1",
		"mismatch-key-2",
		"der-truncated",
	}

	for _, alg := range []string{"ecc", "rsa"} {
		t.Run(alg, func(t *testing.T) {
			for _, c := range cases {
				t.Run(c, func(t *testing.T) {
					var tpmpub tpm.Public
					var cert x509.Certificate
					var priv crypto.PrivateKey
					var pub crypto.PublicKey

					if alg == "ecc" {
						tpmpub, cert, priv = fakeEccEk(t, rand.Reader)
						pub = priv.(*ecdsa.PrivateKey).Public()
					} else {
						tpmpub, cert, priv = fakeRsaEk(t, rand.Reader)
						pub = priv.(*rsa.PrivateKey).Public()
					}

					switch c {
					case "is-ca":
						cert.IsCA = true
					}

					der, err := x509.CreateCertificate(rand.Reader, &cert, &cert, pub, priv)
					test.NoErr(t, err)

					switch c {
					case "normal-padded":
						der = append(der, bytes.Repeat([]byte{0xff}, 256)...)
					case "mismatch-type":
						tpmpub.Type = tpm.TPM_ALG_AES
					case "no-params":
						tpmpub.EccParameters = nil
						tpmpub.RsaParameters = nil
					case "mismatch-key-1":
						switch alg {
						case "ecc":
							tpmpub.EccParameters.Unique.X[0] ^= 0xff
						case "rsa":
							tpmpub.RsaParameters.Exponent = 42
						}
					case "mismatch-key-2":
						switch alg {
						case "ecc":
							tpmpub.EccParameters.Unique.Y[0] ^= 0xff
						case "rsa":
							tpmpub.RsaParameters.Unique[0] ^= 0xff
						}
					case "der-truncated":
						der = der[:len(der)-1]
					}

					_, err = checkEndorsementCertificateAgainstKey(der, tpmpub)
					if strings.HasPrefix(c, "normal") {
						test.NoErr(t, err)
					} else {
						test.WantErr(t, err)
					}
				})
			}
		})
	}
}

func FuzzCheckModuleSpec(f *testing.F) {
	f.Add(int(tpm.TPM_SPEC_FAMILY), 2022, 200, true)
	f.Add(int(tpm.TPM_SPEC_FAMILY)-1, 2022, 200, false)
	f.Add(int(tpm.TPM_SPEC_FAMILY), 2012, 200, false)

	f.Fuzz(func(t *testing.T, family, year, doy int, outcome bool) {
		err := checkModuleCapabilitiesImpl(uint32(family), uint32(year), uint32(doy))
		if outcome {
			test.NoErr(t, err)
		} else {
			test.WantErr(t, err)
		}
	})
}

func FuzzCheckModulePcrs(f *testing.F) {
	f.Add(
		int(tpm.TPM_CAP_PCRS),
		int(tpm.TPM_ALG_SHA1), []byte{0, 1, 2, 3},
		int(tpm.TPM_ALG_SHA256), []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23},
		true)
	f.Add(
		int(tpm.TPM_CAP_PCRS),
		-1, []byte{},
		int(tpm.TPM_ALG_SHA1), []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23},
		false)
	f.Add(
		int(tpm.TPM_CAP_PCRS),
		-1, []byte{},
		int(tpm.TPM_ALG_SHA256), []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23},
		false)
	f.Add(
		int(tpm.TPM_CAP_PCRS),
		-1, []byte{},
		-1, []byte{},
		false)
	f.Add(
		int(tpm.TPM_CAP_ALGS),
		-1, []byte{},
		int(tpm.TPM_ALG_SHA256), []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23},
		false)

	f.Fuzz(func(t *testing.T, capa, h1 int, b1 []byte, h2 int, b2 []byte, outcome bool) {
		var pcrs tpm.PcrSelection

		if h1 > 0 {
			p1 := make([]int, len(b1))
			for i, v := range b1 {
				p1[i] = int(v)
			}
			pcrs = append(pcrs, tpm.PcrSelect{Hash: tpm.U16(h1), Pcr: p1})
		}
		if h2 > 0 {
			p2 := make([]int, len(b2))
			for i, v := range b2 {
				p2[i] = int(v)
			}
			pcrs = append(pcrs, tpm.PcrSelect{Hash: tpm.U16(h2), Pcr: p2})
		}

		data := tpm.CapabilityData{
			Capability: tpm.U32(capa),
			Pcrs:       pcrs,
		}
		err := checkModulePlatformConfigRegsImpl(data)
		if outcome {
			test.NoErr(t, err)
		} else {
			test.WantErr(t, err)
		}
	})
}
