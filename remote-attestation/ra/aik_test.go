package ra

import (
	"crypto/rand"
	"testing"

	test "system-transparency.org/ppg/ra/internal"
	"system-transparency.org/ppg/ra/internal/tpm"
)

func TestVerifyAik(t *testing.T) {
	pub := defaultAikTemplate.Public
	params := *pub.EccParameters
	params.Unique.X = make([]byte, 32)
	params.Unique.Y = make([]byte, 32)
	pub.EccParameters = &params

	err := verifyAttestationIdentityKey(&pub)
	test.NoErr(t, err)
}

func TestVerifyAikFail(t *testing.T) {
	cases := []string{
		"keytype",
		"namealg",
		"objectattributes",
		"authpolicy",
		"eccparameters",
		"symmetric",
		"scheme",
		"schemehashalg",
		"curveid",
		"kdfscheme",
		"pointx",
		"pointy",
	}

	for _, c := range cases {
		t.Run(c, func(t *testing.T) {
			pub := defaultAikTemplate.Public
			params := *pub.EccParameters
			params.Unique.X = make([]byte, 32)
			params.Unique.Y = make([]byte, 32)
			pub.EccParameters = &params

			switch c {
			case "keytype":
				pub.Type = tpm.TPM_ALG_RSA
			case "namealg":
				pub.NameAlg = tpm.TPM_ALG_SHA1
			case "objectattributes":
				pub.ObjectAttributes = tpm.TPMA_OBJECT_RESTRICTED_DECRYPT
			case "authpolicy":
				pub.AuthPolicy = make([]byte, 32)
				rand.Read(pub.AuthPolicy)
			case "eccparameters":
				pub.EccParameters = nil
			case "symmetric":
				pub.EccParameters.Symmetric.Algorithm = tpm.TPM_ALG_AES
			case "scheme":
				pub.EccParameters.Scheme.Scheme = tpm.TPM_ALG_ECDH
			case "schemehashalg":
				pub.EccParameters.Scheme.HashAlg = tpm.TPM_ALG_SHA1
			case "curveid":
				pub.EccParameters.CurveID = tpm.TPM_ECC_NIST_P384
			case "kdfscheme":
				pub.EccParameters.KDF.Scheme = tpm.TPM_ALG_KDF1_SP800_56A
			case "pointx":
				pub.EccParameters.Unique.X = nil
			case "pointy":
				pub.EccParameters.Unique.Y = nil
			}

			err := verifyAttestationIdentityKey(&pub)
			test.WantErr(t, err)
		})
	}
}
