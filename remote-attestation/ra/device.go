package ra

import (
	"context"
	"errors"
	"path/filepath"

	"system-transparency.org/ppg/ra/internal/tpm"
)

var (
	// No TPM device node found.
	ErrNoTPM = errors.New("no TPM found")

	linuxSystemTpmGlobs = []string{
		"/dev/tpm*",
	}
)

func findSystemTPM(ctx context.Context) (tpm.Device, string, error) {
	for _, pat := range linuxSystemTpmGlobs {
		m, err := filepath.Glob(pat)
		if err != nil {
			return nil, "", err
		}
		if len(m) == 0 {
			continue
		}
		for _, path := range m {
			dev, err := tpm.OpenFile(path)
			if err != nil {
				continue
			}
			return dev, path, nil
		}
	}

	return nil, "", ErrNoTPM
}
