package main

import (
	"crypto/ed25519"
	"crypto/rand"
	"encoding/binary"
	"errors"
	"flag"
	"fmt"
	"os"
)

type Signer struct {
	key ed25519.PrivateKey
}

func NewSigner(path string) *Signer {
	key, err := os.ReadFile(path)
	if err != nil {
		panic(err)
	}
	return &Signer{key: key}
}

func (s *Signer) Sign(message []byte) ([]byte, error) {
	return ed25519.Sign(s.key, message), nil
}

type Verifier struct {
	key ed25519.PublicKey
}

func NewVerifier(path string) *Verifier {
	key, err := os.ReadFile(path)
	if err != nil {
		panic(err)
	}
	return &Verifier{key: key}
}

func (v *Verifier) Verify(message, signature []byte) error {
	if ed25519.Verify(v.key, message, signature) {
		return nil
	} else {
		return errors.New("signature verification failed")
	}
}

func printHelp() {
	fmt.Fprintf(flag.CommandLine.Output(), `Usage:
  racmd <command> [flags]

Available commands:
  endorse   Endorse platforms, stboot EFI applications, or OS packages
  quote     Remotely attest platforms
  key       Generate and inspect keys

Use "racmd <command> -help" for more information about a command.
`)
}

func main() {
	if len(os.Args) < 2 {
		printHelp()
		return
	}

	cmd := os.Args[1]
	os.Args = append(os.Args[:1], os.Args[2:]...)

	switch cmd {
	case "endorse":
		endorse()
	case "quote":
		quote()
	case "key":
		key()
	default:
		printHelp()
	}
}

func printKeyHelp() {
	fmt.Printf(`Usage:
  ra key [flags]

Flags:
  -help        Print this help message

  -new         Generate a new ed25519 key pair. Produces 'key' and 'key.pub'.

  -out FILE    Output path for the private key. Used with -new.
`)
}

func key() {
	var newkey = false
	var outPath = ""
	var help = false

	flag.Usage = printKeyHelp
	flag.BoolVar(&newkey, "new", false, "generate a new key pair")
	flag.StringVar(&outPath, "out", "", "output path for the private key")
	flag.BoolVar(&help, "help", false, "print this help message")
	flag.Parse()

	if help {
		printKeyHelp()
	} else if newkey {
		if outPath == "" {
			outPath = "key"
		}
		keyNew(outPath)
	} else {
		printKeyHelp()
	}
}

func keyNew(outPath string) {
	pub, priv, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		panic(err)
	}

	err = os.WriteFile(outPath, priv, 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("Private key written to", outPath)
	err = os.WriteFile(outPath+".pub", pub, 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("Public key written to", outPath+".pub")
}

func writeToFile(path string, data ...[]byte) error {
	f, err := os.Create(path)
	if err != nil {
		return err
	}

	defer f.Close()

	for _, d := range data {
		err := binary.Write(f, binary.LittleEndian, uint32(len(d)))
		if err != nil {
			return err
		}
		_, err = f.Write(d)
		if err != nil {
			return err
		}
	}

	fmt.Println("Data written to", path)
	return nil
}

func readFromFile(path string, data ...*[]byte) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}

	defer f.Close()

	for _, d := range data {
		var length uint32
		err := binary.Read(f, binary.LittleEndian, &length)
		if err != nil {
			return err
		}
		*d = make([]byte, length)
		_, err = f.Read(*d)
		if err != nil {
			return err
		}
	}

	return nil
}
