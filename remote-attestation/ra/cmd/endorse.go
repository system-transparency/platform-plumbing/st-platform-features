package main

import (
	"context"
	"encoding/json"
	"encoding/pem"
	"flag"
	"fmt"
	"os"
	"strings"
	"time"

	"system-transparency.org/ppg/ra"
)

// copy of trust.Policy structure in stboot
type trustPolicy struct {
	SignatureThreshold int    `json:"ospkg_signature_threshold"`
	FetchMethod        string `json:"ospkg_fetch_method"`
}

func printEndorseHelp() {
	fmt.Fprintf(flag.CommandLine.Output(), `Usage:
  ra endorse [flags]

Flags:
  -help                   Print this help message

  -tpm FILE               Path to the TPM device. Required for creating an
                          enrollment request and solving a enrollemnt
                          challenge.

  -endorsement-auth PW    Endorsement hierarchy password. Used with -tpm.

  -owner-auth PW          Owner hierarchy password. Used with -tpm.

  -platform FILE          Path to a platform's enrollment request. Produces an
                          enrollment challenge. Requires -key.

  -challenge FILE         Path to an enrollment challenge. Produces an platform
                          endorsement. Requires -tpm.

  -ospkg FILE             Path to an OS package descriptor or archive. Produces
                          an OS package endorsement. Requires -key.

  -stboot FILE            Path to an stboot EFI application. Produces an stboot
                          endorsement. Requires -root-ca, -https-roots,
                          -trust-policy, and -key.

  -root-ca FILE           Path to the root CA certificate compiled into the
                          stboot EFI application. Used with -stboot.

  -https-roots FILE       Path to the HTTPS roots compiled into the stboot EFI
                          application. Used with -stboot.

  -trust-policy FILE      Path to the trust policy compiled into the stboot EFI
                          application. Used with -stboot.

  -key FILE               Private ed25519 key used to sign endorsements. Used
                          with -platform, -ospkg, and -stboot.

  -out FILE               Output path for the produced endorsement or
                          challenge.

  -simulator              Accept self-signed EK certificates. Useful for
                          enrolling a TPM simulator.

  -linux-measures         Assume the Linux kernel used for stboot does its own
                          measurements in PCR[09].

Examples:

  1. Create an enrollment request:
     $ ra endorse -tpm /dev/tpmrm0
     This command initiates the enrollment process by creating an enrollment
     request. The command is run on the platform that is to be endorsed. It
     creates a file named 'localhost.request.bin'.

  2. Create an enrollment challenge:
     $ ra endorse -platform localhost.request.bin -key <key>
     This command creates an enrollment challenge from a platform's enrollment
     request. It requires a private key to sign the challenge and is run on the
     operator's machine. It creates a file named 'localhost.challenge.bin'.

  3. Finish the enrollment process:
     $ ra endorse -tpm /dev/tpmrm0 -challenge localhost.challenge.bin
     This command finishes the enrollment process. It is run on the platform to
     be enrolled and usesthe TPM to decrypt the challenge, yielding an platform
     endorsement. It creates a file named 'localhost.endorsement.bin'.

  4. Endorse an OS package:
     $ ra endorse -ospkg <ospkg> -key <key>
     This command endorses an OS package. It requires a private key to sign the
     endorsement and creates a file named 'ospkg.endorsement.bin'.

  5. Endorse an stboot EFI application:
     $ ra endorse -stboot BOOTX64.EFI -root-ca <root-cat> \
         -https-roots <https-roots> -trust-policy <trust-policy> -key <key>
     This command endorses an stboot EFI application. It requires a private key
     and the root CA certificate, HTTPS roots, and trust policy compiled into
     the stboot EFI application.
     It creates a file named 'stboot.endorsement.bin'.
`)
}

func endorse() {
	var endorsementAuth = ""
	var ownerAuth = ""
	var devicePath = ""
	var requestPath = ""
	var challengePath = ""
	var ospkgPath = ""
	var stbootPath = ""
	var rootCaPath = ""
	var httpsRootsPath = ""
	var trustPolicyPath = ""
	var keyPath = ""
	var outPath = ""
	var help = false
	var simulator = false
	var linuxMeasures = false

	flag.Usage = printEndorseHelp
	flag.StringVar(&endorsementAuth, "endorsement-auth", "", "endorsement hierarchy password")
	flag.StringVar(&ownerAuth, "owner-auth", "", "owner hierarchy password")
	flag.StringVar(&devicePath, "tpm", "", "path to the TPM device")
	flag.StringVar(&requestPath, "platform", "", "path to the platform request")
	flag.StringVar(&challengePath, "challenge", "", "path to the challenge request")
	flag.StringVar(&ospkgPath, "ospkg", "", "path to the OS package descriptor or archive")
	flag.StringVar(&stbootPath, "stboot", "", "path to the stboot EFI application")
	flag.StringVar(&rootCaPath, "root-ca", "", "path to the root CA certificate")
	flag.StringVar(&httpsRootsPath, "https-roots", "", "path to the HTTPS roots")
	flag.StringVar(&trustPolicyPath, "trust-policy", "", "path to the trust policy")
	flag.StringVar(&keyPath, "key", "", "key to endorse")
	flag.StringVar(&outPath, "out", "", "output path")
	flag.BoolVar(&help, "help", false, "Print this help message")
	flag.BoolVar(&simulator, "simulator", false, "Accept self-signed EK certificates")
	flag.BoolVar(&linuxMeasures, "linux-measures", false, "Linux kernel makes its own measurements")
	flag.Parse()

	if help {
		printEndorseHelp()
	} else if devicePath != "" && challengePath == "" {
		if outPath == "" {
			outPath = "localhost.request.bin"
		}
		endorsementRequest(devicePath, endorsementAuth, ownerAuth, outPath)
	} else if requestPath != "" && keyPath != "" {
		if outPath == "" {
			outPath = "localhost.challenge.bin"
		}
		endorsementChallenge(requestPath, keyPath, outPath, simulator)
	} else if devicePath != "" && challengePath != "" {
		if outPath == "" {
			outPath = "localhost.endorsement.bin"
		}
		endorsementFinish(devicePath, endorsementAuth, ownerAuth, challengePath, outPath)
	} else if ospkgPath != "" && keyPath != "" {
		if outPath == "" {
			outPath = "ospkg.endorsement.bin"
		}
		endorsementOspkg(ospkgPath, keyPath, outPath)
	} else if stbootPath != "" && rootCaPath != "" && httpsRootsPath != "" && trustPolicyPath != "" && keyPath != "" {
		if outPath == "" {
			outPath = "stboot.endorsement.bin"
		}
		endorsementStboot(stbootPath, rootCaPath, httpsRootsPath, trustPolicyPath, keyPath, outPath, linuxMeasures)
	} else {
		printEndorseHelp()
	}
}

func endorsementRequest(devicePath, endorsementAuth, ownerAuth, outPath string) {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	// initiate the enrollemnt by creating an enrollent request.
	reqopts := ra.RequestOptions{
		EndorsementAuth: endorsementAuth,
		OwnerAuth:       ownerAuth,
		DevicePath:      devicePath,
	}
	req, err := ra.Request(ctx, &reqopts)
	if err != nil {
		panic(err)
	}

	err = writeToFile(outPath, req.EndorsementCertificate, req.EventLog,
		req.EndorsementNvIndex, req.EndorsementPublicKey,
		req.AttestationIdentityKey)
	if err != nil {
		panic(err)
	}
}

func endorsementChallenge(platformPath, keyPath, outPath string, simulator bool) {
	var req ra.EnrollmentRequest
	err := readFromFile(platformPath, &req.EndorsementCertificate, &req.EventLog,
		&req.EndorsementNvIndex, &req.EndorsementPublicKey,
		&req.AttestationIdentityKey)
	if err != nil {
		panic(err)
	}

	ctx := context.Background()
	signer := NewSigner(keyPath)

	opts := ra.ChallengeOptions{
		AllowSelfSigned: simulator,
	}
	challenge, err := ra.CreateChallenge(ctx, &req, signer, &opts)
	if err != nil {
		panic(err)
	}

	err = writeToFile(outPath, challenge.IdObject, challenge.EndorsementNvIndex,
		challenge.EncryptedKey, challenge.Nonce, challenge.Ciphertext)
	if err != nil {
		panic(err)
	}
}

func endorsementFinish(devicePath, endorsementAuth, ownerAuth, challengePath, outPath string) {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	var challenge ra.Challenge
	err := readFromFile(challengePath, &challenge.IdObject,
		&challenge.EndorsementNvIndex, &challenge.EncryptedKey, &challenge.Nonce,
		&challenge.Ciphertext)
	if err != nil {
		panic(err)
	}

	// activate the credential and flush all transient TPM state
	opts := ra.FinishOptions{
		EndorsementAuth: endorsementAuth,
		OwnerAuth:       ownerAuth,
		DevicePath:      devicePath,
	}
	enrollment, err := ra.Finish(ctx, &challenge, &opts)
	if err != nil {
		panic(err)
	}

	err = writeToFile(outPath, enrollment.AttestationIdentityKey,
		enrollment.AikSignature, enrollment.PlatformFirmware,
		enrollment.FirmwareSignature)
	if err != nil {
		panic(err)
	}
}

func endorsementOspkg(ospkgPath, keyPath, outPath string) {
	signer := NewSigner(keyPath)

	var descPath, zipPath string

	if strings.HasSuffix(ospkgPath, ".zip") {
		zipPath = ospkgPath
		descPath = strings.TrimSuffix(ospkgPath, ".zip") + ".json"
	} else if strings.HasSuffix(ospkgPath, ".json") {
		descPath = ospkgPath
		zipPath = strings.TrimSuffix(ospkgPath, ".json") + ".zip"
	} else {
		panic("invalid ospkg path, must be either a .json or .zip file")
	}

	desc, err := os.ReadFile(descPath)
	if err != nil {
		panic(err)
	}
	zip, err := os.ReadFile(zipPath)
	if err != nil {
		panic(err)
	}

	opts := ra.OsPackageOptions{
		SystemdReadyPhases: true,
	}
	enrollment, err := ra.EndorseOsPackage(desc, zip, signer, &opts)
	if err != nil {
		panic(err)
	}

	err = writeToFile(outPath, enrollment.Signature,
		enrollment.OsPkgMeasurements, enrollment.StbootMeasurements)
	if err != nil {
		panic(err)
	}
}

func endorsementStboot(stbootPath, rootCaPath, httpsRootsPath, trustPolicyPath, keyPath, outPath string, linuxMeasures bool) {
	signer := NewSigner(keyPath)

	efiapp, err := os.ReadFile(stbootPath)
	if err != nil {
		panic(err)
	}
	rootCa, err := os.ReadFile(rootCaPath)
	if err != nil {
		panic(err)
	}
	der, _ := pem.Decode(rootCa)
	if der == nil {
		panic("invalid root CA certificate")
	}
	rootCa = der.Bytes
	httpsRoots, err := os.ReadFile(httpsRootsPath)
	if err != nil {
		panic(err)
	}
	der, _ = pem.Decode(httpsRoots)
	if der == nil {
		panic("invalid HTTPS roots")
	}
	httpsRoots = der.Bytes

	// normalize trust policy
	trustPolicyFd, err := os.Open(trustPolicyPath)
	if err != nil {
		panic(err)
	}
	defer trustPolicyFd.Close()
	var trustPolicy trustPolicy
	if err := json.NewDecoder(trustPolicyFd).Decode(&trustPolicy); err != nil {
		panic(err)
	}
	trustPolicyBytes, err := json.Marshal(trustPolicy)
	if err != nil {
		panic(err)
	}

	opts := ra.StbootOptions{
		LinuxMeasures: linuxMeasures,
	}
	enrollment, err := ra.EndorseStboot(efiapp, rootCa, httpsRoots, trustPolicyBytes, nil, nil, signer, &opts)
	if err != nil {
		panic(err)
	}

	err = writeToFile(outPath, enrollment.StubMeasurements,
		enrollment.StbootLoadEvent, enrollment.StbootMeasurements,
		enrollment.Signature)
	if err != nil {
		panic(err)
	}
}
