package main

import (
	"context"
	"crypto/rand"
	"errors"
	"flag"
	"fmt"
	"time"

	"system-transparency.org/ppg/ra"
)

func printQuoteHelp() {
	fmt.Fprintf(flag.CommandLine.Output(), `Usage:
  ra quote [flags]

Flags:
  -help                Print this help message

  -tpm FILE            Path to the TPM device. Required for creating an quote
                       for remote attestation. Rquires -endorsement. Produces
                       'localhost.quote.bin'.

  -owner-auth PW       Owner hierarchy password. Used with -tpm.

  -endorsement FILE    Path to the platform endorsement. Used with -tpm.

  -quote FILE          Path to the remote attestation quote to verify. Used
                       with -public, -ospkg, and -stboot.

  -ospkg FILE          Path to the endorsement of the expected OS package. Used
                       with -quote.

  -stboot FILE         Path to the endorsement of the expected stboot EFI
                       application. Used with -quote.

  -hypervisor FILE     Path to the endorsement of the expected OS package
                       running in the hypervisor, if a virtual machine is being
                       attested. Used with -quote.

  -public FILE         Path to the public key used to verify the endorsements.
                       Used with -quote.

  -out FILE            Output path for the produced remote attestation quote.

Examples:

  1. Create a quote for remote attestation:
     $ ra quote -tpm /dev/tpmrm0 -endorsement localhost.endorsement.bin
     This command initiates the remote attestation process by creating a quote
     for the platform it's run on. It requires a platform endorsement and
     produces a file named 'localhost.quote.bin'.

  2. Verify a remote attestation quote:
     $ ra quote -quote localhost.quote.bin -ospkg ospkg.endorsement.bin \
          -stboot stboot.endorsement.bin -public key.pub
     This command verifies a remote attestation quote. It requires the quote,
     endorsements of the expected OS package and stboot EFI application, and the
     public key of the key pair used to sign the endorsements.
`)
}

func quote() {
	var devicePath = ""
	var ownerAuth = ""
	var publicPath = ""
	var endorsementPath = ""
	var ospkgPath = ""
	var hypervisorPath = ""
	var stbootPath = ""
	var quotePath = ""
	var outPath = ""
	var help = false

	flag.Usage = printQuoteHelp
	flag.StringVar(&devicePath, "tpm", "", "path to the TPM device")
	flag.StringVar(&ownerAuth, "owner-auth", "", "owner hierarchy password")
	flag.StringVar(&publicPath, "public", "", "path to the public key")
	flag.StringVar(&endorsementPath, "endorsement", "", "path to the platform endorsement")
	flag.StringVar(&ospkgPath, "ospkg", "", "path to the OS package endorsement")
	flag.StringVar(&hypervisorPath, "hypervisor", "", "path to the OS package endorsement running in the hypervisor")
	flag.StringVar(&stbootPath, "stboot", "", "path to the stboot endorsement")
	flag.StringVar(&quotePath, "quote", "", "path to the quote")
	flag.StringVar(&outPath, "out", "", "output path")
	flag.BoolVar(&help, "help", false, "print this help message")
	flag.Parse()

	if help {
		printQuoteHelp()
	} else if devicePath != "" && endorsementPath != "" {
		if outPath == "" {
			outPath = "localhost.quote.bin"
		}
		quoteEndorsement(devicePath, endorsementPath, ownerAuth, outPath)
	} else if quotePath != "" && publicPath != "" {
		quoteVerify(quotePath, ospkgPath, stbootPath, hypervisorPath, publicPath)
	} else {
		printQuoteHelp()
	}
}

func quoteEndorsement(devicePath, endorsementPath, ownerAuth, outPath string) {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	var nonce [32]byte
	_, err := rand.Read(nonce[:])
	if err != nil {
		panic(err)
	}

	var platform ra.PlatformEndorsement
	err = readFromFile(endorsementPath, &platform.AttestationIdentityKey,
		&platform.AikSignature, &platform.PlatformFirmware,
		&platform.FirmwareSignature)
	if err != nil {
		panic(err)
	}

	opts := ra.AttestOptions{
		DevicePath:   devicePath,
		OwnerAuth:    ownerAuth,
		MinimalQuote: true,
	}
	quote, err := ra.Attest(ctx, nonce[:], &opts)
	if err != nil {
		panic(err)
	}

	err = writeToFile(outPath, nonce[:], quote.Attest, quote.Signature,
		quote.Cel, quote.Pcr, platform.AttestationIdentityKey,
		platform.AikSignature, platform.PlatformFirmware,
		platform.FirmwareSignature)
	if err != nil {
		panic(err)
	}
}

func quoteVerify(quotePath, ospkgPath, stbootPath, hypervisorPath, publicPath string) {
	verifier := NewVerifier(publicPath)

	var nonce []byte
	var quote ra.Quote
	var platform ra.PlatformEndorsement
	err := readFromFile(quotePath, &nonce, &quote.Attest, &quote.Signature,
		&quote.Cel, &quote.Pcr, &platform.AttestationIdentityKey,
		&platform.AikSignature, &platform.PlatformFirmware,
		&platform.FirmwareSignature)
	if err != nil {
		panic(err)
	}
	var ospkg ra.OsPackageEndorsement
	err = readFromFile(ospkgPath, &ospkg.Signature, &ospkg.OsPkgMeasurements,
		&ospkg.StbootMeasurements)
	if err != nil {
		panic(err)
	}
	var stboot ra.StbootEndorsement
	err = readFromFile(stbootPath, &stboot.StubMeasurements,
		&stboot.StbootLoadEvent, &stboot.StbootMeasurements, &stboot.Signature)
	if err != nil {
		panic(err)
	}

	var attErr ra.ErrAttestation
	if hypervisorPath != "" {
		var hypervisor ra.OsPackageEndorsement
		err = readFromFile(hypervisorPath, &hypervisor.Signature,
			&hypervisor.OsPkgMeasurements, &hypervisor.StbootMeasurements)
		if err != nil {
			panic(err)
		}

		err = ra.Verify(nonce, &quote, &platform, &hypervisor, &stboot, &ospkg, verifier)
	} else {
		err = ra.Verify(nonce, &quote, &platform, &ospkg, &stboot, nil, verifier)
	}

	if errors.As(err, &attErr) {
		fmt.Printf("Quote verification failed: %v\n", attErr.Err)

		fmt.Printf("Expected PCR values:\n")
		for i := 0; i < 24; i++ {
			if v, ok := attErr.ExpectedPcr[i]; ok {
				fmt.Printf("  PCR[%2d] %x\n", i, v)
			} else {
				fmt.Printf("  PCR[%2d] ----------------------------------------------------------------\n", i)
			}
		}
		fmt.Printf("Expected quote: %x\n", attErr.ExpectedHash)
		fmt.Printf("Received quote: %x\n", attErr.ReceivedHash)
	} else if err != nil {
		fmt.Printf("Quote verification failed: %v\n", err)
	} else {
		fmt.Println("Quote verified successfully")
	}
}
