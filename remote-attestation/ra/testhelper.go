package ra

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/hmac"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"math/big"
	"os"
	"testing"
	"time"

	test "system-transparency.org/ppg/ra/internal"
	"system-transparency.org/ppg/ra/internal/tpm"
)

type hmacSigner struct {
	Key []byte
}

func (s *hmacSigner) Sign(message []byte) ([]byte, error) {
	mac := hmac.New(sha256.New, s.Key)
	mac.Write(message)
	return mac.Sum(nil), nil
}

func (s *hmacSigner) Verify(message, signature []byte) error {
	mac := hmac.New(sha256.New, s.Key)
	mac.Write(message)
	expected := mac.Sum(nil)
	if !hmac.Equal(expected, signature) {
		return errors.New("signature mismatch")
	}
	return nil
}

func fakeAik(t *testing.T, random io.Reader) (tpm.Public, *ecdsa.PrivateKey) {
	dbin := make([]byte, 32)
	_, err := io.ReadFull(random, dbin)
	test.NoErr(t, err)
	d := new(big.Int).SetBytes(dbin)
	x, y := elliptic.P256().ScalarBaseMult(dbin)
	aikpub := ecdsa.PublicKey{Curve: elliptic.P256(), X: x, Y: y}
	aikpriv := ecdsa.PrivateKey{PublicKey: aikpub, D: d}

	aik := tpm.Public{
		Type:             tpm.TPM_ALG_ECC,
		NameAlg:          tpm.TPM_ALG_SHA256,
		ObjectAttributes: tpm.TPMA_OBJECT_RESTRICTED_SIGN,
		AuthPolicy:       make([]byte, 32),
		EccParameters: &tpm.EccParms{
			Symmetric: defaultAikTemplate.Public.EccParameters.Symmetric,
			Scheme:    defaultAikTemplate.Public.EccParameters.Scheme,
			CurveID:   defaultAikTemplate.Public.EccParameters.CurveID,
			KDF:       defaultAikTemplate.Public.EccParameters.KDF,
			Unique: tpm.EccPoint{
				X: aikpub.X.Bytes(),
				Y: aikpub.Y.Bytes(),
			},
		},
	}

	return aik, &aikpriv
}

func signQuote(t *testing.T, q *tpm.Attest, priv *ecdsa.PrivateKey) tpm.Signature {
	quotebuf, err := q.Pack()
	test.NoErr(t, err)
	sum := sha256.Sum256(quotebuf)
	r, s, err := ecdsa.Sign(rand.Reader, priv, sum[:])
	test.NoErr(t, err)

	sig := tpm.Signature{
		Algorithm: tpm.TPM_ALG_ECDSA,
		Ecc: &tpm.EccSignature{
			Hash: tpm.TPM_ALG_SHA256,
			R:    r.Bytes(),
			S:    s.Bytes(),
		},
	}

	return sig
}

func testOsPkgEndorsement(t *testing.T, base string, signer Signer) *OsPackageEndorsement {
	desc, err := os.ReadFile(fmt.Sprintf("testdata/%s.json", base))
	test.NoErr(t, err)

	zip, err := os.ReadFile(fmt.Sprintf("testdata/%s.zip", base))
	test.NoErr(t, err)

	osopts := OsPackageOptions{
		SystemdReadyPhases: true,
	}
	ospkg, err := EndorseOsPackage(desc, zip, signer, &osopts)
	test.NoErr(t, err)

	return ospkg
}

func testStbootEndorsement(t *testing.T, base string, signer Signer, linuxMeasures bool) *StbootEndorsement {
	efiapp, err := os.ReadFile(fmt.Sprintf("testdata/%s.efi", base))
	test.NoErr(t, err)

	rootpem, err := os.ReadFile(fmt.Sprintf("testdata/%s-rootcert.pem", base))
	test.NoErr(t, err)
	rootder, _ := pem.Decode(rootpem)
	if rootder == nil {
		t.Fatalf("failed to decode root certificate")
	}

	httppem, err := os.ReadFile("testdata/isrgrootx1.pem")
	test.NoErr(t, err)
	httpder, _ := pem.Decode(httppem)
	if httpder == nil {
		t.Fatalf("failed to decode root certificate")
	}

	type trustPolicy struct {
		SignatureThreshold int    `json:"ospkg_signature_threshold"`
		FetchMethod        string `json:"ospkg_fetch_method"`
	}
	tp := trustPolicy{
		SignatureThreshold: 1,
		FetchMethod:        "network",
	}
	tpbuf, err := json.Marshal(tp)
	test.NoErr(t, err)

	stopts := StbootOptions{
		LinuxMeasures: linuxMeasures,
	}
	stboot, err := EndorseStboot(efiapp, rootder.Bytes, httpder.Bytes, tpbuf, nil, nil, signer, &stopts)
	test.NoErr(t, err)

	return stboot
}

func testPlatformEndorsement(t *testing.T, aik *tpm.Public, evlog []record, signer Signer) *PlatformEndorsement {
	firmware, fwsig, err := endorsePlatformFirmware(evlog, signer)
	test.NoErr(t, err)

	aikbuf, err := aik.Pack()
	test.NoErr(t, err)

	aiksig, err := signer.Sign(aikbuf)
	test.NoErr(t, err)

	platform := PlatformEndorsement{
		AttestationIdentityKey: aikbuf,
		AikSignature:           aiksig,
		PlatformFirmware:       firmware,
		FirmwareSignature:      fwsig,
	}

	return &platform
}

func measure(evlog []record, pcrnum int, ds [32]byte, ty int, data []byte) []record {
	evlog = append(evlog, record{
		Pcr:     pcr(pcrnum),
		Digests: []digest{{Alg: int(tpm.TPM_ALG_SHA256), Digest: ds[:]}},
		Content: content{
			PcClientStd: &pcclient{
				Type: uint32(ty),
				Data: data,
			},
		},
	})
	return evlog
}

func fakeEccEk(t *testing.T, random io.Reader) (tpm.Public, x509.Certificate, crypto.PrivateKey) {
	ekkey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	test.NoErr(t, err)

	ekpub := ekkey.PublicKey
	ekcert := &x509.Certificate{
		SerialNumber:          big.NewInt(2),
		Subject:               pkix.Name{CommonName: "Test EK"},
		Issuer:                pkix.Name{CommonName: "Test EK"},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(time.Hour),
		KeyUsage:              x509.KeyUsageKeyAgreement,
		IsCA:                  false,
		BasicConstraintsValid: true,
		MaxPathLen:            0,
		SubjectKeyId:          []byte{1, 2, 3, 4},
		PublicKey:             &ekpub,
		PublicKeyAlgorithm:    x509.ECDSA,
	}

	ek := tpm.Public{
		Type:             tpm.TPM_ALG_ECC,
		NameAlg:          tpm.TPM_ALG_SHA256,
		ObjectAttributes: tpm.TPMA_OBJECT_RESTRICTED_SIGN,
		AuthPolicy:       make([]byte, 32),
		EccParameters: &tpm.EccParms{
			Symmetric: defaultAikTemplate.Public.EccParameters.Symmetric,
			Scheme:    defaultAikTemplate.Public.EccParameters.Scheme,
			CurveID:   defaultAikTemplate.Public.EccParameters.CurveID,
			KDF:       defaultAikTemplate.Public.EccParameters.KDF,
			Unique: tpm.EccPoint{
				X: ekpub.X.Bytes(),
				Y: ekpub.Y.Bytes(),
			},
		},
	}

	return ek, *ekcert, ekkey
}

func fakeRsaEk(t *testing.T, random io.Reader) (tpm.Public, x509.Certificate, crypto.PrivateKey) {
	ekkey, err := rsa.GenerateKey(rand.Reader, 2048)
	test.NoErr(t, err)

	ekpub := ekkey.PublicKey
	ekcert := &x509.Certificate{
		SerialNumber:          big.NewInt(2),
		Subject:               pkix.Name{CommonName: "Test EK"},
		Issuer:                pkix.Name{CommonName: "Test EK"},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(time.Hour),
		KeyUsage:              x509.KeyUsageKeyEncipherment,
		IsCA:                  false,
		BasicConstraintsValid: true,
		MaxPathLen:            0,
		SubjectKeyId:          []byte{1, 2, 3, 4},
		PublicKey:             &ekpub,
		PublicKeyAlgorithm:    x509.RSA,
	}

	ek := tpm.Public{
		Type:             tpm.TPM_ALG_RSA,
		NameAlg:          tpm.TPM_ALG_SHA256,
		ObjectAttributes: tpm.TPMA_OBJECT_RESTRICTED_SIGN,
		AuthPolicy:       make([]byte, 32),
		RsaParameters: &tpm.RsaParms{
			Symmetric: defaultAikTemplate.Public.EccParameters.Symmetric,
			Scheme:    defaultAikTemplate.Public.EccParameters.Scheme,
			KeyBits:   2048,
			Exponent:  tpm.U32(ekpub.E),
			Unique:    ekpub.N.Bytes(),
		},
	}

	return ek, *ekcert, ekkey
}
