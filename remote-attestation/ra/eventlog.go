package ra

import (
	"bytes"
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

var (
	// Could not find the UEFI event log.
	ErrNoEventlog = errors.New("no eventlog")

	tcgEventlogPath = "/sys/kernel/security/%s/binary_bios_measurements"
)

type pcrEvent1 struct {
	PcrIndex uint32
	Type     uint32
	Digest   [20]byte
	Event    []byte
}

func decodeEv1(rd io.Reader) (pcrEvent1, error) {
	var ev pcrEvent1
	if err := binary.Read(rd, binary.LittleEndian, &ev.PcrIndex); err != nil {
		return ev, err
	}
	if err := binary.Read(rd, binary.LittleEndian, &ev.Type); err != nil {
		return ev, err
	}
	if _, err := io.ReadFull(rd, ev.Digest[:]); err != nil {
		return ev, err
	}
	var sz uint32
	if err := binary.Read(rd, binary.LittleEndian, &sz); err != nil {
		return ev, err
	}
	if sz > 0x10000 {
		return ev, ErrUnsupported
	}
	ev.Event = make([]byte, sz)
	_, err := io.ReadFull(rd, ev.Event)
	return ev, err
}

type pcrEvent2 struct {
	PcrIndex uint32
	Type     uint32
	Digests  map[int][]byte
	Event    []byte
}

func decodeEv2(rd io.Reader) (pcrEvent2, error) {
	var ev pcrEvent2
	if err := binary.Read(rd, binary.LittleEndian, &ev.PcrIndex); err != nil {
		return ev, err
	}
	if err := binary.Read(rd, binary.LittleEndian, &ev.Type); err != nil {
		return ev, err
	}
	var num uint32
	if err := binary.Read(rd, binary.LittleEndian, &num); err != nil {
		return ev, err
	}
	ev.Digests = make(map[int][]byte)
	for i := 0; i < int(num); i++ {
		var alg uint16
		if err := binary.Read(rd, binary.LittleEndian, &alg); err != nil {
			return ev, err
		}
		switch alg {
		case sha1Alg:
			ev.Digests[int(alg)] = make([]byte, 20)
		case sha256Alg:
			ev.Digests[int(alg)] = make([]byte, 32)
		case sha384Alg:
			ev.Digests[int(alg)] = make([]byte, 48)
		case sha512Alg:
			ev.Digests[int(alg)] = make([]byte, 64)
		default:
			return ev, ErrUnsupported
		}
		if _, err := io.ReadFull(rd, ev.Digests[int(alg)]); err != nil {
			return ev, err
		}
	}
	var sz uint32
	if err := binary.Read(rd, binary.LittleEndian, &sz); err != nil {
		return ev, err
	}
	if sz > 0x10000 {
		return ev, ErrUnsupported
	}
	ev.Event = make([]byte, sz)
	_, err := io.ReadFull(rd, ev.Event)
	return ev, err
}

func decodePcClientLog(rd io.Reader) ([]pcrEvent2, error) {
	var log []pcrEvent2

	specid, err := decodeEv1(rd)
	if err != nil {
		return nil, err
	}
	if specid.PcrIndex != 0 || specid.Type != 3 {
		return nil, ErrUnsupported
	}
	if len(specid.Event) < len(tcgSpecIdEvent) {
		return nil, ErrUnsupported
	}
	if !bytes.Equal(specid.Event[0:len(tcgSpecIdEvent)], tcgSpecIdEvent) {
		return nil, ErrUnsupported
	}

	ev := pcrEvent2{
		PcrIndex: specid.PcrIndex,
		Type:     specid.Type,
		Digests: map[int][]byte{
			sha1Alg: specid.Digest[:],
		},
		Event: specid.Event,
	}
	log = append(log, ev)

	for {
		ev, err := decodeEv2(rd)
		if errors.Is(err, io.EOF) {
			return log, nil
		}
		if err != nil {
			return nil, err
		}
		log = append(log, ev)
	}
}

func transcodePcClientEventlog(rd io.Reader) ([]record, error) {
	log, err := decodePcClientLog(rd)
	if err != nil {
		return nil, err
	}
	var records []record
	for i, ev := range log {
		var r record
		r.Recnum = recnum(i)
		r.Pcr = pcr(ev.PcrIndex)
		r.Digests = make(digests, 0, len(ev.Digests))
		algs := make([]int, 0, len(ev.Digests))
		for alg := range ev.Digests {
			algs = append(algs, alg)
		}
		sort.Ints(algs)
		for _, alg := range algs {
			r.Digests = append(r.Digests, digest{alg, ev.Digests[alg]})
		}
		r.Content.PcClientStd = &pcclient{
			Type: ev.Type,
			Data: ev.Event,
		}
		records = append(records, r)
	}

	return records, nil
}

func decodeCelEventlog(rd io.Reader) ([]record, error) {
	var records []record
	for {
		var r record
		if err := r.decode(rd); err != nil {
			if errors.Is(err, io.EOF) {
				return records, nil
			}
			return nil, err
		}
		records = append(records, r)
	}
}

func collectEventlogs(ctx context.Context, devicePath string) ([]byte, error) {
	node := filepath.Base(devicePath)
	path := fmt.Sprintf(tcgEventlogPath, node)

	fd, err := os.Open(path)
	if err != nil {
		if strings.HasPrefix(node, "tpmrm") {
			return collectEventlogs(ctx, strings.Replace(node, "tpmrm", "tpm", 1))
		}
		return nil, ErrNoEventlog
	}
	defer fd.Close()
	log, err := transcodePcClientEventlog(fd)
	if err != nil {
		return nil, ErrNoEventlog
	}
	var buf bytes.Buffer
	for _, r := range log {
		if err := r.encode(&buf); err != nil {
			return nil, err
		}
	}
	return buf.Bytes(), nil
}
