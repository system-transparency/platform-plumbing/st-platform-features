package ra

import (
	"bytes"
	"context"
	"crypto/aes"
	"crypto/cipher"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"net/http"
	"sort"
	"strings"
	"time"

	"system-transparency.org/ppg/ra/internal/tpm"
)

var (
	// Did not understand the TPM's response
	ErrUnexpectedResponse = errors.New("unexpected response")
	// Endorsement key is not matching the certificate
	ErrKeyInvalid = errors.New("invalid key")
	// No suitable endorsement key certificate found
	ErrNoEndorsement = errors.New("no endorsement certificate")
	// TPM is too old or too new
	ErrPrereqNotMet = errors.New("prerequesits not met")

	// we prefer ECC over RSA
	ekPriorityList = []struct {
		I uint32
		T *tpm.Template
	}{
		// ECC NIST P521 EK Certificate (H-4)
		{tpm.EkCertificateIndexH4, &tpm.EkTemplateH4},
		// ECC NIST P384 EK Certificate (H-3)
		{tpm.EkCertificateIndexH3, &tpm.EkTemplateH3},
		// ECC NIST P256 EK Certificate (H-2)
		{tpm.EkCertificateIndexH2, &tpm.EkTemplateH2},
		// ECC NIST P256 EK Certificate (L-2)
		{tpm.EkCertificateIndexL2, &tpm.EkTemplateL2},
		// RSA 4096 EK Certificate (H-7)
		{tpm.EkCertificateIndexH7, &tpm.EkTemplateH7},
		// RSA 3072 EK Certificate (H-6)
		{tpm.EkCertificateIndexH6, &tpm.EkTemplateH6},
		// RSA 2048 EK Certificate (H-1)
		{tpm.EkCertificateIndexH1, &tpm.EkTemplateH1},
		// RSA 2048 EK Certificate (L-1)
		{tpm.EkCertificateIndexL1, &tpm.EkTemplateL1},
	}

	minimumSpecDate = "2013-01-01"
	// XXX check recent CVEs
	minimumFirmwareVersions     = map[string]uint64{}
	minimumPlatformConfigRegSet = map[uint16][]int{
		uint16(tpm.TPM_ALG_SHA256): pcClientPcr,
	}
	minimumCommands = []uint32{
		uint32(tpm.TPM_CC_ActivateCredential), uint32(tpm.TPM_CC_Quote),
		uint32(tpm.TPM_CC_NV_Read), uint32(tpm.TPM_CC_NV_ReadPublic),
		uint32(tpm.TPM_CC_CreatePrimary), uint32(tpm.TPM_CC_PCR_Read),
		uint32(tpm.TPM_CC_PCR_Event),
	}
	minimumAlgorithms = []uint16{
		uint16(tpm.TPM_ALG_SHA256), uint16(tpm.TPM_ALG_ECC), uint16(tpm.TPM_ALG_ECDSA),
	}
	minimumCurves = []uint16{uint16(tpm.TPM_ECC_NIST_P256)}
)

// PlatformFirmware is a structure that contains all TPM measurements that are
// influenced by firmware code of configuration. It also includes the
// device-specific Attestation Identity Key. The endorsement is the result of
// running the enrollment protocol.
type PlatformEndorsement struct {
	// TPM2B_PUBLIC of the Attestation Identity Key
	AttestationIdentityKey []byte
	// Signatures of the Attestation Identity Key: Signer.Sign(AttestationIdentityKey)
	AikSignature []byte
	// platformFirmware struct with the firmware measurements
	PlatformFirmware []byte
	// Signatures of the Platform Firmware: Signer.Sign(PlatformFirmware)
	FirmwareSignature []byte
}

// First message in the enrollment protocol. Contains the device's Endorsement
// Key and certificate. The enrollment request is used to encrypt a challenge
// using the EK (see [CreateChallenge]).
type EnrollmentRequest struct {
	// NV Index of the Endorsement Certificate. Used as unique identifier for the
	// EK in case there are multiple usable EKs.
	EndorsementNvIndex []byte
	// DER encoded Endorsement Certificate
	EndorsementCertificate []byte
	// TPM2B_PUBLIC of the Endorsement Key
	EndorsementPublicKey []byte
	// TPM2B_PUBLIC of the Attestation Identity Key
	AttestationIdentityKey []byte
	// CEL encoded UEFI event log
	EventLog []byte
}

// Second message in the enrollment protocol. Contains the challenge that is
// encrypted to the device's Endorsement Key and is decrypted using
// TPM2_ActivateCredential.
type Challenge struct {
	// NV Index of the Endorsement Certificate used to encrypt the credential.
	// Same as in the EnrollmentRequest.
	EndorsementNvIndex []byte
	// EK-encrypted seed. Depends on the EK type.
	IdObject []byte
	// Encrypted AES-GCM key that encrypts the ciphertext
	EncryptedKey []byte
	// GCM nonce
	Nonce []byte
	// AES-GCM-encrypted PlatformEndorsement
	Ciphertext []byte
}

var (
	credentialMagic = [4]byte{'C', 'R', 'E', 'D'}

	// The embedded platform firmware struct is illformed
	ErrInvalidMagic = errors.New("invalid magic")
	// The embedded platform firmware struct has trailing data
	ErrTrailingData = errors.New("trailing data")
)

func encodeByteArrays(magic [4]byte, b ...[]byte) []byte {
	var buf bytes.Buffer
	buf.Write(magic[:])
	for _, v := range b {
		if err := binary.Write(&buf, binary.LittleEndian, uint32(len(v))); err != nil {
			return nil
		}
		if _, err := buf.Write(v); err != nil {
			return nil
		}
	}
	return buf.Bytes()
}

func decodeByteArrays(rd io.Reader, magic [4]byte, fields ...*[]byte) error {
	var m [4]byte
	if err := binary.Read(rd, binary.LittleEndian, &m); err != nil {
		return err
	}
	if m != magic {
		return ErrInvalidMagic
	}
	for i := range fields {
		var sz uint32
		if err := binary.Read(rd, binary.LittleEndian, &sz); err != nil {
			return err
		}
		*fields[i] = make([]byte, sz)
		if _, err := io.ReadFull(rd, *fields[i]); err != nil {
			return err
		}
	}

	if tail, err := io.ReadAll(rd); err == io.EOF || len(tail) == 0 {
		return nil
	}

	return ErrTrailingData
}

func listNvIndexes(ctx context.Context, dev tpm.Device) ([]uint32, error) {
	return listCapabilityInts(ctx, dev, uint32(tpm.TPM_CAP_HANDLES),
		uint32(tpm.TPM_HT_NV_INDEX)<<24, uint32(tpm.TPM_HT_HMAC_SESSION)<<24)
}

func listCommands(ctx context.Context, dev tpm.Device) ([]uint32, error) {
	return listCapabilityInts(ctx, dev, uint32(tpm.TPM_CAP_COMMANDS), 0x0000011F, 0x20000000)
}

func listEccCurves(ctx context.Context, dev tpm.Device) ([]uint32, error) {
	return listCapabilityInts(ctx, dev, uint32(tpm.TPM_CAP_ECC_CURVES), 0, 0xffff)
}

func listCapabilityInts(ctx context.Context, dev tpm.Device, capa, start, end uint32) ([]uint32, error) {
	var indexes []uint32

	for i := start; i <= end; {
		data, more, err := tpm.GetCapability(ctx, dev, capa, i, 0xffff)
		if err != nil {
			return nil, err
		}
		if uint32(data.Capability) != capa {
			return nil, ErrUnexpectedResponse
		}
		ints := []uint32{}
		switch data.Capability {
		case tpm.TPM_CAP_COMMANDS:
			for _, i := range data.Commands {
				ints = append(ints, uint32(i))
			}
		case tpm.TPM_CAP_HANDLES:
			for _, i := range data.Handles {
				ints = append(ints, uint32(i))
			}
		case tpm.TPM_CAP_ECC_CURVES:
			for _, i := range data.EccCurves {
				ints = append(ints, uint32(i))
			}
		default:
			return nil, ErrUnexpectedResponse
		}
		for _, h := range ints {
			if h >= end {
				continue
			}
			indexes = append(indexes, h)
			if i < h {
				i = h + 1
			}
		}
		if !more || len(ints) == 0 {
			break
		}
	}

	return indexes, nil
}

func maxNvBufferSizes(ctx context.Context, dev tpm.Device) (uint32, error) {
	return tpm.TpmProperty(ctx, dev, uint32(tpm.TPM_PT_NV_BUFFER_MAX))
}

func readNvIndex(ctx context.Context, dev tpm.Device, idx uint32, pw string, maxBuffer uint32) (tpm.NvPublic, []byte, error) {
	nvpub, _, err := tpm.NvReadPublic(ctx, dev, idx)
	if err != nil {
		return tpm.NvPublic{}, nil, err
	}

	var data []byte
	remain := uint16(nvpub.DataSize)

	for remain > 0 {
		sz := uint16(remain)
		if sz > uint16(maxBuffer) {
			sz = uint16(maxBuffer)
		}
		part, err := tpm.NvRead(ctx, dev, idx, tpm.Password(pw), idx, sz, uint16(len(data)))
		if err != nil {
			return tpm.NvPublic{}, nil, err
		}
		if len(part) == 0 {
			return tpm.NvPublic{}, nil, ErrUnexpectedResponse
		}
		data = append(data, part...)
		remain -= uint16(len(part))
	}

	return nvpub, data, nil
}

func findEndorsementKeyTemplate(nvIndex uint32) (*tpm.Template, error) {
	for _, ek := range ekPriorityList {
		if ek.I == nvIndex {
			return ek.T, nil
		}
	}

	return nil, ErrNoEndorsement
}

func selectEndorsementKey(ctx context.Context, dev tpm.Device, endorsementAuth string) (*x509.Certificate, *tpm.Public, uint32, error) {
	ekcerts, err := usableEndorsementKeys(ctx, dev, endorsementAuth)
	if err != nil {
		return nil, nil, 0, err
	}

	if len(ekcerts) == 0 {
		return nil, nil, 0, ErrNoEndorsement
	}

	return ekcerts[0].Certificate, ekcerts[0].Public, ekcerts[0].NvIndex, nil
}

type ekCertificateAndKey struct {
	Certificate *x509.Certificate
	Public      *tpm.Public
	NvIndex     uint32
}

func usableEndorsementKeys(ctx context.Context, dev tpm.Device, endorsementAuth string) ([]ekCertificateAndKey, error) {
	indices, err := listNvIndexes(ctx, dev)
	if err != nil {
		return nil, err
	}
	sort.Slice(indices, func(i, j int) bool {
		return indices[i] < indices[j]
	})

	maxBuffer, err := maxNvBufferSizes(ctx, dev)
	if err != nil {
		return nil, err
	}

	ret := make([]ekCertificateAndKey, 0, len(ekPriorityList))

	for _, ek := range ekPriorityList {
		off := sort.Search(len(indices), func(i int) bool {
			return indices[i] >= ek.I
		})
		if off >= len(indices) || indices[off] != ek.I {
			continue
		}

		nvpub, der, err := readNvIndex(ctx, dev, ek.I, endorsementAuth, maxBuffer)
		if err != nil {
			return nil, err
		}

		sane := nvpub.DataSize > 0 && nvpub.Attributes&tpm.TPMA_NV_AUTHREAD != 0 &&
			nvpub.Attributes&tpm.TPMA_NV_READLOCKED == 0
		if !sane {
			return nil, ErrKeyInvalid
		}

		h, pub, _, _, _, _, err := tpm.CreatePrimary(ctx, dev, tpm.Password(endorsementAuth), tpm.TPM_RH_ENDORSEMENT, "", *ek.T, nil, nil)
		if err != nil {
			return nil, err
		}
		if err := tpm.FlushContext(ctx, dev, h); err != nil {
			return nil, err
		}

		cert, err := checkEndorsementCertificateAgainstKey(der, pub)
		if err != nil {
			continue
		}

		r := ekCertificateAndKey{Certificate: cert, Public: &pub, NvIndex: ek.I}
		ret = append(ret, r)
	}

	return ret, nil
}

func checkEndorsementCertificateAgainstKey(nvdata []byte, pub tpm.Public) (*x509.Certificate, error) {
	var err error

	// some TPM vendors think it's a great idea to pad the certificate with 0xff
	var cert *x509.Certificate
	for {
		cert, err = x509.ParseCertificate(nvdata)
		if err == nil {
			break
		}
		if nvdata[len(nvdata)-1] != 0xff {
			return nil, err
		}
		nvdata = nvdata[:len(nvdata)-1]
	}

	// TCG EK Credential Profile version 2.5, rev 2, section 3.2
	if cert.Version != 3 {
		return nil, ErrKeyInvalid
	}
	if cert.BasicConstraintsValid && cert.IsCA {
		return nil, ErrKeyInvalid
	}

	switch t := cert.PublicKey.(type) {
	case *rsa.PublicKey:
		if pub.Type != tpm.TPM_ALG_RSA {
			return nil, ErrKeyInvalid
		}
		if pub.RsaParameters == nil {
			return nil, ErrKeyInvalid
		}
		parms := pub.RsaParameters
		exp := int(parms.Exponent)
		if exp == 0 {
			exp = 0x10001
		}
		if exp != t.E {
			return nil, ErrKeyInvalid
		}
		if !bytes.Equal([]byte(parms.Unique), t.N.Bytes()) {
			return nil, ErrKeyInvalid
		}

	case *ecdsa.PublicKey:
		if pub.Type != tpm.TPM_ALG_ECC {
			return nil, ErrKeyInvalid
		}
		if pub.EccParameters == nil {
			return nil, ErrKeyInvalid
		}
		parms := pub.EccParameters
		if !bytes.Equal([]byte(parms.Unique.X), t.X.Bytes()) {
			return nil, ErrKeyInvalid
		}
		if !bytes.Equal([]byte(parms.Unique.Y), t.Y.Bytes()) {
			return nil, ErrKeyInvalid
		}

	default:
		return nil, ErrKeyInvalid
	}

	return cert, nil
}

func checkModulePrerequisits(ctx context.Context, dev tpm.Device) (string, error) {
	err := checkModuleSpecification(ctx, dev)
	if err != nil {
		return "", err
	}
	venstr, err := checkModuleVulnerabilities(ctx, dev)
	if err != nil {
		return "", err
	}
	err = checkModulePlatformConfigRegs(ctx, dev)
	if err != nil {
		return "", err
	}
	err = checkModuleCapabilities(ctx, dev)
	if err != nil {
		return "", err
	}

	return venstr, nil
}

func checkModuleSpecification(ctx context.Context, dev tpm.Device) error {
	// TPM family should be 2.0
	family, err := tpm.TpmProperty(ctx, dev, uint32(tpm.TPM_PT_FAMILY_INDICATOR))
	if err != nil {
		return err
	}
	year, err := tpm.TpmProperty(ctx, dev, uint32(tpm.TPM_PT_YEAR))
	if err != nil {
		return err
	}
	doy, err := tpm.TpmProperty(ctx, dev, uint32(tpm.TPM_PT_DAY_OF_YEAR))
	if err != nil {
		return err
	}

	return checkModuleCapabilitiesImpl(family, year, doy)
}

func checkModuleCapabilitiesImpl(family uint32, year uint32, doy uint32) error {
	if family != tpm.TPM_SPEC_FAMILY {
		return ErrPrereqNotMet
	}

	// the implemented spec should be at least 2013
	specDate := time.Date(int(year), time.January, int(doy), 0, 0, 0, 0, time.UTC)
	minSpecDate, _ := time.Parse(time.DateOnly, minimumSpecDate)
	if !specDate.After(minSpecDate) {
		return ErrPrereqNotMet
	}

	return nil
}

func checkModuleVulnerabilities(ctx context.Context, dev tpm.Device) (string, error) {
	venid, err := tpm.TpmProperty(ctx, dev, uint32(tpm.TPM_PT_MANUFACTURER))
	if err != nil {
		return "", ErrPrereqNotMet
	}

	vendor, ok := tpm.VendorIds[venid]
	if !ok {
		vendor = string([]byte{
			uint8(venid >> 24),
			uint8(venid >> 16),
			uint8(venid >> 8),
			uint8(venid),
		})
		vendor = strings.TrimSpace(vendor)
	}

	minfw, ok := minimumFirmwareVersions[vendor]
	if ok {
		fwverhi, err := tpm.TpmProperty(ctx, dev, uint32(tpm.TPM_PT_FIRMWARE_VERSION_1))
		if err != nil {
			return "", ErrPrereqNotMet
		}

		fwverlo, err := tpm.TpmProperty(ctx, dev, uint32(tpm.TPM_PT_FIRMWARE_VERSION_2))
		if err != nil {
			return "", ErrPrereqNotMet
		}

		if minfw > (uint64(fwverhi)<<32)|uint64(fwverlo) {
			return "", ErrPrereqNotMet
		}
	}

	var buf bytes.Buffer
	venprops := []uint32{
		uint32(tpm.TPM_PT_VENDOR_STRING_1),
		uint32(tpm.TPM_PT_VENDOR_STRING_2),
		uint32(tpm.TPM_PT_VENDOR_STRING_3),
		uint32(tpm.TPM_PT_VENDOR_STRING_4),
	}
	for _, prop := range venprops {
		v, err := tpm.TpmProperty(ctx, dev, prop)
		if err != nil {
			continue
		}
		if err := binary.Write(&buf, binary.BigEndian, v); err != nil {
			return "", err
		}
	}

	ret := fmt.Sprintf("%s %s", vendor, strings.TrimSpace(buf.String()))
	return ret, nil
}

func checkModulePlatformConfigRegs(ctx context.Context, dev tpm.Device) error {
	data, _, err := tpm.GetCapability(ctx, dev, uint32(tpm.TPM_CAP_PCRS), 0, 1)
	if err != nil {
		return err
	}

	return checkModulePlatformConfigRegsImpl(data)
}

func checkModulePlatformConfigRegsImpl(data tpm.CapabilityData) error {
	if data.Capability != tpm.TPM_CAP_PCRS {
		return ErrPrereqNotMet
	}
	hit := []uint16{}
outer:
	for _, sel := range data.Pcrs {
		bank, ok := minimumPlatformConfigRegSet[uint16(sel.Hash)]
		if !ok {
			continue
		}
		sort.Ints(sel.Pcr)
		for _, p := range bank {
			if sort.SearchInts(sel.Pcr, p) >= len(sel.Pcr) || sel.Pcr[sort.SearchInts(sel.Pcr, p)] != p {
				continue outer
			}
		}
		hit = append(hit, uint16(sel.Hash))
	}

	if len(hit) >= len(minimumPlatformConfigRegSet) {
		return nil
	} else {
		return ErrPrereqNotMet
	}
}

func checkModuleCapabilities(ctx context.Context, dev tpm.Device) error {
	// commands
	cmds, err := listCommands(ctx, dev)
	if err != nil {
		return err
	}
	sort.Slice(cmds, func(i, j int) bool {
		return cmds[i]&0xffff < cmds[j]&0xffff
	})
	for _, cmd := range minimumCommands {
		idx := sort.Search(len(cmds), func(i int) bool {
			return cmds[i]&0xffff >= cmd
		})
		if idx >= len(cmds) || cmds[idx]&0xffff != cmd {
			return ErrPrereqNotMet
		}
	}

	// algorithms
	sort.Slice(minimumAlgorithms, func(i, j int) bool {
		return minimumAlgorithms[i] < minimumAlgorithms[j]
	})
	var prev []uint16
outer:
	for _, alg := range minimumAlgorithms {
		for len(prev) > 0 {
			p := prev[0]
			prev = prev[1:]
			if p == alg {
				continue outer
			}
		}

		data, _, err := tpm.GetCapability(ctx, dev, uint32(tpm.TPM_CAP_ALGS), uint32(alg), 0xffff)
		if err != nil {
			return err
		}
		if data.Capability != tpm.TPM_CAP_ALGS {
			return ErrUnexpectedResponse
		}
		if uint16(data.Algorithms[0].Alg) != alg {
			return ErrPrereqNotMet
		}
		for _, a := range data.Algorithms[:1] {
			prev = append(prev, uint16(a.Alg))
		}
		sort.Slice(prev, func(i, j int) bool {
			return prev[i] >= prev[j]
		})
	}

	// curves
	curves, err := listEccCurves(ctx, dev)
	if err != nil {
		return err
	}
	sort.Slice(curves, func(i, j int) bool {
		return curves[i] < curves[j]
	})
	for _, cv := range minimumCurves {
		idx := sort.Search(len(curves), func(i int) bool {
			return uint16(curves[i]) >= cv
		})
		if idx >= len(curves) || uint16(curves[idx]) != cv {
			return ErrPrereqNotMet
		}
	}

	return nil
}

// Options for the [Request] function.
type RequestOptions struct {
	// Auth Value of the TPM Endorsement hierarchy. Default is the empty string.
	EndorsementAuth string
	// Auth Value of the TPM Owner hierarchy. Default is the empty string.
	OwnerAuth string
	// Path to the TPM device node. If empty, the system TPM is used.
	DevicePath string

	// Testing only: Event log entries to use. If empty, the system event log is used.
	eventlog []record
	device   tpm.Device
}

// Initiate the enrollment protocol by generating an EnrollmentRequest. The
// verifies the TPM prerequisites and selects a usable Endorsement Key and
// certificate. It generates an Attestation Identity Key and collects the event
// log. The context ctx can be used to cancel the operation.
func Request(ctx context.Context, opts *RequestOptions) (*EnrollmentRequest, error) {
	var dev tpm.Device
	var err error

	if opts == nil {
		opts = new(RequestOptions)
	}

	devicePath := opts.DevicePath

	// connect to TPM
	if opts.device != nil {
		dev = opts.device
	} else {
		if devicePath != "" {
			dev, err = tpm.OpenTPM(devicePath)
		} else {
			dev, devicePath, err = findSystemTPM(ctx)
		}
		if err != nil {
			return nil, err
		}
		defer dev.Close()
	}

	_, err = checkModulePrerequisits(ctx, dev)
	if err != nil {
		return nil, err
	}

	ekcert, ekpub, ekindex, err := selectEndorsementKey(ctx, dev, opts.EndorsementAuth)
	if err != nil {
		return nil, err
	}
	ekbuf, err := ekpub.Pack()
	if err != nil {
		return nil, err
	}

	// derive AIK
	aik, err := deriveAttestationIdentityKey(ctx, dev, opts.OwnerAuth)
	if err != nil {
		return nil, err
	}
	aikbuf, err := aik.Pack()
	if err != nil {
		return nil, err
	}

	// combine event logs into one cel encoded log
	var binlog []byte
	if len(opts.eventlog) > 0 {
		buf := new(bytes.Buffer)
		err = encodeRecords(buf, opts.eventlog)
		if err != nil {
			return nil, err
		}
		binlog = buf.Bytes()
	} else {
		binlog, err = collectEventlogs(ctx, devicePath)
		if err != nil {
			return nil, err
		}
	}

	buf := new(bytes.Buffer)
	if err := binary.Write(buf, binary.BigEndian, ekindex); err != nil {
		return nil, err
	}
	req := EnrollmentRequest{
		EndorsementNvIndex:     buf.Bytes(),
		EndorsementCertificate: ekcert.Raw,
		EndorsementPublicKey:   ekbuf,
		AttestationIdentityKey: aikbuf,
		EventLog:               binlog,
	}
	return &req, nil
}

// A Signer is used to abstract away signing of endorsements. Together with a
// Verifier they must implement an EUF-CMA secure signature scheme.
type Signer interface {
	Sign(message []byte) ([]byte, error)
}

// Verifies signatures produced by a corresponding [Signer].
type Verifier interface {
	Verify(message, signature []byte) error
}

// Options for the [CreateChallenge] function.
type ChallengeOptions struct {
	// Allow the Endorsement Key certificate to be self-signed. This is useful
	// for testing and attesting an TPM simulator.
	AllowSelfSigned bool
	// Allow the Endorsement Key certificate to be signed by an unknown CA.
	// Should be used carefully. The authenticity of the Endorsement Key
	// certificate is the only guarantee that the device is not using a
	// simulator.
	AllowUnknownCa bool
	// HTTP client to use for online CRL checks. If nil, the default HTTP client
	// is used.
	Client *http.Client
	// If true the online CRL check is skipped.
	SkipRevokationCheck bool

	// Testing only
	random io.Reader
}

// Uses the Enrollment Request to create a Challenge for the device to be
// attested. The function verifies the Endorsment Key certificate to make sure
// it is issued by a known TPM vendor, follows the TPM EK Credential Profile,
// and is not revoked. It also verifies the Attestation Identity Key matches
// the [defaultAikTemplate]. The context ctx can be used to cancel the online
// revokation check. The signer is used to sign the Platform Endorsemement that
// is part of the challenge. The opts can be used to configure the challenge
// creation. See [ChallengeOptions].
//
// Because this function requires the signer, it must be run on a secure device
// outside the reach of the device to be attested and any potential attacker.
func CreateChallenge(ctx context.Context, request *EnrollmentRequest, signer Signer, opts *ChallengeOptions) (*Challenge, error) {
	if opts == nil {
		opts = new(ChallengeOptions)
	}
	if opts.random == nil {
		opts.random = rand.Reader
	}

	// verify EK cert
	roots, err := vendorRoots()
	if err != nil {
		return nil, err
	}
	if opts.AllowSelfSigned {
		cert, err := x509.ParseCertificate(request.EndorsementCertificate)
		if err == nil {
			roots.AddCert(cert)
		}
	}
	ekcert, err := x509.ParseCertificate(request.EndorsementCertificate)
	if err != nil {
		return nil, err
	}
	ekveri, err := verifyEndorsementCertificate(ekcert, roots, opts.AllowUnknownCa)
	if err != nil {
		return nil, err
	}

	// revokation check
	if !opts.SkipRevokationCheck {
		err = checkRevokation(ctx, opts.Client, ekveri)
		if err != nil {
			return nil, err
		}
	}

	// verify aik
	aik, err := tpm.UnpackPublic(request.AttestationIdentityKey)
	if err != nil {
		return nil, err
	}
	err = verifyAttestationIdentityKey(aik)
	if err != nil {
		return nil, err
	}

	// endorse platform measurements
	evlog := bytes.NewReader(request.EventLog)
	log, err := decodeCelEventlog(evlog)
	if err != nil {
		return nil, err
	}
	firmware, firmwareSig, err := endorsePlatformFirmware(log, signer)
	if err != nil {
		return nil, err
	}

	// certify aik
	sig, err := signer.Sign(request.AttestationIdentityKey)
	if err != nil {
		return nil, err
	}

	// encrypt challenge
	ekpub, err := tpm.UnpackPublic(request.EndorsementPublicKey)
	if err != nil {
		return nil, err
	}
	cred := encodeByteArrays(credentialMagic, sig, firmware, firmwareSig)

	key := make([]byte, 32)
	if _, err := io.ReadFull(opts.random, key); err != nil {
		return nil, err
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	nonce := make([]byte, 12)
	if _, err := io.ReadFull(opts.random, nonce); err != nil {
		return nil, err
	}
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	ciphertext := aesgcm.Seal(nil, nonce, cred, nil)

	idobj, enckey, err := aik.MakeCredential(*ekpub, tpm.Buffer(key), opts.random)
	if err != nil {
		return nil, err
	}
	idobjbuf, err := idobj.Pack()
	if err != nil {
		return nil, err
	}

	// build challenge
	challenge := Challenge{
		EndorsementNvIndex: request.EndorsementNvIndex,
		IdObject:           idobjbuf,
		EncryptedKey:       enckey,
		Nonce:              nonce,
		Ciphertext:         ciphertext,
	}
	return &challenge, nil
}

// Options for the [Finish] function.
type FinishOptions struct {
	// Path to the TPM device node. If empty, the system TPM is used.
	DevicePath string
	// Auth Value of the TPM Endorsement hierarchy. Default is the empty string.
	EndorsementAuth string
	// Auth Value of the TPM Owner hierarchy. Default is the empty string.
	OwnerAuth string
	// Verifier to check the signature of the Platform Endorsement. If nil, the
	// verification is skipped. This is not a security issue as the endorsement
	// is AEAD encrypted. It just serves as a sanity check.
	Verifier Verifier

	// Testing only
	random io.Reader
	device tpm.Device
}

// The final step in the enrollment protocol. Solve the challenge generated by
// [CreateChallenge]. The function decrypts the challenge using the TPM's
// credential activation function and returns the decrypted Platform
// Endorsement. The context ctx can be used to cancel the operation. The opts
// can be used to configure the challenge solving. See [FinishOptions].
func Finish(ctx context.Context, challenge *Challenge, opts *FinishOptions) (*PlatformEndorsement, error) {
	var dev tpm.Device
	var err error

	if opts == nil {
		opts = new(FinishOptions)
	}
	if opts.random == nil {
		opts.random = rand.Reader
	}

	// connect to TPM
	if opts.device != nil {
		dev = opts.device
	} else {
		devicePath := opts.DevicePath
		if devicePath != "" {
			dev, err = tpm.OpenTPM(devicePath)
		} else {
			dev, _, err = findSystemTPM(ctx)
		}
		if err != nil {
			return nil, err
		}
		defer dev.Close()
	}

	ownerPass := tpm.Password(opts.OwnerAuth)
	endorsementPass := tpm.Password(opts.EndorsementAuth)

	eknvidx := uint32(binary.BigEndian.Uint32(challenge.EndorsementNvIndex))
	ek, err := findEndorsementKeyTemplate(eknvidx)
	if err != nil {
		return nil, err
	}

	ekHandle, ekpub, _, _, _, _, err := tpm.CreatePrimary(ctx, dev, ownerPass, tpm.TPM_RH_ENDORSEMENT, "", *ek, nil, tpm.PcrSelection{})
	if err != nil {
		return nil, err
	}
	defer func() { err = tpm.FlushContext(ctx, dev, ekHandle) }()

	aikHandle, aikpub, err := loadAttestationIdentityKey(ctx, dev, opts.OwnerAuth)
	if err != nil {
		return nil, err
	}
	defer func() { err = tpm.FlushContext(ctx, dev, aikHandle) }()

	var authSess tpm.Auth
	if ekpub.ObjectAttributes&tpm.TPMA_OBJECT_USERWITHAUTH == 0 {
		nonce := make([]byte, 32)
		_, err = io.ReadFull(opts.random, nonce)
		if err != nil {
			return nil, err
		}

		sess, err := tpm.StartAuthSession(ctx, dev, tpm.TPM_ALG_SHA256, opts.random)
		if err != nil {
			return nil, err
		}
		defer func() { err = tpm.FlushContext(ctx, dev, sess) }()

		_, _, err = tpm.PolicySecret(ctx, dev, tpm.TPM_RH_ENDORSEMENT, sess, endorsementPass, nil, nil, nil, 0)
		if err != nil {
			return nil, err
		}

		authSess = tpm.Policy(sess)
	} else {
		authSess = tpm.Password(opts.EndorsementAuth)
	}

	idobj, err := tpm.UnpackIdObject(challenge.IdObject)
	if err != nil {
		return nil, err
	}

	key, err := tpm.ActivateCredential(ctx, dev, aikHandle, ekHandle, tpm.Password(""), authSess, *idobj, challenge.EncryptedKey)
	if err != nil {
		return nil, err
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	cred, err := aesgcm.Open(nil, challenge.Nonce, challenge.Ciphertext, nil)
	if err != nil {
		return nil, err
	}

	var en PlatformEndorsement
	buf := bytes.NewReader(cred)
	err = decodeByteArrays(buf, credentialMagic, &en.AikSignature, &en.PlatformFirmware, &en.FirmwareSignature)
	if err != nil {
		return nil, err
	}

	en.AttestationIdentityKey, err = aikpub.Pack()
	if err != nil {
		return nil, err
	}

	// optinally verify the signature on the endorsement. this is only a sanity
	// check. the endorsement AEAD encrypted when we recieve it.
	if opts.Verifier != nil {
		if err := opts.Verifier.Verify(en.AttestationIdentityKey, en.AikSignature); err != nil {
			return nil, err
		}
		if err := opts.Verifier.Verify(en.PlatformFirmware, en.FirmwareSignature); err != nil {
			return nil, err
		}
	}

	return &en, err
}
