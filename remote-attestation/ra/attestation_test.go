package ra

import (
	"bytes"
	"context"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"io"
	mrand "math/rand"
	"os"
	"strings"
	"testing"

	test "system-transparency.org/ppg/ra/internal"
	"system-transparency.org/ppg/ra/internal/tpm"
)

func TestGenerateQuote(t *testing.T) {
	dev := tpm.SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()

	ctx := context.Background()
	_, _, _, err := generateQuote(ctx, dev, "", nil)
	test.NoErr(t, err)
}

func TestPcrCalc(t *testing.T) {
	signer := hmacSigner{Key: []byte("testkey")}

	fd, err := os.OpenFile("testdata/qemu.eventlog", os.O_RDONLY, 0)
	if test.NoErr(t, err) {
		defer fd.Close()
	}
	evlog, err := transcodePcClientEventlog(fd)
	test.NoErr(t, err)

	mustHex := func(s string) []byte {
		b, err := hex.DecodeString(s)
		if err != nil {
			t.Fatalf("failed to decode hex string %s: %v", s, err)
		}
		return b
	}

	pcr := map[int][]byte{
		0:  mustHex("829F9979CC5DD8519215DFA26073441EBF6F0D62B93DC6F8F17AB34017BFD943"),
		1:  mustHex("DD7274F21362A8A813AE587A4BAE5E87B1C3665212CE519854C3597768A3E633"),
		2:  mustHex("D202FBB8E12919FF5CC10689C96BFA91BEA12ED53D633AFAFB49037532D450AF"),
		3:  mustHex("3D458CFE55CC03EA1F443F1562BEEC8DF51C75E14A9FCF9A7234A13F198E7969"),
		4:  mustHex("9657ba1f658de6c55d3f363045788efca03a8464897e22e0a0506845f178325c"),
		5:  mustHex("2220A93C33ED6FBB12AE4A0C211CDCD0A5243FE0A24684BD2F634A1543639523"),
		6:  mustHex("3D458CFE55CC03EA1F443F1562BEEC8DF51C75E14A9FCF9A7234A13F198E7969"),
		7:  mustHex("65CAF8DD1E0EA7A6347B635D2B379C93B9A1351EDC2AFC3ECDA700E534EB3068"),
		8:  mustHex("0000000000000000000000000000000000000000000000000000000000000000"),
		9:  mustHex("0000000000000000000000000000000000000000000000000000000000000000"),
		10: mustHex("CDEBE49DB057DE3DCF9C47AC0AF57AA4BA8C78F7FBDFDA9C00B5B22BD95A69C3"),
		11: mustHex("db85b884b85a34d62074530a46c54ce28229c5ad60eaf36c723954c554c22896"),
		12: mustHex("0c5cd607274021585a393bfc85ab83400b6054295dd5177200e0e490472b3b09"),
		13: mustHex("318467aa16db7983a307fb5bdf5138724156e28d1e8526b0befd34a5156bf422"),
		14: mustHex("D3735899D9FA7162447CA631F0BA2CD5EB57D0965A756D78291DA33072610EB2"),
		15: mustHex("0000000000000000000000000000000000000000000000000000000000000000"),
		16: mustHex("0000000000000000000000000000000000000000000000000000000000000000"),
		17: mustHex("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"),
		18: mustHex("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"),
		19: mustHex("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"),
		20: mustHex("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"),
		21: mustHex("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"),
		22: mustHex("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"),
		23: mustHex("0000000000000000000000000000000000000000000000000000000000000000"),
	}

	firmware, _, err := endorsePlatformFirmware(evlog, &signer)
	test.NoErr(t, err)

	platform := PlatformEndorsement{
		PlatformFirmware: firmware,
	}

	efiapp, err := os.ReadFile("testdata/stboot-0.3.6.efi")
	test.NoErr(t, err)

	desc, err := os.ReadFile("testdata/stimage-small.json")
	test.NoErr(t, err)

	zip, err := os.ReadFile("testdata/stimage-small.zip")
	test.NoErr(t, err)

	rootpem, err := os.ReadFile("testdata/stboot-0.3.6-rootcert.pem")
	test.NoErr(t, err)
	rootder, _ := pem.Decode(rootpem)
	if rootder == nil {
		t.Fatalf("failed to decode root certificate")
	}

	httppem, err := os.ReadFile("testdata/isrgrootx1.pem")
	test.NoErr(t, err)
	httpder, _ := pem.Decode(httppem)
	if httpder == nil {
		t.Fatalf("failed to decode root certificate")
	}

	type trustPolicy struct {
		SignatureThreshold int    `json:"ospkg_signature_threshold"`
		FetchMethod        string `json:"ospkg_fetch_method"`
	}
	tp := trustPolicy{
		SignatureThreshold: 1,
		FetchMethod:        "network",
	}
	tpbuf, err := json.Marshal(tp)
	test.NoErr(t, err)

	stboot, err := EndorseStboot(efiapp, rootder.Bytes, httpder.Bytes, tpbuf, nil, nil, &signer, nil)
	test.NoErr(t, err)

	ospkg, err := EndorseOsPackage(desc, zip, &signer, nil)
	test.NoErr(t, err)

	computed, _, err := computePcr(&platform, ospkg, stboot, nil)
	test.NoErr(t, err)

	t.Logf("                                    Observed                             |                                 Computed\n")
	for _, i := range pcClientPcr {
		sign := "="
		if !bytes.Equal(pcr[i], computed[i]) {
			sign = "|"
			t.Fail()
		}
		t.Logf("PCR[%2d] %x %s %x\n", i, pcr[i], sign, computed[i])
	}

	for _, ev := range evlog {
		t.Logf("%s", ev.String())
	}
}

func TestAttestation(t *testing.T) {
	if os.Getenv("TEST_TPM") != "" && os.Getenv("TEST_TPM_FRESH") == "" {
		t.Skip("skipping test; set TEST_TPM_FRESH=1 to enable. Make sure to have a fresh TPM simulator instance.")
		return
	}
	dev := tpm.SelectTestTpm(t)
	if dev == nil {
		return
	}
	defer dev.Close()

	random := mrand.New(mrand.NewSource(42))

	// simulate some boot events
	evlog := make([]record, 0)
	for pcrnum := 0; pcrnum < 16; pcrnum++ {
		data := make([]byte, 32)
		_, err := random.Read(data)
		test.NoErr(t, err)
		evlog = measure(evlog, pcrnum, sha256.Sum256(data), 1, data)
	}

	// add events for the firmware measurement extraction state machine
	sep := []byte("\x00\x00\x00\x00")
	evlog = measure(evlog, 4, sha256.Sum256(sep), separator, sep)
	evlog = measure(evlog, 4, sha256.Sum256(nil), efiBootServicesApplication, nil)
	evlog = measure(evlog, 4, sha256.Sum256(nil), efiBootServicesApplication, nil)
	evlog = measure(evlog, 5, sha256.Sum256([]byte(exitBootServices)), efiAction, []byte(exitBootServices))
	evlog = measure(evlog, 5, sha256.Sum256([]byte(exitBootServicesReturn)), efiAction, []byte(exitBootServicesReturn))

	// create endorsements
	efiapp, err := os.ReadFile("testdata/stboot-0.3.6.efi")
	test.NoErr(t, err)

	desc, err := os.ReadFile("testdata/stimage-small.json")
	test.NoErr(t, err)

	zip, err := os.ReadFile("testdata/stimage-small.zip")
	test.NoErr(t, err)

	rootpem, err := os.ReadFile("testdata/stboot-0.3.6-rootcert.pem")
	test.NoErr(t, err)
	rootder, _ := pem.Decode(rootpem)
	if rootder == nil {
		t.Fatalf("failed to decode root certificate")
	}

	httppem, err := os.ReadFile("testdata/isrgrootx1.pem")
	test.NoErr(t, err)
	httpder, _ := pem.Decode(httppem)
	if httpder == nil {
		t.Fatalf("failed to decode root certificate")
	}

	type trustPolicy struct {
		SignatureThreshold int    `json:"ospkg_signature_threshold"`
		FetchMethod        string `json:"ospkg_fetch_method"`
	}
	tp := trustPolicy{
		SignatureThreshold: 1,
		FetchMethod:        "network",
	}
	tpbuf, err := json.Marshal(tp)
	test.NoErr(t, err)

	signer := hmacSigner{Key: []byte("test")}
	stboot, err := EndorseStboot(efiapp, rootder.Bytes, httpder.Bytes, tpbuf, nil, nil, &signer, nil)
	test.NoErr(t, err)

	ospkg, err := EndorseOsPackage(desc, zip, &signer, nil)
	test.NoErr(t, err)

	//  enroll the platform
	ctx := context.Background()
	reqopts := RequestOptions{
		eventlog: evlog,
		device:   dev,
	}
	req, err := Request(ctx, &reqopts)
	test.NoErr(t, err)

	chopts := ChallengeOptions{
		AllowSelfSigned: true,
		random:          random,
	}
	ch, err := CreateChallenge(ctx, req, &signer, &chopts)
	test.NoErr(t, err)

	fopts := FinishOptions{
		Verifier: &signer,
		device:   dev,
		random:   random,
	}
	platform, err := Finish(ctx, ch, &fopts)
	test.NoErr(t, err)

	// compute the expected PCR values
	computedlog, err := computeEvents(platform, ospkg, stboot, nil)
	test.NoErr(t, err)

	// measure the computed PCR values into the TPM simulator
	for _, ev := range computedlog {
		ds := ev.Digests[0].Digest
		vals := tpm.DigestValues{tpm.Ha{Alg: tpm.TPM_ALG_SHA256, Digest: ds}}
		err = tpm.PcrExtend(ctx, dev, tpm.U32(ev.Pcr), tpm.Password(""), vals)
		test.NoErr(t, err)
	}

	// attest
	nonce := make([]byte, 32)
	_, err = random.Read(nonce)
	test.NoErr(t, err)

	aopts := AttestOptions{
		eventlog: evlog,
		device:   dev,
	}
	quote, err := Attest(ctx, nonce, &aopts)
	test.NoErr(t, err)

	err = Verify(nonce, quote, platform, ospkg, stboot, nil, &signer)
	test.NoErr(t, err)

	if err == nil {
		t.Logf("Attestation successful")
	}
}

func TestEndorseSignatures(t *testing.T) {
	cases := []string{
		"normal",
		"wrongDigest",
		"brokenAikSig",
		"brokenFwSig",
		"brokenOspkgSig",
		"brokenStbootSig",
		"brokenQuoteSig",
		"wrongSigner",
		"wrongNonce",
	}

	for i, c := range cases {
		t.Run(c, func(t *testing.T) {
			random := mrand.New(mrand.NewSource(42 * int64(i)))

			// simulate some boot events
			evlog := make([]record, 0)
			for pcrnum := 0; pcrnum < 16; pcrnum++ {
				data := make([]byte, 32)
				_, err := rand.Read(data)
				test.NoErr(t, err)
				evlog = measure(evlog, pcrnum, sha256.Sum256(data), 1, data)
			}

			// add events for the firmware measurement extraction state machine
			sep := []byte("\x00\x00\x00\x00")
			evlog = measure(evlog, 4, sha256.Sum256(sep), separator, sep)
			evlog = measure(evlog, 4, sha256.Sum256(nil), efiBootServicesApplication, nil)
			evlog = measure(evlog, 4, sha256.Sum256(nil), efiBootServicesApplication, nil)
			evlog = measure(evlog, 5, sha256.Sum256([]byte(exitBootServices)), efiAction, []byte(exitBootServices))
			evlog = measure(evlog, 5, sha256.Sum256([]byte(exitBootServicesReturn)), efiAction, []byte(exitBootServicesReturn))

			// create endorsements
			aik, aikpriv := fakeAik(t, random)

			signer := hmacSigner{Key: []byte("test")}

			ospkg := testOsPkgEndorsement(t, "stimage-small", &signer)
			stboot := testStbootEndorsement(t, "stboot-0.3.6", &signer, false)
			platform := testPlatformEndorsement(t, &aik, evlog, &signer)

			if c == "brokenAikSig" {
				platform.AikSignature[0] ^= 0x01
			}
			if c == "brokenFwSig" {
				platform.FirmwareSignature[0] ^= 0x01
			}
			if c == "brokenOspkgSig" {
				ospkg.Signature[0] ^= 0x01
			}
			if c == "brokenStbootSig" {
				stboot.Signature[0] ^= 0x01
			}

			_, expected, err := computePcr(platform, ospkg, stboot, nil)
			test.NoErr(t, err)
			if c == "wrongDigest" {
				expected[0] ^= 0x01
			}

			aikqname, err := qualifiedNameOwnerHierarchy(&aik)
			test.NoErr(t, err)
			attest := tpm.Attest{
				Magic:  tpm.TPM_GENERATED_VALUE,
				Type:   tpm.TPM_ST_ATTEST_QUOTE,
				Signer: aikqname,
				Extra:  make([]byte, 32),
				Quote: &tpm.QuoteInfo{
					Select: []tpm.PcrSelect{{Hash: tpm.TPM_ALG_SHA256, Pcr: pcClientPcr}},
					Digest: expected,
				},
			}
			_, err = rand.Read(attest.Extra)
			test.NoErr(t, err)
			nonce := append([]byte(nil), attest.Extra...)

			if c == "wrongSigner" {
				attest.Signer[0] ^= 0x01
			}
			if c == "wrongNonce" {
				nonce[0] ^= 0x01
			}

			attestbuf, err := attest.Pack()
			test.NoErr(t, err)

			sig := signQuote(t, &attest, aikpriv)
			if c == "brokenQuoteSig" {
				sig.Ecc.R[0] ^= 0x01
			}
			sigbuf, err := sig.Pack()
			test.NoErr(t, err)

			quote := Quote{
				Attest:    attestbuf,
				Signature: sigbuf,
			}

			err = Verify(nonce, &quote, platform, ospkg, stboot, nil, &signer)
			if c == "normal" {
				test.NoErr(t, err)
			} else {
				test.WantErr(t, err)
			}
		})
	}
}

func TestPrecomputeRealMachines(t *testing.T) {
	machines := []string{
		"x11schln4f-910",
		"h12ssl",
		"dl160",
		"sr630",
	}

	signer := hmacSigner{Key: []byte("test")}
	stboot035 := testStbootEndorsement(t, "stboot-0.3.5", &signer, true)
	stboot036 := testStbootEndorsement(t, "stboot-0.3.6", &signer, true)
	ospkgsmall := testOsPkgEndorsement(t, "stimage-small", &signer)
	ospkglarge := testOsPkgEndorsement(t, "stimage-large", &signer)
	aik, _ := fakeAik(t, rand.Reader)
	for _, m := range machines {
		t.Run(m, func(t *testing.T) {
			// platform endorsement computed from stboot-0.3.6 with small ospkg
			small036bin, err := os.ReadFile(fmt.Sprintf("testdata/%s-0.3.6-small.eventlog", m))
			test.NoErr(t, err)
			small036evlog, err := transcodePcClientEventlog(bytes.NewReader(small036bin))
			test.NoErr(t, err)

			platform := testPlatformEndorsement(t, &aik, small036evlog, &signer)

			// four combinations
			var small035, small036, large035, large036 map[string][]string

			small035json, err := os.ReadFile(fmt.Sprintf("testdata/%s-0.3.5-small.pcr", m))
			test.NoErr(t, err)
			err = json.Unmarshal(small035json, &small035)
			test.NoErr(t, err)

			small036json, err := os.ReadFile(fmt.Sprintf("testdata/%s-0.3.6-small.pcr", m))
			test.NoErr(t, err)
			err = json.Unmarshal(small036json, &small036)
			test.NoErr(t, err)

			large035json, err := os.ReadFile(fmt.Sprintf("testdata/%s-0.3.5-large.pcr", m))
			test.NoErr(t, err)
			err = json.Unmarshal(large035json, &large035)
			test.NoErr(t, err)

			large036json, err := os.ReadFile(fmt.Sprintf("testdata/%s-0.3.6-large.pcr", m))
			test.NoErr(t, err)
			err = json.Unmarshal(large036json, &large036)
			test.NoErr(t, err)

			type casedata struct {
				name   string
				pcr    map[string][]string
				ospkg  *OsPackageEndorsement
				stboot *StbootEndorsement
			}
			cases := []casedata{
				{"stboot-0.3.5 small", small035, ospkgsmall, stboot035},
				{"stboot-0.3.5 large", large035, ospkglarge, stboot035},
				{"stboot-0.3.6 small", small036, ospkgsmall, stboot036},
				{"stboot-0.3.6 large", large036, ospkglarge, stboot036},
			}

			for _, c := range cases {
				t.Run(c.name, func(t *testing.T) {
					computed, _, err := computePcr(platform, c.ospkg, c.stboot, nil)
					test.NoErr(t, err)
					for _, pcrnum := range pcClientPcr {
						comp := computed[pcrnum]
						is, err := hex.DecodeString(c.pcr["sha256"][pcrnum])
						test.NoErr(t, err)
						if !bytes.Equal(is, comp) {
							t.Logf("PCR[%2d]\nobserved %x\ncomputed %x\n", pcrnum, is, comp)
							t.Errorf(c.name)
						}
					}
				})
			}
		})
	}
}

func TestVerifyAttest(t *testing.T) {
	cases := []string{
		"normal",
		"wrong-magic",
		"wrong-type",
		"wrong-signer",
		"wrong-extra",
		"wrong-namealg",
		"quote-is-nil",
		"pcr-select-0",
		"pcr-select-2",
		"pcr-hash",
		"pcr-select-not-pcclient",
		"pcr-select-short",
		"wrong-digest",
	}

	for _, c := range cases {
		t.Run(c, func(t *testing.T) {
			aik, _ := fakeAik(t, rand.Reader)
			qname, err := qualifiedNameOwnerHierarchy(&aik)
			test.NoErr(t, err)

			nonce := make([]byte, 32)
			_, err = io.ReadFull(rand.Reader, nonce)
			test.NoErr(t, err)

			expected := make([]byte, 32)
			_, err = io.ReadFull(rand.Reader, expected)
			test.NoErr(t, err)

			att := tpm.Attest{
				Magic:  tpm.TPM_GENERATED_VALUE,
				Type:   tpm.TPM_ST_ATTEST_QUOTE,
				Signer: append([]byte{}, qname...),
				Extra:  append([]byte{}, nonce...),
				Clock: tpm.ClockInfo{
					Clock:        12,
					ResetCount:   23,
					RestartCount: 34,
					Safe:         1,
				},
				FirmwareVersion: 0x11223344_55667788,
				Quote: &tpm.QuoteInfo{
					Select: []tpm.PcrSelect{{Hash: tpm.TPM_ALG_SHA256, Pcr: pcClientPcr}},
					Digest: append([]byte{}, expected...),
				},
			}

			switch c {
			case "wrong-magic":
				att.Magic = 0x11223344
			case "wrong-type":
				att.Type = tpm.TPM_ST_ATTEST_CREATION
			case "wrong-signer":
				att.Signer[0] ^= 0x01
			case "wrong-extra":
				att.Extra[0] ^= 0x01
			case "wrong-namealg":
				aik.NameAlg = tpm.TPM_ALG_NULL
			case "quote-is-nil":
				att.Quote = nil
			case "pcr-select-0":
				att.Quote.Select = []tpm.PcrSelect{}
			case "pcr-select-2":
				att.Quote.Select = append(att.Quote.Select, tpm.PcrSelect{Hash: tpm.TPM_ALG_SHA256, Pcr: pcClientPcr})
			case "pcr-hash":
				att.Quote.Select[0].Hash = tpm.TPM_ALG_SHA1
			case "pcr-select-not-pcclient":
				att.Quote.Select[0].Pcr = []int{0, 1, 2, 3, 4, 5, 6, 7,
					8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23}
			case "pcr-select-short":
				att.Quote.Select[0].Pcr = []int{0}
			case "wrong-digest":
				att.Quote.Digest[0] ^= 0x01
			}

			err = verifyAttest(&att, &aik, nonce, expected)
			if c == "normal" {
				test.NoErr(t, err)
			} else {
				test.WantErr(t, err)
			}
		})
	}
}

func TestVerifySig(t *testing.T) {
	aikpub, aikpriv := fakeAik(t, rand.Reader)

	cases := []string{
		"normal-sha256",
		"normal-sha384",
		"normal-sha512",
		"broken-r",
		"broken-s",
		"unknown-alg",
		"unknown-hash",
		"not-ecc",
		"broken-aik",
		"rsa-aik",
	}

	for _, c := range cases {
		t.Run(c, func(t *testing.T) {
			msg := make([]byte, 32)
			_, err := io.ReadFull(rand.Reader, msg)
			test.NoErr(t, err)

			var hash tpm.U16
			var sum []byte

			switch c {
			default:
				fallthrough
			case "normal-sha256":
				sum256 := sha256.Sum256(msg)
				sum = sum256[:]
				hash = tpm.TPM_ALG_SHA256
			case "normal-sha384":
				sum384 := sha512.Sum384(msg)
				sum = sum384[:]
				hash = tpm.TPM_ALG_SHA384
			case "normal-sha512":
				sum512 := sha512.Sum512(msg)
				sum = sum512[:]
				hash = tpm.TPM_ALG_SHA512
			}

			r, s, err := ecdsa.Sign(rand.Reader, aikpriv, sum)
			test.NoErr(t, err)

			sig := tpm.Signature{
				Algorithm: tpm.TPM_ALG_ECDSA,
				Ecc: &tpm.EccSignature{
					Hash: hash,
					R:    r.Bytes(),
					S:    s.Bytes(),
				},
			}

			switch c {
			case "broken-r":
				sig.Ecc.R[0] ^= 0x01
			case "broken-s":
				sig.Ecc.S[0] ^= 0x01
			case "unknown-alg":
				sig.Algorithm = tpm.TPM_ALG_NULL
			case "unknown-hash":
				sig.Ecc.Hash = tpm.TPM_ALG_NULL
			case "not-ecc":
				sig.Ecc = nil
			case "broken-aik":
				aikpub.EccParameters.CurveID = tpm.TPM_ALG_NULL
			case "rsa-aik":
				rsapub, err := rsa.GenerateKey(rand.Reader, 2048)
				test.NoErr(t, err)
				aikpub.Type = tpm.TPM_ALG_RSA
				aikpub.RsaParameters = &tpm.RsaParms{
					Symmetric: aikpub.EccParameters.Symmetric,
					Scheme:    aikpub.EccParameters.Scheme,
					KeyBits:   2048,
					Exponent:  tpm.U32(rsapub.E),
					Unique:    rsapub.N.Bytes(),
				}
				aikpub.EccParameters = nil
			}

			err = verifySignature(msg, &sig, &aikpub)
			if strings.HasPrefix(c, "normal") {
				test.NoErr(t, err)
			} else {
				test.WantErr(t, err)
			}
		})
	}
}
