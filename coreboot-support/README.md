# Prototype ST coreboot support

The goal of this ST plumbing project is to prototype coreboot support for the System Transparency. 
In detail, this means to be able to boot stboot from a machine that runs coreboot firmware. 
We're building a functional prototype with no maintenance guarantees. 
Note: It is completely uncertain if and when coreboot support will be added to the official ST agenda.

## Overview

To run stboot with coreboot, stboot needs to find its way into a coreboot payload. An easy way is to use coreboot's LinuxBoot payload. A LinuxBoot payload is a Linux kernel plus initramfs. The init-process will be stboot in that case. 

## Minimum Viable Product

The current stboot image (output of the tooling) already includes the required artifacts for a LinuxBoot payload (kernel + initramfs).
To get the basic functionality, the following tasks need to be done: 
- Create script or modify tooling to output the kernel + initramfs without further packaging into UKI / disc image
- Set up a coreboot config for LinuxBoot payload
- Build coreboot passing this config and named kernel and initramfs

The provided scripts could build coreboot to be run in QEMU. This way it delivers a suitable base to adapt for other required machines. 

This MVP cover the following ST feature:
- netbootloader
- signed OS packages
- TPM measurements of stboot to the extent of the current stboot release

The MVP hast the following constraints
- the coreboot image needs to be built per host (since from the available options to locate the hostconfig only one will work: include into initramfs)

### Build a demo

Refer to [guide.md](./qemu-x86/guide.md)

### Evaluation

ST works as a Linux payload for coreboot running at QEMU x86 q35 emulation mainboard. 

The most critical part is the size of the ST artifacts:
```
$ du -h \
system-transparency/out/artifacts/stboot.vmlinuz \
system-transparency/out/artifacts/initramfs\:incl-hostconfig.cpio \
coreboot/build/coreboot.rom 
3,7M    system-transparency/out/artifacts/stboot.vmlinuz
10M     system-transparency/out/artifacts/initramfs:incl-hostconfig.cpio
16M     coreboot/build/coreboot.rom
```
**16M is the maximum for a wide range of flash chips**

From the 10M of the initrd only a few kB are consumed by files such as root certs or host-config. The majority is consumed by the stboot Go binary. 

Further consideration could be to evaluate coreboot file system (cbfs) as a storage for the hostconfig as a counterpart to storing it in a UEFI variable. This would allow to not need to build a host-specific coreboot rom. However this does not reduce the size of the coreboot ROM, in turn it would require to include a provisioning system (special OS package according to stboots provisioning system) into the initrd. This would definately exede the available flash size.

## SiFive Unmatched Board Support

After MVP is done, the goal is to make that project run on the SiFive Unmatched board.
Refer to [guide.md](./riscv-unmached/guide.md)

### Evaluation

ST works as a Linux payload for coreboot running at RISCV Unmached board. 

Drawbacks: 
- No hardware random number generator. In this case the used DHCP lib of stboot can make problems (see workaround in the guid)
- Still not possible to run stboot as pid 1 of a pure initramfs. Works as u-root busybox uinit-command. (Further investigation needed)

## Further Challenges

To be feature-complete with the current status-quo releases, solutions needs to be found for the following:
- Alternative storage location for the hostconfig in some form of NVRAM on the host
- measurement mechanism of coreboot (where and how to measure the payload) measured vs. verified boot?
- How to adapt the UEFI secureboot behavior / adapt at all?
