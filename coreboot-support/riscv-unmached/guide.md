# ST on coreboot for HIFive unmached

This guide describes how to build stboot, the System Transparency bootloader as a LinuxBoot payload for coreboot.
The produced coreboot ROM is built for a RISCV architecture and tested on the _HiFive unmached_ board. 

The following steps refer to: 
- RISCV Linux kernel commit 3ca112b71f3
- coreboot commit 99b069baa6
- stboot commit 4453e31 branch update-deps-v0.3.4
- u-root commit 47bc9bbe
- em100 commit a1d938f8b42

## Prerequisites

The instructions are tested on Ubuntu 22.04 and Go version 1.21.

Make sure to checkout the submodules for coreboot and ST
```
git submodule init
git submodule update
```
**Check the repositories in the subfolder to match the commits mentioned above.**

Make sure to have the following dependencies installed and setup:

### RISCV Linux kernel

In this tutorial the [RISCV Linux kernel tree](https://git.kernel.org/pub/scm/linux/kernel/git/riscv/linux.git/) is used.

Since stboot is using kexec to boot the OS packages and even the riscv kernel tree has no full kexec support for RISCV architecture, the kernel code must be modified with a patch from Maximilian Brune.
```
From: Maximilian Brune <brune.max@aol.de>
Date: Fri, 8 Dec 2023 03:40:05 +0100
Subject: [PATCH] riscv: add kexec support for non ELF images

Currently only ELF images can be loaded by kexec in riscv.

This patch adds support for loading riscv kernels based on the riscv
image header. Most of the code is taken from the arm64 implementation.

Tested on SiFive HiFive Unmatched Mainboard to kexec into ubuntu kernel

Signed-off-by: Maximilian Brune <brune.max@aol.de>
```


1) Install toolchain to cross-compile RISCV Linux kernel
```
sudo apt-get -y install gcc-riscv64-linux-gnu
```

2) Inside the `linux` directory apply the patch for full kexec support
```
git am ../riscv-add-kexec-support-for-non-ELF-images.patch
```

### coreboot

1) Install the device tree tool
```
sudo apt-get install -y device-tree-compiler
```
2) Inside the `coreboot` directory build the coreboot riscv toolchain
```
make crossgcc-riscv CPUS=$(nproc)
```

### u-root Initramfs Builder

The u-root tool is used here to build the ST initramfs (in contrast to the usual ST toolchain). This is due to problems with in the init phase of stboot when running as pid 1 on the unmached board. 

With the u-root tool and its [Mulit-Module feature](https://github.com/u-root/u-root?tab=readme-ov-file#multi-module-workspace-builds) an initrafms can be build with u-roots init programm and stboot as a following uinit process.

1) Inside the `u-root-workspace/uroot` directory build the u-root initramfs-builder
```
go build
```

2) Inside the `u-root-workspace` directory create a go workspace to use multi-module builds with u-root
```
 go work init stboot/ u-root/
 ```
### EM100 Flash Emulator

The _HiFive unmached_ board needs to be connected to an EM100 flash emulator for this tutorial. This flash emulator gets loaded with the coreboot ROM image and presents it to the host on boot-up.

1) Install libusb
```
sudo apt install libusb-1.0-0-dev
```
2) Install libcurl
```
sudo apt-get install libcurl4-gnutls-dev
```

**Note: the `em100` tool need special privileges to operate, either use `sudo` or equip your user with nessesary rights (e.g. `plugdev` user group)**

3) Inside the `em100` directory build and update the em100 tool
```
make
EM100_HOME=. ./em100 -U
```

### Terminal Programm

This tutorial uses _minicom_ to interact with the serial port of the _HiFive unmached_ board. 

1) Install minicom
```
sudo apt-get install minicom
```

2) Turn off hardware flow controll
  - start minicom
  - `Ctrl-A O` for configuration
  - select `Serial port setup`
  - `F` to disable hardware flow controll
  - save and exit settings

**Make sure to check that on each usage of minicom during this tutorial**

### Hardware setup

1) Connect the EM100 to the _HiFive unmached_ board and to a USB port of your machine.

2) Connect the serial port of the _HiFive unmached_ to your machine using an suitable adapter. For this tutorial it is assumed that the serial port can reached via `/dev/ttyUSB1`

3) Connect Ethernet

![Hardware Setup](./hardware-setup.jpeg)

| EM100 Pinout | HiFive unmached GPIO |
|:------------:|:--------------------:|
| GND          | GND                  |
| CS1          | QSPI1_CS0            |
| MISO         | QSPI1_DQ1            |
| MOSI         | QSPI1_DQ0            |
| CLK          | QSPI1_SCK            |

## Build Instructions

In order to build the coreboot ROM image, three major artifacts are required which are ebbeded into eachother.:

a) An initramfs containing stboot binary and ST config files is compiled into

b) A RISCV Linux kernel patched to fully support kexec, build into

c) coreboot with the former kernel as Linux payload

### Build configurations

1) From this directory place the Linux defconfig
```
cp linux-riscv-unmached-defconfig linux/arch/riscv/configs/defconfig
```
2) Inside the above defconfig adapt `CONFIG_INITRAMFS_SOURCE`. For this tutorial the initramfs output path is `u-root-workspace/initramfs-uroot-stboot.cpio` 

3) From this directory place the coreboot defconfig
```
cp coreboot-defconfig coreboot/configs/defconfig
```
4) Inside the above defconfig adapt `CONFIG_PAYLOAD_FILE`. For this tutorial the kernel output path is `linux/arch/riscv/boot/Image`

### Build coreboot ROM

1) Inside the `u-root-workspace` directory build the initramfs
```
UROOT_NOHWRNG=1 \
GOARCH=riscv64 ./u-root/u-root \
-o initramfs-uroot-stboot.cpio \
-uinitcmd="stboot -loglevel d" \
-defaultsh elvish \
-files "../root.cert:etc/trust_policy/ospkg_signing_root.pem" \
-files "../isrgrootx1.pem:etc/trust_policy/tls_roots.pem" \
-files "../host_config.json:etc/host_configuration.json" \
-files "../trust_policy.json:etc/trust_policy/trust_policy.json" \
stboot/ u-root/cmds/core/*
```

*Note: This will create a initramfs with all u-root core tools included for further debug and plumbing capabilities during the boot.*

*Note: UROOT_NOHWRNG=1 is needed because of the following: 1)The board does not have an hardware random number generator. 2) The strict behavior of https://github.com/u-root/uio/blob/d2acac8f37018c514adec45c51f58eace3795df4/rand/random_linux.go#L28 which is used by the DHCP library stboot uses (it has nothing to do with the fact that stboot is build as a u-root busybox here)* 

**Alternatively** use the following command to build a minimal initramfs:
```
UROOT_NOHWRNG=1 \
GOARCH=riscv64 ./u-root/u-root \
-o initramfs-uroot-stboot.cpio \
-uinitcmd="stboot -loglevel d" \
-defaultsh "" \
-files "../root.cert:etc/trust_policy/ospkg_signing_root.pem" \
-files "../isrgrootx1.pem:etc/trust_policy/tls_roots.pem" \
-files "../host_config.json:etc/host_configuration.json" \
-files "../trust_policy.json:etc/trust_policy/trust_policy.json" \
stboot/ u-root/cmds/core/init
```
2) Inside the `linux` directory build the Linux kernel including the initramfs
```
make distclean
ARCH=riscv CROSS_COMPILE=riscv64-linux-gnu- make defconfig
ARCH=riscv CROSS_COMPILE=riscv64-linux-gnu- make -j$(nproc)
```
3) Inside the `coreboot` directory build the coreboot ROM with the Linux kernel paylaod
```
make distclean
make defconfig
make -j$(nproc)
```

### Run the System

Make sure the the hardware is connected as described earlier.
Mayby you need to adapt the device paths in the following commands according to your setup.

1) Inside the `em100` directory start the EM100 flash emulator
```
EM100_HOME=. ./em100 --stop -c IS25WP256D --download ../coreboot/build/coreboot.rom --start
```
2) In a seperate terminal start minicom
```
minicom -b 115200 -D /dev/ttyUSB1
```
**Remember to disable hardware flow control**

3) Power on the board :)

This should boot sucessfully up to the point where stboot tries to load the OS-package from the 
internet. Which will fail (in ters you hav not set up a provisioning server).
```
stboot: 1970/01/01 00:00:12 [INFO] DHCP successful - eth0
stboot: 1970/01/01 00:00:12 [INFO] Loading OS package via network
stboot: 1970/01/01 00:00:12 [DEBUG] OS package pointer: http://10.0.2.2:8080/os-pkg-example-ubuntu20.json
stboot: 1970/01/01 00:00:12 [DEBUG] Downloading http://10.0.2.2:8080/os-pkg-example-ubuntu20.json
stboot: 1970/01/01 00:00:12 [INFO] Downloading "http://10.0.2.2:8080/os-pkg-example-ubuntu20.json"
stboot: 1970/01/01 00:00:12 [WARN] download of "http://10.0.2.2:8080/os-pkg-example-ubuntu20.json" failed: Get "http://10.0.2.2:8080/os-pkg-example-ubune
stboot: 1970/01/01 00:00:13 [WARN] download of "http://10.0.2.2:8080/os-pkg-example-ubuntu20.json" failed: Get "http://10.0.2.2:8080/os-pkg-example-ubune
stboot: 1970/01/01 00:00:14 [WARN] download of "http://10.0.2.2:8080/os-pkg-example-ubuntu20.json" failed: Get "http://10.0.2.2:8080/os-pkg-example-ubune
stboot: 1970/01/01 00:00:15 [WARN] download of "http://10.0.2.2:8080/os-pkg-example-ubuntu20.json" failed: Get "http://10.0.2.2:8080/os-pkg-example-ubune
stboot: 1970/01/01 00:00:16 [WARN] download of "http://10.0.2.2:8080/os-pkg-example-ubuntu20.json" failed: Get "http://10.0.2.2:8080/os-pkg-example-ubune
stboot: 1970/01/01 00:00:17 [WARN] download of "http://10.0.2.2:8080/os-pkg-example-ubuntu20.json" failed: Get "http://10.0.2.2:8080/os-pkg-example-ubune
stboot: 1970/01/01 00:00:18 [WARN] download of "http://10.0.2.2:8080/os-pkg-example-ubuntu20.json" failed: Get "http://10.0.2.2:8080/os-pkg-example-ubune
stboot: 1970/01/01 00:00:19 [WARN] download of "http://10.0.2.2:8080/os-pkg-example-ubuntu20.json" failed: Get "http://10.0.2.2:8080/os-pkg-example-ubune
stboot: 1970/01/01 00:00:19 [DEBUG] Skip http://10.0.2.2:8080/os-pkg-example-ubuntu20.json: hit retries limit
stboot: 1970/01/01 00:00:19 [DEBUG] all provisioning URLs failed
stboot: 1970/01/01 00:00:19 [ERROR] fetching OS package via network failed: download failed
stboot: 1970/01/01 00:00:19 [ERROR] boot failed
stboot: 1970/01/01 00:00:25 [INFO] Recover ...
reboot: Restarting system
```





