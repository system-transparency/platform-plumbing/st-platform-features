# ST on HIFive unmached

Linux Kernel von risc 5 nehmen `git://git.kernel.org/pub/scm/linux/kernel/git/riscv/linux.git`

Linux kernel commit hash: `3ca112b71f3`

Cross Toolchain zum Linuxkernel bauen: `sudo apt-get -y install gcc-riscv64-linux-gnu`

u-root commit `47bc9bbe71ca`

`go build`

initramfs bauen 

```bash
GOARCH=riscv64 ./u-root \
	-uroot-source . \
	-defaultsh elvish \
	-o initramfs.cpio \
	boot coreboot-app \
	./cmds/core/* \
	./cmds/boot/*
```

(go version 21 required)

1. Zeile in defconfig anpassen mit pfad zur initramfs ( muss bei risc5 in den kernel compelliert werden)

`cp defconfig-riscv-uroot-unmatched <linux-repo>/arch/riscv/configs/defconfig`

```bash
ARCH=riscv CROSS_COMPILE=riscv64-linux-gnu- make defconfig
ARCH=riscv CROSS_COMPILE=riscv64-linux-gnu- make -j$(nproc)
```

linux kernel pfad in coreboot defconfig anpassen

`cp defconfig <coreboot-repo>/configs/defconfig` 

coreboot riscv toolchain: `make crossgcc-riscv CPUS=$(nproc)` 

decive tree tool : `sudo apt-get install -y device-tree-compiler` 

```bash
make distclean
make defconfig
make -j$(nproc)
```

EM100 Flash Emulator `git clone "[https://review.coreboot.org/em100](https://review.coreboot.org/em100)"` 

commit `a1d938f8b42`

```bash
cd em100
make
EM100_HOME=. ./em100 -U
EM100_HOME=. ./em100 --stop -c IS25WP256D --download ../coreboot/build/coreboot.rom --start
```

(benoetigt libusb `sudo apt install libusb-1.0-0-dev` )

(benoetigt libcurl `sudo apt-get install libcurl4-gnutls-dev`)

(benoetigt root, Die alternative zu root ist dich in die `plugdev` gruppe hinzuzufuegen und die `60-dediprog-em100pro.rules` datei nach /etc/udev/rules.d zu kopieren. Dafuer musste dannach aber neustarten)

Dannach ein extra terminal oeffnen und den befehl eingeben fuer die serielle:

```
cu --baud 115200 -l /dev/ttyUSB1

//alternativ minicom (be sure to turn off Hardware Flow Control)ls
minicom -b 115200 -D /dev/ttyUSB1
```

kexec path fuer RISCV Linux kernel:

`git am 0001-ris...` 

test in u-root core initrd mit Ubuntu RISCV Image auf SD Karte:

```bash
mkdir /mnt
mount /dev/mmcblk0p1 /mnt
kexec -d --cmdline "console=ttySIF0 root=/dev/mmcblk0p1 debug" \
          --initrd /mnt/boot/initrd.img-6.5.0-9-generic \
          /mnt/boot/vmlinuz-6.5.0-9-generic
```

stboot initrd fuer RISCV bauen (erstmal u-root+stboot):

```bash
GOARCH=riscv64 ./u-root \
-defaultsh elvish \
-o initramfs-uroot-stboot.cpio \
-files "../../system-transparency/out/keys/example_keys/root.cert:etc/trust_policy/ospkg_signing_root.pem" \
-files "../../system-transparency/contrib/initramfs-includes/isrgrootx1.pem:etc/trust_policy/tls_roots.pem" \
-files "../../system-transparency/out/artifacts/host_config.json:etc/host_configuration.json" \
-files "../../system-transparency/out/artifacts/trust_policy.json:etc/trust_policy/trust_policy.json" \
./cmds/core/*

```

Um DHCP zum laufen zu bekommen, in der linux kernel config
network support -> network options -> packet socket enablen

Cryptographic API
|->Random number generation 
   |-> 

Hardware Random Number Generator Core support
modulename: rng-core.ko
configname: CONFIG_HW_RANDOM
Linux Kernel Configuration
└─>Device Drivers
   └─>Character devices
      └─>Hardware Random Number Generator Core support