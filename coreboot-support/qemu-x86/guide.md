# ST on coreboot for Qemu

This guide describes how to build stboot, the System Transparency bootloader as a LinuxBoot payload for coreboot.
The produced coreboot ROM is built for a x86 Qemu architecture. 

The following steps refer to: 
- coreboot commit 4efd2e3aae, Feb 27 2024
- System Transparency collection-release [st-1.0.0](https://dist.system-transparency.org/st/), Feb 23 2024, which implies
  - stboot v0.3.6
  - stmgr v0.3.3
  - docs v0.2.5

This guide therefore sticks to the [st-1.0.0 ST build guide](https://docs.system-transparency.org/st-1.0.0/docs/introduction/build/) pointing out divergences where nessesary.

## Prerequisites

The instructions are tested on Ubuntu 22.04.

Make sure to checkout the submodules for coreboot and ST
```
git submodule init
git submodule update
```

Check for the following coreboot dependencies:

```bison build-essential curl flex git gnat libncurses5-dev libssl-dev m4 zlib1g-dev pkg-config```

_(according to https://doc.coreboot.org/tutorial/part1.html)_

```qemu-system-x86 ovmf ncat ca-certificates git cpio pigz```

_(according to https://docs.system-transparency.org/st-1.0.0/docs/introduction/build/)_

## Build Instructions

### System Transparency

#### Kernel & Initrd

To get a kernel and initrd containing stboot, issue the ST boootloaader build script for a stboot ISO as it produces them as intermediate artifacts. 

The following instructions are adapted from [ST build guide](https://docs.system-transparency.org/st-1.0.0/docs/introduction/build/). Check out the explanations there for further details on the performed instructions. Therefor `/stimages` will be the working directory for the next steps.

```
cd stimages
mkdir -p go/bin; GOBIN="$(realpath go/bin)"; export GOBIN
export PATH="$GOBIN":"$PATH"
go install system-transparency.org/stmgr@v0.3.3
```

First, create signing keys and root certificate:
```
(umask 0077 && mkdir keys)
(cd keys && stmgr keygen certificate --isCA)
(cd keys && stmgr keygen certificate --rootCert rootcert.pem --rootKey rootkey.pem)
```

Issue the build process for a stboot ISO:
```
contrib/stboot/build-stboot http://10.0.2.2:8080/my-os.json keys/rootcert.pem
```
Now you should find among other build artifacts in `stboot.tmp/stboot.cpio.gz`. 
This initramfs contains stboot as init. The kernel will be used from `contrib/stboot/linuxboot.vmlinuz`


#### OS Package

To demo a working stboot fetching an OS package via network, also create an example OS package:
```
stmgr ospkg create \
    --label="My example ST system" \
    --initramfs=../ospkg/ubuntu-focal-amd64.cpio.gz \
    --kernel=../ospkg/ubuntu-focal-amd64.vmlinuz \
    --cmdline="console=ttyS0,115200n8 ro rdinit=/lib/systemd/systemd" \
    --url=http://10.0.2.2:8080/my-os.zip \
    --out=my-os.zip
```

```
stmgr ospkg sign \
    --cert keys/cert.pem \
    --key keys/key.pem \
    --ospkg my-os
```

That's it for the ST part of the build. Leaf the stimage directory:
```
cd ../
```

### coreboot

After having the ST kernel and initrd built, these are used to build a coreboot rom with a Linux payload for QEMU x86 q35.

**Note: It is important to select the correct mainboard model `QEMU x86 q35/ich9`! With the default `QEMU x86 i440fx/piix4`there will be problems regarding the size of flash memory.**

```
cd coreboot
```

The following instructions are adapted from the [coreboot tutorial](https://doc.coreboot.org/tutorial/part1.html)

```
make crossgcc-i386 CPUS=$(nproc)
```

The coreboot build need the following settings:
- Mainboard/Mainboard vendor set to `Emulation`
- Mainboard/Mainboard model set to `QEMU x86 q35/ich9`
- Payload/Payload to add set to `A Linux payload`
- Payload/Linux path and filename set to `../stimages/contrib/stboot/linuxboot.vmlinuz`
- Payload/Linux initrd set to `../stimages/stboot.tmp/stboot.cpio.gz`

Either use `make menuconfig` or apply the provided defconfig:
```
make defconfig KBUILD_DEFCONFIG=../st-defconfig
```

Finally, build coreboot:
```
make CPUS=$(nproc)
```
The final ROM image is in `build/coreboot.rom` and the coreboot build process is done

```
cd ../
```

## Run QEMU
To start a local server to provide the demo OS package use the following hack:

```
(for e in json zip
     do ncat -lc "printf 'HTTP/1.1 200 OK\n\n'; cat stimages/my-os.$e" \
           0.0.0.0 8080; done) &
```

Start QEMU with the created coreboot ROM and network settings matching the demo setup:
```
qemu-system-x86_64 -M q35 \
-bios coreboot/build/coreboot.rom \
-serial stdio \
-net user,hostfwd=tcp::2222-:22,hostfwd=tcp::3000-:3000 \
-net nic \
-object rng-random,filename=/dev/urandom,id=rng0 \
-device virtio-rng-pci,rng=rng0 \
-rtc base=localtime \
-m 4G
```

